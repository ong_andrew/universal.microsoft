﻿using Newtonsoft.Json;
using Universal.Common.Serialization;

namespace Universal.Microsoft.Bot.Connector.DirectLine
{
    public class Conversation : JsonSerializable<Conversation>
    {
        [JsonProperty("conversationId")]
        public string ConversationId { get; set; }
        [JsonProperty("token")]
        public string Token { get; set; }
        [JsonProperty("expires_in")]
        public int? ExpiresIn { get; set; }
        public bool ShouldSerializeExpiresIn() { return ExpiresIn != null; }
        [JsonProperty("streamUrl")]
        public string StreamUrl { get; set; }
        public bool ShouldSerializeStreamUrl() { return StreamUrl != null; }
    }
}
