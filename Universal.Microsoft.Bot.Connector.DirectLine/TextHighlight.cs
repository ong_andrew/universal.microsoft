﻿using Newtonsoft.Json;
using System.ComponentModel;
using Universal.Common.Serialization;

namespace Universal.Microsoft.Bot.Connector.DirectLine
{
    public class TextHighlight : JsonSerializable<TextHighlight>
    {
        [JsonProperty("occurrence")]
        public int? Occurrence { get; set; }
        [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeOccurrence() { return Occurrence != null; }
        [JsonProperty("text")]
        public string Text { get; set; }
    }
}
