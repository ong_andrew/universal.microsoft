﻿using Newtonsoft.Json;
using System.ComponentModel;
using Universal.Common.Serialization;

namespace Universal.Microsoft.Bot.Connector.DirectLine
{
    public class SemanticAction : JsonSerializable<SemanticAction>
    {
        [JsonProperty("entities")]
        public object[] Entities { get; set; }
        [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeEntities() { return Entities != null; }
        [JsonProperty("id")]
        public string Id { get; set; }
        [JsonProperty("state")]
        public string State { get; set; }
        [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeState() { return State != null; }
    }
}
