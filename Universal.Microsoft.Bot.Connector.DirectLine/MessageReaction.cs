﻿using Newtonsoft.Json;
using Universal.Common.Serialization;

namespace Universal.Microsoft.Bot.Connector.DirectLine
{
    public class MessageReaction : JsonSerializable<MessageReaction>
    {
        [JsonProperty("type")]
        public string Type { get; set; }
    }
}
