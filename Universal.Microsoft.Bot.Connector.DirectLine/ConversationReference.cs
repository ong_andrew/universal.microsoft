﻿using Newtonsoft.Json;
using System.ComponentModel;
using Universal.Common.Serialization;

namespace Universal.Microsoft.Bot.Connector.DirectLine
{
    public class ConversationReference : JsonSerializable<ConversationReference>
    {
        [JsonProperty("activityId")]
        public string ActivityId { get; set; }
        [JsonProperty("bot")]
        public ChannelAccount Bot { get; set; }
        [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeBot() { return Bot != null; }
        [JsonProperty("channelId")]
        public string ChannelId { get; set; }
        [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeChannelId() { return ChannelId != null; }
        [JsonProperty("conversation")]
        public ConversationAccount Conversation { get; set; }
        [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeConversation() { return Conversation != null; }
        [JsonProperty("serviceUrl")]
        public string ServiceUrl { get; set; }
        [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeServiceUrl() { return ServiceUrl != null; }
        [JsonProperty("user")]
        public ChannelAccount User { get; set; }
        [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeUser() { return User != null; }
    }
}
