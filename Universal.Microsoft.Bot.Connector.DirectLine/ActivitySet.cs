﻿using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using Universal.Common.Serialization;

namespace Universal.Microsoft.Bot.Connector.DirectLine
{
    [JsonObject]
    public class ActivitySet : JsonSerializable<ActivitySet>, IEnumerable<Activity>
    {
        [JsonProperty("activities")]
        public Activity[] Activities { get; set; }
        [JsonProperty("watermark")]
        public string Watermark { get; set; }

        public IEnumerator<Activity> GetEnumerator()
        {
            return ((IEnumerable<Activity>)Activities).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable<Activity>)Activities).GetEnumerator();
        }
    }
}
