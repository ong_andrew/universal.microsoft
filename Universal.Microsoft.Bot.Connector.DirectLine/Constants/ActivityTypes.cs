﻿namespace Universal.Microsoft.Bot.Connector.DirectLine
{
    public static class ActivityTypes
    {
        public const string ContactRelationUpdate = "contactRelationUpdate";
        public const string ConversationUpdate = "conversationUpdate";
        public const string DeleteUserData = "deleteUserData";
        public const string EndOfConversation = "endOfConversation";
        public const string Event = "event";
        public const string Handoff = "handoff";
        public const string InstallationUpdate = "installationUpdate";
        public const string Invoke = "invoke";
        public const string Message = "message";
        public const string MessageUpdate = "messageUpdate";
        public const string MessageDelete = "messageDelete";
        public const string MessageReaction = "messageReaction";
        public const string Suggestion = "suggestion";
        public const string Trace = "trace";
        public const string Typing = "typing";
    }
}
