﻿namespace Universal.Microsoft.Bot.Connector.DirectLine
{
    /// <summary>
    /// Represents the direction of the activity, relative to the client.
    /// </summary>
    public enum ActivityDirection
    {
        /// <summary>
        /// Represents an activity that did not originate from this client.
        /// </summary>
        Incoming,
        /// <summary>
        /// Represents an activity that originated from this client.
        /// </summary>
        Outgoing
    };
}
