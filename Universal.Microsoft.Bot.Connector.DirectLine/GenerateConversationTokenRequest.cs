﻿using Newtonsoft.Json;
using System.ComponentModel;
using Universal.Common.Serialization;

namespace Universal.Microsoft.Bot.Connector.DirectLine
{
    public class GenerateConversationTokenRequest : JsonSerializable<GenerateConversationTokenRequest>
    {
        [JsonProperty("user")]
        public UserInfo User { get; set; }
        [EditorBrowsable(EditorBrowsableState.Never)]
        public bool ShouldSerializeUser() { return User != null; }
        [JsonProperty("trustedOrigins")]
        public string[] TrustedOrigins { get; set; }
        [EditorBrowsable(EditorBrowsableState.Never)]
        public bool ShouldSerializeTrustedOrigins() { return TrustedOrigins != null; }

        public class UserInfo
        {
            [JsonProperty("id")]
            public string Id { get; set; }
            [JsonProperty("name")]
            public string Name { get; set; }
        }
    }
}
