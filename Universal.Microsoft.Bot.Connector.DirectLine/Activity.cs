﻿using Newtonsoft.Json;
using System.ComponentModel;
using Universal.Common.Serialization;

namespace Universal.Microsoft.Bot.Connector.DirectLine
{
    public class Activity : JsonSerializable<Activity>
    {
        [JsonProperty("action")]
        public string Action { get; set; }
        [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeAction() { return Action != null; }
        [JsonProperty("attachmentLayout")]
        public string AttachmentLayout { get; set; }
        [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeAttachmentLayout() { return AttachmentLayout != null; }
        [JsonProperty("attachments")]
        public Attachment[] Attachments { get; set; }
        [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeAttachments() { return Attachments != null; }
        [JsonProperty("callerId")]
        public string CallerId { get; set; }
        [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeCallerId() { return CallerId != null; }
        [JsonProperty("channelData")]
        public object ChannelData { get; set; }
        [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeChannelData() { return ChannelData != null; }
        [JsonProperty("channelId")]
        public string ChannelId { get; set; }
        [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeChannelId() { return ChannelId != null; }
        [JsonProperty("code")]
        public string Code { get; set; }
        [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeCode() { return Code != null; }
        [JsonProperty("conversation")]
        public ConversationAccount Conversation { get; set; }
        [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeConversation() { return Conversation != null; }
        [JsonProperty("deliveryMode")]
        public string DeliveryMode { get; set; }
        [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeDeliveryMode() { return DeliveryMode != null; }
        [JsonProperty("entities")]
        public object[] Entities { get; set; }
        [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeEntities() { return Entities != null; }
        [JsonProperty("expiration")]
        public string Expiration { get; set; }
        [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeExpiration() { return Expiration != null; }
        [JsonProperty("from")]
        public ChannelAccount From { get; set; }
        [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeFrom() { return From != null; }
        [JsonProperty("historyDisclosed")]
        public bool? HistoryDisclosed { get; set; }
        [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeHistoryDisclosed() { return HistoryDisclosed != null; }
        [JsonProperty("id")]
        public string Id { get; set; }
        [JsonProperty("importance")]
        public string Importance { get; set; }
        [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeImportance() { return Importance != null; }
        [JsonProperty("inputHint")]
        public string InputHint { get; set; }
        [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeInputHint() { return InputHint != null; }
        [JsonProperty("label")]
        public string Label { get; set; }
        [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeLabel() { return Label != null; }
        [JsonProperty("listenFor")]
        public string[] ListenFor { get; set; }
        [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeListenFor() { return ListenFor != null; }
        [JsonProperty("locale")]
        public string Locale { get; set; }
        [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeLocale() { return Locale != null; }
        [JsonProperty("localTimestamp")]
        public string LocalTimestamp { get; set; }
        [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeLocalTimestamp() { return LocalTimestamp != null; }
        [JsonProperty("localTimezone")]
        public string LocalTimezone { get; set; }
        [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeLocalTimezone() { return LocalTimezone != null; }
        [JsonProperty("membersAdded")]
        public ChannelAccount[] MembersAdded { get; set; }
        [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeMembersAdded() { return MembersAdded != null; }
        [JsonProperty("membersRemoved")]
        public ChannelAccount[] MembersRemoved { get; set; }
        [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeMembersRemoved() { return MembersRemoved != null; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeName() { return Name != null; }
        [JsonProperty("reactionsAdded")]
        public MessageReaction[] ReactionsAdded { get; set; }
        [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeReactionsAdded() { return ReactionsAdded != null; }
        [JsonProperty("reactionsRemoved")]
        public MessageReaction[] ReactionsRemoved { get; set; }
        [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeReactionsRemoved() { return ReactionsRemoved != null; }
        [JsonProperty("recipient")]
        public ChannelAccount Recipient { get; set; }
        [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeRecipient() { return Recipient != null; }
        [JsonProperty("relatesTo")]
        public ConversationReference RelatesTo { get; set; }
        [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeRelatesTo() { return RelatesTo != null; }
        [JsonProperty("replyToId")]
        public string ReplyToId { get; set; }
        [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeReplyToId() { return ReplyToId != null; }
        [JsonProperty("semanticAction")]
        public SemanticAction SemanticAction { get; set; }
        [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeSemanticAction() { return SemanticAction != null; }
        [JsonProperty("serviceUrl")]
        public string ServiceUrl { get; set; }
        [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeServiceUrl() { return ServiceUrl != null; }
        [JsonProperty("speak")]
        public string Speak { get; set; }
        [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeSpeak() { return Speak != null; }
        [JsonProperty("suggestedActions")]
        public SuggestedActions SuggestedActions { get; set; }
        [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeSuggestedActions() { return SuggestedActions != null; }
        [JsonProperty("summary")]
        public string Summary { get; set; }
        [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeSummary() { return Summary != null; }
        [JsonProperty("text")]
        public string Text { get; set; }
        [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeText() { return Text != null; }
        [JsonProperty("textFormat")]
        public string TextFormat { get; set; }
        [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeTextFormat() { return TextFormat != null; }
        [JsonProperty("textHighlights")]
        public TextHighlight[] TextHighlights { get; set; }
        [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeTextHighlights() { return TextHighlights != null; }
        [JsonProperty("timestamp")]
        public string Timestamp { get; set; }
        [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeTimestamp() { return Timestamp != null; }
        [JsonProperty("topicName")]
        public string TopicName { get; set; }
        [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeTopicName() { return TopicName != null; }
        [JsonProperty("type")]
        public string Type { get; set; }
        [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeType() { return Type != null; }
        [JsonProperty("value")]
        public object Value { get; set; }
        [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeValue() { return Value != null; }
        [JsonProperty("valueType")]
        public string ValueType { get; set; }
        [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeValueType() { return ValueType != null; }
    }
}
