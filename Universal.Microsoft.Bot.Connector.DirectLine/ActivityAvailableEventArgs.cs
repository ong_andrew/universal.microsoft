﻿namespace Universal.Microsoft.Bot.Connector.DirectLine
{
    /// <summary>
    /// Represents data from an activity being received event.
    /// </summary>
    public class ActivityAvailableEventArgs
    {
        /// <summary>
        /// The activity received.
        /// </summary>
        public Activity Activity { get; set; }
        /// <summary>
        /// The direction of the activity.
        /// </summary>
        public ActivityDirection Direction { get; set; }
    }
}