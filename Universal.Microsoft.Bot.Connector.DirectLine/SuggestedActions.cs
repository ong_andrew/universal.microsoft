﻿using Newtonsoft.Json;
using System.ComponentModel;
using Universal.Common.Serialization;

namespace Universal.Microsoft.Bot.Connector.DirectLine
{
    public class SuggestedActions : JsonSerializable<SuggestedActions>
    {
        [JsonProperty("actions")]
        public CardAction[] Actions { get; set; }
        [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeActions() { return Actions != null; }
        [JsonProperty("to")]
        public string[] To { get; set; }
        [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeTo() { return To != null; }
    }
}
