﻿using Newtonsoft.Json;
using System.ComponentModel;
using Universal.Common.Serialization;

namespace Universal.Microsoft.Bot.Connector.DirectLine
{
    public class ConversationAccount : JsonSerializable<ConversationAccount>
    {
        [JsonProperty("aadObjectId")]
        public string AadObjectId { get; set; }
        [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeAadObjectId() { return AadObjectId != null; }
        [JsonProperty("conversationType")]
        public string ConversationType { get; set; }
        [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeConversationType() { return ConversationType != null; }
        [JsonProperty("id")]
        public string Id { get; set; }
        [JsonProperty("isGroup")]
        public bool? IsGroup { get; set; }
        [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeIsGroup() { return IsGroup != null; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeName() { return Name != null; }
        [JsonProperty("role")]
        public string Role { get; set; }
        [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeRole() { return Role != null; }
        [JsonProperty("tenantId")]
        public string TenantId { get; set; }
        [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeTenantId() { return TenantId != null; }
    }
}
