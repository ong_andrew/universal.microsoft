﻿using Newtonsoft.Json;
using System.ComponentModel;
using Universal.Common.Serialization;

namespace Universal.Microsoft.Bot.Connector.DirectLine
{
    public class Attachment : JsonSerializable<Attachment>
    {
        /// <summary>
        /// The content of the attachment. If the attachment is a rich card, set this property to the rich card object. This property and the contentUrl property are mutually exclusive.
        /// </summary>
        [JsonProperty("content")]
        public object Content { get; set; }
        [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeContent() { return Content != null; }
        /// <summary>
        /// The media type of the content in the attachment.
        /// </summary>
        [JsonProperty("contentType")]
        public string ContentType { get; set; }
        /// <summary>
        /// URL for the content of the attachment. For example, if the attachment is an image, you can set contentUrl to the URL that represents the location of the image. Supported protocols are: HTTP, HTTPS, File, and Data.
        /// </summary>
        [JsonProperty("contentUrl")]
        public string ContentUrl { get; set; }
        [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeContentUrl() { return ContentUrl != null; }
        /// <summary>
        /// Name of the attachment.
        /// </summary>
        [JsonProperty("name")]
        public string Name { get; set; }
        /// <summary>
        /// URL to a thumbnail image that the channel can use if it supports using an alternative, smaller form of content or contentUrl. For example, if you set contentType to application/word and set contentUrl to the location of the Word document, you might include a thumbnail image that represents the document. The channel could display the thumbnail image instead of the document. When the user clicks the image, the channel would open the document.
        /// </summary>
        [JsonProperty("thumbnailUrl")]
        public string ThumbnailUrl { get; set; }
    }
}
