﻿using Newtonsoft.Json;
using Universal.Common.Serialization;

namespace Universal.Microsoft.Bot.Connector.DirectLine
{
    public class ResourceResponse : JsonSerializable<ResourceResponse>
    {
        [JsonProperty("id")]
        public string Id { get; set; }
    }
}
