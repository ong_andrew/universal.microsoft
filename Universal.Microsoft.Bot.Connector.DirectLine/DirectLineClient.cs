﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Universal.Common;
using Universal.Common.Net.Http;
using Universal.Microsoft.Bot.Connector.DirectLine.Extensions;
using UriBuilder = Universal.Common.UriBuilder;

namespace Universal.Microsoft.Bot.Connector.DirectLine
{
    /// <summary>
    /// Class that provides access to the Microsoft Bot Framework's Direct Line APIs.
    /// </summary>
    public class DirectLineClient : HttpServiceClient
    {
        protected string mDirectLineSecret;

        /// <summary>
        /// Instantiates a new instance of the <see cref="DirectLineClient"/> with the given secret.
        /// </summary>
        /// <param name="directLineSecret"></param>
        public DirectLineClient(string directLineSecret)
        {
            mDirectLineSecret = directLineSecret;
        }

        protected override Task HandleNonSuccessCodeAsync(HttpResponseMessage httpResponseMessage, CancellationToken cancellationToken)
        {
            throw new HttpException(httpResponseMessage);
        }

        /// <summary>
        /// Generates but does not start a conversation asynchronously, returning a token that can be used to start a given conversation at a later time.
        /// </summary>
        /// <returns></returns>
        public async Task<Conversation> GenerateConversationTokenAsync()
        {
            return await GenerateConversationTokenAsync(CancellationToken.None).ConfigureAwait(false);
        }

        /// <summary>
        /// Generates but does not start a conversation asynchronously, returning a token that can be used to start a given conversation at a later time.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<Conversation> GenerateConversationTokenAsync(GenerateConversationTokenRequest request)
        {
            return await GenerateConversationTokenAsync(request, CancellationToken.None).ConfigureAwait(false);
        }

        /// <summary>
        /// Generates but does not start a conversation asynchronously, returning a token that can be used to start a given conversation at a later time.
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<Conversation> GenerateConversationTokenAsync(CancellationToken cancellationToken)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Post, new Uri("https://directline.botframework.com/v3/directline/tokens/generate"), (httpRequestMessage) =>
            {
                httpRequestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", mDirectLineSecret);
            }, cancellationToken).ConfigureAwait(false))
            {
                return Conversation.FromString(await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
        }

        /// <summary>
        /// Generates but does not start a conversation asynchronously, returning a token that can be used to start a given conversation at a later time.
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<Conversation> GenerateConversationTokenAsync(GenerateConversationTokenRequest request, CancellationToken cancellationToken)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Post, new Uri("https://directline.botframework.com/v3/directline/tokens/generate"), new JsonContent(request.ToString()), (httpRequestMessage) =>
            {
                httpRequestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", mDirectLineSecret);
            }, cancellationToken).ConfigureAwait(false))
            {
                return Conversation.FromString(await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
        }

        /// <summary>
        /// Refreshes a given conversation's token.
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<Conversation> RefreshConversationTokenAsync(string token)
        {
            return await RefreshConversationTokenAsync(token, CancellationToken.None).ConfigureAwait(false);
        }

        /// <summary>
        /// Refreshes a given conversation's token.
        /// </summary>
        /// <param name="token"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<Conversation> RefreshConversationTokenAsync(string token, CancellationToken cancellationToken)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Post, new Uri("https://directline.botframework.com/v3/directline/tokens/refresh"), (httpRequestMessage) =>
            {
                httpRequestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", token);
            }, cancellationToken).ConfigureAwait(false))
            {
                return Conversation.FromString(await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
        }

        /// <summary>
        /// Starts a new conversation.
        /// </summary>
        /// <returns></returns>
        public async Task<Conversation> StartConversationAsync()
        {
            return await StartConversationAsync(CancellationToken.None).ConfigureAwait(false);
        }

        /// <summary>
        /// Starts a new conversation.
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<Conversation> StartConversationAsync(CancellationToken cancellationToken)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Post, new Uri("https://directline.botframework.com/v3/directline/conversations"), (httpRequestMessage) =>
            {
                httpRequestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", mDirectLineSecret);
            }, cancellationToken).ConfigureAwait(false))
            {
                return Conversation.FromString(await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
        }

        /// <summary>
        /// Starts a conversation previously instantiated by generating a token for it.
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<Conversation> StartConversationWithTokenAsync(string token)
        {
            return await StartConversationWithTokenAsync(token, CancellationToken.None).ConfigureAwait(false);
        }

        /// <summary>
        /// Starts a conversation previously instantiated by generating a token for it.
        /// </summary>
        /// <param name="token"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<Conversation> StartConversationWithTokenAsync(string token, CancellationToken cancellationToken)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Post, new Uri("https://directline.botframework.com/v3/directline/conversations"), (httpRequestMessage) =>
            {
                httpRequestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", token);
            }, cancellationToken).ConfigureAwait(false))
            {
                return Conversation.FromString(await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
        }

        /// <summary>
        /// Reconnects to a conversation by using the direct line secret.
        /// </summary>
        /// <param name="conversationId"></param>
        /// <returns></returns>
        public async Task<Conversation> ReconnectToConversationAsync(string conversationId)
        {
            return await ReconnectToConversationAsync(conversationId, null).ConfigureAwait(false);
        }

        /// <summary>
        /// Reconnects to a conversation by using the direct line secret.
        /// </summary>
        /// <param name="conversationId"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<Conversation> ReconnectToConversationAsync(string conversationId, CancellationToken cancellationToken)
        {
            return await ReconnectToConversationAsync(conversationId, null, cancellationToken).ConfigureAwait(false);
        }

        /// <summary>
        /// Reconnects to a conversation by using the direct line secret at the given watermark.
        /// </summary>
        /// <param name="conversationId"></param>
        /// <param name="watermark"></param>
        /// <returns></returns>
        public async Task<Conversation> ReconnectToConversationAsync(string conversationId, string watermark)
        {
            return await ReconnectToConversationAsync(conversationId, watermark, CancellationToken.None).ConfigureAwait(false);
        }

        /// <summary>
        /// Reconnects to a conversation by using the direct line secret at the given watermark.
        /// </summary>
        /// <param name="conversationId"></param>
        /// <param name="watermark"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<Conversation> ReconnectToConversationAsync(string conversationId, string watermark, CancellationToken cancellationToken)
        {
            UriBuilder uriBuilder = new UriBuilder($"https://directline.botframework.com/v3/directline/conversations/{conversationId}");

            if (watermark != null)
            {
                uriBuilder.AddQuery("watermark", watermark);
            }

            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Get, uriBuilder, (httpRequestMessage) =>
            {
                httpRequestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", mDirectLineSecret);
            }, cancellationToken).ConfigureAwait(false))
            {
                return Conversation.FromString(await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
        }

        /// <summary>
        /// Reconnects to a conversation by using the given conversation token.
        /// </summary>
        /// <param name="token"></param>
        /// <param name="conversationId"></param>
        /// <returns></returns>
        public async Task<Conversation> ReconnectToConversationWithTokenAsync(string token, string conversationId)
        {
            return await ReconnectToConversationWithTokenAsync(token, conversationId, null).ConfigureAwait(false);
        }

        /// <summary>
        /// Reconnects to a conversation by using the given conversation token.
        /// </summary>
        /// <param name="token"></param>
        /// <param name="conversationId"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<Conversation> ReconnectToConversationWithTokenAsync(string token, string conversationId, CancellationToken cancellationToken)
        {
            return await ReconnectToConversationWithTokenAsync(token, conversationId, null, cancellationToken).ConfigureAwait(false);
        }

        /// <summary>
        /// Reconnects to a conversation by using the given conversation token with the given watermark.
        /// </summary>
        /// <param name="token"></param>
        /// <param name="conversationId"></param>
        /// <param name="watermark"></param>
        /// <returns></returns>
        public async Task<Conversation> ReconnectToConversationWithTokenAsync(string token, string conversationId, string watermark)
        {
            return await ReconnectToConversationWithTokenAsync(token, conversationId, watermark, CancellationToken.None).ConfigureAwait(false);
        }

        /// <summary>
        /// Reconnects to a conversation by using the given conversation token with the given watermark.
        /// </summary>
        /// <param name="token"></param>
        /// <param name="conversationId"></param>
        /// <param name="watermark"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<Conversation> ReconnectToConversationWithTokenAsync(string token, string conversationId, string watermark, CancellationToken cancellationToken)
        {
            UriBuilder uriBuilder = new UriBuilder($"https://directline.botframework.com/v3/directline/conversations/{conversationId}");

            if (watermark != null)
            {
                uriBuilder.AddQuery("watermark", watermark);
            }

            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Get, uriBuilder, (httpRequestMessage) =>
            {
                httpRequestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", token);
            }, cancellationToken).ConfigureAwait(false))
            {
                return Conversation.FromString(await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
        }

        /// <summary>
        /// Posts an activity asynchronously.
        /// </summary>
        /// <param name="conversationId"></param>
        /// <param name="activity"></param>
        /// <returns></returns>
        public async Task<ResourceResponse> PostActivityAsync(string conversationId, Activity activity)
        {
            return await PostActivityAsync(conversationId, activity, CancellationToken.None).ConfigureAwait(false);
        }

        /// <summary>
        /// Posts an activity asynchronously.
        /// </summary>
        /// <param name="conversationId"></param>
        /// <param name="activity"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ResourceResponse> PostActivityAsync(string conversationId, Activity activity, CancellationToken cancellationToken)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Post, new UriBuilder($"https://directline.botframework.com/v3/directline/conversations/{conversationId}/activities"),
                new JsonContent(activity.ToString()),
                (httpRequestMessage) =>
                {
                    httpRequestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", mDirectLineSecret);
                }, cancellationToken).ConfigureAwait(false))
            {
                return ResourceResponse.FromString(await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
        }

        /// <summary>
        /// Posts an activity asynchronously.
        /// </summary>
        /// <param name="token"></param>
        /// <param name="conversationId"></param>
        /// <param name="activity"></param>
        /// <returns></returns>
        public async Task<ResourceResponse> PostActivityWithTokenAsync(string token, string conversationId, Activity activity)
        {
            return await PostActivityWithTokenAsync(token, conversationId, activity, CancellationToken.None).ConfigureAwait(false);
        }

        /// <summary>
        /// Posts an activity asynchronously.
        /// </summary>
        /// <param name="token"></param>
        /// <param name="conversationId"></param>
        /// <param name="activity"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ResourceResponse> PostActivityWithTokenAsync(string token, string conversationId, Activity activity, CancellationToken cancellationToken)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Post, new UriBuilder($"https://directline.botframework.com/v3/directline/conversations/{conversationId}/activities"),
                new JsonContent(activity.ToString()),
                (httpRequestMessage) =>
                {
                    httpRequestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", token);
                }, cancellationToken).ConfigureAwait(false))
            {
                return ResourceResponse.FromString(await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
        }

        /// <summary>
        /// Gets the activities for a given conversation.
        /// </summary>
        /// <param name="conversationId"></param>
        /// <returns></returns>
        public async Task<ActivitySet> GetActivitiesAsync(string conversationId)
        {
            return await GetActivitiesAsync(conversationId, null, CancellationToken.None).ConfigureAwait(false);
        }

        /// <summary>
        /// Gets the activities for a given conversation.
        /// </summary>
        /// <param name="conversationId"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ActivitySet> GetActivitiesAsync(string conversationId, CancellationToken cancellationToken)
        {
            return await GetActivitiesAsync(conversationId, null, cancellationToken).ConfigureAwait(false);
        }

        /// <summary>
        /// Gets the activities for a given conversation.
        /// </summary>
        /// <param name="conversationId"></param>
        /// <param name="watermark"></param>
        /// <returns></returns>
        public async Task<ActivitySet> GetActivitiesAsync(string conversationId, string watermark)
        {
            return await GetActivitiesAsync(conversationId, watermark, CancellationToken.None).ConfigureAwait(false);
        }

        /// <summary>
        /// Gets the activities for a given conversation.
        /// </summary>
        /// <param name="conversationId"></param>
        /// <param name="watermark"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ActivitySet> GetActivitiesAsync(string conversationId, string watermark, CancellationToken cancellationToken)
        {
            UriBuilder uriBuilder = new UriBuilder($"https://directline.botframework.com/v3/directline/conversations/{conversationId}/activities");

            if (watermark != null)
            {
                uriBuilder.AddQuery("watermark", watermark);
            }

            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Get, uriBuilder,
                (httpRequestMessage) =>
                {
                    httpRequestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", mDirectLineSecret);
                }, cancellationToken).ConfigureAwait(false))
            {
                string responseBody = await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false);
                ActivitySet activitySet = ActivitySet.FromString(responseBody);


                if (activitySet.All(x => x.Id != null))
                {
                    return activitySet;
                }
                else
                {
                    activitySet.TryRecoverActivityIds(responseBody);

                    return activitySet;
                }
            }
        }

        /// <summary>
        /// Uploads a file to the conversation.
        /// </summary>
        /// <param name="conversationId"></param>
        /// <param name="userId"></param>
        /// <param name="fileName"></param>
        /// <param name="fileStream"></param>
        /// <returns></returns>
        public async Task<ResourceResponse> UploadFileAsync(string conversationId, string userId, string fileName, Stream fileStream)
        {
            return await UploadFileWithTokenAsync(mDirectLineSecret, conversationId, userId, null, fileName, fileStream, CancellationToken.None).ConfigureAwait(false);
        }

        /// <summary>
        /// Uploads a file to the conversation.
        /// </summary>
        /// <param name="conversationId"></param>
        /// <param name="userId"></param>
        /// <param name="fileName"></param>
        /// <param name="fileStream"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ResourceResponse> UploadFileAsync(string conversationId, string userId, string fileName, Stream fileStream, CancellationToken cancellationToken)
        {
            return await UploadFileWithTokenAsync(mDirectLineSecret, conversationId, userId, null, fileName, fileStream, cancellationToken).ConfigureAwait(false);
        }

        /// <summary>
        /// Uploads a file to the conversation.
        /// </summary>
        /// <param name="conversationId"></param>
        /// <param name="userId"></param>
        /// <param name="activity"></param>
        /// <param name="fileName"></param>
        /// <param name="fileStream"></param>
        /// <returns></returns>
        public async Task<ResourceResponse> UploadFileAsync(string conversationId, string userId, Activity activity, string fileName, Stream fileStream)
        {
            return await UploadFileWithTokenAsync(mDirectLineSecret, conversationId, userId, activity, fileName, fileStream, CancellationToken.None).ConfigureAwait(false);
        }

        /// <summary>
        /// Uploads a file to the conversation.
        /// </summary>
        /// <param name="conversationId"></param>
        /// <param name="userId"></param>
        /// <param name="activity"></param>
        /// <param name="fileName"></param>
        /// <param name="fileStream"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ResourceResponse> UploadFileAsync(string conversationId, string userId, Activity activity, string fileName, Stream fileStream, CancellationToken cancellationToken)
        {
            return await UploadFileWithTokenAsync(mDirectLineSecret, conversationId, userId, activity, fileName, fileStream, cancellationToken).ConfigureAwait(false);
        }

        /// <summary>
        /// Uploads a file to the conversation.
        /// </summary>
        /// <param name="token"></param>
        /// <param name="conversationId"></param>
        /// <param name="fileName"></param>
        /// <param name="fileStream"></param>
        /// <returns></returns>
        public async Task<ResourceResponse> UploadFileWithTokenAsync(string token, string conversationId, string fileName, Stream fileStream)
        {
            return await UploadFileWithTokenAsync(token, conversationId, null, null, fileName, fileStream, CancellationToken.None).ConfigureAwait(false);
        }

        /// <summary>
        /// Uploads a file to the conversation.
        /// </summary>
        /// <param name="token"></param>
        /// <param name="conversationId"></param>
        /// <param name="fileName"></param>
        /// <param name="fileStream"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ResourceResponse> UploadFileWithTokenAsync(string token, string conversationId, string fileName, Stream fileStream, CancellationToken cancellationToken)
        {
            return await UploadFileWithTokenAsync(token, conversationId, null, null, fileName, fileStream, cancellationToken).ConfigureAwait(false);
        }

        /// <summary>
        /// Uploads a file to the conversation.
        /// </summary>
        /// <param name="token"></param>
        /// <param name="conversationId"></param>
        /// <param name="userId"></param>
        /// <param name="fileName"></param>
        /// <param name="fileStream"></param>
        /// <returns></returns>
        public async Task<ResourceResponse> UploadFileWithTokenAsync(string token, string conversationId, string userId,  string fileName, Stream fileStream)
        {
            return await UploadFileWithTokenAsync(token, conversationId, userId, null, fileName, fileStream, CancellationToken.None).ConfigureAwait(false);
        }

        /// <summary>
        /// Uploads a file to the conversation.
        /// </summary>
        /// <param name="token"></param>
        /// <param name="conversationId"></param>
        /// <param name="userId"></param>
        /// <param name="fileName"></param>
        /// <param name="fileStream"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ResourceResponse> UploadFileWithTokenAsync(string token, string conversationId, string userId, string fileName, Stream fileStream, CancellationToken cancellationToken)
        {
            return await UploadFileWithTokenAsync(token, conversationId, userId, null, fileName, fileStream, cancellationToken).ConfigureAwait(false);
        }

        /// <summary>
        /// Uploads a file to the conversation.
        /// </summary>
        /// <param name="token"></param>
        /// <param name="conversationId"></param>
        /// <param name="userId"></param>
        /// <param name="activity"></param>
        /// <param name="fileName"></param>
        /// <param name="fileStream"></param>
        /// <returns></returns>
        public async Task<ResourceResponse> UploadFileWithTokenAsync(string token, string conversationId, string userId, Activity activity, string fileName, Stream fileStream)
        {
            return await UploadFileWithTokenAsync(token, conversationId, userId, activity, fileName, fileStream, CancellationToken.None).ConfigureAwait(false);
        }

        /// <summary>
        /// Uploads a file to the conversation.
        /// </summary>
        /// <param name="token"></param>
        /// <param name="conversationId"></param>
        /// <param name="userId"></param>
        /// <param name="activity"></param>
        /// <param name="fileName"></param>
        /// <param name="fileStream"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ResourceResponse> UploadFileWithTokenAsync(string token, string conversationId, string userId, Activity activity, string fileName, Stream fileStream, CancellationToken cancellationToken)
        {
            return await UploadFilesWithTokenAsync(token, conversationId, userId, activity, new List<(string, Stream)>() { (fileName, fileStream) }, cancellationToken).ConfigureAwait(false);
        }

        /// <summary>
        /// Uploads a file to the conversation.
        /// </summary>
        /// <param name="conversationId"></param>
        /// <param name="userId"></param>
        /// <param name="files"></param>
        /// <returns></returns>
        public async Task<ResourceResponse> UploadFilesAsync(string conversationId, string userId, IEnumerable<(string FileName, Stream FileStream)> files)
        {
            return await UploadFilesWithTokenAsync(mDirectLineSecret, conversationId, userId, null, files, CancellationToken.None).ConfigureAwait(false);
        }

        /// <summary>
        /// Uploads a file to the conversation.
        /// </summary>
        /// <param name="conversationId"></param>
        /// <param name="userId"></param>
        /// <param name="files"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ResourceResponse> UploadFilesAsync(string conversationId, string userId, IEnumerable<(string FileName, Stream FileStream)> files, CancellationToken cancellationToken)
        {
            return await UploadFilesWithTokenAsync(mDirectLineSecret, conversationId, userId, null, files, cancellationToken).ConfigureAwait(false);
        }

        /// <summary>
        /// Uploads a file to the conversation.
        /// </summary>
        /// <param name="conversationId"></param>
        /// <param name="userId"></param>
        /// <param name="activity"></param>
        /// <param name="files"></param>
        /// <returns></returns>
        public async Task<ResourceResponse> UploadFilesAsync(string conversationId, string userId, Activity activity, IEnumerable<(string FileName, Stream FileStream)> files)
        {
            return await UploadFilesWithTokenAsync(mDirectLineSecret, conversationId, userId, activity, files, CancellationToken.None).ConfigureAwait(false);
        }

        /// <summary>
        /// Uploads a file to the conversation.
        /// </summary>
        /// <param name="conversationId"></param>
        /// <param name="userId"></param>
        /// <param name="activity"></param>
        /// <param name="files"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ResourceResponse> UploadFilesAsync(string conversationId, string userId, Activity activity, IEnumerable<(string FileName, Stream FileStream)> files, CancellationToken cancellationToken)
        {
            return await UploadFilesWithTokenAsync(mDirectLineSecret, conversationId, userId, activity, files, cancellationToken).ConfigureAwait(false);
        }

        /// <summary>
        /// Uploads a file to the conversation.
        /// </summary>
        /// <param name="token"></param>
        /// <param name="conversationId"></param>
        /// <param name="files"></param>
        /// <returns></returns>
        public async Task<ResourceResponse> UploadFilesWithTokenAsync(string token, string conversationId, IEnumerable<(string FileName, Stream FileStream)> files)
        {
            return await UploadFilesWithTokenAsync(token, conversationId, null, null, files, CancellationToken.None).ConfigureAwait(false);
        }

        /// <summary>
        /// Uploads a file to the conversation.
        /// </summary>
        /// <param name="token"></param>
        /// <param name="conversationId"></param>
        /// <param name="files"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ResourceResponse> UploadFilesWithTokenAsync(string token, string conversationId, IEnumerable<(string FileName, Stream FileStream)> files, CancellationToken cancellationToken)
        {
            return await UploadFilesWithTokenAsync(token, conversationId, null, null, files, cancellationToken).ConfigureAwait(false);
        }

        /// <summary>
        /// Uploads a file to the conversation.
        /// </summary>
        /// <param name="token"></param>
        /// <param name="conversationId"></param>
        /// <param name="userId"></param>
        /// <param name="files"></param>
        /// <returns></returns>
        public async Task<ResourceResponse> UploadFilesWithTokenAsync(string token, string conversationId, string userId, IEnumerable<(string FileName, Stream FileStream)> files)
        {
            return await UploadFilesWithTokenAsync(token, conversationId, userId, null, files, CancellationToken.None).ConfigureAwait(false);
        }

        /// <summary>
        /// Uploads a file to the conversation.
        /// </summary>
        /// <param name="token"></param>
        /// <param name="conversationId"></param>
        /// <param name="userId"></param>
        /// <param name="files"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ResourceResponse> UploadFilesWithTokenAsync(string token, string conversationId, string userId, IEnumerable<(string FileName, Stream FileStream)> files, CancellationToken cancellationToken)
        {
            return await UploadFilesWithTokenAsync(token, conversationId, userId, null, files, cancellationToken).ConfigureAwait(false);
        }

        /// <summary>
        /// Uploads a file to the conversation.
        /// </summary>
        /// <param name="token"></param>
        /// <param name="conversationId"></param>
        /// <param name="userId"></param>
        /// <param name="activity"></param>
        /// <param name="files"></param>
        /// <returns></returns>
        public async Task<ResourceResponse> UploadFilesWithTokenAsync(string token, string conversationId, string userId, Activity activity, IEnumerable<(string FileName, Stream FileStream)> files)
        {
            return await UploadFilesWithTokenAsync(token, conversationId, userId, activity, files, CancellationToken.None).ConfigureAwait(false);
        }

        /// <summary>
        /// Uploads a file to the conversation.
        /// </summary>
        /// <param name="token"></param>
        /// <param name="conversationId"></param>
        /// <param name="userId"></param>
        /// <param name="activity"></param>
        /// <param name="files"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ResourceResponse> UploadFilesWithTokenAsync(string token, string conversationId, string userId, Activity activity, IEnumerable<(string FileName, Stream FileStream)> files, CancellationToken cancellationToken)
        {
            UriBuilder uriBuilder = new UriBuilder($"https://directline.botframework.com/v3/directline/conversations/{conversationId}/upload");

            if (userId != null)
            {
                uriBuilder.AddQuery(nameof(userId), userId);
            }

            if (files == null)
            {
                throw new ArgumentNullException(nameof(files));
            }

            MultipartFormDataContent multipartFormDataContent = new MultipartFormDataContent();

            if (activity != null)
            {
                StringContent activityContent = new StringContent(activity.ToString(), Encoding.UTF8, "application/vnd.microsoft.activity");
                activityContent.Headers.ContentDisposition = new ContentDispositionHeaderValue("form-data");
                multipartFormDataContent.Add(activityContent, "activity", "blob");
            }
            
            foreach ((string name, Stream file) in files)
            {
                StreamContent streamContent = new StreamContent(file);
                streamContent.Headers.ContentType = new MediaTypeHeaderValue(MediaType.FromExtension(Path.GetExtension(name).ToLowerInvariant()) ?? MediaTypes.Application.OctetStream);
                streamContent.Headers.ContentDisposition = new ContentDispositionHeaderValue("form-data");

                multipartFormDataContent.Add(streamContent, "file", name);
            }

            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Post, uriBuilder, multipartFormDataContent, httpRequestMessage =>
            {
                httpRequestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", token);
            }, cancellationToken).ConfigureAwait(false))
            {
                return ResourceResponse.FromString(await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
        }
    }
}
