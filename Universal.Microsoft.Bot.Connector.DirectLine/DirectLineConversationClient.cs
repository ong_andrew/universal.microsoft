﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Universal.Common;
using Universal.Common.Net.WebSocket;

namespace Universal.Microsoft.Bot.Connector.DirectLine
{
    /// <summary>
    /// High level abstraction to manage a single direct line conversation.
    /// </summary>
    public class DirectLineConversationClient : IDisposable
    {
        private bool mUseWebSocket;

        /// <summary>
        /// Gets the <see cref="DirectLine.DirectLineClient" /> associated with this <see cref="DirectLineConversationClient" />.
        /// This should not be disposed as its lifecycle is tied to the <see cref="DirectLineConversationClient" />.
        /// </summary>
        public DirectLineClient DirectLineClient { get; protected set; }
        protected WebSocketClient WebSocketClient { get; set; }
        /// <summary>
        /// Gets the conversation associated with this client if it has been configured.
        /// </summary>
        public Conversation Conversation { get; protected set; }
        /// <summary>
        /// Gets a value indicating if the conversation is connected. 
        /// </summary>
        public bool Connected { get; protected set; }
        protected string Watermark { get; set; }

        protected CancellationTokenSource CancellationTokenSource { get; set; }

        /// <summary>
        /// Raised when a new <see cref="Activity"/> is available.
        /// </summary>
        public event EventHandler<ActivityAvailableEventArgs> OnActivityAvailable;

        protected List<string> OutgoingActivityIds { get; set; }
        protected SemaphoreSlim IncomingOutgoingSynchronizationSemaphore { get; set; }

        /// <summary>
        /// Creates a new instance of the <see cref="DirectLineConversationClient"/> class with the given secret or token.
        /// </summary>
        /// <param name="secretOrToken"></param>
        public DirectLineConversationClient(string secretOrToken) : this(secretOrToken, true) { }

        /// <summary>
        /// Creates a new instance of the <see cref="DirectLineConversationClient"/> class with the given secret or token.
        /// </summary>
        /// <param name="secretOrToken"></param>
        /// <param name="useWebSocket"></param>
        public DirectLineConversationClient(string secretOrToken, bool useWebSocket)
        {
            Connected = false;
            OutgoingActivityIds = new List<string>();

            DirectLineClient = new DirectLineClient(secretOrToken);
            IncomingOutgoingSynchronizationSemaphore = new SemaphoreSlim(1);

            mUseWebSocket = useWebSocket;
        }

        /// <summary>
        /// Reconnect to the specified conversation asynchronously.
        /// </summary>
        /// <param name="conversationId"></param>
        /// <returns></returns>
        public async Task<Conversation> ReconnectToConversationAsync(string conversationId)
        {
            return await ReconnectToConversationAsync(conversationId, CancellationToken.None).ConfigureAwait(false);
        }

        /// <summary>
        /// Reconnect to the specified conversation asynchronously.
        /// </summary>
        /// <param name="conversationId"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<Conversation> ReconnectToConversationAsync(string conversationId, CancellationToken cancellationToken)
        {
            if (disposedValue)
            {
                throw new ObjectDisposedException(nameof(DirectLineConversationClient));
            }

            if (Connected)
            {
                throw new InvalidOperationException("Already connected to a conversation.");
            }

            Conversation = await DirectLineClient.ReconnectToConversationAsync(conversationId, cancellationToken: cancellationToken).ConfigureAwait(false);
            await StartListeningLoopAsync().ConfigureAwait(false);
            Connected = true;

            return Conversation;
        }

        /// <summary>
        /// Starts a new conversation asynchronously and begins listening for activities.
        /// </summary>
        /// <returns></returns>
        public async Task<Conversation> StartConversationAsync()
        {
            return await StartConversationAsync(CancellationToken.None).ConfigureAwait(false);
        }

        /// <summary>
        /// Starts a new conversation asynchronously and begins listening for activities.
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<Conversation> StartConversationAsync(CancellationToken cancellationToken)
        {
            if (disposedValue)
            {
                throw new ObjectDisposedException(nameof(DirectLineConversationClient));
            }

            if (Connected)
            {
                throw new InvalidOperationException("Already connected to a conversation.");
            }

            Conversation = await DirectLineClient.StartConversationAsync(cancellationToken).ConfigureAwait(false);
            await StartListeningLoopAsync().ConfigureAwait(false);
            Connected = true;

            return Conversation;
        }

        protected async Task StartListeningLoopAsync()
        {
            if (mUseWebSocket)
            {
                WebSocketClient = new WebSocketClient(new Uri(Conversation.StreamUrl));

                WebSocketClient.OnMessageReceived += OnWebSocketMessageReceived;

                await WebSocketClient.OpenAsync().ConfigureAwait(false);
            }
            else 
            {
                CancellationTokenSource = new CancellationTokenSource();
#pragma warning disable CS4014
                Task.Run(async () =>
                {
                    while (!CancellationTokenSource.Token.IsCancellationRequested)
                    {
                        ActivitySet activitySet = await DirectLineClient.GetActivitiesAsync(Conversation.ConversationId, Watermark);
                        ProcessActivitySet(activitySet);

                        await Task.Delay(200);
                    }
                });
#pragma warning restore
            }
        }

        private void OnWebSocketMessageReceived(object sender, MessageReceivedEventArgs eventArgs)
        {
            if (eventArgs.Message.OpCode == OpCode.Text)
            {
                TextMessage textMessage = (TextMessage)eventArgs.Message;
                if (!textMessage.Text.IsNullOrEmpty())
                {
                    ActivitySet activitySet = ActivitySet.FromString(textMessage.Text);
                    if (!activitySet.All(x => x.Id != null))
                    {
                        activitySet.TryRecoverActivityIds(textMessage.Text);
                    }
                    ProcessActivitySet(activitySet);
                }
            }
        }

        protected void ProcessActivitySet(ActivitySet activitySet)
        {
            Watermark = activitySet.Watermark;

            foreach (Activity activity in activitySet.Activities)
            {
                IncomingOutgoingSynchronizationSemaphore.Wait();
                try
                {
                    OnActivityAvailable?.Invoke(this, new ActivityAvailableEventArgs()
                    {
                        Activity = activity,
                        Direction = OutgoingActivityIds.Contains(activity.Id) ? ActivityDirection.Outgoing : ActivityDirection.Incoming
                    });
                }
                finally
                {
                    IncomingOutgoingSynchronizationSemaphore.Release();
                }
            }
        }

        /// <summary>
        /// Posts the given activity asynchronously.
        /// </summary>
        /// <param name="activity"></param>
        /// <returns></returns>
        public async Task<ResourceResponse> PostActivityAsync(Activity activity)
        {
            return await PostActivityAsync(activity, CancellationToken.None).ConfigureAwait(false);
        }

        /// <summary>
        /// Posts the given activity asynchronously.
        /// </summary>
        /// <param name="activity"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ResourceResponse> PostActivityAsync(Activity activity, CancellationToken cancellationToken)
        {
            if (disposedValue)
            {
                throw new ObjectDisposedException(nameof(DirectLineConversationClient));
            }

            if (!Connected)
            {
                throw new InvalidOperationException("Client must be connected before posting.");
            }

            await IncomingOutgoingSynchronizationSemaphore.WaitAsync();
            try
            {
                ResourceResponse resourceResponse = await DirectLineClient.PostActivityAsync(Conversation.ConversationId, activity, cancellationToken).ConfigureAwait(false);
                OutgoingActivityIds.Add(resourceResponse.Id);
                return resourceResponse;
            }
            finally
            {
                IncomingOutgoingSynchronizationSemaphore.Release();
            }
        }

        /// <summary>
        /// Disconnects the client if it is connected.
        /// </summary>
        /// <returns></returns>
        public async Task CloseAsync()
        {
            if (Connected)
            {
                if (mUseWebSocket)
                {
                    if (WebSocketClient != null)
                    {
                        WebSocketClient.OnMessageReceived -= OnWebSocketMessageReceived;

                        if (WebSocketClient.State == WebSocketState.Connected)
                        {
                            await WebSocketClient.CloseAsync().ConfigureAwait(false);
                        }

                        WebSocketClient = null;
                    }
                }
                else
                {
                    if (CancellationTokenSource != null)
                    {
                        if (!CancellationTokenSource.Token.IsCancellationRequested)
                        {
                            CancellationTokenSource.Cancel();
                        }

                        CancellationTokenSource = null;
                    }
                }
            }
        }

        #region IDisposable Support
        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    CloseAsync().ConfigureAwait(false).GetAwaiter().GetResult();
                    WebSocketClient?.Dispose();
                    ((IDisposable)DirectLineClient)?.Dispose();
                }

                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }
        #endregion
    }
}
