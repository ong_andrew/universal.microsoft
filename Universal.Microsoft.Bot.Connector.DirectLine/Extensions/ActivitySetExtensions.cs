﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Universal.Microsoft.Bot.Connector.DirectLine
{
    /// <summary>
    /// Extension methods for the <see cref="ActivitySet"/> class.
    /// </summary>
    public static class ActivitySetExtensions
    {
        /// <summary>
        /// Attempts to receover null activity Ids from the raw JSON.
        /// </summary>
        /// <param name="ActivitySet"></param>
        /// <param name="rawJson"></param>
        /// <returns></returns>
        public static bool TryRecoverActivityIds(this ActivitySet ActivitySet, string rawJson)
        {
            try
            {
                bool foundActivities = false;
                bool foundArrayStart = false;
                bool foundArrayEnd = false;
                int depth = 0;
                bool prepareForId = false;
                int currentIndex = -1;
                List<string> correctedIds = new List<string>();
                using (JsonTextReader jsonTextReader = new JsonTextReader(new StringReader(rawJson)))
                {
                    while (jsonTextReader.Read())
                    {
                        if (!foundActivities && jsonTextReader.TokenType == JsonToken.PropertyName && jsonTextReader.Value.ToString() == "activities")
                        {
                            foundActivities = true;
                            continue;
                        }

                        if (foundActivities)
                        {
                            if (!foundArrayStart)
                            {
                                if (jsonTextReader.TokenType == JsonToken.StartArray)
                                {
                                    foundArrayStart = true;
                                }
                                else
                                {
                                    throw new InvalidOperationException("Could not find array start.");
                                }
                            }

                            if (foundArrayStart && !foundArrayEnd && depth == 0 && jsonTextReader.TokenType == JsonToken.EndArray)
                            {
                                foundArrayEnd = true;
                                break;
                            }

                            if (jsonTextReader.TokenType == JsonToken.StartObject)
                            {
                                depth++;

                                if (depth == 1)
                                {
                                    currentIndex++;
                                }
                            }

                            if (jsonTextReader.TokenType == JsonToken.EndObject)
                            {
                                depth--;
                            }

                            if (depth == 1 && jsonTextReader.TokenType == JsonToken.PropertyName && jsonTextReader.Value.ToString() == "id")
                            {
                                prepareForId = true;
                                continue;
                            }

                            if (prepareForId && jsonTextReader.TokenType == JsonToken.String || jsonTextReader.TokenType == JsonToken.Null)
                            {
                                string id = jsonTextReader.Value?.ToString();

                                if (currentIndex == correctedIds.Count)
                                {
                                    correctedIds.Add(id);
                                }
                                else
                                {
                                    string existingId = correctedIds.ElementAt(currentIndex);
                                    string preferredId = id ?? existingId;

                                    correctedIds.RemoveAt(currentIndex);
                                    correctedIds.Add(preferredId);
                                }

                                prepareForId = false;
                            }
                            else
                            {
                                prepareForId = false;
                            }
                        }
                    }
                }

                if (correctedIds.Count == ActivitySet.Count())
                {
                    for (int i = 0; i < ActivitySet.Count(); i++)
                    {
                        Activity activity = ActivitySet.ElementAt(i);
                        if (activity.Id == null)
                        {
                            activity.Id = correctedIds.ElementAt(i);
                        }
                    }
                }
            }
            catch { return false; }

            return true;
        }
    }
}

namespace Universal.Microsoft.Bot.Connector.DirectLine.Extensions
{
    /// <summary>
    /// Extension methods for the <see cref="ActivitySet"/> class.
    /// </summary>
    [Obsolete("Use the equivalent in the base namespace.")]
    public static class ActivitySetExtensions
    {
        /// <summary>
        /// Attempts to receover null activity Ids from the raw JSON.
        /// </summary>
        /// <param name="ActivitySet"></param>
        /// <param name="rawJson"></param>
        /// <returns></returns>
        [Obsolete("Use the equivalent in the base namespace.")]
        public static bool TryRecoverActivityIds(this ActivitySet ActivitySet, string rawJson)
        {
            try
            {
                bool foundActivities = false;
                bool foundArrayStart = false;
                bool foundArrayEnd = false;
                int depth = 0;
                bool prepareForId = false;
                int currentIndex = -1;
                List<string> correctedIds = new List<string>();
                using (JsonTextReader jsonTextReader = new JsonTextReader(new StringReader(rawJson)))
                {
                    while (jsonTextReader.Read())
                    {
                        if (!foundActivities && jsonTextReader.TokenType == JsonToken.PropertyName && jsonTextReader.Value.ToString() == "activities")
                        {
                            foundActivities = true;
                            continue;
                        }

                        if (foundActivities)
                        {
                            if (!foundArrayStart)
                            {
                                if (jsonTextReader.TokenType == JsonToken.StartArray)
                                {
                                    foundArrayStart = true;
                                }
                                else
                                {
                                    throw new InvalidOperationException("Could not find array start.");
                                }
                            }

                            if (foundArrayStart && !foundArrayEnd && depth == 0 && jsonTextReader.TokenType == JsonToken.EndArray)
                            {
                                foundArrayEnd = true;
                                break;
                            }

                            if (jsonTextReader.TokenType == JsonToken.StartObject)
                            {
                                depth++;

                                if (depth == 1)
                                {
                                    currentIndex++;
                                }
                            }

                            if (jsonTextReader.TokenType == JsonToken.EndObject)
                            {
                                depth--;
                            }

                            if (depth == 1 && jsonTextReader.TokenType == JsonToken.PropertyName && jsonTextReader.Value.ToString() == "id")
                            {
                                prepareForId = true;
                                continue;
                            }

                            if (prepareForId && jsonTextReader.TokenType == JsonToken.String || jsonTextReader.TokenType == JsonToken.Null)
                            {
                                string id = jsonTextReader.Value?.ToString();

                                if (currentIndex == correctedIds.Count)
                                {
                                    correctedIds.Add(id);
                                }
                                else
                                {
                                    string existingId = correctedIds.ElementAt(currentIndex);
                                    string preferredId = id ?? existingId;

                                    correctedIds.RemoveAt(currentIndex);
                                    correctedIds.Add(preferredId);
                                }

                                prepareForId = false;
                            }
                            else
                            {
                                prepareForId = false;
                            }
                        }
                    }
                }

                if (correctedIds.Count == ActivitySet.Count())
                {
                    for (int i = 0; i < ActivitySet.Count(); i++)
                    {
                        Activity activity = ActivitySet.ElementAt(i);
                        if (activity.Id == null)
                        {
                            activity.Id = correctedIds.ElementAt(i);
                        }
                    }
                }
            }
            catch { return false; }

            return true;
        }
    }
}
