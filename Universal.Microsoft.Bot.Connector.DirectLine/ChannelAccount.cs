﻿using Newtonsoft.Json;
using System.ComponentModel;
using Universal.Common.Serialization;

namespace Universal.Microsoft.Bot.Connector.DirectLine
{
    public class ChannelAccount : JsonSerializable<ChannelAccount>
    {
        [JsonProperty("aadObjectId")]
        public string AadObjectId { get; set; }
        [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeAadObjectId() { return AadObjectId != null; }
        [JsonProperty("id")]
        public string Id { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("role")]
        public string Role { get; set; }
        [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeRole() { return Role != null; }
    }
}
