﻿using Newtonsoft.Json;
using System.ComponentModel;
using Universal.Common.Serialization;

namespace Universal.Microsoft.Bot.Connector.DirectLine
{
    public class CardAction : JsonSerializable<CardAction>
    {
        [JsonProperty("channelData")]
        public string ChannelData { get; set; }
        [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeChannelData() { return ChannelData != null; }
        [JsonProperty("displayText")]
        public string DisplayText { get; set; }
        [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeDisplayText() { return DisplayText != null; }
        [JsonProperty("image")]
        public string Image { get; set; }
        [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeImage() { return Image != null; }
        [JsonProperty("text")]
        public string Text { get; set; }
        [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeText() { return Text != null; }
        [JsonProperty("title")]
        public string Title { get; set; }
        [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeTitle() { return Title != null; }
        [JsonProperty("type")]
        public string Type { get; set; }
        [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeType() { return Type != null; }
        [JsonProperty("value")]
        public object Value { get; set; }
        [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeValue() { return Value != null; }
    }
}
