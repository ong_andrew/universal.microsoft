﻿using Microsoft.Graph;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Universal.Microsoft.Graph
{
    /// <summary>
    /// Extensions for the <see cref="ICollectionPage{T}"/> class.
    /// </summary>
    public static class ICollectionPageExtensions
    {
        /// <summary>
        /// Iterates over the entire collection page, returning the entire collection at the end of the iteration.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="collectionPage"></param>
        /// <param name="baseClient"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public static async Task<IEnumerable<T>> IterateAsync<T>(this ICollectionPage<T> collectionPage, IBaseClient baseClient, CancellationToken cancellationToken = default)
        {
            List<T> ts = new List<T>();
            PageIterator<T> pageIterator = PageIterator<T>.CreatePageIterator(baseClient, collectionPage, t => { ts.Add(t); return true; });
            await pageIterator.IterateAsync(cancellationToken).ConfigureAwait(false);
            return ts;
        }
    }
}
