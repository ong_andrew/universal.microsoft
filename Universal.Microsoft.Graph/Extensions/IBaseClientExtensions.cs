﻿using Microsoft.Graph;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Universal.Microsoft.Graph
{
    /// <summary>
    /// Extensions for the <see cref="IBaseClient"/> interface.
    /// </summary>
    public static class IBaseClientExtensions
    {
        /// <summary>
        /// Gets the entire collection of entities from the initial collection page.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="baseClient"></param>
        /// <param name="collectionPage"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public static async Task<IEnumerable<T>> GetCollectionAsync<T>(this IBaseClient baseClient, ICollectionPage<T> collectionPage, CancellationToken cancellationToken = default)
        {
            return await collectionPage.IterateAsync(baseClient, cancellationToken).ConfigureAwait(false);
        }
    }
}
