﻿using Microsoft.EntityFrameworkCore;
using System;

namespace Universal.Microsoft.EntityFrameworkCore
{
    /// <summary>
    /// Extensions for the <see cref="DbSet{TEntity}"/> class.
    /// </summary>
    public static class DbSetExtensions
    {
        /// <summary>
        /// Removes all entities from the <see cref="DbSet{TEntity}"/>. This uses EF Core's database-agnostic methods and may not be suitable for large tables.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dbSet"></param>
        public static void Clear<T>(this DbSet<T> dbSet) where T : class
        {
            dbSet.RemoveRange(dbSet);
        }
    }
}
