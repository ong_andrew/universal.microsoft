﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Universal.Microsoft.EntityFrameworkCore
{
    /// <summary>
    /// Extensions for the <see cref="ModelBuilder"/> class.
    /// </summary>
    public static class ModelBuilderExtensions
    {
        /// <summary>
        /// Finds and calls all static methods decorated with <see cref="ModelBuilderAttribute"/> that take in a single parameter of <see cref="ModelBuilder"/> type in the specific assembly on the provided <see cref="ModelBuilder"/> instance.
        /// </summary>
        /// <param name="modelBuilder"></param>
        /// <param name="assembly"></param>
        /// <returns></returns>
        public static ModelBuilder ConfigureFromAssembly(this ModelBuilder modelBuilder, Assembly assembly)
        {
            IEnumerable<MethodInfo> configureModelBuilderMethodInfos = assembly.GetTypes().SelectMany(
                x => x.GetMethods(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static).Where(
                    y =>
                        (y.ReturnType == typeof(ModelBuilder) || y.ReturnType == typeof(void)) &&
                        y.GetParameters().Count() == 1 &&
                        y.GetParameters().Single().ParameterType == typeof(ModelBuilder) &&
                        y.GetCustomAttribute<ModelBuilderAttribute>() != null));

            foreach (MethodInfo configureModelBuilderMethodInfo in configureModelBuilderMethodInfos)
            {
                modelBuilder = (ModelBuilder)configureModelBuilderMethodInfo.Invoke(null, new object[] { modelBuilder });
            }

            return modelBuilder;
        }
    }
}
