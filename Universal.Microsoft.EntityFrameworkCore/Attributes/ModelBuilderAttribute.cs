﻿using Microsoft.EntityFrameworkCore;
using System;

namespace Universal.Microsoft.EntityFrameworkCore
{
    /// <summary>
    /// Indicates that this method should be picked up by the call to <see cref="ModelBuilderExtensions.ConfigureFromAssembly(ModelBuilder, System.Reflection.Assembly)"/>.
    /// The method should be static and take in a single parameter of <see cref="ModelBuilder"/>, and return either <see cref="void"/> or <see cref="ModelBuilder"/>.
    /// </summary>
    [AttributeUsage(AttributeTargets.Method)]
    public class ModelBuilderAttribute : Attribute
    {
    }
}
