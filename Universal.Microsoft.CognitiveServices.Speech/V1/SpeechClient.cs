﻿using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Universal.Common;
using Universal.Common.Net.Http;

namespace Universal.Microsoft.CognitiveServices.Speech.V1
{
    /// <summary>
    /// Client for interacting with the V1 speech cognitive services.
    /// </summary>
    public class SpeechClient : HttpServiceClient
    {
        private string mRegion;
        private string mSubscriptionKey;

        /// <summary>
        /// Creates a new instance of the <see cref="SpeechClient"/> class with the given subscription key.
        /// </summary>
        /// <param name="region">See <see cref="Regions"/>.</param>
        /// <param name="subscriptionKey"></param>
        public SpeechClient(string region, string subscriptionKey)
        {
            mRegion = region;
            mSubscriptionKey = subscriptionKey;
        }

        protected override HttpClient CreateHttpClient()
        {
            HttpClient httpClient = base.CreateHttpClient();
            httpClient.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", mSubscriptionKey);
            return httpClient;
        }

        protected override Task HandleNonSuccessCodeAsync(HttpResponseMessage httpResponseMessage, CancellationToken cancellationToken)
        {
            throw new HttpException(httpResponseMessage);
        }

        /// <summary>
        /// Recognizes the speech, supporting dictating of punctuation.
        /// </summary>
        /// <param name="audioBytes"></param>
        /// <param name="endpointId"></param>
        /// <returns></returns>
        public async Task<RecognizeResponse> RecognizeDictationAsync(byte[] audioBytes, string endpointId)
        {
            return await RecognizeDictationAsync(audioBytes, endpointId, default).ConfigureAwait(false);
        }

        /// <summary>
        /// Recognizes the speech, supporting dictating of punctuation.
        /// </summary>
        /// <param name="audioBytes"></param>
        /// <param name="endpointId"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<RecognizeResponse> RecognizeDictationAsync(byte[] audioBytes, string endpointId, CancellationToken cancellationToken)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(
                HttpMethod.Post,
                new UriBuilder($"https://{mRegion}.stt.speech.microsoft.com/speech/recognition/dictation/cognitiveservices/v1?cid={endpointId}"),
                new ByteArrayContent(audioBytes),
                (httpRequestMessage) =>
                {
                    httpRequestMessage.Content.Headers.TryAddWithoutValidation("Content-type", "audio/wav; codec=audio/pcm; samplerate=16000");
                },
                cancellationToken).ConfigureAwait(false))
            {
                return RecognizeResponse.FromString(await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
        }

        /// <summary>
        /// Recognizes the speech through a custom endpoint.
        /// </summary>
        /// <param name="audioBytes"></param>
        /// <param name="endpointId"></param>
        /// <returns></returns>
        public async Task<RecognizeResponse> RecognizeInteractiveAsync(byte[] audioBytes, string endpointId)
        {
            return await RecognizeInteractiveAsync(audioBytes, endpointId, default).ConfigureAwait(false);
        }

        /// <summary>
        /// Recognizes the speech through a custom endpoint.
        /// </summary>
        /// <param name="audioBytes"></param>
        /// <param name="endpointId"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<RecognizeResponse> RecognizeInteractiveAsync(byte[] audioBytes, string endpointId, CancellationToken cancellationToken)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(
                HttpMethod.Post,
                new UriBuilder($"https://{mRegion}.stt.speech.microsoft.com/speech/recognition/interactive/cognitiveservices/v1?cid={endpointId}"),
                new ByteArrayContent(audioBytes),
                (httpRequestMessage) =>
                {
                    httpRequestMessage.Content.Headers.TryAddWithoutValidation("Content-type", "audio/wav; codec=audio/pcm; samplerate=16000");
                },
                cancellationToken).ConfigureAwait(false))
            {
                return RecognizeResponse.FromString(await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
        }

        /*
        public async Task<string> GetEndpointsAsync()
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Get, mUriGenerator.Endpoints()))
            {
                return await httpResponseMessage.Content.ReadAsStringAsync();
            }
        }

        public async Task<string> GetEndpointAsync(string id)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Get, mUriGenerator.Endpoints(id)))
            {
                return await httpResponseMessage.Content.ReadAsStringAsync();
            }
        }

        public async Task<string> NewEndpointAsync(string jsonString)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Post, mUriGenerator.Endpoints(), new JsonContent(jsonString)))
            {
                IEnumerable<string> headerValues = httpResponseMessage.Headers.GetValues("Operation-Location");
                string operationLocation = headerValues.FirstOrDefault();
                UriBuilder operationLocationUri = new UriBuilder(operationLocation);
                string returnId = operationLocationUri.Segments.Last();

                return returnId;
            }
        }

        public async Task DeleteEndpointAsync(string id)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Delete, mUriGenerator.Endpoints(id)))
            {
            }
        }

        public async Task<string> GetDatasetsAsync()
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Get, mUriGenerator.Datasets()))
            {
                return await httpResponseMessage.Content.ReadAsStringAsync();
            }
        }

        public async Task<string> GetDatasetAsync(string id)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Get, mUriGenerator.Datasets(id)))
            {
                return await httpResponseMessage.Content.ReadAsStringAsync();
            }
        }

        public async Task<string> NewAcousticDatasetAsync(AcousticDataset acousticDataset, string audioFileName, string transcriptionFileName)
        {
            //AcousticDataset myTestDataset = AcousticDataset.FromString(jsonString);
            MultipartFormDataContent myContent = acousticDataset.ToMultiPartFormDataContent(audioFileName, transcriptionFileName);

            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Post, mUriGenerator.DatasetsUpload(), myContent))
            {
                IEnumerable<string> headerValues = httpResponseMessage.Headers.GetValues("Operation-Location");
                string operationLocation = headerValues.FirstOrDefault();
                UriBuilder operationLocationUri = new UriBuilder(operationLocation);
                string returnId = operationLocationUri.Segments.Last();

                return returnId;
            }
        }

        public async Task<string> NewLanguageDataset(LanguageDataset languageDataset, string transcriptionFileName)
        {
            //AcousticDataset myTestDataset = AcousticDataset.FromString(jsonString);
            MultipartFormDataContent myContent = languageDataset.ToMultiPartFormDataContent(transcriptionFileName);

            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Post, mUriGenerator.DatasetsUpload(), myContent))
            {
                IEnumerable<string> headerValues = httpResponseMessage.Headers.GetValues("Operation-Location");
                string operationLocation = headerValues.FirstOrDefault();
                UriBuilder operationLocationUri = new UriBuilder(operationLocation);
                string returnId = operationLocationUri.Segments.Last();

                return returnId;
            }
        }

        public async Task DeleteDatasetAsync(string id)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Delete, mUriGenerator.Datasets(id)))
            {
            }
        }

        public async Task<string> NewModelAsync(string jsonString)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Post, mUriGenerator.Models(), new JsonContent(jsonString)))
            {
                IEnumerable<string> headerValues = httpResponseMessage.Headers.GetValues("Operation-Location");
                string operationLocation = headerValues.FirstOrDefault();
                UriBuilder operationLocationUri = new UriBuilder(operationLocation);
                string returnId = operationLocationUri.Segments.Last();

                return returnId;
            }
        }

        public async Task<string> GetModelsAsync()
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Get, mUriGenerator.Models()))
            {
                return await httpResponseMessage.Content.ReadAsStringAsync();
            }
        }

        public async Task<string> GetModelAsync(string id)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Get, mUriGenerator.Models(id)))
            {
                return await httpResponseMessage.Content.ReadAsStringAsync();
            }
        }

        public async Task DeleteModelAsync(string id)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Delete, mUriGenerator.Models(id)))
            {
            }
        }

        private class UriGenerator
        {
            private enum HostType
            {
                Cris,
                SpeechToText,
                TextToSpeech
            };

            private string ToHost(string region, HostType hostType)
            {
                switch (hostType)
                {
                    case HostType.Cris:
                        {
                            return $"{region}.cris.ai";
                        }
                    case HostType.SpeechToText:
                        {
                            return $"{region}.stt.speech.microsoft.com";
                        }
                    case HostType.TextToSpeech:
                        {
                            return $"{region}.tts.speech.microsoft.com";
                        }
                    default:
                        {
                            throw new ArgumentException($"Unknown host type: {hostType.ToString()}");
                        }
                }
            }

            protected string mRegion;

            public UriGenerator(string region)
            {
                mRegion = region;
            }

            public UriBuilder Cris()
            {
                return new UriBuilder("https", ToHost(mRegion, HostType.Cris));
            }

            public UriBuilder Cris(string path)
            {
                UriBuilder uriBuilder = Cris();
                uriBuilder.Path = path;
                return uriBuilder;
            }

            public UriBuilder SpeechToText()
            {
                return new UriBuilder("https", ToHost(mRegion, HostType.SpeechToText));
            }

            public UriBuilder TextToSpeech()
            {
                return new UriBuilder("https", ToHost(mRegion, HostType.TextToSpeech));
            }
            
            public UriBuilder AccuracyTests()
            {
                return Cris("/api/speechtotext/v2.0/accuracytests");
            }

            public UriBuilder AccuracyTests(string id)
            {
                return AccuracyTests().AddSegment(id);
            }
            
            public UriBuilder Datasets()
            {
                return Cris("/api/speechtotext/v2.0/datasets");
            }

            public UriBuilder Datasets(string id)
            {
                return Datasets().AddSegment(id);
            }

            public UriBuilder DatasetsLocales()
            {
                return Datasets().AddSegment("locales");
            }

            public UriBuilder DatasetsUpload()
            {
                return Datasets().AddSegment("upload");
            }
            
            public UriBuilder Endpoints()
            {
                return Cris("/api/speechtotext/v2.0/endpoints");
            }

            public UriBuilder Endpoints(string id)
            {
                return Endpoints().AddSegment(id);
            }

            public UriBuilder EndpointsLocales(string id)
            {
                return Endpoints(id).AddSegment("locales");
            }

            public UriBuilder EndpointsData(string id)
            {
                return Endpoints(id).AddSegment("data");
            }
            
            public UriBuilder EndpointsDataTask(string endpointId, string taskId)
            {
                return Endpoints(endpointId).AddSegment(taskId);
            }
            
            public UriBuilder Models()
            {
                return Cris("/api/speechtotext/v2.0/models");
            }

            public UriBuilder Models(string id)
            {
                return Models().AddSegment(id);
            }

            public UriBuilder ModelsLocales(string id)
            {
                return Models(id).AddSegment("locales");
            }
            
            public UriBuilder TranscriptionEndpoint(string id)
            {
                return Cris("speech/recognition/interactive/cognitiveservices/v1").AddQuery("cid", id);
            }
        }
        */
    }
}
