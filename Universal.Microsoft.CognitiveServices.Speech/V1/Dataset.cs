﻿/*
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using Universal.Common;
using Universal.Common.Serialization;

namespace Universal.Microsoft.CognitiveServices.Speech.V1
{
    public class AcousticDataset : JsonSerializable<AcousticDataset>
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("locale")]
        public string Locale { get; set; }

        //request only
        [JsonProperty("audiodata")]
        public string AudioDataPath { get; set; }

        //request only
        [JsonProperty("transcriptions")]
        public string TranscriptionsPath { get; set; }

        //response only
        [JsonProperty("dataImportKind")]
        public string DataImportKind { get; set; }

        //response only
        [JsonProperty("id")]
        public string Id { get; set; }

        //response only
        [JsonProperty("createdDateTime")]
        public DateTime CreatedDateTime { get; set; }

        //response only
        [JsonProperty("lastActionDateTime")]
        public DateTime LastActionDateTime { get; set; }

        //response only
        [JsonProperty("status")]
        public string Status { get; set; }

        //response only
        [JsonProperty("properties")]
        public Dictionary<string, string> Properties { get; set; }


        public AcousticDataset(string name, string audioDataPath, string transcriptionsPath)
        {
            //required in request
            Name = name;
            Locale = "en-us";
            AudioDataPath = audioDataPath;
            TranscriptionsPath = transcriptionsPath;
            DataImportKind = "Acoustic";
            Description = Name;
        }

        public MultipartFormDataContent ToMultiPartFormDataContent(string audioFileName, string transcriptionFileName)
        {

            byte[] audioByteArray = File.ReadAllBytes(AudioDataPath);
            byte[] transcriptionByteArray = File.ReadAllBytes(TranscriptionsPath);

            MultipartFormDataContent content = new MultipartFormDataContent();
            content.Add(new StringContent(Name), "name");
            content.Add(new StringContent(Description), "description");
            content.Add(new StringContent(Locale), "locale");
            content.Add(new StringContent(DataImportKind), "dataImportKind");

            content.Add(new ByteArrayContent(audioByteArray), "audiodata", audioFileName);
            content.Last().Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue(MediaTypes.Application.Zip);
            content.Add(new ByteArrayContent(transcriptionByteArray), "transcriptions", transcriptionFileName);
            content.Last().Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue(MediaTypes.Text.Plain);

            return content;
        }
    }

    //Other datasets are here for completion but could be incorrect, should create a base class


    public class LanguageDataset : JsonSerializable<LanguageDataset>
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("locale")]
        public string Locale { get; set; }

        //request only
        [JsonProperty("languagedata")]
        public string LanguageDataPath { get; set; }

        //response only
        [JsonProperty("dataImportKind")]
        public string DataImportKind { get; set; }

        //response only
        [JsonProperty("id")]
        public string Id { get; set; }

        //response only
        [JsonProperty("createdDateTime")]
        public DateTime CreatedDateTime { get; set; }

        //response only
        [JsonProperty("lastActionDateTime")]
        public DateTime LastActionDateTime { get; set; }

        //response only
        [JsonProperty("status")]
        public string Status { get; set; }

        //response only
        [JsonProperty("properties")]
        public Dictionary<string, string> Properties { get; set; }


        public LanguageDataset(string name, string languagedata)
        {
            //required in request
            Name = name;
            Locale = "en-us";
            LanguageDataPath = languagedata;
            DataImportKind = "Language";
            Description = Name;
        }

        public MultipartFormDataContent ToMultiPartFormDataContent(string languageFileName)
        {

            byte[] languageByteArray = File.ReadAllBytes(LanguageDataPath);

            MultipartFormDataContent content = new MultipartFormDataContent();
            content.Add(new StringContent(Name), "name");
            content.Add(new StringContent(Description), "description");
            content.Add(new StringContent(Locale), "locale");
            content.Add(new StringContent(DataImportKind), "dataImportKind");

            content.Add(new ByteArrayContent(languageByteArray), "languagedata", languageFileName);
            content.Last().Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue(MediaTypes.Text.Plain);

            return content;
        }
    }
}
*/