﻿using Newtonsoft.Json;
using Universal.Common.Serialization;

namespace Universal.Microsoft.CognitiveServices.Speech.V1
{
    public class RecognizeResponse : JsonSerializable<RecognizeResponse>
    {
        [JsonProperty("RecognitionStatus")]
        public string RecognitionStatus { get; set; }
        [JsonProperty("DisplayText")]
        public string DisplayText { get; set; }
        [JsonProperty("Offset")]
        public long Offset { get; set; }
        [JsonProperty("Duration")]
        public long Duration { get; set; }
    }
}
