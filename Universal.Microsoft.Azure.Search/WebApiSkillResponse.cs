﻿using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using Universal.Common.Serialization;

namespace Universal.Microsoft.Azure.Search
{
    /// <summary>
    /// Class that represents a response from the custom skill to the search service.
    /// </summary>
    [JsonObject]
    public class WebApiSkillResponse : JsonSerializable<WebApiSkillResponse>, IEnumerable<WebApiSkillResponse.Record>
    {
        [JsonProperty("values")]
        public IList<Record> Values { get; set; }

        public WebApiSkillResponse()
        {
            Values = new List<Record>();
        }

        public class Record : JsonSerializable<Record>
        {
            [JsonProperty("recordId")]
            public string RecordId { get; set; }
            [JsonProperty("data")]
            public Dictionary<string, object> Data { get; set; }
            [JsonProperty("errors")]
            public List<Issue> Errors { get; set; }
            [JsonProperty("warnings")]
            public List<Issue> Warnings { get; set; }

            public Record()
            {
                Data = new Dictionary<string, object>();
                Errors = new List<Issue>();
                Warnings = new List<Issue>();
            }
        }

        public class Issue : JsonSerializable<Issue>
        {
            [JsonProperty("message")]
            public string Message { get; set; }

            public static implicit operator Issue(string message)
            {
                return new Issue() { Message = message };
            }
        }

        IEnumerator<Record> IEnumerable<Record>.GetEnumerator()
        {
            return ((IEnumerable<Record>)Values).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable<Record>)Values).GetEnumerator();
        }
    }

    /// <summary>
    /// Class that represents a response from the custom skill to the search service.
    /// </summary>
    [JsonObject]
    public class WebApiSkillResponse<T> : JsonSerializable<WebApiSkillResponse<T>>, IEnumerable<WebApiSkillResponse<T>.Record<T>>
    {
        [JsonProperty("values")]
        public IList<Record<T>> Values { get; set; }

        public WebApiSkillResponse()
        {
            Values = new List<Record<T>>();
        }

        public class Record<X> : JsonSerializable<Record<X>>
        {
            [JsonProperty("recordId")]
            public string RecordId { get; set; }
            [JsonProperty("data")]
            public X Data { get; set; }
            [JsonProperty("errors")]
            public List<Issue> Errors { get; set; }
            [JsonProperty("warnings")]
            public List<Issue> Warnings { get; set; }

            public Record()
            {
                Errors = new List<Issue>();
                Warnings = new List<Issue>();
            }
        }

        public class Issue : JsonSerializable<Issue>
        {
            [JsonProperty("message")]
            public string Message { get; set; }

            public static implicit operator Issue(string message)
            {
                return new Issue() { Message = message };
            }
        }

        IEnumerator<Record<T>> IEnumerable<Record<T>>.GetEnumerator()
        {
            return ((IEnumerable<Record<T>>)Values).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable<Record<T>>)Values).GetEnumerator();
        }
    }
}
