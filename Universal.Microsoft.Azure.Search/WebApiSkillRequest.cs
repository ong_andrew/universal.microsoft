﻿using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using Universal.Common.Serialization;

namespace Universal.Microsoft.Azure.Search
{
    /// <summary>
    /// Class that represents a request from the search service to the custom skill.
    /// </summary>
    [JsonObject]
    public class WebApiSkillRequest : JsonSerializable<WebApiSkillRequest>, IEnumerable<WebApiSkillRequest.Record>
    {
        [JsonProperty("values")]
        public IList<Record> Values { get; set; }

        public WebApiSkillRequest()
        {
            Values = new List<Record>();
        }

        public class Record : JsonSerializable<Record>
        {
            [JsonProperty("recordId")]
            public string RecordId { get; set; }
            [JsonProperty("data")]
            public Dictionary<string, object> Data { get; set; }

            public Record()
            {
                Data = new Dictionary<string, object>();
            }
        }

        IEnumerator<Record> IEnumerable<Record>.GetEnumerator()
        {
            return ((IEnumerable<Record>)Values).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable<Record>)Values).GetEnumerator();
        }
    }

    /// <summary>
    /// Class that represents a request from the search service to the custom skill.
    /// </summary>
    [JsonObject]
    public class WebApiSkillRequest<T> : JsonSerializable<WebApiSkillRequest<T>>, IEnumerable<WebApiSkillRequest<T>.Record<T>>
    {
        [JsonProperty("values")]
        public IList<Record<T>> Values { get; set; }

        public WebApiSkillRequest()
        {
            Values = new List<Record<T>>();
        }

        public class Record<X> : JsonSerializable<Record<X>>
        {
            [JsonProperty("recordId")]
            public string RecordId { get; set; }
            [JsonProperty("data")]
            public X Data { get; set; }

            public Record()
            {
            }
        }

        IEnumerator<Record<T>> IEnumerable<Record<T>>.GetEnumerator()
        {
            return ((IEnumerable<Record<T>>)Values).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable<Record<T>>)Values).GetEnumerator();
        }
    }
}
