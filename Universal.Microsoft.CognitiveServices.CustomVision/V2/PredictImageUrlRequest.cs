﻿using Newtonsoft.Json;
using Universal.Common.Serialization;

namespace Universal.Microsoft.CognitiveServices.CustomVision.V2
{
    public class PredictImageUrlRequest : JsonSerializable<PredictImageUrlRequest>
    {
        [JsonProperty("url")]
        public string Url { get; set; }
    }
}
