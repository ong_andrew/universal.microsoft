﻿using Newtonsoft.Json;
using Universal.Common.Serialization;

namespace Universal.Microsoft.CognitiveServices.CustomVision.V2
{
    public class CreateProjectResponse : JsonSerializable<CreateProjectResponse>
    {
        [JsonProperty("id")]
        public string Id { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("description")]
        public string Description { get; set; }
        [JsonProperty("Settings")]
        public SettingsItem Settings { get; set; }
        [JsonProperty("created")]
        public string Created { get; set; }
        [JsonProperty("lastModified")]
        public string LastModified { get; set; }
        [JsonProperty("thumbnailUri")]
        public string ThumbnailUri { get; set; }

        public CreateProjectResponse()
        {
            Settings = new SettingsItem();
        }

        public class SettingsItem
        {
            [JsonProperty("domainId")]
            public string DomainId { get; set; }
        }
    }
}