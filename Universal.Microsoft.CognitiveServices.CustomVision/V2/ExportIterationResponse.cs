﻿using Newtonsoft.Json;
using Universal.Common.Serialization;

namespace Universal.Microsoft.CognitiveServices.CustomVision.V2
{
    public class ExportIterationResponse : JsonSerializable<ExportIterationResponse>
    {
        [JsonProperty("platform")]
        public string Platform { get; set; }
        [JsonProperty("status")]
        public string Status { get; set; }
        [JsonProperty("downloadUri")]
        public string DownloadUri { get; set; }
        [JsonProperty("flavor")]
        public string Flavor { get; set; }
    }
}
