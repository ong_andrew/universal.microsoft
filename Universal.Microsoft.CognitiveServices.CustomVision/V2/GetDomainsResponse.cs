﻿using Newtonsoft.Json;
using System.Collections.Generic;
using Universal.Common.Serialization;

namespace Universal.Microsoft.CognitiveServices.CustomVision.V2
{
    public class GetDomainsResponse : JsonCollectionSerializable<GetDomainsResponse, GetDomainsResponse.Domain>
    {
        public GetDomainsResponse()
        {
            Collection = new List<Domain>();
        }

        public class Domain
        {
            [JsonProperty("id")]
            public string Id { get; set; }
            [JsonProperty("name")]
            public string Name { get; set; }
            [JsonProperty("type")]
            public string Type { get; set; }
            [JsonProperty("exportable")]
            public bool Exportable { get; set; }
            [JsonProperty("enabled")]
            public bool Enabled { get; set; }
        }
    }
}
