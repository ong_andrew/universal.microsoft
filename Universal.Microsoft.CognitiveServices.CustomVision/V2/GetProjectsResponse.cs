﻿using Newtonsoft.Json;
using System.Collections.Generic;
using Universal.Common.Serialization;

namespace Universal.Microsoft.CognitiveServices.CustomVision.V2
{
    public class GetProjectsResponse : JsonCollectionSerializable<GetProjectsResponse, GetProjectsResponse.Project>
    {
        public GetProjectsResponse()
        {
            Collection = new List<Project>();
        }

        public class Project
        {
            [JsonProperty("id")]
            public string Id { get; set; }
            [JsonProperty("name")]
            public string Name { get; set; }
            [JsonProperty("description")]
            public string Description { get; set; }
            [JsonProperty("Settings")]
            public Settings Settings { get; set; }
            [JsonProperty("created")]
            public string Created { get; set; }
            [JsonProperty("lastModified")]
            public string LastModified { get; set; }
            [JsonProperty("thumbnailUri")]
            public string ThumbnailUri { get; set; }

            public Project()
            {
                Settings = new Settings();
            }
        }

        public class Settings
        {
            [JsonProperty("domainId")]
            public string DomainId { get; set; }
        }
    }
}
