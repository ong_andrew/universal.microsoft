﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Universal.Common.Net.Http;

namespace Universal.Microsoft.CognitiveServices.CustomVision.V2
{
    /// <summary>
    /// Client for interacting with the Microsoft Custom Vision V2 service.
    /// </summary>
    public class CustomVisionClient : HttpServiceClient
    {
        protected UriGenerator mUriGenerator;
        protected string mTrainingKey;
        protected string mPredictionKey;

        /// <summary>
        /// Initializes a new instance of the <see cref="CustomVisionClient"/> that can only be used for the training endpoints.
        /// </summary>
        /// <param name="trainingKey"></param>
        /// <param name="host"></param>
        /// <returns></returns>
        public static CustomVisionClient CreateTrainingClient(string trainingKey, string host = Hosts.SouthCentralUS)
        {
            return new CustomVisionClient(trainingKey, null, host);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CustomVisionClient"/> that can only be used for the prediction endpoints.
        /// </summary>
        /// <param name="predictionKey"></param>
        /// <param name="host"></param>
        /// <returns></returns>
        public static CustomVisionClient CreatePredictionClient(string predictionKey, string host = Hosts.SouthCentralUS)
        {
            return new CustomVisionClient(null, predictionKey, host);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CustomVisionClient"/> with the given training and prediction keys. If not provided, they are not sent along with the requests.
        /// </summary>
        /// <param name="trainingKey">Subscription key which provides access to this API.</param>
        /// <param name="predictionKey">Subscription key which provides access to this API.</param>
        /// <param name="host">The base URL for the service, defaults to "southcentralus.api.cognitive.microsoft.com".</param>
        public CustomVisionClient(string trainingKey, string predictionKey, string host = Hosts.SouthCentralUS)
        {
            mTrainingKey = trainingKey;
            mPredictionKey = predictionKey;
            mUriGenerator = new UriGenerator(host);
        }

        protected override HttpClient CreateHttpClient()
        {
            HttpClient httpClient = base.CreateHttpClient();
            if (mTrainingKey != null)
            {
                httpClient.DefaultRequestHeaders.Add("Training-Key", mTrainingKey);
            }

            if (mPredictionKey != null)
            {
                httpClient.DefaultRequestHeaders.Add("Prediction-key", mPredictionKey);
            }
            return httpClient;
        }

        protected override Task HandleNonSuccessCodeAsync(HttpResponseMessage httpResponseMessage, CancellationToken cancellationToken)
        {
            try
            {
                throw new CustomVisionException(httpResponseMessage);
            }
            catch (JsonSerializationException)
            {
                throw new HttpException(httpResponseMessage);
            }
        }

        /// <summary>
        /// This API accepts body content as multipart/form-data and application/octet-stream. When using multipart multiple image files can be sent at once, with a maximum of 64 files
        /// </summary>
        /// <param name="projectId">The project id</param>
        /// <param name="images"></param>
        /// <param name="tagIds">The tags ids with which to tag each image. Limited to 20</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<CreateImagesFromDataResponse> CreateImagesFromDataAsync(string projectId, IEnumerable<byte[]> images, string[] tagIds = null, CancellationToken cancellationToken = default)
        {
            MultipartFormDataContent content = new MultipartFormDataContent();

            foreach (byte[] image in images)
            {
                content.Add(new ByteArrayContent(image));
            }

            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Post, mUriGenerator.CreateImagesFromData(projectId, tagIds), content, cancellationToken: cancellationToken).ConfigureAwait(false))
            {
                return CreateImagesFromDataResponse.FromString(await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
        }

        /// <summary>
        /// Associate a set of images with a set of tags
        /// </summary>
        /// <param name="projectId">The project id</param>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<CreateImageTagsResponse> CreateImageTagsAsync(string projectId, CreateImageTagsRequest request, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Post, mUriGenerator.CreateImageTags(projectId), new JsonContent(request.ToString()), cancellationToken: cancellationToken).ConfigureAwait(false))
            {
                return CreateImageTagsResponse.FromString(await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
        }

        /// <summary>
        /// Create a project
        /// </summary>
        /// <param name="name">Name of the project</param>
        /// <param name="description">The description of the project</param>
        /// <param name="domainId">The id of the domain to use for this project. Defaults to General</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<CreateProjectResponse> CreateProjectAsync(string name, string description = null, string domainId = null, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Post, mUriGenerator.CreateProject(name, description, domainId), cancellationToken: cancellationToken).ConfigureAwait(false))
            {
                return CreateProjectResponse.FromString(await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
        }

        /// <summary>
        /// Create a tag for the project
        /// </summary>
        /// <param name="projectId">The project id</param>
        /// <param name="name">The tag name</param>
        /// <param name="description">Optional description for the tag</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<CreateTagResponse> CreateTagAsync(string projectId, string name, string description = null, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Post, mUriGenerator.CreateTag(projectId, name, description), cancellationToken: cancellationToken).ConfigureAwait(false))
            {
                return CreateTagResponse.FromString(await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
        }

        /// <summary>
        /// Delete images from the set of training images
        /// </summary>
        /// <param name="projectId">The project id</param>
        /// <param name="imageIds">Ids of the images to be deleted. Limted to 256 images per batch</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task DeleteImagesAsync(string projectId, string[] imageIds, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Delete, mUriGenerator.Images(projectId, imageIds), cancellationToken: cancellationToken).ConfigureAwait(false))
            {
            }
        }

        /// <summary>
        /// Delete a specific iteration of a project
        /// </summary>
        /// <param name="projectId"></param>
        /// <param name="iterationId"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task DeleteIterationAsync(string projectId, string iterationId, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Delete, mUriGenerator.Iterations(projectId, iterationId), cancellationToken: cancellationToken).ConfigureAwait(false))
            {
            }
        }

        /// <summary>
        /// Delete a specific project
        /// </summary>
        /// <param name="projectId"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task DeleteProjectAsync(string projectId, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Delete, mUriGenerator.Projects(projectId), cancellationToken: cancellationToken).ConfigureAwait(false))
            {
            }
        }

        /// <summary>
        /// Export a trained iteration
        /// </summary>
        /// <param name="projectId">The project id</param>
        /// <param name="iterationId">The iteration id</param>
        /// <param name="platform">The target platform, <see cref="Platforms"/>.</param>
        /// <param name="flavor">The flavor of the target platform, <see cref="Flavors"/>.</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ExportIterationResponse> ExportIterationAsync(string projectId, string iterationId, string platform, string flavor = null, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Post, mUriGenerator.Export(projectId, iterationId, platform, flavor), cancellationToken: cancellationToken).ConfigureAwait(false))
            {
                return ExportIterationResponse.FromString(await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
        }

        /// <summary>
        /// Get information about a specific domain
        /// </summary>
        /// <param name="domainId">The id of the domain to get information about</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<GetDomainResponse> GetDomainAsync(string domainId, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Get, mUriGenerator.Domains(domainId), cancellationToken: cancellationToken).ConfigureAwait(false))
            {
                return GetDomainResponse.FromString(await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
        }

        /// <summary>
        /// Get a list of the available domains
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<GetDomainsResponse> GetDomainsAsync(CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Get, mUriGenerator.Domains(), cancellationToken: cancellationToken).ConfigureAwait(false))
            {
                return GetDomainsResponse.FromString(await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
        }

        /// <summary>
        /// Get the list of exports for a specific iteration
        /// </summary>
        /// <param name="projectId">The project id</param>
        /// <param name="iterationId">The iteration id</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<GetExportsResponse> GetExportsAsync(string projectId, string iterationId, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Get, mUriGenerator.Export(projectId, iterationId), cancellationToken: cancellationToken).ConfigureAwait(false))
            {
                return GetExportsResponse.FromString(await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
        }

        /// <summary>
        /// Get images by id for a given project iteration
        /// </summary>
        /// <param name="projectId">The project id</param>
        /// <param name="imageIds">The list of image ids to retrieve. Limited to 256</param>
        /// <param name="iterationId">The iteration id. Defaults to workspace</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<GetImagesByIdsResponse> GetImagesByIdsAsync(string projectId, string[] imageIds = null, string iterationId = null, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Get, mUriGenerator.GetImagesById(projectId, imageIds, iterationId), cancellationToken: cancellationToken).ConfigureAwait(false))
            {
                return GetImagesByIdsResponse.FromString(await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
        }

        /// <summary>
        /// Get a specific iteration
        /// </summary>
        /// <param name="projectId">The id of the project the iteration belongs to</param>
        /// <param name="iterationId">The id of the iteration to get</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<GetIterationResponse> GetIterationAsync(string projectId, string iterationId, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Get, mUriGenerator.Iterations(projectId, iterationId), cancellationToken: cancellationToken).ConfigureAwait(false))
            {
                return GetIterationResponse.FromString(await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
        }

        /// <summary>
        /// Get detailed performance information about a trained iteration
        /// </summary>
        /// <param name="projectId">The id of the project the iteration belongs to</param>
        /// <param name="iterationId">The id of the iteration to get</param>
        /// <param name="threshold">The threshold used to determine true predictions</param>
        /// <param name="overlapThreshold">If applicable, the bounding box overlap threshold used to determine true predictions</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<GetIterationPerformanceResponse> GetIterationPerformanceAsync(string projectId, string iterationId, float? threshold = null, float? overlapThreshold = null, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Get, mUriGenerator.IterationPerformance(projectId, iterationId, threshold, overlapThreshold), cancellationToken: cancellationToken).ConfigureAwait(false))
            {
                return GetIterationPerformanceResponse.FromString(await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
        }

        /// <summary>
        /// Get iterations for the project
        /// </summary>
        /// <param name="projectId">The project id</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<GetIterationsResponse> GetIterationsAsync(string projectId, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Get, mUriGenerator.Iterations(projectId), cancellationToken: cancellationToken).ConfigureAwait(false))
            {
                return GetIterationsResponse.FromString(await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
        }

        /// <summary>
        /// Get a specific project
        /// </summary>
        /// <param name="projectId"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<GetProjectResponse> GetProjectAsync(string projectId, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Get, mUriGenerator.Projects(projectId), cancellationToken: cancellationToken).ConfigureAwait(false))
            {
                return GetProjectResponse.FromString(await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
        }

        /// <summary>
        /// Get your projects
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<GetProjectsResponse> GetProjectsAsync(CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Get, mUriGenerator.Projects(), cancellationToken: cancellationToken).ConfigureAwait(false))
            {
                return GetProjectsResponse.FromString(await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
        }

        /// <summary>
        /// Get information about a specific tag
        /// </summary>
        /// <param name="projectId">The project this tag belongs to</param>
        /// <param name="tagId">The tag id</param>
        /// <param name="iterationId">The iteration to retrieve this tag from. Optional, defaults to current training set</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<GetTagResponse> GetTagAsync(string projectId, string tagId, string iterationId = null, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Get, mUriGenerator.Tags(projectId, tagId, iterationId), cancellationToken: cancellationToken).ConfigureAwait(false))
            {
                return GetTagResponse.FromString(await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
        }

        /// <summary>
        /// Get the tags for a given project and iteration
        /// </summary>
        /// <param name="projectId">The project id</param>
        /// <param name="iterationId">The iteration id. Defaults to workspace</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<GetTagsResponse> GetTagsAsync(string projectId, string iterationId = null, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Get, mUriGenerator.Tags(projectId, null, iterationId), cancellationToken: cancellationToken).ConfigureAwait(false))
            {
                return GetTagsResponse.FromString(await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
        }

        /// <summary>
        /// The filtering is on an and/or relationship. For example, if the provided tag ids are for the "Dog" and "Cat" tags, then only images tagged with Dog and/or Cat will be returned
        /// </summary>
        /// <param name="projectId">The project id</param>
        /// <param name="iterationId">The iteration id. Defaults to workspace</param>
        /// <param name="tagIds">A list of tags ids to filter the images to count.Defaults to all tags when null.</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<int> GetTaggedImageCountAsync(string projectId, string iterationId = null, string[] tagIds = null, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Get, mUriGenerator.CountTaggedImages(projectId, iterationId, tagIds), cancellationToken: cancellationToken).ConfigureAwait(false))
            {
                return int.Parse(await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
        }

        /// <summary>
        /// This API supports batching and range selection. By default it will only return first 50 images matching images. Use the and parameters to control how many images to return in a given batch. The filtering is on an and/or relationship. For example, if the provided tag ids are for the "Dog" and "Cat" tags, then only images tagged with Dog and/or Cat will be returned
        /// </summary>
        /// <param name="projectId">The project id</param>
        /// <param name="iterationId">The iteration id. Defaults to workspace</param>
        /// <param name="tagIds">A list of tags ids to filter the images. Defaults to all tagged images when null. Limited to 20</param>
        /// <param name="orderBy">The ordering.Defaults to newest</param>
        /// <param name="take">Maximum number of images to return. Defaults to 50, limited to 256</param>
        /// <param name="skip">Number of images to skip before beginning the image batch. Defaults to 0</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<GetTaggedImagesResponse> GetTaggedImagesAsync(string projectId, string iterationId = null, string[] tagIds = null, string orderBy = null, int? take = null, int? skip = null, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Get, mUriGenerator.GetTaggedImages(projectId, iterationId, tagIds, orderBy, take, skip), cancellationToken: cancellationToken).ConfigureAwait(false))
            {
                return GetTaggedImagesResponse.FromString(await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
        }

        /// <summary>
        /// Gets the number of untagged images
        /// </summary>
        /// <param name="projectId">The project id</param>
        /// <param name="iterationId">The iteration id. Defaults to workspace</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<int> GetUntaggedImageCountAsync(string projectId, string iterationId = null, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Get, mUriGenerator.CountUntaggedImages(projectId, iterationId), cancellationToken: cancellationToken).ConfigureAwait(false))
            {
                return int.Parse(await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
        }

        /// <summary>
        /// This API supports batching and range selection. By default it will only return first 50 images matching images. Use the and parameters to control how many images to return in a given batch.
        /// </summary>
        /// <param name="projectId">The project id</param>
        /// <param name="iterationId">Defaults to workspace</param>
        /// <param name="orderBy">Defaults to newest</param>
        /// <param name="take">Maximum number of images to return. Defaults to 50, limited to 256</param>
        /// <param name="skip">Number of images to skip before beginning the image batch. Defaults to 0</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<GetUntaggedImagesResponse> GetUntaggedImagesAsync(string projectId, string iterationId = null, string orderBy = null, int? take = null, int? skip = null, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Get, mUriGenerator.GetUntaggedImages(projectId, iterationId, orderBy, take, skip), cancellationToken: cancellationToken).ConfigureAwait(false))
            {
                return GetUntaggedImagesResponse.FromString(await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
        }

        /// <summary>
        /// Predict an image and saves the result
        /// </summary>
        /// <param name="projectId">The project id</param>
        /// <param name="imageBytes"></param>
        /// <param name="iterationId">Specifies the id of a particular iteration to evaluate against. The default iteration for the project will be used when not specified</param>
        /// <param name="application">Specifies the name of application using the endpoint</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<PredictImageResponse> PredictImageAsync(string projectId, byte[] imageBytes, string iterationId = null, string application = null, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Post, mUriGenerator.PredictImage(projectId, iterationId, application), new ByteArrayContent(imageBytes), cancellationToken: cancellationToken).ConfigureAwait(false))
            {
                return PredictImageResponse.FromString(await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
        }

        /// <summary>
        /// Predict an image without saving the result
        /// </summary>
        /// <param name="projectId">The project id</param>
        /// <param name="imageBytes"></param>
        /// <param name="iterationId">Specifies the id of a particular iteration to evaluate against. The default iteration for the project will be used when not specified</param>
        /// <param name="application">Specifies the name of application using the endpoint</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<PredictImageResponse> PredictImageWithNoStoreAsync(string projectId, byte[] imageBytes, string iterationId = null, string application = null, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Post, mUriGenerator.PredictImage(projectId, iterationId, application, true), new ByteArrayContent(imageBytes), cancellationToken: cancellationToken).ConfigureAwait(false))
            {
                return PredictImageResponse.FromString(await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
        }

        /// <summary>
        /// Predict an image url and saves the result
        /// </summary>
        /// <param name="projectId">The project id</param>
        /// <param name="request">The request body</param>
        /// <param name="iterationId">Specifies the id of a particular iteration to evaluate against.The default iteration for the project will be used when not specified</param>
        /// <param name="application">Specifies the name of application using the endpoint</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<PredictImageResponse> PredictImageUrlAsync(string projectId, PredictImageUrlRequest request, string iterationId = null, string application = null, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Post, mUriGenerator.PredictImageUrl(projectId, iterationId, application), new JsonContent(request.ToString()), cancellationToken: cancellationToken).ConfigureAwait(false))
            {
                return PredictImageResponse.FromString(await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
        }

        /// <summary>
        /// Predict an image url without saving the result
        /// </summary>
        /// <param name="projectId">The project id</param>
        /// <param name="request">The request body</param>
        /// <param name="iterationId">Specifies the id of a particular iteration to evaluate against.The default iteration for the project will be used when not specified</param>
        /// <param name="application">Specifies the name of application using the endpoint</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<PredictImageResponse> PredictImageUrlWithNoStoreAsync(string projectId, PredictImageUrlRequest request, string iterationId = null, string application = null, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Post, mUriGenerator.PredictImageUrl(projectId, iterationId, application, true), new JsonContent(request.ToString()), cancellationToken: cancellationToken).ConfigureAwait(false))
            {
                return PredictImageResponse.FromString(await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
        }

        /// <summary>
        /// Quick test an image
        /// </summary>
        /// <param name="projectId">The project id</param>
        /// <param name="imageBytes"></param>
        /// <param name="iterationId">Specifies the id of a particular iteration to evaluate against. The default iteration for the project will be used when not specified.</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<QuickTestImageResponse> QuickTestImageAsync(string projectId, byte[] imageBytes, string iterationId = null, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Post, mUriGenerator.QuickTestImage(projectId, iterationId), new ByteArrayContent(imageBytes), cancellationToken: cancellationToken).ConfigureAwait(false))
            {
                return QuickTestImageResponse.FromString(await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
        }

        /// <summary>
        /// Quick test an image url
        /// </summary>
        /// <param name="projectId"></param>
        /// <param name="request"></param>
        /// <param name="iterationId"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<QuickTestImageResponse> QuickTestImageUrlAsync(string projectId, QuickTestImageUrlRequest request, string iterationId = null, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Post, mUriGenerator.QuickTestImageUrl(projectId, iterationId), new JsonContent(request.ToString()), cancellationToken: cancellationToken).ConfigureAwait(false))
            {
                return QuickTestImageResponse.FromString(await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
        }

        /// <summary>
        /// Queues project for training
        /// </summary>
        /// <param name="projectId"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<TrainProjectResponse> TrainProjectAsync(string projectId, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Post, mUriGenerator.TrainProject(projectId), cancellationToken: cancellationToken).ConfigureAwait(false))
            {
                return TrainProjectResponse.FromString(await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
        }
    }
}
