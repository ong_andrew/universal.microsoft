﻿using Newtonsoft.Json;
using System.Collections.Generic;
using Universal.Common.Serialization;

namespace Universal.Microsoft.CognitiveServices.CustomVision.V2
{
    public class CreateImageTagsResponse : JsonSerializable<CreateImageTagsResponse>
    {
        [JsonProperty("created")]
        public List<Tag> Created { get; set; }
        [JsonProperty("duplicated")]
        public List<Tag> Duplicated { get; set; }
        [JsonProperty("exceeded")]
        public List<Tag> Exceeded { get; set; }

        public CreateImageTagsResponse()
        {
            Created = new List<Tag>();
            Duplicated = new List<Tag>();
            Exceeded = new List<Tag>();
        }

        public class Tag
        {
            [JsonProperty("imageId")]
            public string ImageId { get; set; }
            [JsonProperty("tagId")]
            public string TagId { get; set; }
        }
    }
}
