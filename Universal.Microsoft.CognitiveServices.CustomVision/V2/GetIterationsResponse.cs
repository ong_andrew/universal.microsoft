﻿using Newtonsoft.Json;
using System.Collections.Generic;
using Universal.Common.Serialization;

namespace Universal.Microsoft.CognitiveServices.CustomVision.V2
{
    public class GetIterationsResponse : JsonCollectionSerializable<GetIterationsResponse, GetIterationsResponse.Iteration>
    {
        public GetIterationsResponse()
        {
            Collection = new List<Iteration>();
        }

        public class Iteration
        {
            [JsonProperty("id")]
            public string Id { get; set; }
            [JsonProperty("name")]
            public string Name { get; set; }
            [JsonProperty("isDefault")]
            public bool IsDefault { get; set; }
            [JsonProperty("status")]
            public string Status { get; set; }
            [JsonProperty("created")]
            public string Created { get; set; }
            [JsonProperty("lastModified")]
            public string LastModified { get; set; }
            [JsonProperty("trainedAt")]
            public string TrainedAt { get; set; }
            [JsonProperty("projectId")]
            public string ProjectId { get; set; }
            [JsonProperty("exportable")]
            public bool Exportable { get; set; }
            [JsonProperty("domainId")]
            public string DomainId { get; set; }
        }
    }
}