﻿using Newtonsoft.Json;
using System.Collections.Generic;
using Universal.Common.Serialization;

namespace Universal.Microsoft.CognitiveServices.CustomVision.V2
{
    public class GetIterationPerformanceResponse : JsonSerializable<GetIterationPerformanceResponse>
    {
        [JsonProperty("perTagPerformance")]
        public List<TagPerformance> PerTagPerformance { get; set; }
        [JsonProperty("precision")]
        public float Precision { get; set; }
        [JsonProperty("precisionStdDeviation")]
        public float PrecisionStdDeviation { get; set; }
        [JsonProperty("recall")]
        public float Recall { get; set; }
        [JsonProperty("recallStdDeviation")]
        public float RecallStdDeviation { get; set; }
        [JsonProperty("averagePrecision")]
        public float AveragePrecision { get; set; }

        public GetIterationPerformanceResponse()
        {
            PerTagPerformance = new List<TagPerformance>();
        }

        public class TagPerformance
        {
            [JsonProperty("id")]
            public string Id { get; set; }
            [JsonProperty("name")]
            public string Name { get; set; }
            [JsonProperty("precision")]
            public float Precision { get; set; }
            [JsonProperty("precisionStdDeviation")]
            public float PrecisionStdDeviation { get; set; }
            [JsonProperty("recall")]
            public float Recall { get; set; }
            [JsonProperty("recallStdDeviation")]
            public float RecallStdDeviation { get; set; }
            [JsonProperty("averagePrecision")]
            public float AveragePrecision { get; set; }
        }
    }
}
