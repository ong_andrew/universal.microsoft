﻿using Universal.Common;
using UriBuilder = Universal.Common.UriBuilder;

namespace Universal.Microsoft.CognitiveServices.CustomVision.V2
{
    public class UriGenerator
    {
        protected string mHost;

        public UriGenerator(string host)
        {
            mHost = host;
        }

        public UriBuilder Base()
        {
            return new UriBuilder("https", mHost);
        }

        public UriBuilder Base(string path)
        {
            UriBuilder uriBuilder = Base();
            uriBuilder.Path = path;
            return uriBuilder;
        }

        public UriBuilder Domains()
        {
            return Base("/customvision/v2.0/Training/domains");
        }
        
        public UriBuilder CreateImagesFromData(string projectId, string[] tagIds = null)
        {
            UriBuilder uriBuilder = Projects(projectId);
            uriBuilder.AddSegment("images");
            
            if (tagIds != null)
            {
                uriBuilder.AddQuery("tagIds", tagIds);
            }
            return uriBuilder;
        }

        public UriBuilder CreateImageTags(string projectId)
        {
            UriBuilder uriBuilder = Projects(projectId);
            uriBuilder.AddSegments("images", "tags");
            return uriBuilder;
        }

        public UriBuilder CreateProject(string name, string description = null, string domainId = null)
        {
            UriBuilder uriBuilder = Projects();
            uriBuilder.AddQuery("name", name);
            if (description != null)
            {
                uriBuilder.AddQuery("description", description);
            }

            if (domainId != null)
            {
                uriBuilder.AddQuery("domainId", domainId);
            }

            return uriBuilder;
        }

        public UriBuilder CreateTag(string projectId, string name, string description = null)
        {
            UriBuilder uriBuilder = Tags(projectId);
            uriBuilder.AddQuery("name", name);
            if (description != null)
            {
                uriBuilder.AddQuery("description", description);
            }

            return uriBuilder;
        }

        public UriBuilder Export(string projectId, string iterationId)
        {
            UriBuilder uriBuilder = Iterations(projectId, iterationId);
            uriBuilder.AddSegment("export");
            return uriBuilder;
        }

        public UriBuilder Export(string projectId, string iterationId, string platform, string flavor = null)
        {
            UriBuilder uriBuilder = Export(projectId, iterationId);
            uriBuilder.AddQuery("platform", platform);

            if (flavor != null)
            {
                uriBuilder.AddQuery("flavor", flavor);
            }

            return uriBuilder;
        }

        public UriBuilder Images(string projectId)
        {
            UriBuilder uriBuilder = Projects(projectId);
            uriBuilder.AddSegments("images");
            return uriBuilder;
        }

        public UriBuilder Images(string projectId, string[] imageIds)
        {
            UriBuilder uriBuilder = Images(projectId);

            if (imageIds != null)
            {
                uriBuilder.AddQuery("imageIds", imageIds);
            }

            return uriBuilder;
        }

        public UriBuilder GetImagesById(string projectId, string[] imageIds = null, string iterationId = null)
        {
            UriBuilder uriBuilder = Images(projectId);
            uriBuilder.AddSegment("id");

            if (imageIds != null)
            {
                uriBuilder.AddQuery("imageIds", imageIds);
            }

            if (iterationId != null)
            {
                uriBuilder.AddQuery("iterationId", iterationId);
            }

            return uriBuilder;
        }

        public UriBuilder Iterations(string projectId)
        {
            UriBuilder uriBuilder = Projects(projectId);
            uriBuilder.AddSegment("iterations");
            return uriBuilder;
        }

        public UriBuilder Iterations(string projectId, string iterationId)
        {
            UriBuilder uriBuilder = Iterations(projectId);
            uriBuilder.AddSegment(iterationId);
            return uriBuilder;
        }

        public UriBuilder IterationPerformance(string projectId, string iterationId, float? threshold = null, float? overlapThreshold = null)
        {
            UriBuilder uriBuilder = Iterations(projectId, iterationId);
            uriBuilder.AddSegment("performance");

            if (threshold != null)
            {
                uriBuilder.AddQuery("threshold", threshold.Value);
            }

            if (overlapThreshold != null)
            {
                uriBuilder.AddQuery("overlapThreshold", overlapThreshold.Value);
            }

            return uriBuilder;
        }

        public UriBuilder Domains(string domainId)
        {
            UriBuilder uriBuilder = Domains();
            uriBuilder.AddSegment(domainId);
            return uriBuilder;
        }

        public UriBuilder PredictImage(string projectId, string iterationId, string application, bool noStore = false)
        {
            UriBuilder uriBuilder = Base("/customvision/v2.0/Prediction");
            uriBuilder.AddSegment(projectId);
            uriBuilder.AddSegment("image");

            if (noStore)
            {
                uriBuilder.AddSegment("nostore");
            }

            if (!iterationId.IsNullOrEmpty())
            {
                uriBuilder.AddQuery("iterationId", iterationId);
            }

            if (!application.IsNullOrEmpty())
            {
                uriBuilder.AddQuery("application", application);
            }

            return uriBuilder;
        }

        public UriBuilder PredictImageUrl(string projectId, string iterationId, string application, bool noStore = false)
        {
            UriBuilder uriBuilder = Base("/customvision/v2.0/Prediction");
            uriBuilder.AddSegment(projectId);
            uriBuilder.AddSegment("url");

            if (noStore)
            {
                uriBuilder.AddSegment("nostore");
            }

            if (!iterationId.IsNullOrEmpty())
            {
                uriBuilder.AddQuery("iterationId", iterationId);
            }

            if (!application.IsNullOrEmpty())
            {
                uriBuilder.AddQuery("application", application);
            }

            return uriBuilder;
        }

        public UriBuilder Projects()
        {
            return Base("/customvision/v2.0/Training/projects");
        }

        public UriBuilder Projects(string projectId)
        {
            UriBuilder uriBuilder = Projects();
            uriBuilder.AddSegment(projectId);
            return uriBuilder;
        }

        public UriBuilder TrainProject(string projectId)
        {
            return Projects(projectId).AddSegment("train");
        }

        public UriBuilder QuickTestImage(string projectId, string iterationId = null)
        {
            UriBuilder uriBuilder = Projects(projectId);
            uriBuilder.AddSegments("quicktest", "image");

            if (iterationId != null)
            {
                uriBuilder.AddQuery("iterationId", iterationId);
            }

            return uriBuilder;
        }

        public UriBuilder QuickTestImageUrl(string projectId, string iterationId = null)
        {
            UriBuilder uriBuilder = Projects(projectId);
            uriBuilder.AddSegments("quicktest", "url");

            if (iterationId != null)
            {
                uriBuilder.AddQuery("iterationId", iterationId);
            }

            return uriBuilder;
        }

        public UriBuilder Tags(string projectId)
        {
            UriBuilder uriBuilder = Projects(projectId);
            uriBuilder.AddSegment("tags");
            return uriBuilder;
        }

        public UriBuilder Tags(string projectId, string tagId, string iterationId = null)
        {
            UriBuilder uriBuilder = Tags(projectId);
            uriBuilder.AddSegment(tagId);

            if (iterationId != null)
            {
                uriBuilder.AddQuery("iterationId", iterationId);
            }

            return uriBuilder;
        }

        public UriBuilder TaggedImages(string projectId)
        {
            UriBuilder uriBuilder = Projects(projectId);
            uriBuilder.AddSegments("images", "untagged");

            return uriBuilder;
        }

        public UriBuilder GetTaggedImages(string projectId, string iterationId = null, string[] tagIds = null, string orderBy = null, int? take = null, int? skip = null)
        {
            UriBuilder uriBuilder = TaggedImages(projectId);

            if (iterationId != null)
            {
                uriBuilder.AddQuery("iterationId", iterationId);
            }

            if (tagIds != null)
            {
                uriBuilder.AddQuery("tagIds", tagIds);
            }

            if (orderBy != null)
            {
                uriBuilder.AddQuery("orderBy", orderBy);
            }

            if (take != null)
            {
                uriBuilder.AddQuery("take", take.Value);
            }

            if (skip != null)
            {
                uriBuilder.AddQuery("skip", skip.Value);
            }

            return uriBuilder;
        }

        public UriBuilder CountTaggedImages(string projectId, string iterationId = null, string[] tagIds = null)
        {
            UriBuilder uriBuilder = TaggedImages(projectId);
            uriBuilder.AddSegment("count");

            if (iterationId != null)
            {
                uriBuilder.AddQuery("iterationId", iterationId);
            }

            if (tagIds != null)
            {
                uriBuilder.AddQuery("tagIds", tagIds);
            }

            return uriBuilder;
        }

        public UriBuilder UntaggedImages(string projectId)
        {
            UriBuilder uriBuilder = Projects(projectId);
            uriBuilder.AddSegments("images", "tagged");

            return uriBuilder;
        }

        

        public UriBuilder GetUntaggedImages(string projectId, string iterationId = null, string orderBy = null, int? take = null, int? skip = null)
        {
            UriBuilder uriBuilder = UntaggedImages(projectId);

            if (iterationId != null)
            {
                uriBuilder.AddQuery("iterationId", iterationId);
            }

            if (orderBy != null)
            {
                uriBuilder.AddQuery("orderBy", orderBy);
            }

            if (take != null)
            {
                uriBuilder.AddQuery("take", take.Value);
            }

            if (skip != null)
            {
                uriBuilder.AddQuery("skip", skip.Value);
            }

            return uriBuilder;
        }

        public UriBuilder CountUntaggedImages(string projectId, string iterationId = null)
        {
            UriBuilder uriBuilder = UntaggedImages(projectId);
            uriBuilder.AddSegment("count");

            if (iterationId != null)
            {
                uriBuilder.AddQuery("iterationId", iterationId);
            }

            return uriBuilder;
        }
    }
}
