﻿using Newtonsoft.Json;
using System.Collections.Generic;
using Universal.Common.Serialization;

namespace Universal.Microsoft.CognitiveServices.CustomVision.V2
{
    public class CreateImageTagsRequest : JsonSerializable<CreateImageTagsRequest>
    {
        [JsonProperty("tags")]
        public List<Tag> Tags { get; set; }

        public CreateImageTagsRequest()
        {
            Tags = new List<Tag>();
        }

        public class Tag
        {
            [JsonProperty("imageId")]
            public string ImageId { get; set; }
            [JsonProperty("tagId")]
            public string TagId { get; set; }
        }
    }
}