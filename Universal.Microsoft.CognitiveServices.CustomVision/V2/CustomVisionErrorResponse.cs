﻿using Newtonsoft.Json;
using Universal.Common.Serialization;

namespace Universal.Microsoft.CognitiveServices.CustomVision.V2
{
    /// <summary>
    /// Represents an error returned from the Custom Vision service.
    /// </summary>
    public class CustomVisionErrorResponse : JsonSerializable<CustomVisionErrorResponse>
    {
        [JsonProperty("code")]
        public string Code { get; set; }
        [JsonProperty("message")]
        public string Message { get; set; }
    }
}