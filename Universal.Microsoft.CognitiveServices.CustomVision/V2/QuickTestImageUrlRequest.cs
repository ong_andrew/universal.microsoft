﻿using Newtonsoft.Json;
using Universal.Common.Serialization;

namespace Universal.Microsoft.CognitiveServices.CustomVision.V2
{
    public class QuickTestImageUrlRequest : JsonSerializable<QuickTestImageUrlRequest>
    {
        [JsonProperty("url")]
        public string Url { get; set; }
    }
}
