﻿using Newtonsoft.Json;
using System.Collections.Generic;
using Universal.Common.Serialization;

namespace Universal.Microsoft.CognitiveServices.CustomVision.V2
{
    public class CreateImagesFromDataResponse : JsonSerializable<CreateImagesFromDataResponse>
    {
        [JsonProperty("isBatchSuccessful")]
        public bool IsBatchSuccessful { get; set; }
        [JsonProperty("images")]
        public List<Image> Images { get; set; }

        public CreateImagesFromDataResponse()
        {
            Images = new List<Image>();
        }

        public class Image
        {
            [JsonProperty("sourceUrl")]
            public string SourceUrl { get; set; }
            [JsonProperty("status")]
            public string Status { get; set; }
            [JsonProperty("image")]
            public ImageData ImageData { get; set; }

            public Image()
            {
                ImageData = new ImageData();
            }
        }

        public class ImageData
        {
            [JsonProperty("id")]
            public string Id { get; set; }
            [JsonProperty("created")]
            public string Created { get; set; }
            [JsonProperty("width")]
            public int Width { get; set; }
            [JsonProperty("height")]
            public int Height { get; set; }
            [JsonProperty("imageUri")]
            public string ImageUri { get; set; }
            [JsonProperty("thumbnailUri")]
            public string ThumbnailUri { get; set; }
            [JsonProperty("tags")]
            public List<Tag> Tags { get; set; }
            [JsonProperty("regions")]
            public List<Region> Regions { get; set; }

            public ImageData()
            {
                Tags = new List<Tag>();
                Regions = new List<Region>();
            }
        }

        public class Tag
        {
            [JsonProperty("tagId")]
            public string TagId { get; set; }
            [JsonProperty("tagName")]
            public string TagName { get; set; }
            [JsonProperty("created")]
            public string Created { get; set; }
        }

        public class Region
        {
            [JsonProperty("regionId")]
            public string RegionId { get; set; }
            [JsonProperty("tagName")]
            public string TagName { get; set; }
            [JsonProperty("created")]
            public string Created { get; set; }
            [JsonProperty("tagId")]
            public string TagId { get; set; }
            [JsonProperty("left")]
            public float Left { get; set; }
            [JsonProperty("top")]
            public float Top { get; set; }
            [JsonProperty("width")]
            public float Width { get; set; }
            [JsonProperty("height")]
            public float Height { get; set; }
        }
    }
}
