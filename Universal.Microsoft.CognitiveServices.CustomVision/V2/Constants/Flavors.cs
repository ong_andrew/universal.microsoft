﻿namespace Universal.Microsoft.CognitiveServices.CustomVision.V2
{
    public static class Flavors
    {
        public const string Linux = "Linux";
        public const string Windows = "Windows";
    }
}