﻿namespace Universal.Microsoft.CognitiveServices.CustomVision.V2
{
    public static class Platforms
    {
        public const string CoreML = "CoreML";
        public const string TensorFlow = "TensorFlow";
        public const string DockerFile = "DockerFile";
        public const string Onnx = "ONNX";
    }
}