﻿namespace Universal.Microsoft.CognitiveServices.CustomVision.V2
{
    public static class ExportStatuses
    {
        public const string Exporting = "Exporting";
        public const string Failed = "Failed";
        public const string Done = "Done";
    }
}
