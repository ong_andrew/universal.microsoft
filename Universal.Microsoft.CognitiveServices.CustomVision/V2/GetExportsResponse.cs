﻿using Newtonsoft.Json;
using System.Collections.Generic;
using Universal.Common.Serialization;

namespace Universal.Microsoft.CognitiveServices.CustomVision.V2
{
    public class GetExportsResponse : JsonCollectionSerializable<GetExportsResponse, GetExportsResponse.Export>
    {
        public GetExportsResponse()
        {
            Collection = new List<Export>();
        }

        public class Export
        {
            [JsonProperty("platform")]
            public string Platform { get; set; }
            [JsonProperty("status")]
            public string Status { get; set; }
            [JsonProperty("downloadUri")]
            public string DownloadUri { get; set; }
            [JsonProperty("flavor")]
            public string Flavor { get; set; }
        }
    }
}
