﻿using Newtonsoft.Json;
using System.Collections.Generic;
using Universal.Common.Serialization;

namespace Universal.Microsoft.CognitiveServices.CustomVision.V2
{
    public class GetTagsResponse : JsonCollectionSerializable<GetTagsResponse, GetTagsResponse.Tag>
    {
        public GetTagsResponse()
        {
            Collection = new List<Tag>();
        }

        public class Tag
        {
            [JsonProperty("id")]
            public string Id { get; set; }
            [JsonProperty("name")]
            public string Name { get; set; }
            [JsonProperty("description")]
            public string Description { get; set; }
            [JsonProperty("imageCount")]
            public int ImageCount { get; set; }
        }
    }
}
