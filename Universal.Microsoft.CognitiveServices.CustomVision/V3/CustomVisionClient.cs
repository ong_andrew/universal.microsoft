﻿using Newtonsoft.Json;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Universal.Common;
using Universal.Common.Net.Http;

namespace Universal.Microsoft.CognitiveServices.CustomVision.V3
{
    /// <summary>
    /// Client for interacting with the Microsoft Custom Vision V2 service.
    /// </summary>
    public class CustomVisionClient : HttpServiceClient
    {
        private UriGenerator mUriGenerator;
        protected string mTrainingKey;
        protected string mPredictionKey;

        /// <summary>
        /// Initializes a new instance of the <see cref="CustomVisionClient"/> that can only be used for the training endpoints.
        /// </summary>
        /// <param name="trainingKey"></param>
        /// <param name="host"></param>
        /// <returns></returns>
        public static CustomVisionClient CreateTrainingClient(string trainingKey, string host = Hosts.SouthCentralUS)
        {
            return new CustomVisionClient(trainingKey, null, host);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CustomVisionClient"/> that can only be used for the prediction endpoints.
        /// </summary>
        /// <param name="predictionKey"></param>
        /// <param name="host"></param>
        /// <returns></returns>
        public static CustomVisionClient CreatePredictionClient(string predictionKey, string host = Hosts.SouthCentralUS)
        {
            return new CustomVisionClient(null, predictionKey, host);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CustomVisionClient"/> with the given training and prediction keys. If not provided, they are not sent along with the requests.
        /// </summary>
        /// <param name="trainingKey">Subscription key which provides access to this API.</param>
        /// <param name="predictionKey">Subscription key which provides access to this API.</param>
        /// <param name="host">The base URL for the service, defaults to "southcentralus.api.cognitive.microsoft.com".</param>
        public CustomVisionClient(string trainingKey, string predictionKey, string host = Hosts.SouthCentralUS)
        {
            mTrainingKey = trainingKey;
            mPredictionKey = predictionKey;
            mUriGenerator = new UriGenerator(host);
        }

        protected override HttpClient CreateHttpClient()
        {
            HttpClient httpClient = base.CreateHttpClient();
            if (mTrainingKey != null)
            {
                httpClient.DefaultRequestHeaders.Add("Training-Key", mTrainingKey);
            }

            if (mPredictionKey != null)
            {
                httpClient.DefaultRequestHeaders.Add("Prediction-key", mPredictionKey);
            }
            return httpClient;
        }

        protected override Task HandleNonSuccessCodeAsync(HttpResponseMessage httpResponseMessage, CancellationToken cancellationToken)
        {
            try
            {
                throw new CustomVisionException(httpResponseMessage);
            }
            catch (JsonSerializationException)
            {
                throw new HttpException(httpResponseMessage);
            }
        }

        /// <summary>
        /// Classify an image and saves the result.
        /// </summary>
        /// <param name="projectId">Format - uuid. The project id.</param>
        /// <param name="publishedName">Specifies the name of the model to evaluate against.</param>
        /// <param name="imageBytes"></param>
        /// <param name="application">Optional. Specifies the name of application using the endpoint.</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ClassifyImageResponse> ClassifyImageAsync(string projectId, string publishedName, byte[] imageBytes, string application = null, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Post, mUriGenerator.ClassifyImage(projectId, publishedName, application), new ByteArrayContent(imageBytes), cancellationToken: cancellationToken).ConfigureAwait(false))
            {
                return ClassifyImageResponse.FromString(await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
        }

        /// <summary>
        /// Classify an image without saving the result.
        /// </summary>
        /// <param name="projectId">Format - uuid. The project id.</param>
        /// <param name="publishedName">Specifies the name of the model to evaluate against.</param>
        /// <param name="imageBytes"></param>
        /// <param name="application">Optional.Specifies the name of application using the endpoint.</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ClassifyImageResponse> ClassifyImageWithNoStoreAsync(string projectId, string publishedName, byte[] imageBytes, string application = null, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Post, mUriGenerator.ClassifyImageWithNoStore(projectId, publishedName, application), new ByteArrayContent(imageBytes), cancellationToken: cancellationToken).ConfigureAwait(false))
            {
                return ClassifyImageResponse.FromString(await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
        }

        /// <summary>
        /// Classify an image url and saves the result.
        /// </summary>
        /// <param name="projectId">Format - uuid. The project id.</param>
        /// <param name="publishedName">Specifies the name of the model to evaluate against.</param>
        /// <param name="request"></param>
        /// <param name="application">Optional. Specifies the name of application using the endpoint.</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ClassifyImageResponse> ClassifyImageUrlAsync(string projectId, string publishedName, ClassifyImageUrlRequest request, string application = null, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Post, mUriGenerator.ClassifyImageUrl(projectId, publishedName, application), new JsonContent(request.ToString()), cancellationToken: cancellationToken).ConfigureAwait(false))
            {
                return ClassifyImageResponse.FromString(await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
        }

        /// <summary>
        /// Classify an image url without saving the result.
        /// </summary>
        /// <param name="projectId">Format - uuid. The project id.</param>
        /// <param name="publishedName">Specifies the name of the model to evaluate against.</param>
        /// <param name="request"></param>
        /// <param name="application">Optional. Specifies the name of application using the endpoint.</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ClassifyImageResponse> ClassifyImageUrlWithNoStoreAsync(string projectId, string publishedName, ClassifyImageUrlRequest request, string application = null, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Post, mUriGenerator.ClassifyImageUrlWithNoStore(projectId, publishedName, application), new JsonContent(request.ToString()), cancellationToken: cancellationToken).ConfigureAwait(false))
            {
                return ClassifyImageResponse.FromString(await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
        }

        /// <summary>
        /// Detect objects in an image and saves the result.
        /// </summary>
        /// <param name="projectId">Format - uuid. The project id.</param>
        /// <param name="publishedName">Specifies the name of the model to evaluate against.</param>
        /// <param name="imageBytes"></param>
        /// <param name="application">Optional. Specifies the name of application using the endpoint.</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<DetectImageResponse> DetectImageAsync(string projectId, string publishedName, byte[] imageBytes, string application = null, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Post, mUriGenerator.DetectImage(projectId, publishedName, application), new ByteArrayContent(imageBytes), cancellationToken: cancellationToken).ConfigureAwait(false))
            {
                return DetectImageResponse.FromString(await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
        }

        /// <summary>
        /// Detect objects in an image without saving the result.
        /// </summary>
        /// <param name="projectId"></param>
        /// <param name="publishedName"></param>
        /// <param name="imageBytes"></param>
        /// <param name="application"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<DetectImageResponse> DetectImageWithNoStoreAsync(string projectId, string publishedName, byte[] imageBytes, string application = null, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Post, mUriGenerator.DetectImageWithNoStore(projectId, publishedName, application), new ByteArrayContent(imageBytes), cancellationToken: cancellationToken).ConfigureAwait(false))
            {
                return DetectImageResponse.FromString(await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
        }

        /// <summary>
        /// Detect objects in an image url and saves the result.
        /// </summary>
        /// <param name="projectId">Format - uuid. The project id.</param>
        /// <param name="publishedName">Specifies the name of the model to evaluate against.</param>
        /// <param name="request"></param>
        /// <param name="application">Optional.Specifies the name of application using the endpoint.</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<DetectImageResponse> DetectImageUrlAsync(string projectId, string publishedName, DetectImageUrlRequest request, string application = null, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Post, mUriGenerator.DetectImageUrl(projectId, publishedName, application), new JsonContent(request.ToString()), cancellationToken: cancellationToken).ConfigureAwait(false))
            {
                return DetectImageResponse.FromString(await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
        }

        /// <summary>
        /// Detect objects in an image url without saving the result.
        /// </summary>
        /// <param name="projectId">Format - uuid. The project id.</param>
        /// <param name="publishedName">Specifies the name of the model to evaluate against.</param>
        /// <param name="request"></param>
        /// <param name="application">Optional.Specifies the name of application using the endpoint.</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<DetectImageResponse> DetectImageUrlWithNoStoreAsync(string projectId, string publishedName, DetectImageUrlRequest request, string application = null, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Post, mUriGenerator.DetectImageUrlWithNoStore(projectId, publishedName, application), new JsonContent(request.ToString()), cancellationToken: cancellationToken).ConfigureAwait(false))
            {
                return DetectImageResponse.FromString(await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
        }

        /// <summary>
        /// Get a specific iteration.
        /// </summary>
        /// <param name="projectId">Format - uuid. The id of the project the iteration belongs to.</param>
        /// <param name="iterationId">Format - uuid. The id of the iteration to get.</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<GetIterationResponse> GetIterationAsync(string projectId, string iterationId, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Get, mUriGenerator.Iterations(projectId, iterationId), cancellationToken: cancellationToken).ConfigureAwait(false))
            {
                return GetIterationResponse.FromString(await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
        }

        /// <summary>
        /// Get iterations for the project.
        /// </summary>
        /// <param name="projectId">Format - uuid. The project id.</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<GetIterationsResponse> GetIterationsAsync(string projectId, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Get, mUriGenerator.Iterations(projectId), cancellationToken: cancellationToken).ConfigureAwait(false))
            {
                return GetIterationsResponse.FromString(await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
        }

        private class UriGenerator
        {
            protected string mHost;

            public UriGenerator(string host)
            {
                mHost = host;
            }

            public UriBuilder Base()
            {
                return new UriBuilder("https", mHost)
                    .AddSegments("customvision", "v3.0");
            }

            public UriBuilder Prediction()
            {
                return Base().AddSegment("Prediction");
            }

            public UriBuilder Training()
            {
                return Base().AddSegment("training");
            }

            public UriBuilder Classify(string projectId)
            {
                UriBuilder uriBuilder = Prediction()
                    .AddSegments(
                        projectId,
                        "classify"
                    );

                return uriBuilder;
            }

            public UriBuilder ClassifyIteration(string projectId, string publishedName)
            {
                return Classify(projectId).AddSegments(
                    "iterations",
                    publishedName);
            }

            public UriBuilder ClassifyImage(string projectId, string publishedName, string application)
            {
                UriBuilder uriBuilder = ClassifyIteration(projectId, publishedName).AddSegment("image");
                if (!application.IsNullOrEmpty())
                {
                    uriBuilder.AddQuery("application", application);
                }
                return uriBuilder;
            }

            public UriBuilder ClassifyImageWithNoStore(string projectId, string publishedName, string application)
            {
                return ClassifyImage(projectId, publishedName, application).AddSegment("nostore");
            }

            public UriBuilder ClassifyImageUrl(string projectId, string publishedName, string application)
            {
                UriBuilder uriBuilder = ClassifyIteration(projectId, publishedName).AddSegment("url");
                if (!application.IsNullOrEmpty())
                {
                    uriBuilder.AddQuery("application", application);
                }
                return uriBuilder;
            }

            public UriBuilder ClassifyImageUrlWithNoStore(string projectId, string publishedName, string application)
            {
                return ClassifyImageUrl(projectId, publishedName, application).AddSegment("nostore");
            }

            public UriBuilder Detect(string projectId)
            {
                UriBuilder uriBuilder = Prediction()
                    .AddSegments(
                        projectId,
                        "detect"
                    );

                return uriBuilder;
            }

            public UriBuilder DetectIteration(string projectId, string publishedName)
            {
                return Detect(projectId).AddSegments(
                    "iterations",
                    publishedName);
            }

            public UriBuilder DetectImage(string projectId, string publishedName, string application)
            {
                UriBuilder uriBuilder = DetectIteration(projectId, publishedName).AddSegment("image");
                if (!application.IsNullOrEmpty())
                {
                    uriBuilder.AddQuery("application", application);
                }
                return uriBuilder;
            }

            public UriBuilder DetectImageWithNoStore(string projectId, string publishedName, string application)
            {
                return DetectImage(projectId, publishedName, application).AddSegment("nostore");
            }

            public UriBuilder DetectImageUrl(string projectId, string publishedName, string application)
            {
                UriBuilder uriBuilder = DetectIteration(projectId, publishedName).AddSegment("url");
                if (!application.IsNullOrEmpty())
                {
                    uriBuilder.AddQuery("application", application);
                }
                return uriBuilder;
            }

            public UriBuilder DetectImageUrlWithNoStore(string projectId, string publishedName, string application)
            {
                return DetectImageUrl(projectId, publishedName, application).AddSegment("nostore");
            }

            public UriBuilder Iterations(string projectId)
            {
                return Projects(projectId).AddSegment("iterations");
            }

            public UriBuilder Iterations(string projectId, string iterationId)
            {
                return Iterations(projectId).AddSegment(iterationId);
            }

            public UriBuilder Projects()
            {
                return Training().AddSegment("projects");
            }

            public UriBuilder Projects(string projectId)
            {
                return Projects().AddSegment(projectId);
            }
        }
    }
}
