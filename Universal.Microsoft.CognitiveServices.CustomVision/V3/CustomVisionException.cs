﻿using System;
using System.Net.Http;
using Universal.Common.Net.Http;

namespace Universal.Microsoft.CognitiveServices.CustomVision.V3
{
    public class CustomVisionException : HttpException<CustomVisionErrorResponse>
    {
        public CustomVisionException()
        {
        }

        public CustomVisionException(string message) : base(message)
        {
        }

        public CustomVisionException(string message, Exception innerException) : base(message, innerException)
        {
        }

        public CustomVisionException(HttpResponseMessage httpResponseMessage) : base(
            (int)httpResponseMessage.StatusCode,
            httpResponseMessage.ReasonPhrase,
            httpResponseMessage.Content != null ? CustomVisionErrorResponse.FromString(httpResponseMessage.Content.ReadAsStringAsync().GetAwaiter().GetResult()) : null)
        {
        }
    }
}