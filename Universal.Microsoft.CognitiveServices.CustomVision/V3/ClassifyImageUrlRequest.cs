﻿using Newtonsoft.Json;
using Universal.Common.Serialization;

namespace Universal.Microsoft.CognitiveServices.CustomVision.V3
{
    public class ClassifyImageUrlRequest : JsonSerializable<ClassifyImageUrlRequest>
    {
        [JsonProperty("url")]
        public string Url { get; set; }

        public ClassifyImageUrlRequest() { }
        public ClassifyImageUrlRequest(string url) : this()
        {
            Url = url;
        }

        public static implicit operator ClassifyImageUrlRequest(string url)
        {
            return new ClassifyImageUrlRequest(url);
        }
    }
}
