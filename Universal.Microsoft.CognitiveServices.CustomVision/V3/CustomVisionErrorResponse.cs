﻿using Newtonsoft.Json;
using Universal.Common.Serialization;

namespace Universal.Microsoft.CognitiveServices.CustomVision.V3
{
    public class CustomVisionErrorResponse : JsonSerializable<CustomVisionErrorResponse>
    {
        [JsonProperty("code")]
        public string Code { get; set; }
        [JsonProperty("message")]
        public string Message { get; set; }
    }
}
