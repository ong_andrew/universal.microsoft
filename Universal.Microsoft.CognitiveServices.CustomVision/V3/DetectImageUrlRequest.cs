﻿using Newtonsoft.Json;
using Universal.Common.Serialization;

namespace Universal.Microsoft.CognitiveServices.CustomVision.V3
{
    public class DetectImageUrlRequest : JsonSerializable<DetectImageUrlRequest>
    {
        [JsonProperty("url")]
        public string Url { get; set; }

        public DetectImageUrlRequest() { }
        public DetectImageUrlRequest(string url) : this()
        {
            Url = url;
        }

        public static implicit operator DetectImageUrlRequest(string url)
        {
            return new DetectImageUrlRequest(url);
        }
    }
}
