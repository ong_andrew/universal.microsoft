﻿using Newtonsoft.Json;
using System.Collections.Generic;
using Universal.Common.Serialization;

namespace Universal.Microsoft.CognitiveServices.CustomVision.V3
{
    public class DetectImageResponse : JsonSerializable<DetectImageResponse>
    {
        [JsonProperty("id")]
        public string Id { get; set; }
        [JsonProperty("project")]
        public string Project { get; set; }
        [JsonProperty("iteration")]
        public string Iteration { get; set; }
        [JsonProperty("created")]
        public string Created { get; set; }
        [JsonProperty("predictions")]
        public List<Prediction> Predictions { get; set; }

        public class Prediction
        {
            [JsonProperty("probability")]
            public float Probability { get; set; }
            [JsonProperty("tagId")]
            public string TagId { get; set; }
            [JsonProperty("tagName")]
            public string TagName { get; set; }
            [JsonProperty("boundingBox")]
            public BoundingBox BoundingBox { get; set; }

            public Prediction()
            {
                BoundingBox = new BoundingBox();
            }
        }

        public class BoundingBox
        {
            [JsonProperty("left")]
            public float Left { get; set; }
            [JsonProperty("top")]
            public float Top { get; set; }
            [JsonProperty("width")]
            public float Width { get; set; }
            [JsonProperty("height")]
            public float Height { get; set; }
        }
    }
}
