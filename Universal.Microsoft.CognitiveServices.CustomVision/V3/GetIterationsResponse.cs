﻿using Newtonsoft.Json;
using System.Collections.Generic;
using Universal.Common.Serialization;

namespace Universal.Microsoft.CognitiveServices.CustomVision.V3
{
    public class GetIterationsResponse : JsonCollectionSerializable<GetIterationsResponse, GetIterationsResponse.Iteration>
    {
        public GetIterationsResponse()
        {
            Collection = new List<Iteration>();
        }

        public class Iteration
        {
            [JsonProperty("id")]
            public string Id { get; set; }
            [JsonProperty("name")]
            public string Name { get; set; }
            [JsonProperty("status")]
            public string Status { get; set; }
            [JsonProperty("created")]
            public string Created { get; set; }
            [JsonProperty("lastModified")]
            public string LastModified { get; set; }
            [JsonProperty("trainedAt")]
            public string TrainedAt { get; set; }
            [JsonProperty("projectId")]
            public string ProjectId { get; set; }
            [JsonProperty("exportable")]
            public bool Exportable { get; set; }
            [JsonProperty("domainId")]
            public string DomainId { get; set; }
            [JsonProperty("classificationType")]
            public string ClassificationType { get; set; }
            [JsonProperty("trainingType")]
            public string TrainingType { get; set; }
            [JsonProperty("reservedBudgetInHours")]
            public int ReservedBudgetInHours { get; set; }
            [JsonProperty("publishName")]
            public string PublishName { get; set; }
            [JsonProperty("originalPublishResourceId")]
            public string OriginalPublishResourceId { get; set; }
            [JsonProperty("exportableTo")]
            public List<string> ExportableTo { get; set; }

            public Iteration()
            {
                ExportableTo = new List<string>();
            }
        }
    }
}