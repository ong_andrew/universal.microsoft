﻿using Newtonsoft.Json;
using System.Collections.Generic;
using Universal.Common.Serialization;

namespace Universal.Microsoft.CognitiveServices.Face.V1
{
    public class GroupResponse : JsonSerializable<GroupResponse>
    {
        [JsonProperty("groups")]
        public List<List<string>> Groups { get; set; }
        [JsonProperty("messyGroup")]
        public List<string> MessyGroup { get; set; }

        public GroupResponse()
        {
            Groups = new List<List<string>>();
            MessyGroup = new List<string>();
        }
    }
}
