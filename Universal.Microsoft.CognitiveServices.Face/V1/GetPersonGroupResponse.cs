﻿using Newtonsoft.Json;
using Universal.Common.Serialization;

namespace Universal.Microsoft.CognitiveServices.Face.V1
{
    public class GetPersonGroupResponse : JsonSerializable<GetPersonGroupResponse>
    {
        [JsonProperty("personGroupId")]
        public string PersonGroupId { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("userData")]
        public string UserData { get; set; }
    }
}