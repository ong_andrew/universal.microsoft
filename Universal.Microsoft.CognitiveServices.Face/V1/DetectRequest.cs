﻿using Newtonsoft.Json;
using Universal.Common.Serialization;

namespace Universal.Microsoft.CognitiveServices.Face.V1
{
    public class DetectRequest : JsonSerializable
    {
        [JsonProperty("url")]
        public string Url { get; set; }
    }
}