﻿using Newtonsoft.Json;
using Universal.Common.Serialization;

namespace Universal.Microsoft.CognitiveServices.Face.V1
{
    public class CreatePersonGroupPersonResponse : JsonSerializable<CreatePersonGroupPersonResponse>
    {
        [JsonProperty("personId")]
        public string PersonId { get; set; }
    }
}
