﻿namespace Universal.Microsoft.CognitiveServices.Face.V1
{
    /// <summary>
    /// Class representing a bounding box for a face.
    /// </summary>
    public class TargetFace
    {
        /// <summary>
        /// The horizontal coordinate of the upper-left pixel.
        /// </summary>
        public int Left { get; set; }
        /// <summary>
        /// The vertical coordinate of the upper-left pixel.
        /// </summary>
        public int Top { get; set; }
        /// <summary>
        /// The width of the bounding box.
        /// </summary>
        public int Width { get; set; }
        /// <summary>
        /// The height of the bounding box.
        /// </summary>
        public int Height { get; set; }

        public TargetFace(int left, int top, int width, int height)
        {
            Left = left;
            Top = top;
            Width = width;
            Height = height;
        }

        public override string ToString()
        {
            return $"{Left},{Top},{Width},{Height}";
        }

        public static implicit operator TargetFace(DetectResponse.FaceRectangle faceRectangle)
        {
            return new TargetFace(faceRectangle.Left, faceRectangle.Top, faceRectangle.Width, faceRectangle.Height);
        }
    }
}