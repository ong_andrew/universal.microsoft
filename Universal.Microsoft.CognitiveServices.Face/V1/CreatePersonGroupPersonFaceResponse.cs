﻿using Newtonsoft.Json;
using Universal.Common.Serialization;

namespace Universal.Microsoft.CognitiveServices.Face.V1
{
    public class CreatePersonGroupPersonFaceResponse : JsonSerializable<CreatePersonGroupPersonFaceResponse>
    {
        [JsonProperty("persistedFaceId")]
        public string PersistedFaceId { get; set; }
    }
}
