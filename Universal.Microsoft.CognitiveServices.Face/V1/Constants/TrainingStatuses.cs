﻿namespace Universal.Microsoft.CognitiveServices.Face.V1
{
    /// <summary>
    /// Represents possible training statuses.
    /// </summary>
    public static class TrainingStatuses
    {
        public const string NotStarted = "notstarted";
        public const string Running = "running";
        public const string Succeeded = "succeeded";
        public const string Failed = "failed";
    }
}
