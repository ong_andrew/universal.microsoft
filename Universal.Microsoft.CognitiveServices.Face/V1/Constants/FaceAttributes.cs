﻿namespace Universal.Microsoft.CognitiveServices.Face.V1
{
    /// <summary>
    /// Represents facial attributes that can be returned with the detect endpoint.
    /// </summary>
    public static class FaceAttributes
    {
        public const string Age = "age";
        public const string Gender = "gender";
        public const string HeadPose = "headPose";
        public const string Smile = "smile";
        public const string FacialHair = "facialHair";
        public const string Glasses = "glasses";
        public const string Emotion = "emotion";
        public const string Hair = "hair";
        public const string Makeup = "makeup";
        public const string Occlusion = "occlusion";
        public const string Accessories = "accessories";
        public const string Blur = "blur";
        public const string Exposure = "exposure";
        public const string Noise = "noise";
    }
}
