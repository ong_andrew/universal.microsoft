﻿using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;
using Universal.Common;
using Universal.Common.Net.Http;

using UriBuilder = Universal.Common.UriBuilder;

namespace Universal.Microsoft.CognitiveServices.Face.V1
{
    /// <summary>
    /// Class for interacting with the Microsoft Cognitive Services Face API.
    /// </summary>
    public class FaceClient : HttpServiceClient
    {
        private UriGenerator mUriGenerator;
        private string mSubscriptionKey;

        protected override HttpClient CreateHttpClient()
        {
            HttpClient httpClient = base.CreateHttpClient();
            httpClient.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", mSubscriptionKey);
            return httpClient;
        }

        /// <summary>
        /// Create a new instance of the <see cref="FaceClient"/> class with the given subscription key.
        /// </summary>
        /// <param name="subscriptionKey"></param>
        /// <param name="host"></param>
        public FaceClient(string subscriptionKey, string host = Hosts.WestUS)
        {
            mUriGenerator = new UriGenerator(host);
            mSubscriptionKey = subscriptionKey;
        }

        protected override Task HandleNonSuccessCodeAsync(HttpResponseMessage httpResponseMessage, CancellationToken cancellationToken)
        {
            throw new HttpException(httpResponseMessage);
        }

        /// <summary>
        /// Detect human faces in an image and returns face locations, and optionally with faceIds, landmarks, and attributes.
        /// </summary>
        /// <param name="request"></param>
        /// <param name="returnFaceId">A value indicating whether the operation should return faceIds of detected faces.</param>
        /// <param name="returnFaceLandmarks">A value indicating whether the operation should return landmarks of the detected faces.</param>
        /// <param name="returnFaceAttributes">Analyze and return the one or more specified face attributes in the comma-separated string like "returnFaceAttributes=age,gender". Supported face attributes include age, gender, headPose, smile, facialHair, glasses and emotion. Note that each face attribute analysis has additional computational and time cost.</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<DetectResponse> DetectAsync(DetectRequest request, bool? returnFaceId = null, bool? returnFaceLandmarks = null, IEnumerable<string> returnFaceAttributes = null, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Post, mUriGenerator.Detect(returnFaceId, returnFaceLandmarks, returnFaceAttributes != null ? string.Join(",", returnFaceAttributes) : null), new JsonContent(request.ToString()), cancellationToken: cancellationToken).ConfigureAwait(false))
            {
                return DetectResponse.FromString(await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
        }

        /// <summary>
        /// Detect human faces in an image and returns face locations, and optionally with faceIds, landmarks, and attributes.
        /// </summary>
        /// <param name="image"></param>
        /// <param name="returnFaceId">A value indicating whether the operation should return faceIds of detected faces.</param>
        /// <param name="returnFaceLandmarks">A value indicating whether the operation should return landmarks of the detected faces.</param>
        /// <param name="returnFaceAttributes">Analyze and return the one or more specified face attributes in the comma-separated string like "returnFaceAttributes=age,gender". Supported face attributes include age, gender, headPose, smile, facialHair, glasses and emotion. Note that each face attribute analysis has additional computational and time cost.</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<DetectResponse> DetectAsync(byte[] image, bool? returnFaceId = null, bool? returnFaceLandmarks = null, IEnumerable<string> returnFaceAttributes = null, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Post, mUriGenerator.Detect(returnFaceId, returnFaceLandmarks, returnFaceAttributes != null ? string.Join(",", returnFaceAttributes) : null), new ByteArrayContent(image), (httpRequestMessage) =>
            {
                httpRequestMessage.Content.Headers.ContentType = new MediaTypeHeaderValue(MediaTypes.Application.OctetStream);
            }, cancellationToken: cancellationToken).ConfigureAwait(false))
            {
                return DetectResponse.FromString(await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
        }

        /// <summary>
        /// Given query face's faceId, to search the similar-looking faces from a faceId array, a face list or a large face list. faceId array contains the faces created by Face - Detect, which will expire 24 hours after creation. A "faceListId" is created by FaceList - Create containing persistedFaceIds that will not expire. And a "largeFaceListId" is created by LargeFaceList - Create containing persistedFaceIds that will also not expire. Depending on the input the returned similar faces list contains faceIds or persistedFaceIds ranked by similarity.
        /// Find similar has two working modes, "matchPerson" and "matchFace". "matchPerson" is the default mode that it tries to find faces of the same person as possible by using internal same-person thresholds.It is useful to find a known person's other photos. Note that an empty list will be returned if no faces pass the internal thresholds. "matchFace" mode ignores same-person thresholds and returns ranked similar faces anyway, even the similarity is low. It can be used in the cases like searching celebrity-looking faces.
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<FindSimilarResponse> FindSimilarAsync(FindSimilarRequest request, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Post, mUriGenerator.FindSimilars(), new JsonContent(request.ToString()), cancellationToken: cancellationToken).ConfigureAwait(false))
            {
                return FindSimilarResponse.FromString(await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
        }

        /// <summary>
        /// Divide candidate faces into groups based on face similarity. 
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<GroupResponse> GroupAsync(GroupRequest request, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Post, mUriGenerator.Group(), new JsonContent(request.ToString()), cancellationToken: cancellationToken).ConfigureAwait(false))
            {
                return GroupResponse.FromString(await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
        }

        /// <summary>
        /// 1-to-many identification to find the closest matches of the specific query person face from a person group or large person group.
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<IdentifyResponse> IdentifyAsync(IdentifyRequest request, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Post, mUriGenerator.Identify(), new JsonContent(request.ToString()), cancellationToken: cancellationToken).ConfigureAwait(false))
            {
                return IdentifyResponse.FromString(await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
        }

        /// <summary>
        /// Verify whether two faces belong to a same person or whether one face belongs to a person. 
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<VerifyResponse> VerifyAsync(VerifyRequest request, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Post, mUriGenerator.Verify(), new JsonContent(request.ToString()), cancellationToken: cancellationToken).ConfigureAwait(false))
            {
                return VerifyResponse.FromString(await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
        }

        /// <summary>
        /// Add a face to a specified face list, up to 1,000 faces. 
        /// </summary>
        /// <param name="faceListId"></param>
        /// <param name="request"></param>
        /// <param name="userData"></param>
        /// <param name="targetFace"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<CreateFaceListFaceResponse> CreateFaceListFaceAsync(string faceListId, CreateFaceListFaceRequest request, string userData = null, TargetFace targetFace = null, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Post, mUriGenerator.FaceListsAddFace(faceListId, userData, targetFace?.ToString()), new JsonContent(request.ToString()), cancellationToken: cancellationToken).ConfigureAwait(false))
            {
                return CreateFaceListFaceResponse.FromString(await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
        }

        /// <summary>
        /// Create an empty face list with user-specified faceListId, name and an optional userData. Up to 64 face lists are allowed in one subscription. 
        /// </summary>
        /// <param name="faceListId"></param>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task CreateFaceListAsync(string faceListId, CreateFaceListRequest request, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Put, mUriGenerator.FaceLists(faceListId), new JsonContent(request.ToString()), cancellationToken: cancellationToken).ConfigureAwait(false))
            {
            }
        }

        /// <summary>
        /// Delete a specified face list. The related face images in the face list will be deleted, too.
        /// </summary>
        /// <param name="faceListId"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task DeleteFaceListAsync(string faceListId, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Delete, mUriGenerator.FaceLists(faceListId), cancellationToken: cancellationToken).ConfigureAwait(false))
            {
            }
        }

        /// <summary>
        /// Delete a face from a face list by specified faceListId and persisitedFaceId. The related face image will be deleted, too. 
        /// Adding/deleting faces to/from a same face list are processed sequentially and to/from different face lists are in parallel.
        /// </summary>
        /// <param name="faceListId"></param>
        /// <param name="persistedFaceId"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task DeleteFaceListFaceAsync(string faceListId, string persistedFaceId, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Delete, mUriGenerator.FaceListsPersistedFaces(faceListId, persistedFaceId), cancellationToken: cancellationToken).ConfigureAwait(false))
            {
            }
        }

        /// <summary>
        /// Retrieve a face list’s faceListId, name, userData and faces in the face list.
        /// </summary>
        /// <param name="faceListId"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<GetFaceListResponse> GetFaceListAsync(string faceListId, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Get, mUriGenerator.FaceLists(faceListId), cancellationToken: cancellationToken).ConfigureAwait(false))
            {
                return GetFaceListResponse.FromString(await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
        }

        /// <summary>
        /// List face lists’ faceListId, name and userData. 
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ListFaceListsResponse> ListFaceListsAsync(CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Get, mUriGenerator.FaceLists(), cancellationToken: cancellationToken).ConfigureAwait(false))
            {
                return ListFaceListsResponse.FromString(await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
        }

        /// <summary>
        /// Update information of a face list, including name and userData.
        /// </summary>
        /// <param name="faceListId"></param>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task UpdateFaceListAsync(string faceListId, UpdateFaceListRequest request, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(new HttpMethod("PATCH"), mUriGenerator.FaceLists(faceListId), new JsonContent(request.ToString()), cancellationToken: cancellationToken).ConfigureAwait(false))
            {
            }
        }

        /// <summary>
        /// Create a new person group with specified personGroupId, name and user-provided userData.
        /// </summary>
        /// <param name="personGroupId"></param>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task CreatePersonGroupAsync(string personGroupId, CreatePersonGroupRequest request, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Put, mUriGenerator.PersonGroups(personGroupId), new JsonContent(request.ToString()), cancellationToken: cancellationToken).ConfigureAwait(false))
            {
            }
        }

        /// <summary>
        /// Delete an existing person group. Persisted face features of all people in the person group will also be deleted.
        /// </summary>
        /// <param name="personGroupId"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task DeletePersonGroupAsync(string personGroupId, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Delete, mUriGenerator.PersonGroups(personGroupId), cancellationToken: cancellationToken).ConfigureAwait(false))
            {
            }
        }

        /// <summary>
        /// Retrieve the information of a person group, including its name and userData.
        /// </summary>
        /// <param name="personGroupId"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<GetPersonGroupResponse> GetPersonGroupAsync(string personGroupId, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Get, mUriGenerator.PersonGroups(personGroupId), cancellationToken: cancellationToken).ConfigureAwait(false))
            {
                return GetPersonGroupResponse.FromString(await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
        }

        /// <summary>
        /// List person groups and their information.
        /// </summary>
        /// <param name="start"></param>
        /// <param name="top"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ListPersonGroupsResponse> ListPersonGroupsAsync(string start = null, int? top = null, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Get, mUriGenerator.PersonGroupsList(start, top), cancellationToken: cancellationToken).ConfigureAwait(false))
            {
                return ListPersonGroupsResponse.FromString(await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
        }

        /// <summary>
        /// Retrieve the training status of a person group (completed or ongoing).
        /// </summary>
        /// <param name="personGroupId"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<GetPersonGroupTrainingStatusResponse> GetPersonGroupTrainingStatus(string personGroupId, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Get, mUriGenerator.PersonGroupsTraining(personGroupId), cancellationToken: cancellationToken).ConfigureAwait(false))
            {
                return GetPersonGroupTrainingStatusResponse.FromString(await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
        }

        /// <summary>
        /// Queue a person group training task, the training task may not be started immediately.
        /// </summary>
        /// <param name="personGroupId"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task TrainPersonGroupAsync(string personGroupId, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Post, mUriGenerator.PersonGroupsTrain(personGroupId), cancellationToken: cancellationToken).ConfigureAwait(false))
            {
            }
        }

        /// <summary>
        /// Update an existing person group's display name and userData. The properties which does not appear in request body will not be updated.
        /// </summary>
        /// <param name="personGroupId"></param>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task UpdatePersonGroupAsync(string personGroupId, UpdatePersonGroupRequest request, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(new HttpMethod("PATCH"), mUriGenerator.PersonGroups(personGroupId), new JsonContent(request.ToString()), cancellationToken: cancellationToken).ConfigureAwait(false))
            {
            }
        }

        /// <summary>
        /// Add a representative face to a person for identification.The input face is specified as an image with a targetFace rectangle.
        /// </summary>
        /// <param name="personGroupId"></param>
        /// <param name="personId"></param>
        /// <param name="request"></param>
        /// <param name="userData"></param>
        /// <param name="targetFace">A face rectangle to specify the target face to be added to a person in the format of "targetFace=left,top,width,height". E.g. "targetFace=10,10,100,100". If there is more than one face in the image, targetFace is required to specify which face to add. No targetFace means there is only one face detected in the entire image.</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<CreatePersonGroupPersonFaceResponse> CreatePersonGroupPersonFaceAsync(string personGroupId, string personId, CreatePersonGroupPersonFaceRequest request, string userData = null, TargetFace targetFace = null, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Post, mUriGenerator.PersonGroupsPersonsPersistedFacesAddFace(personGroupId, personId, userData, targetFace != null ? targetFace.ToString() : null), new JsonContent(request.ToString()), cancellationToken: cancellationToken).ConfigureAwait(false))
            {
                return CreatePersonGroupPersonFaceResponse.FromString(await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
        }

        /// <summary>
        /// Add a representative face to a person for identification.The input face is specified as an image with a targetFace rectangle.
        /// </summary>
        /// <param name="personGroupId"></param>
        /// <param name="personId"></param>
        /// <param name="image"></param>
        /// <param name="userData"></param>
        /// <param name="targetFace">A face rectangle to specify the target face to be added to a person in the format of "targetFace=left,top,width,height". E.g. "targetFace=10,10,100,100". If there is more than one face in the image, targetFace is required to specify which face to add. No targetFace means there is only one face detected in the entire image.</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<CreatePersonGroupPersonFaceResponse> CreatePersonGroupPersonFaceAsync(string personGroupId, string personId, byte[] image, string userData = null, TargetFace targetFace = null, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Post, mUriGenerator.PersonGroupsPersonsPersistedFacesAddFace(personGroupId, personId, userData, targetFace != null ? targetFace.ToString() : null), new ByteArrayContent(image), (httpRequestMessage) => 
            {
                httpRequestMessage.Content.Headers.ContentType = new MediaTypeHeaderValue(MediaTypes.Application.OctetStream);
            }, cancellationToken: cancellationToken).ConfigureAwait(false))
            {
                return CreatePersonGroupPersonFaceResponse.FromString(await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
        }

        /// <summary>
        /// Create a new person in a specified person group.
        /// </summary>
        /// <param name="personGroupId"></param>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<CreatePersonGroupPersonResponse> CreatePersonGroupPersonAsync(string personGroupId, CreatePersonGroupPersonRequest request, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Post, mUriGenerator.PersonGroupsPersons(personGroupId), new JsonContent(request.ToString()), cancellationToken: cancellationToken).ConfigureAwait(false))
            {
                return CreatePersonGroupPersonResponse.FromString(await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
        }

        /// <summary>
        /// Delete an existing person from a person group. All stored person data, and face features in the person entry will be deleted.
        /// </summary>
        /// <param name="personGroupId"></param>
        /// <param name="personId"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task DeletePersonGroupPersonAsync(string personGroupId, string personId, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Delete, mUriGenerator.PersonGroupsPersons(personGroupId, personId), cancellationToken: cancellationToken).ConfigureAwait(false))
            {
            }
        }

        /// <summary>
        /// Delete a face from a person. Relative feature for the persisted face will also be deleted.
        /// </summary>
        /// <param name="personGroupId"></param>
        /// <param name="personId"></param>
        /// <param name="persistedFaceId"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task DeletePersonGroupPersonFaceAsync(string personGroupId, string personId, string persistedFaceId, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Delete, mUriGenerator.PersonGroupsPersonsPersistedFaces(personGroupId, personId, persistedFaceId), cancellationToken: cancellationToken).ConfigureAwait(false))
            {
            }
        }

        /// <summary>
        /// Retrieve a person's information, including registered persisted faces, name and userData.
        /// </summary>
        /// <param name="personGroupId"></param>
        /// <param name="personId"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<GetPersonGroupPersonResponse> GetPersonGroupPersonAsync(string personGroupId, string personId, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Get, mUriGenerator.PersonGroupsPersons(personGroupId, personId), cancellationToken: cancellationToken).ConfigureAwait(false))
            {
                return GetPersonGroupPersonResponse.FromString(await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
        }

        /// <summary>
        /// Retrieve information about a persisted face (specified by persistedFaceId, personId and its belonging personGroupId).
        /// </summary>
        /// <param name="personGroupId"></param>
        /// <param name="personId"></param>
        /// <param name="persistedFaceId"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<GetPersonGroupPersonFaceResponse> GetPersonGroupPersonFaceAsync(string personGroupId, string personId, string persistedFaceId, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Get, mUriGenerator.PersonGroupsPersonsPersistedFaces(personGroupId, personId, persistedFaceId), cancellationToken: cancellationToken).ConfigureAwait(false))
            {
                return GetPersonGroupPersonFaceResponse.FromString(await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
        }

        /// <summary>
        /// List all persons in a person group, and retrieve person information (including personId, name, userData and persistedFaceIds of registered faces of the person).
        /// </summary>
        /// <param name="personGroupId"></param>
        /// <param name="start"></param>
        /// <param name="top"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ListPersonGroupPersonsResponse> ListPersonGroupPersonsAsync(string personGroupId, string start = null, int? top = null, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Get, mUriGenerator.PersonGroupsPersonsList(personGroupId, start, top), cancellationToken: cancellationToken).ConfigureAwait(false))
            {
                return ListPersonGroupPersonsResponse.FromString(await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
        }

        /// <summary>
        /// Update name or userData of a person.
        /// </summary>
        /// <param name="personGroupId"></param>
        /// <param name="personId"></param>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task UpdatePersonGroupPersonAsync(string personGroupId, string personId, UpdatePersonGroupPersonRequest request, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(new HttpMethod("PATCH"), mUriGenerator.PersonGroupsPersons(personGroupId, personId), new JsonContent(request.ToString()), cancellationToken: cancellationToken).ConfigureAwait(false))
            {
            }
        }

        /// <summary>
        /// Update a person persisted face's userData field.
        /// </summary>
        /// <param name="personGroupId"></param>
        /// <param name="personId"></param>
        /// <param name="persistedFaceId"></param>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task UpdatePersonGroupPersonFaceAsync(string personGroupId, string personId, string persistedFaceId, UpdatePersonGroupPersonFaceRequest request, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(new HttpMethod("PATCH"), mUriGenerator.PersonGroupsPersonsPersistedFaces(personGroupId, personId, persistedFaceId), new JsonContent(request.ToString()), cancellationToken: cancellationToken).ConfigureAwait(false))
            {
            }
        }

        public class UriGenerator
        {
            protected string mHost;

            public UriGenerator(string host)
            {
                mHost = host;
            }

            public UriBuilder Base()
            {
                return new UriBuilder("https", mHost);
            }

            public UriBuilder Base(string path)
            {
                UriBuilder uriBuilder = Base();
                uriBuilder.Path = path;
                return uriBuilder;
            }

            public UriBuilder Identify()
            {
                return Base("/face/v1.0/identify");
            }

            public UriBuilder Detect(bool? returnFaceId, bool? returnFaceLandmarks, string returnFaceAttributes)
            {
                UriBuilder result = Base("/face/v1.0/detect");

                if (returnFaceId != null)
                {
                    result.AddQuery("returnFaceId", returnFaceId.Value.ToString());
                }

                if (returnFaceLandmarks != null)
                {
                    result.AddQuery("returnFaceLandmarks", returnFaceLandmarks.Value.ToString());
                }

                if (returnFaceAttributes != null)
                {
                    result.AddQuery("returnFaceAttributes", returnFaceAttributes);
                }

                return result;
            }

            public UriBuilder FindSimilars()
            {
                return Base("/face/v1.0/findsimilars");
            }

            public UriBuilder Group()
            {
                return Base("/face/v1.0/group");
            }

            public UriBuilder Verify()
            {
                return Base("/face/v1.0/verify");
            }

            public UriBuilder FaceLists()
            {
                return Base("/face/v1.0/facelists");
            }

            public UriBuilder FaceLists(string faceListId)
            {
                return FaceLists().AddSegment(faceListId);
            }

            public UriBuilder FaceListsPersistedFaces(string faceListId)
            {
                return FaceLists(faceListId).AddSegment("persistedFaces");
            }

            public UriBuilder FaceListsPersistedFaces(string faceListId, string persistedFaceId)
            {
                return FaceListsPersistedFaces(faceListId).AddSegment(persistedFaceId);
            }

            public UriBuilder FaceListsAddFace(string faceListId, string userData, string targetFace)
            {
                UriBuilder result = FaceListsPersistedFaces(faceListId);

                if (userData != null)
                {
                    result.AddQuery("userData", userData);
                }

                if (targetFace != null)
                {
                    result.AddQuery("targetFace", targetFace);
                }

                return result;
            }

            public UriBuilder PersonGroups()
            {
                return Base("/face/v1.0/persongroups");
            }

            public UriBuilder PersonGroups(string id)
            {
                return PersonGroups().AddSegment(id);
            }

            public UriBuilder PersonGroupsTraining(string id)
            {
                return PersonGroups(id).AddSegment("training");
            }

            public UriBuilder PersonGroupsList(string start, int? top)
            {
                UriBuilder result = PersonGroups();
                if (start != null)
                {
                    result.AddQuery("start", start);
                }

                if (top != null)
                {
                    result.AddQuery("top", top.Value.ToString());
                }

                return result;
            }

            public UriBuilder PersonGroupsTrain(string id)
            {
                return PersonGroups(id).AddSegment("train");
            }

            public UriBuilder PersonGroupsPersons(string personGroupId)
            {
                return PersonGroups(personGroupId).AddSegment("persons");
            }

            public UriBuilder PersonGroupsPersons(string personGroupId, string personId)
            {
                return PersonGroupsPersons(personGroupId).AddSegment(personId);
            }

            public UriBuilder PersonGroupsPersonsPersistedFaces(string personGroupId, string personId)
            {
                return PersonGroupsPersons(personGroupId, personId).AddSegment("persistedFaces");
            }

            public UriBuilder PersonGroupsPersonsPersistedFaces(string personGroupId, string personId, string persistedFaceId)
            {
                return PersonGroupsPersonsPersistedFaces(personGroupId, personId).AddSegment(persistedFaceId);
            }

            public UriBuilder PersonGroupsPersonsPersistedFacesAddFace(string personGroupId, string personId, string userData, string targetFace)
            {
                UriBuilder result = PersonGroupsPersonsPersistedFaces(personGroupId, personId);

                if (userData != null)
                {
                    result.AddQuery("userData", userData);
                }

                if (targetFace != null)
                {
                    result.AddQuery("targetFace", targetFace);
                }

                return result;
            }

            public UriBuilder PersonGroupsPersonsList(string personGroupId, string start, int? top)
            {
                UriBuilder result = PersonGroupsPersons(personGroupId);

                if (start != null)
                {
                    result.AddQuery("start", start);
                }

                if (top != null)
                {
                    result.AddQuery("top", top.Value.ToString());
                }

                return result;
            }
        }
    }
}
