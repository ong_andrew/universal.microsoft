﻿using Newtonsoft.Json;
using System.Collections.Generic;
using Universal.Common.Serialization;

namespace Universal.Microsoft.CognitiveServices.Face.V1
{
    public class ListPersonGroupPersonsResponse : JsonCollectionSerializable<ListPersonGroupPersonsResponse, ListPersonGroupPersonsResponse.PersonGroupPerson>
    {
        public ListPersonGroupPersonsResponse()
        {
            Collection = new List<PersonGroupPerson>();
        }

        public class PersonGroupPerson
        {
            [JsonProperty("personId")]
            public string PersonId { get; set; }
            [JsonProperty("persistedFaceIds")]
            public List<string> PersistedFaceIds { get; set; }
            [JsonProperty("name")]
            public string Name { get; set; }
            [JsonProperty("userData")]
            public string UserData { get; set; }

            public PersonGroupPerson()
            {
                PersistedFaceIds = new List<string>();
            }
        }
    }
}
