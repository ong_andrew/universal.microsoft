﻿using Newtonsoft.Json;
using System.ComponentModel;
using Universal.Common;
using Universal.Common.Serialization;

namespace Universal.Microsoft.CognitiveServices.Face.V1
{
    public class UpdatePersonGroupPersonFaceRequest : JsonSerializable
    {
        [JsonProperty("userData")]
        public string UserData { get; set; }
        [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeUserData() { return !UserData.IsNullOrEmpty(); }
    }
}
