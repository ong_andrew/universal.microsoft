﻿using Newtonsoft.Json;
using System.Collections.Generic;
using Universal.Common.Serialization;

namespace Universal.Microsoft.CognitiveServices.Face.V1
{
    public class ListPersonGroupsResponse : JsonCollectionSerializable<ListPersonGroupsResponse, ListPersonGroupsResponse.PersonGroup>
    {
        public ListPersonGroupsResponse()
        {
            Collection = new List<PersonGroup>();
        }

        public class PersonGroup
        {
            [JsonProperty("personGroupId")]
            public string PersonGroupId { get; set; }
            [JsonProperty("name")]
            public string Name { get; set; }
            [JsonProperty("userData")]
            public string UserData { get; set; }
        }
    }
}
