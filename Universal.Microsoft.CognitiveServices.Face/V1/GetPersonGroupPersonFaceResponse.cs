﻿using Newtonsoft.Json;
using Universal.Common.Serialization;

namespace Universal.Microsoft.CognitiveServices.Face.V1
{
    public class GetPersonGroupPersonFaceResponse : JsonSerializable<GetPersonGroupPersonFaceResponse>
    {
        [JsonProperty("persistedFaceId")]
        public string PersistedFaceId { get; set; }
        [JsonProperty("userData")]
        public string UserData { get; set; }
    }
}
