﻿using Newtonsoft.Json;
using Universal.Common.Serialization;

namespace Universal.Microsoft.CognitiveServices.Face.V1
{
    public class VerifyResponse : JsonSerializable<VerifyResponse>
    {
        [JsonProperty("isIdentical")]
        public bool IsIdentical { get; set; }
        [JsonProperty("confidence")]
        public double Confidence { get; set; }
    }
}
