﻿using Newtonsoft.Json;
using System.Collections.Generic;
using Universal.Common.Serialization;

namespace Universal.Microsoft.CognitiveServices.Face.V1
{
    public class GetPersonGroupPersonResponse : JsonSerializable<GetPersonGroupPersonResponse>
    {
        [JsonProperty("personId")]
        public string PersonId { get; set; }
        [JsonProperty("persistedFaceIds")]
        public List<string> PersistedFaceIds { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("userData")]
        public string UserData { get; set; }

        public GetPersonGroupPersonResponse()
        {
            PersistedFaceIds = new List<string>();
        }
    }
}
