﻿using Newtonsoft.Json;
using System.Collections.Generic;
using Universal.Common.Serialization;

namespace Universal.Microsoft.CognitiveServices.Face.V1
{
    public class GetFaceListResponse : JsonSerializable<GetFaceListResponse>
    {
        [JsonProperty("faceListId")]
        public string FaceListId { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("userData")]
        public string UserData { get; set; }
        [JsonProperty("persistedFaces")]
        public List<PersistedFace> PersistedFaces { get; set; }

        public GetFaceListResponse()
        {
            PersistedFaces = new List<PersistedFace>();
        }

        public class PersistedFace
        {
            [JsonProperty("persistedFaceId")]
            public string PersistedFaceId { get; set; }
            [JsonProperty("userData")]
            public string UserData { get; set; }
        }
    }
}
