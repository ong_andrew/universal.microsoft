﻿using Newtonsoft.Json;
using Universal.Common.Serialization;

namespace Universal.Microsoft.CognitiveServices.Face.V1
{
    public class CreateFaceListFaceResponse : JsonSerializable<CreateFaceListFaceResponse>
    {
        [JsonProperty("persistedFaceId")]
        public string PersistedFaceId { get; set; }
    }
}
