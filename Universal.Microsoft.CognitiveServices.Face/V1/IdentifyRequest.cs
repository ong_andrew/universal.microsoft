﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel;
using Universal.Common;
using Universal.Common.Serialization;

namespace Universal.Microsoft.CognitiveServices.Face.V1
{
    public class IdentifyRequest : JsonSerializable
    {
        /// <summary>
        /// Array of query faces faceIds, created by the Face - Detect.Each of the faces are identified independently.The valid number of faceIds is between[1, 10].
        /// </summary>
        [JsonProperty("faceIds")]
        public List<string> FaceIds { get; set; }

        /// <summary>
        /// PersonGroupId of the target person group, created by PersonGroup - Create. Parameter personGroupId and largePersonGroupId should not be provided at the same time.
        /// </summary>
        [JsonProperty("personGroupId")]
        public string PersonGroupId { get; set; }
        [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializePersonGroupId() { return !PersonGroupId.IsNullOrEmpty();}

        /// <summary>
        /// LargePersonGroupId of the target large person group, created by LargePersonGroup - Create. Parameter personGroupId and largePersonGroupId should not be provided at the same time.
        /// </summary>
        [JsonProperty("largePersonGroupId")]
        public string LargePersonGroupId { get; set; }
        [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeLargePersonGroupId() { return !LargePersonGroupId.IsNullOrEmpty(); }

        /// <summary>
        /// The range of maxNumOfCandidatesReturned is between 1 and 5 (default is 1).
        /// </summary>
        [JsonProperty("maxNumOfCandidatesReturned")]
        public int? MaxNumOfCandidatesReturned { get; set; }
        [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeMaxNumOfCandidatesReturned() { return MaxNumOfCandidatesReturned != null; }

        /// <summary>
        /// Confidence threshold of identification, used to judge whether one face belong to one person. The range of confidenceThreshold is [0, 1] (default specified by algorithm).
        /// </summary>
        [JsonProperty("confidenceThreshold")]
        public double? ConfidenceThreshold { get; set; }
        [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializConfidenceThreshold() { return ConfidenceThreshold != null; }

        public IdentifyRequest()
        {
            FaceIds = new List<string>();
        }
    }
}
