﻿using Newtonsoft.Json;
using System;
using Universal.Common.Serialization;

namespace Universal.Microsoft.CognitiveServices.Face.V1
{
    public class GetPersonGroupTrainingStatusResponse : JsonSerializable<GetPersonGroupTrainingStatusResponse>
    {
        [JsonProperty("status")]
        public string Status { get; set; }
        [JsonProperty("createdDateTime")]
        public DateTime CreatedDateTime { get; set; }
        [JsonProperty("lastActionDateTime")]
        public DateTime LastActionDateTime { get; set; }
        [JsonProperty("message")]
        public string Message { get; set; }
    }
}
