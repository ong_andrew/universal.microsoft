﻿using Newtonsoft.Json;
using System.Collections.Generic;
using Universal.Common.Serialization;

namespace Universal.Microsoft.CognitiveServices.Face.V1
{
    public class GroupRequest : JsonSerializable
    {
        [JsonProperty("faceIds")]
        public List<string> FaceIds { get; set; }

        public GroupRequest()
        {
            FaceIds = new List<string>();
        }
    }
}
