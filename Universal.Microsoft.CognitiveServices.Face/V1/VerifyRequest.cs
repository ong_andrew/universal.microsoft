﻿using Newtonsoft.Json;
using System.ComponentModel;
using Universal.Common;

namespace Universal.Microsoft.CognitiveServices.Face.V1
{
    public class VerifyRequest
    {
        /// <summary>
        /// faceId of one face, comes from Face - Detect.
        /// </summary>
        [JsonProperty("faceId1")]
        public string FaceId1 { get; set; }
        [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeFaceId1() { return !FaceId1.IsNullOrEmpty(); }
        /// <summary>
        /// faceId of another face, comes from Face - Detect.
        /// </summary>
        [JsonProperty("faceId2")]
        public string FaceId2 { get; set; }
        [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeFaceId2() { return !FaceId2.IsNullOrEmpty(); }
        /// <summary>
        /// faceId of the face, comes from Face - Detect.
        /// </summary>
        [JsonProperty("faceId")]
        public string FaceId { get; set; }
        [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeFaceId() { return !FaceId.IsNullOrEmpty(); }
        /// <summary>
        /// Using existing personGroupId and personId for fast loading a specified person. personGroupId is created in PersonGroup - Create. Parameter personGroupId and largePersonGroupId should not be provided at the same time.
        /// </summary>
        [JsonProperty("personGroupId")]
        public string PersonGroupId { get; set; }
        [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializePersonGroupId() { return !PersonGroupId.IsNullOrEmpty(); }
        /// <summary>
        /// Using existing largePersonGroupId and personId for fast loading a specified person. largePersonGroupId is created in LargePersonGroup - Create. Parameter personGroupId and largePersonGroupId should not be provided at the same time.
        /// </summary>
        [JsonProperty("largePersonGroupId")]
        public string LargePersonGroupId { get; set; }
        [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeLargePersonGroupId() { return !LargePersonGroupId.IsNullOrEmpty(); }
        /// <summary>
        /// Specify a certain person in a person group or a large person group. personId is created in PersonGroup Person - Create or LargePersonGroup Person - Create.
        /// </summary>
        [JsonProperty("personId")]
        public string PersonId { get; set; }
        [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializePersonId() { return !PersonId.IsNullOrEmpty(); }

    }
}
