﻿using Newtonsoft.Json;
using System.Collections.Generic;
using Universal.Common.Serialization;

namespace Universal.Microsoft.CognitiveServices.Face.V1
{
    public class DetectResponse : JsonCollectionSerializable<DetectResponse, DetectResponse.Face>
    {
        public DetectResponse()
        {
            Collection = new List<Face>();
        }

        public class Face
        {
            [JsonProperty("faceId")]
            public string FaceId { get; set; }
            [JsonProperty("faceRectangle")]
            public FaceRectangle FaceRectangle { get; set; }
            [JsonProperty("faceLandmarks")]
            public Facelandmarks FaceLandmarks { get; set; }
            [JsonProperty("faceAttributes")]
            public FaceAttributes FaceAttributes { get; set; }
        }

        public class FaceRectangle
        {
            [JsonProperty("width")]
            public int Width { get; set; }
            [JsonProperty("height")]
            public int Height { get; set; }
            [JsonProperty("left")]
            public int Left { get; set; }
            [JsonProperty("top")]
            public int Top { get; set; }
        }

        public class Facelandmarks
        {
            [JsonProperty("pupilLeft")]
            public Coordinate PupilLeft { get; set; }
            [JsonProperty("pupilRight")]
            public Coordinate PupilRight { get; set; }
            [JsonProperty("noseTip")]
            public Coordinate NoseTip { get; set; }
            [JsonProperty("mouthLeft")]
            public Coordinate MouthLeft { get; set; }
            [JsonProperty("mouthRight")]
            public Coordinate MouthRight { get; set; }
            [JsonProperty("eyebrowLeftOuter")]
            public Coordinate EyebrowLeftOuter { get; set; }
            [JsonProperty("eyebrowLeftInner")]
            public Coordinate EyebrowLeftInner { get; set; }
            [JsonProperty("eyeLeftOuter")]
            public Coordinate EyeLeftOuter { get; set; }
            [JsonProperty("eyeLeftTop")]
            public Coordinate EyeLeftTop { get; set; }
            [JsonProperty("eyeLeftBottom")]
            public Coordinate EyeLeftBottom { get; set; }
            [JsonProperty("eyeLeftInner")]
            public Coordinate EyeLeftInner { get; set; }
            [JsonProperty("eyebrowRightInner")]
            public Coordinate EyebrowRightInner { get; set; }
            [JsonProperty("eyebrowRightOuter")]
            public Coordinate EyebrowRightOuter { get; set; }
            [JsonProperty("eyeRightInner")]
            public Coordinate EyeRightInner { get; set; }
            [JsonProperty("eyeRightTop")]
            public Coordinate EyeRightTop { get; set; }
            [JsonProperty("eyeRightBottom")]
            public Coordinate EyeRightBottom { get; set; }
            [JsonProperty("eyeRightOuter")]
            public Coordinate EyeRightOuter { get; set; }
            [JsonProperty("noseRootLeft")]
            public Coordinate NoseRootLeft { get; set; }
            [JsonProperty("noseRootRight")]
            public Coordinate NoseRootRight { get; set; }
            [JsonProperty("noseLeftAlarTop")]
            public Coordinate NoseLeftAlarTop { get; set; }
            [JsonProperty("noseRightAlarTop")]
            public Coordinate NoseRightAlarTop { get; set; }
            [JsonProperty("noseLeftAlarOutTip")]
            public Coordinate NoseLeftAlarOutTip { get; set; }
            [JsonProperty("noseRightAlarOutTip")]
            public Coordinate NoseRightAlarOutTip { get; set; }
            [JsonProperty("upperLipTop")]
            public Coordinate UpperLipTop { get; set; }
            [JsonProperty("upperLipBottom")]
            public Coordinate UpperLipBottom { get; set; }
            [JsonProperty("underLipTop")]
            public Coordinate UnderLipTop { get; set; }
            [JsonProperty("underLipBottom")]
            public Coordinate UnderLipBottom { get; set; }
        }

        public class Coordinate
        {
            [JsonProperty("x")]
            public double X { get; set; }
            [JsonProperty("y")]
            public double Y { get; set; }
        }

        public class FaceAttributes
        {
            [JsonProperty("age")]
            public double Age { get; set; }
            [JsonProperty("gender")]
            public string Gender { get; set; }
            [JsonProperty("smile")]
            public double Smile { get; set; }
            [JsonProperty("facialHair")]
            public FacialHair FacialHair { get; set; }
            [JsonProperty("glasses")]
            public string Glasses { get; set; }
            [JsonProperty("headPose")]
            public HeadPose HeadPose { get; set; }
            [JsonProperty("emotion")]
            public Emotion Emotion { get; set; }
            [JsonProperty("hair")]
            public Hair Hair { get; set; }
            [JsonProperty("makeup")]
            public Makeup Makeup { get; set; }
            [JsonProperty("occlusion")]
            public Occlusion Occlusion { get; set; }
            [JsonProperty("accessories")]
            public List<Accessory> Accessories { get; set; }
            [JsonProperty("blur")]
            public Blur Blur { get; set; }
            [JsonProperty("exposure")]
            public Exposure Exposure { get; set; }
            [JsonProperty("noise")]
            public Noise Noise { get; set; }
        }

        public class FacialHair
        {
            [JsonProperty("moustache")]
            public double Moustache { get; set; }
            [JsonProperty("beard")]
            public double Beard { get; set; }
            [JsonProperty("sideburns")]
            public double Sideburns { get; set; }
        }

        public class HeadPose
        {
            [JsonProperty("roll")]
            public double Roll { get; set; }
            [JsonProperty("yaw")]
            public double Yaw { get; set; }
            [JsonProperty("pitch")]
            public double Pitch { get; set; }
        }

        public class Emotion
        {
            [JsonProperty("anger")]
            public double Anger { get; set; }
            [JsonProperty("contempt")]
            public double Contempt { get; set; }
            [JsonProperty("disgust")]
            public double Disgust { get; set; }
            [JsonProperty("fear")]
            public double Fear { get; set; }
            [JsonProperty("happiness")]
            public double Happiness { get; set; }
            [JsonProperty("neutral")]
            public double Neutral { get; set; }
            [JsonProperty("sadness")]
            public double Sadness { get; set; }
            [JsonProperty("surprise")]
            public double Surprise { get; set; }
        }

        public class Hair
        {
            [JsonProperty("bald")]
            public double Bald { get; set; }
            [JsonProperty("invisible")]
            public bool Invisible { get; set; }
            [JsonProperty("hairColor")]
            public List<Haircolor> HairColor { get; set; }
        }

        public class Haircolor
        {
            [JsonProperty("color")]
            public string Color { get; set; }
            [JsonProperty("confidence")]
            public double Confidence { get; set; }
        }

        public class Makeup
        {
            [JsonProperty("eyeMakeup")]
            public bool EyeMakeup { get; set; }
            [JsonProperty("lipMakeup")]
            public bool LipMakeup { get; set; }
        }

        public class Occlusion
        {
            [JsonProperty("foreheadOccluded")]
            public bool ForeheadOccluded { get; set; }
            [JsonProperty("eyeOccluded")]
            public bool EyeOccluded { get; set; }
            [JsonProperty("mouthOccluded")]
            public bool MouthOccluded { get; set; }
        }

        public class Blur
        {
            [JsonProperty("blurLevel")]
            public string BlurLevel { get; set; }
            [JsonProperty("value")]
            public double Value { get; set; }
        }

        public class Exposure
        {
            [JsonProperty("exposureLevel")]
            public string ExposureLevel { get; set; }
            [JsonProperty("value")]
            public double Value { get; set; }
        }

        public class Noise
        {
            [JsonProperty("noiseLevel")]
            public string NoiseLevel { get; set; }
            [JsonProperty("value")]
            public double Value { get; set; }
        }

        public class Accessory
        {
            [JsonProperty("type")]
            public string Type { get; set; }
            [JsonProperty("confidence")]
            public double Confidence { get; set; }
        }
    }
}
