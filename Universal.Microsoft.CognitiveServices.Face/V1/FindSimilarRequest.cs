﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Universal.Common;
using Universal.Common.Serialization;

namespace Universal.Microsoft.CognitiveServices.Face.V1
{
    public class FindSimilarRequest : JsonSerializable<FindSimilarRequest>
    {
        /// <summary>
        /// faceId of the query face. User needs to call Face - Detect first to get a valid faceId. Note that this faceId is not persisted and will expire 24 hours after the detection call.
        /// </summary>
        [JsonProperty("faceId")]
        public string FaceId { get; set; }
        /// <summary>
        /// An existing user-specified unique candidate face list, created in FaceList - Create. Face list contains a set of persistedFaceIds which are persisted and will never expire. Parameter faceListId, largeFaceListId and faceIds should not be provided at the same time.
        /// </summary>
        [JsonProperty("faceListId")]
        public string FaceListId { get; set; }
        [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeFaceListId() { return !FaceListId.IsNullOrEmpty(); }
        /// <summary>
        /// An existing user-specified unique candidate large face list, created in LargeFaceList - Create. Large face list contains a set of persistedFaceIds which are persisted and will never expire. Parameter faceListId, largeFaceListId and faceIds should not be provided at the same time.
        /// </summary>
        [JsonProperty("largeFaceListId")]
        public string LargeFaceListId { get; set; }
        [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeLargeFaceListId() { return !LargeFaceListId.IsNullOrEmpty(); }
        /// <summary>
        /// An array of candidate faceIds. All of them are created by Face - Detect and the faceIds will expire 24 hours after the detection call. The number of faceIds is limited to 1000. Parameter faceListId, largeFaceListId and faceIds should not be provided at the same time.
        /// </summary>
        [JsonProperty("faceIds")]
        public List<string> FaceIds { get; set; }
        [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeFaceIds() { return FaceIds != null && FaceIds.Any(); }
        /// <summary>
        /// Optional parameter. 
        /// The number of top similar faces returned.
        /// The valid range is [1, 1000].It defaults to 20.
        /// </summary>
        [JsonProperty("maxNumOfCandidatesReturned")]
        public int? MaxNumOfCandidatesReturned { get; set; }
        [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeMaxNumOfCandidatesReturned() { return MaxNumOfCandidatesReturned != null; }
        /// <summary>
        /// Optional parameter. 
        /// Similar face searching mode.It can be "matchPerson" or "matchFace". It defaults to "matchPerson".
        /// </summary>
        [JsonProperty("mode")]
        public string Mode { get; set; }
        [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeMode() { return !Mode.IsNullOrEmpty(); }

        public FindSimilarRequest()
        {
            FaceIds = new List<string>();
        }
    }
}
