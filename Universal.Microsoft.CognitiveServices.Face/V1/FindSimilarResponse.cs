﻿using Newtonsoft.Json;
using System.Collections.Generic;
using Universal.Common.Serialization;

namespace Universal.Microsoft.CognitiveServices.Face.V1
{
    public class FindSimilarResponse : JsonCollectionSerializable<FindSimilarResponse, FindSimilarResponse.Face>
    {
        public FindSimilarResponse()
        {
            Collection = new List<Face>();
        }

        public class Face
        {
            [JsonProperty("persistedFaceId")]
            public string PersistedFaceId { get; set; }
            [JsonProperty("faceId")]
            public string FaceId { get; set; }
            [JsonProperty("confidence")]
            public double Confidence { get; set; }
        }
    }
}