﻿using Newtonsoft.Json;
using System.Collections.Generic;
using Universal.Common.Serialization;

namespace Universal.Microsoft.CognitiveServices.Face.V1
{
    public class ListFaceListsResponse : JsonCollectionSerializable<ListFaceListsResponse, ListFaceListsResponse.FaceList>
    {
        public ListFaceListsResponse()
        {
            Collection = new List<FaceList>();
        }

        public class FaceList
        {
            [JsonProperty("faceListId")]
            public string FaceListId { get; set; }
            [JsonProperty("name")]
            public string Name { get; set; }
            [JsonProperty("userData")]
            public string UserData { get; set; }
        }
    }
}
