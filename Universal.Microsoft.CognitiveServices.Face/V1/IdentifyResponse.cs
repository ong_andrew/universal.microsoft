﻿using Newtonsoft.Json;
using System.Collections.Generic;
using Universal.Common.Serialization;

namespace Universal.Microsoft.CognitiveServices.Face.V1
{
    public class IdentifyResponse : JsonCollectionSerializable<IdentifyResponse, IdentifyResponse.FaceResult>
    {
        public IdentifyResponse()
        {
            Collection = new List<FaceResult>();
        }

        public class FaceResult
        {
            [JsonProperty("faceId")]
            public string FaceId { get; set; }
            [JsonProperty("candidates")]
            public List<Candidate> Candidates { get; set; }

            public FaceResult()
            {
                Candidates = new List<Candidate>();
            }
        }

        public class Candidate
        {
            [JsonProperty("personId")]
            public string PersonId { get; set; }
            [JsonProperty("confidence")]
            public double Confidence { get; set; }
        }
    }
}
