﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Microsoft.Extensions.Configuration
{
    /// <summary>
    /// Extensions for <see cref="IConfigurationBuilder"/>.
    /// </summary>
    public static class IConfigurationBuilderExtensions
    {
        /// <summary>
        /// Adds a JSON configuration source to the <paramref name="builder"/>.
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="assembly"></param>
        /// <param name="type"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public static IConfigurationBuilder AddJsonEmbeddedResource(this IConfigurationBuilder builder, Assembly assembly, Type type, string name)
        {
            return builder.AddJsonEmbeddedResource(assembly, $"{type.Namespace}.{name}");
        }

        /// <summary>
        /// Adds a JSON configuration source to the <paramref name="builder"/>.
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="assembly"></param>
        /// <param name="type"></param>
        /// <param name="name"></param>
        /// <param name="optional"></param>
        /// <returns></returns>
        public static IConfigurationBuilder AddJsonEmbeddedResource(this IConfigurationBuilder builder, Assembly assembly, Type type, string name, bool optional)
        {
            return builder.AddJsonEmbeddedResource(assembly, $"{type.Namespace}.{name}", optional);
        }

        /// <summary>
        /// Adds a JSON configuration source to the <paramref name="builder"/>.
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="assembly"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public static IConfigurationBuilder AddJsonEmbeddedResource(this IConfigurationBuilder builder, Assembly assembly, string name)
        {
            return builder.AddJsonEmbeddedResource(assembly, name, false);
        }

        /// <summary>
        /// Adds a JSON configuration source to the <paramref name="builder"/>.
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="assembly"></param>
        /// <param name="name"></param>
        /// <param name="optional"></param>
        /// <returns></returns>
        public static IConfigurationBuilder AddJsonEmbeddedResource(this IConfigurationBuilder builder, Assembly assembly, string name, bool optional)
        {
            bool resourceExists = assembly.GetManifestResourceNames().Any(x => x == name);

            if (resourceExists)
            {
                Stream stream = assembly.GetManifestResourceStream(name);
                builder.AddJsonStream(stream);
            }
            else
            {
                if (!optional)
                {
                    throw new InvalidOperationException($"The specified JSON resource '{name}' does not exist.");
                }
            }

            return builder;
        }
    }
}
