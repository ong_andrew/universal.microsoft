﻿using System;

namespace Universal.Microsoft.Authentication
{
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false)]
    internal class FormParameterAttribute : Attribute
    {
        public string Name { get; set; }

        public FormParameterAttribute() : this(null) { }
        public FormParameterAttribute(string name)
        {
            Name = name;
        }
    }
}
