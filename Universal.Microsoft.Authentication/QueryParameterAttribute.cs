﻿using System;

namespace Universal.Microsoft.Authentication
{
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false)]
    internal class QueryParameterAttribute : Attribute
    {
        public string Name { get; set; }

        public QueryParameterAttribute() : this(null) { }
        public QueryParameterAttribute(string name)
        {
            Name = name;
        }
    }
}
