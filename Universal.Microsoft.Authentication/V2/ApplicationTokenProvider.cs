﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Universal.Microsoft.Authentication.V2
{
    /// <summary>
    /// Represents a provider of an application security token that does not cache the token results.
    /// </summary>
    public class ApplicationTokenProvider : ITokenProvider
    {
        protected string mTenant;
        protected string mClientId;
        protected string mClientSecret;
        protected IEnumerable<string> mScopes;

        /// <summary>
        /// Initializes a new instance of the <see cref="ApplicationTokenProvider"/> class, which does not cache any <see cref="TokenResponse"/>s.
        /// </summary>
        /// <param name="tenant"></param>
        /// <param name="clientId"></param>
        /// <param name="clientSecret"></param>
        /// <param name="scopes"></param>
        public ApplicationTokenProvider(string tenant, string clientId, string clientSecret, IEnumerable<string> scopes = null)
        {
            mTenant = tenant;
            mClientId = clientId;
            mClientSecret = clientSecret;
            mScopes = scopes ?? new List<string>() { "https://graph.microsoft.com/.default" };
        }

        /// <summary>
        /// Retrieves a new application token asynchronously.
        /// </summary>
        /// <returns></returns>
        public async Task<TokenResponse> GetTokenAsync()
        {
            return await GetTokenAsync(default).ConfigureAwait(false);
        }

        /// <summary>
        /// Retrieves a new application token asynchronously.
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<TokenResponse> GetTokenAsync(CancellationToken cancellationToken)
        {
            using (AuthenticationClient authenticationClient = new AuthenticationClient(mTenant, mClientId, mClientSecret))
            {
                return await authenticationClient.GetTokenByClientCredentialsAsync(mScopes, cancellationToken).ConfigureAwait(false);
            }
        }
    }
}
