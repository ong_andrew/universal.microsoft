﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Universal.Common;

namespace Universal.Microsoft.Authentication.V2
{
    /// <summary>
    /// Represents parameters used to request a token from the /token endpoint.
    /// </summary>
    public class TokenRequest
    {
        /// <summary>
        /// The {tenant} value in the path of the request can be used to control who can sign into the application. The allowed values are common, organizations, consumers, and tenant identifiers. For more detail, see protocol basics.
        /// </summary>
        public string Tenant { get; set; }
        /// <summary>
        /// The Application (client) ID that the Azure portal – App registrations page assigned to your app.
        /// </summary>
        [FormParameter("client_id")]
        public string ClientId { get; set; }

        private ISet<string> mScopes { get; set; } = new HashSet<string>();
        /// <summary>
        /// A space-separated list of scopes. The scopes must all be from a single resource, along with OIDC scopes (profile, openid, email). For a more detailed explanation of scopes, refer to permissions, consent, and scopes. This is a Microsoft extension to the authorization code flow, intended to allow apps to declare the resource they want the token for during token redemption.
        /// </summary>
        [FormParameter("scope")]
        public string Scope
        {
            get
            {
                if (mScopes != null && mScopes.Any())
                {
                    return string.Join(" ", mScopes);
                }
                else
                {
                    return null;
                }
            }
            set
            {
                if (value != null)
                {
                    mScopes = IEnumerableExtensions.ToHashSet(value.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries));
                }
                else
                {
                    mScopes.Clear();
                }
            }
        }

        /// <summary>
        /// The authorization_code that you acquired in the first leg of the flow.
        /// </summary>
        [FormParameter("code")]
        public string Code { get; set; }

        /// <summary>
        /// The same redirect_uri value that was used to acquire the authorization_code.
        /// </summary>
        [FormParameter("redirect_uri")]
        public string RedirectUri { get; set; }

        /// <summary>
        /// Must be authorization_code for the authorization code flow. Must be refresh_token for refreshing the token.
        /// </summary>
        [FormParameter("grant_type")]
        public string GrantType { get; set; }

        /// <summary>
        /// The same code_verifier that was used to obtain the authorization_code. Required if PKCE was used in the authorization code grant request. For more information, see the PKCE RFC.
        /// </summary>
        [FormParameter("code_verifier")]
        public string CodeVerifier { get; set; }

        /// <summary>
        /// The application secret that you created in the app registration portal for your app. You shouldn't use the application secret in a native app or single page app because client_secrets can't be reliably stored on devices or web pages. It's required for web apps and web APIs, which have the ability to store the client_secret securely on the server side. Like all parameters discussed here, the client secret must be URL-encoded before being sent, a step usually performed by the SDK. For more information on uri encoding, see the URI Generic Syntax specification.
        /// </summary>
        [FormParameter("client_secret")]
        public string ClientSecret { get; set; }

        /// <summary>
        /// The value must be set to urn:ietf:params:oauth:client-assertion-type:jwt-bearer in order to use a certificate credential.
        /// </summary>
        [FormParameter("client_assertion_type")]
        public string ClientAssertionType { get; set; }

        /// <summary>
        /// An assertion (a JSON web token) that you need to create and sign with the certificate you registered as credentials for your application. Read about certificate credentials to learn how to register your certificate and the format of the assertion.
        /// </summary>
        [FormParameter("client_assertion")]
        public string ClientAssertion { get; set; }

        /// <summary>
        /// The refresh_token that you acquired in the second leg of the flow.
        /// </summary>
        [FormParameter("refresh_token")]
        public string RefreshToken { get; set; }

        /// <summary>
        /// Adds all supported Open ID scopes.
        /// </summary>
        /// <returns></returns>
        public TokenRequest AddOpenIdScopes()
        {
            return AddOpenIdScope()
                .AddEmailScope()
                .AddProfileScope()
                .AddOfflineAccessScope();
        }

        /// <summary>
        /// Adds the 'openid' scope.
        /// </summary>
        /// <returns></returns>
        public TokenRequest AddOpenIdScope()
        {
            return AddScope("openid");
        }

        /// <summary>
        /// Adds the 'email' scope.
        /// </summary>
        /// <returns></returns>
        public TokenRequest AddEmailScope()
        {
            return AddScope("email");
        }

        /// <summary>
        /// Adds the 'profile' scope.
        /// </summary>
        /// <returns></returns>
        public TokenRequest AddProfileScope()
        {
            return AddScope("profile");
        }

        /// <summary>
        /// Adds the 'offline_access' scope.
        /// </summary>
        /// <returns></returns>
        public TokenRequest AddOfflineAccessScope()
        {
            return AddScope("offline_access");
        }

        /// <summary>
        /// Adds a scope, prefixing 'https://vault.azure.net/'.
        /// </summary>
        /// <param name="scope"></param>
        /// <returns></returns>
        public TokenRequest AddAzureKeyVaultScope(string scope)
        {
            return AddScope($"https://vault.azure.net/{scope}");
        }

        /// <summary>
        /// Adds scopes, prefixing 'https://vault.azure.net/'.
        /// </summary>
        /// <param name="scopes"></param>
        /// <returns></returns>
        public TokenRequest AddAzureKeyVaultScopes(params string[] scopes)
        {
            return AddScopes(scopes.Select(x => $"https://vault.azure.net/{x}").ToArray());
        }

        /// <summary>
        /// Adds a scope, prefixing 'https://graph.microsoft.com/'.
        /// </summary>
        /// <param name="scope"></param>
        /// <returns></returns>
        public TokenRequest AddMicrosoftGraphScope(string scope)
        {
            return AddScope($"https://graph.microsoft.com/{scope}");
        }

        /// <summary>
        /// Adds scopes, prefixing 'https://graph.microsoft.com/'.
        /// </summary>
        /// <param name="scopes"></param>
        /// <returns></returns>
        public TokenRequest AddMicrosoftGraphScopes(params string[] scopes)
        {
            return AddScopes(scopes.Select(x => $"https://graph.microsoft.com/{x}").ToArray());
        }

        /// <summary>
        /// Adds a scope, prefixing 'https://outlook.office.com/'.
        /// </summary>
        /// <param name="scope"></param>
        /// <returns></returns>
        public TokenRequest AddOffice365MailScope(string scope)
        {
            return AddScope($"https://outlook.office.com/{scope}");
        }

        /// <summary>
        /// Adds scopes, prefixing 'https://outlook.office.com/'.
        /// </summary>
        /// <param name="scopes"></param>
        /// <returns></returns>
        public TokenRequest AddOffice365MailScope(params string[] scopes)
        {
            return AddScopes(scopes.Select(x => $"https://outlook.office.com/{x}").ToArray());
        }

        /// <summary>
        /// Adds the specified scope.
        /// </summary>
        /// <param name="scope"></param>
        /// <returns></returns>
        public TokenRequest AddScope(string scope)
        {
            mScopes.Add(scope);
            return this;
        }

        /// <summary>
        /// Adds the specified scopes.
        /// </summary>
        /// <param name="scopes"></param>
        /// <returns></returns>
        public TokenRequest AddScopes(params string[] scopes)
        {
            mScopes.AddRange(scopes);
            return this;
        }

        /// <summary>
        /// Sets grant_type to 'authorization_code'.
        /// </summary>
        /// <returns></returns>
        public TokenRequest WithAuthorizationCodeGrantType()
        {
            return WithGrantType("authorization_code");
        }

        /// <summary>
        /// Sets grant_type to 'client_credentials'.
        /// </summary>
        /// <returns></returns>
        public TokenRequest WithClientCredentialsGrantType()
        {
            return WithGrantType("client_credentials");
        }

        /// <summary>
        /// Sets grant_type to 'refresh_token'.
        /// </summary>
        /// <returns></returns>
        public TokenRequest WithRefreshTokenGrantType()
        {
            return WithGrantType("refresh_token");
        }

        protected TokenRequest WithGrantType(string grantType)
        {
            GrantType = grantType;
            return this;
        }

        /// <summary>
        /// Sets the client_assertion_type to 'urn:ietf:params:oauth:client-assertion-type:jwt-bearer' and client_assertion to the specified value.
        /// </summary>
        /// <param name="clientAssertion"></param>
        /// <returns></returns>
        public TokenRequest WithJwtBearerAssertion(string clientAssertion)
        {
            ClientAssertionType = "urn:ietf:params:oauth:client-assertion-type:jwt-bearer";
            ClientAssertion = clientAssertion;
            return this;
        }

        /// <summary>
        /// Gets the <see cref="Uri"/> this request should be posted to.
        /// </summary>
        public Uri Uri
        {
            get => new Uri($"https://login.microsoftonline.com/{Tenant}/oauth2/v2.0/token");
        }

        /// <summary>
        /// Gets the parameters of this request as a dictionary
        /// </summary>
        /// <returns></returns>
        public IDictionary<string, string> ParameterDictionary
        {
            get 
            {
                Dictionary<string, string> parameterDictionary = new Dictionary<string, string>();

                foreach (FieldInfo fieldInfo in GetType().GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance).Where(x => x.GetCustomAttribute<FormParameterAttribute>() != null))
                {
                    FormParameterAttribute queryParameterAttribute = fieldInfo.GetCustomAttribute<FormParameterAttribute>();

                    if (fieldInfo.GetValue(this) != null)
                    {
                        string name = queryParameterAttribute.Name ?? fieldInfo.Name;
                        string value = fieldInfo.GetValue(this).ToString();

                        parameterDictionary.Add(name, value);
                    }
                }

                foreach (PropertyInfo propertyInfo in GetType().GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance).Where(x => x.GetCustomAttribute<FormParameterAttribute>() != null))
                {
                    FormParameterAttribute queryParameterAttribute = propertyInfo.GetCustomAttribute<FormParameterAttribute>();

                    if (propertyInfo.GetValue(this) != null)
                    {
                        string name = queryParameterAttribute.Name ?? propertyInfo.Name;
                        string value = propertyInfo.GetValue(this).ToString();

                        parameterDictionary.Add(name, value);
                    }
                }

                return parameterDictionary;
            }
        }

        /// <summary>
        /// Creates a new <see cref="TokenRequest"/> for using the authorization code flow.
        /// </summary>
        /// <param name="tenant"></param>
        /// <param name="clientId"></param>
        /// <param name="code"></param>
        /// <param name="redirectUri"></param>
        /// <returns></returns>
        public static TokenRequest ForAuthorizationCodeFlow(string tenant, string clientId, string code, string redirectUri)
        {
            TokenRequest tokenRequest = new TokenRequest()
            {
                Tenant = tenant,
                ClientId = clientId,
                Code = code,
                RedirectUri = redirectUri
            }.WithAuthorizationCodeGrantType();

            return tokenRequest;
        }

        /// <summary>
        /// Creates a new <see cref="TokenRequest"/> for using the client credentials flow.
        /// </summary>
        /// <param name="tenant"></param>
        /// <param name="clientId"></param>
        /// <param name="clientSecret"></param>
        /// <returns></returns>
        public static TokenRequest ForClientCredentialsFlow(string tenant, string clientId, string clientSecret)
        {
            TokenRequest tokenRequest = new TokenRequest()
            {
                Tenant = tenant,
                ClientId = clientId,
                ClientSecret = clientSecret
            }.WithClientCredentialsGrantType();

            return tokenRequest;
        }

        /// <summary>
        /// Creates a new <see cref="TokenRequest"/> for using the refresh token flow.
        /// </summary>
        /// <param name="tenant"></param>
        /// <param name="clientId"></param>
        /// <param name="refreshToken"></param>
        /// <returns></returns>
        public static TokenRequest ForRefreshTokenFlow(string tenant, string clientId, string refreshToken)
        {
            TokenRequest tokenRequest = new TokenRequest()
            {
                Tenant = tenant,
                ClientId = clientId,
                RefreshToken = refreshToken
            }.WithRefreshTokenGrantType();

            return tokenRequest;
        }
    }
}
