﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Universal.Common.Serialization;

namespace Universal.Microsoft.Authentication.V2
{
    /// <summary>
    /// Token provider that uses an initial <see cref="V2.TokenResponse"/> to continuously refresh and can be serialized and deserialized to resume operation.
    /// </summary>
    public class RefreshingTokenProvider : JsonSerializable<RefreshingTokenProvider>, ITokenProvider
    {
        [JsonProperty("tenant")]
        public string Tenant { get; set; }
        [JsonProperty("clientId")]
        public string ClientId { get; set; }
        [JsonProperty("clientSecret")]
        public string ClientSecret { get; set; }
        [JsonProperty("scopes")]
        public List<string> Scopes { get; set; }
        [JsonProperty("tokenResponse")]
        public TokenResponse TokenResponse { get; set; }

        /// <summary>
        /// Instantiates a new instance of the <see cref="RefreshingTokenProvider"/> by providing an initial <see cref="V2.TokenResponse"/>.
        /// </summary>
        /// <param name="tenant"></param>
        /// <param name="clientId"></param>
        /// <param name="clientSecret"></param>
        /// <param name="scopes"></param>
        /// <param name="tokenResponse"></param>
        public RefreshingTokenProvider(string tenant, string clientId, string clientSecret, IEnumerable<string> scopes, TokenResponse tokenResponse)
        {
            Tenant = tenant;
            ClientId = clientId;
            ClientSecret = clientSecret;
            Scopes = scopes.ToList();
            TokenResponse = tokenResponse;
        }

        /// <summary>
        /// Returns a token.
        /// </summary>
        /// <returns></returns>
        public async Task<TokenResponse> GetTokenAsync()
        {
            return await GetTokenAsync(default).ConfigureAwait(false);
        }

        /// <summary>
        /// Returns a token.
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<TokenResponse> GetTokenAsync(CancellationToken cancellationToken)
        {
            JwtSecurityToken jwtSecurityToken = new JwtSecurityToken(TokenResponse.AccessToken);
            if (DateTime.UtcNow + TimeSpan.FromMinutes(5) >= jwtSecurityToken.ValidTo)
            {
                using (AuthenticationClient authenticationClient = new AuthenticationClient(Tenant, ClientId, ClientSecret))
                {
                    TokenResponse = await authenticationClient.GetTokenByRefreshTokenAsync(TokenResponse.RefreshToken, Scopes).ConfigureAwait(false);
                }
            }

            return TokenResponse;
        }
    }
}
