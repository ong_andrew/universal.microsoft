﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Universal.Common.Net.Http;
using UriBuilder = Universal.Common.UriBuilder;

namespace Universal.Microsoft.Authentication.V2
{
    /// <summary>
    /// Class to interact with the authentication endpoints provided by Azure AD.
    /// </summary>
    public class AuthenticationClient : JsonHttpServiceClient
    {
        private string mTenant;
        private string mClientId;
        private string mClientSecret;
        private string mRedirectUri;
        
        /// <summary>
        /// Creates a new instance of the <see cref="AuthenticationClient"/> class.
        /// </summary>
        /// <param name="tenant"></param>
        /// <param name="clientId"></param>
        /// <param name="clientSecret"></param>
        /// <param name="redirectUri">Recommended to be specified, but may be omitted if only client credentials are used.</param>
        public AuthenticationClient(string tenant, string clientId, string clientSecret, string redirectUri = null)
        {
            mTenant = tenant;
            mClientId = clientId;
            mClientSecret = clientSecret;
            mRedirectUri = redirectUri;
        }

        protected override Task HandleNonSuccessCodeAsync(HttpResponseMessage httpResponseMessage, CancellationToken cancellationToken)
        {
            if (httpResponseMessage.StatusCode == System.Net.HttpStatusCode.BadRequest)
            {
                throw new AuthenticationException(httpResponseMessage);
            }
            else
            {
                throw new HttpException(httpResponseMessage);
            }
        }

        /// <summary>
        /// Creates a <see cref="Uri"/> that can be used to initiate the authorization code flow.
        /// </summary>
        /// <param name="scopes"></param>
        /// <param name="optionalParameters"></param>
        /// <returns></returns>
        public Uri CreateAuthorizationCodeUri(IEnumerable<string> scopes, Dictionary<string, string> optionalParameters = null)
        {
            AuthorizeUriBuilder authorizeUriBuilder = new AuthorizeUriBuilder()
            {
                Tenant = mTenant,
                ClientId = mClientId,
                Scope = string.Join(" ", scopes),
                RedirectUri = mRedirectUri
            }.AddCodeResponseType();

            UriBuilder uriBuilder = new UriBuilder(authorizeUriBuilder.Uri);
            if (optionalParameters != null)
            {
                foreach (KeyValuePair<string, string> optionalParameter in optionalParameters)
                {
                    uriBuilder.AddQuery(optionalParameter.Key, optionalParameter.Value);
                }
            }
            return uriBuilder.Uri;
        }

        /// <summary>
        /// Creates a <see cref="Uri"/> that can be used to initiate the implicit flow.
        /// </summary>
        /// <param name="requestIdToken"></param>
        /// <param name="requestAccessToken"></param>
        /// <param name="scopes"></param>
        /// <param name="optionalParameters"></param>
        /// <returns></returns>
        public Uri CreateImplicitFlowAuthorizeUri(bool requestIdToken, bool requestAccessToken, IEnumerable<string> scopes, Dictionary<string, string> optionalParameters = null)
        {
            AuthorizeUriBuilder authorizeUriBuilder = new AuthorizeUriBuilder()
            {
                Tenant = mTenant,
                ClientId = mClientId,
                Scope = string.Join(" ", scopes),
                RedirectUri = mRedirectUri
            };

            if (requestIdToken)
            {
                authorizeUriBuilder.AddIdTokenResponseType();
            }

            if (requestAccessToken)
            {
                authorizeUriBuilder.AddTokenResponseType();
            }

            UriBuilder uriBuilder = new UriBuilder(authorizeUriBuilder.Uri);
            if (optionalParameters != null)
            {
                foreach (KeyValuePair<string, string> optionalParameter in optionalParameters)
                {
                    uriBuilder.AddQuery(optionalParameter.Key, optionalParameter.Value);
                }
            }
            return uriBuilder.Uri;
        }

        /// <summary>
        /// Retrieves a <see cref="TokenResponse"/> by exchanging an authorization code.
        /// </summary>
        /// <param name="authorizationCode"></param>
        /// <param name="scopes"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<TokenResponse> GetTokenByAuthorizationCodeAsync(string authorizationCode, IEnumerable<string> scopes, CancellationToken cancellationToken = default)
        {
            return await PostAsync<TokenResponse>(new Uri($"https://login.microsoftonline.com/{mTenant}/oauth2/v2.0/token"), new FormUrlEncodedContent(new Dictionary<string, string>
            {
                ["grant_type"] = "authorization_code",
                ["client_id"] = mClientId,
                ["code"] = authorizationCode,
                ["redirect_uri"] = mRedirectUri,
                ["client_secret"] = mClientSecret,
                ["scope"] = string.Join(" ", scopes)
            }), cancellationToken: cancellationToken).ConfigureAwait(false);
        }

        /// <summary>
        /// Retrieves a <see cref="TokenResponse"/> by exchanging a refresh token.
        /// </summary>
        /// <param name="refreshToken"></param>
        /// <param name="scopes"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<TokenResponse> GetTokenByRefreshTokenAsync(string refreshToken, IEnumerable<string> scopes, CancellationToken cancellationToken = default)
        {
            return await PostAsync<TokenResponse>(new Uri($"https://login.microsoftonline.com/{mTenant}/oauth2/v2.0/token"), new FormUrlEncodedContent(new Dictionary<string, string>
            {
                ["grant_type"] = "refresh_token",
                ["client_id"] = mClientId,
                ["refresh_token"] = refreshToken,
                ["client_secret"] = mClientSecret,
                ["redirect_uri"] = mRedirectUri,
                ["scope"] = string.Join(" ", scopes)
            }), cancellationToken: cancellationToken).ConfigureAwait(false);
        }

        /// <summary>
        /// Retrieves a <see cref="TokenResponse"/> by providing client credentials.
        /// </summary>
        /// <param name="scopes"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<TokenResponse> GetTokenByClientCredentialsAsync(IEnumerable<string> scopes, CancellationToken cancellationToken = default)
        {
            return await PostAsync<TokenResponse>(new Uri($"https://login.microsoftonline.com/{mTenant}/oauth2/v2.0/token"), new FormUrlEncodedContent(new Dictionary<string, string>
            {
                ["grant_type"] = "client_credentials",
                ["client_id"] = mClientId,
                ["client_secret"] = mClientSecret,
                ["scope"] = string.Join(" ", scopes)
            }), cancellationToken: cancellationToken).ConfigureAwait(false);
        }

        protected override Task<JsonContent> SerializeAsync(object requestBody, CancellationToken cancellationToken)
        {
            return Task.FromResult(new JsonContent(JsonConvert.SerializeObject(requestBody)));
        }

        protected override async Task<T> DeserializeAsync<T>(HttpContent httpContent, CancellationToken cancellationToken)
        {
            return JsonConvert.DeserializeObject<T>(await httpContent.ReadAsStringAsync().ConfigureAwait(false));
        }
    }
}
