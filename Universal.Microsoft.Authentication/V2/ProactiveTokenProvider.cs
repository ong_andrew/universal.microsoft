﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using Timer = System.Timers.Timer;

namespace Universal.Microsoft.Authentication.V2
{
    /// <summary>
    /// A token provider that actively retrieves and refreshes token given a fixed refresh interval.
    /// </summary>
    public class ProactiveTokenProvider : ITokenProvider, IDisposable
    {
        protected string mTenant;
        protected string mClientId;
        protected string mClientSecret;
        protected IEnumerable<string> mScopes;

        protected DateTime mLastRetrieved;
        protected TokenResponse mToken;
        protected bool mHasValidToken;
        protected Timer mTimer;

        /// <summary>
        /// Initializes a new instance of <see cref="ProactiveTokenProvider"/>.
        /// </summary>
        /// <param name="tenant"></param>
        /// <param name="clientId"></param>
        /// <param name="clientSecret"></param>
        /// <param name="scopes"></param>
        /// <param name="refreshInterval"></param>
        public ProactiveTokenProvider(string tenant, string clientId, string clientSecret, IEnumerable<string> scopes = null, long refreshInterval = 45 * 60 * 1000)
        {
            mTenant = tenant;
            mClientId = clientId;
            mClientSecret = clientSecret;
            mScopes = scopes ?? new List<string>() { "https://graph.microsoft.com/.default" };

            mHasValidToken = false;
            mTimer = new Timer(refreshInterval);
            mTimer.AutoReset = true;
            mTimer.Elapsed += OnTimerElapsed;
            mTimer.Start();
        }

        /// <summary>
        /// Retrieves a token from the cache if it is still valid, otherwise retrieves a new one asynchronously.
        /// </summary>
        /// <returns></returns>
        public async Task<TokenResponse> GetTokenAsync()
        {
            return await GetTokenAsync(default).ConfigureAwait(false);
        }

        /// <summary>
        /// Retrieves a token from the cache if it is still valid, otherwise retrieves a new one asynchronously.
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<TokenResponse> GetTokenAsync(CancellationToken cancellationToken)
        {
            if (mHasValidToken)
            {
                return new TokenResponse(mToken, mLastRetrieved);
            }
            else
            {
                mToken = await RetrieveTokenAsync(cancellationToken).ConfigureAwait(false);

                return new TokenResponse(mToken, mLastRetrieved);
            }
        }

        protected async Task<TokenResponse> RetrieveTokenAsync(CancellationToken cancellationToken = default)
        {
            mHasValidToken = false;
            mLastRetrieved = DateTime.UtcNow;
            using (AuthenticationClient authenticationClient = new AuthenticationClient(mTenant, mClientId, mClientSecret))
            {
                TokenResponse token = await authenticationClient.GetTokenByClientCredentialsAsync(mScopes, cancellationToken).ConfigureAwait(false);
                mHasValidToken = true;

                return token;
            }
        }

        protected async void OnTimerElapsed(object sender, ElapsedEventArgs eventArgs)
        {
            mToken = await RetrieveTokenAsync().ConfigureAwait(false);
        }

        #region IDisposable Support
        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    mTimer.Elapsed -= OnTimerElapsed;
                    mTimer.Stop();
                    mTimer.Dispose();
                    mTimer = null;
                }
                
                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }
        #endregion
    }
}
