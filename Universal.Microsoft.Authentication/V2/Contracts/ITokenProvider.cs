﻿using System.Threading;
using System.Threading.Tasks;

namespace Universal.Microsoft.Authentication.V2
{
    /// <summary>
    /// Represents an object that is able to provide an access token.
    /// </summary>
    public interface ITokenProvider
    {
        /// <summary>
        /// Retrieves a token asynchronously.
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task<TokenResponse> GetTokenAsync(CancellationToken cancellationToken = default);
    }
}
