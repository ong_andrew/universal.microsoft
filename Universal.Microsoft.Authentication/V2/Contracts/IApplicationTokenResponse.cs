﻿namespace Universal.Microsoft.Authentication.V2
{
    /// <summary>
    /// Represents an application security token response.
    /// </summary>
    public interface IApplicationTokenResponse : ITokenResponse
    {
    }
}
