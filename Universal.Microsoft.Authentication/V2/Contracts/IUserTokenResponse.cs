﻿using System.Collections.Generic;

namespace Universal.Microsoft.Authentication.V2
{
    /// <summary>
    /// Represents a user security token response.
    /// </summary>
    public interface IUserTokenResponse : ITokenResponse
    {
        /// <summary>
        /// The scopes that this token is valid for.
        /// </summary>
        List<string> Scopes { get; set; }
        /// <summary>
        /// The resource that this token is valid for.
        /// </summary>
        string Resource { get; set; }
        /// <summary>
        /// The associated refresh token that can be used to obtain a new access token.
        /// </summary>
        string RefreshToken { get; set; }
        /// <summary>
        /// The associated ID token for id_token flows.
        /// </summary>
        string IdToken { get; set; }
    }
}
