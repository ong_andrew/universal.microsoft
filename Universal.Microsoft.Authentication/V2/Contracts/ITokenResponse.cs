﻿namespace Universal.Microsoft.Authentication.V2
{
    /// <summary>
    /// Represents a security token response.
    /// </summary>
    public interface ITokenResponse
    {
        /// <summary>
        /// The type of token.
        /// </summary>
        string Type { get; set; }
        /// <summary>
        /// The number of seconds that the token will expire in.
        /// </summary>
        int ExpiresIn { get; set; }
        /// <summary>
        /// 
        /// </summary>
        int ExtExpiresIn { get; set; }
        /// <summary>
        /// The access token associated with this token.
        /// </summary>
        string AccessToken { get; set; }
    }
}
