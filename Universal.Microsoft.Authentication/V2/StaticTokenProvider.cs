﻿using System.Threading;
using System.Threading.Tasks;

namespace Universal.Microsoft.Authentication.V2
{
    /// <summary>
    /// A simple token provider that returns access token specified on construction.
    /// </summary>
    public class StaticTokenProvider : ITokenProvider
    {
        protected TokenResponse mToken;

        /// <summary>
        /// Initializes a new instance of <see cref="StaticTokenProvider"/> using the given access token.
        /// </summary>
        /// <param name="accessToken"></param>
        public StaticTokenProvider(string accessToken)
        {
            mToken = new TokenResponse()
            {
                AccessToken = accessToken,
                Type = "Bearer"
            };
        }

        /// <summary>
        /// Initializes a new instance of <see cref="StaticTokenProvider"/> using the given security token.
        /// </summary>
        /// <param name="token"></param>
        public StaticTokenProvider(TokenResponse token)
        {
            mToken = token;
        }
        
        /// <summary>
        /// Returns the token provided on construction.
        /// </summary>
        /// <returns></returns>
        public Task<TokenResponse> GetTokenAsync()
        {
            return Task.FromResult(mToken);
        }

        /// <summary>
        /// Returns the token provided on construction.
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task<TokenResponse> GetTokenAsync(CancellationToken cancellationToken)
        {
            return Task.FromResult(mToken);
        }
    }
}
