﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Universal.Microsoft.Authentication.V2
{
    /// <summary>
    /// A token provider that passively retrieves and refreshes access tokens when they expire.
    /// </summary>
    public class PassiveTokenProvider : ITokenProvider
    {
        protected string mTenant;
        protected string mClientId;
        protected string mClientSecret;
        protected IEnumerable<string> mScopes;

        protected DateTime mLastRetrieved;
        protected TokenResponse mToken;

        /// <summary>
        /// Initializes a new instance of the <see cref="PassiveTokenProvider"/> class.
        /// </summary>
        /// <param name="tenant"></param>
        /// <param name="clientId"></param>
        /// <param name="clientSecret"></param>
        /// <param name="scopes"></param>
        public PassiveTokenProvider(string tenant, string clientId, string clientSecret, IEnumerable<string> scopes = null)
        {
            mTenant = tenant;
            mClientId = clientId;
            mClientSecret = clientSecret;
            mScopes = scopes ?? new List<string>() { "https://graph.microsoft.com/.default" };
        }

        /// <summary>
        /// Returns a valid <see cref="TokenResponse"/>, retrieving one asynchronously if it has expired.
        /// </summary>
        /// <returns></returns>
        public async Task<TokenResponse> GetTokenAsync()
        {
            return await GetTokenAsync(default).ConfigureAwait(false);
        }

        /// <summary>
        /// Returns a valid <see cref="TokenResponse"/>, retrieving one asynchronously if it has expired.
        /// </summary>
        /// <returns></returns>
        public async Task<TokenResponse> GetTokenAsync(CancellationToken cancellationToken)
        {
            if (mToken == null)
            {
                mToken = await RetrieveTokenAsync().ConfigureAwait(false);
            }

            TokenResponse token = new TokenResponse(mToken, mLastRetrieved);
            if (token.ExpiresIn == 0)
            {
                return await RetrieveTokenAsync(cancellationToken).ConfigureAwait(false);
            }
            else
            {
                return token;
            }
        }

        protected async Task<TokenResponse> RetrieveTokenAsync(CancellationToken cancellationToken = default)
        {
            mLastRetrieved = DateTime.UtcNow;
            using (AuthenticationClient authenticationClient = new AuthenticationClient(mTenant, mClientId, mClientSecret))
            {
                TokenResponse token = await authenticationClient.GetTokenByClientCredentialsAsync(mScopes, cancellationToken).ConfigureAwait(false);

                return token;
            }
        }
    }
}
