﻿using Newtonsoft.Json;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Universal.Common.Net.Http;

namespace Universal.Microsoft.Authentication.V2
{
    /// <summary>
    /// A lightweight implementation of a client that retrieves tokens from the /token endpoint.
    /// </summary>
    public class TokenClient : JsonHttpServiceClient
    {
        private HttpContent ConvertToHttpContent(TokenRequest tokenRequest)
        {
            return new FormUrlEncodedContent(tokenRequest.ParameterDictionary);
        }

        /// <summary>
        /// Gets a token based on the specified parameters.
        /// </summary>
        /// <param name="tokenRequest"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<TokenResponse> GetTokenAsync(TokenRequest tokenRequest, CancellationToken cancellationToken = default)
        {
            return await PostAsync<TokenResponse>(tokenRequest.Uri, ConvertToHttpContent(tokenRequest), cancellationToken).ConfigureAwait(false);
        }

        protected override Task HandleNonSuccessCodeAsync(HttpResponseMessage httpResponseMessage, CancellationToken cancellationToken)
        {
            throw new HttpException(httpResponseMessage);
        }

        protected override Task<JsonContent> SerializeAsync(object requestBody, CancellationToken cancellationToken)
        {
            return Task.FromResult(new JsonContent(JsonConvert.SerializeObject(requestBody)));
        }

        protected override async Task<T> DeserializeAsync<T>(HttpContent httpContent, CancellationToken cancellationToken)
        {
            return JsonConvert.DeserializeObject<T>(await httpContent.ReadAsStringAsync().ConfigureAwait(false));
        }
    }
}
