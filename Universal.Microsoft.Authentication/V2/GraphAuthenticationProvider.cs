﻿using Microsoft.Graph;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace Universal.Microsoft.Authentication.V2
{
    /// <summary>
    /// Authentication provider for HTTP requests to Microsoft Graph
    /// </summary>
    public class GraphAuthenticationProvider : IAuthenticationProvider
    {
        protected ITokenProvider mTokenProvider;

        /// <summary>
        /// Initializes a new instance of the <see cref="GraphAuthenticationProvider"/> with the specified <see cref="ITokenProvider"/>.
        /// </summary>
        /// <param name="applicationTokenProvider"></param>
        public GraphAuthenticationProvider(ITokenProvider applicationTokenProvider)
        {
            mTokenProvider = applicationTokenProvider;
        }

        /// <summary>
        /// Adds authentication headers to a given <see cref="HttpRequestMessage"/>.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task AuthenticateRequestAsync(HttpRequestMessage request)
        {
            TokenResponse token = await mTokenProvider.GetTokenAsync(default).ConfigureAwait(false);

            request.Headers.Authorization = new AuthenticationHeaderValue(token.Type, token.AccessToken);
        }
    }
}
