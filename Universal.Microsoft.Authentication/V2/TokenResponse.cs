﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Universal.Common;
using Universal.Common.Serialization;

namespace Universal.Microsoft.Authentication.V2
{
    /// <summary>
    /// Represents a security token.
    /// </summary>
    public class TokenResponse : JsonSerializable<TokenResponse>, IApplicationTokenResponse, IUserTokenResponse
    {
        /// <summary>
        /// The type of token.
        /// </summary>
        [JsonProperty("token_type")]
        public string Type { get; set; }
        /// <summary>
        /// The remaining lifetime of the token, in seconds.
        /// </summary>
        [JsonProperty("expires_in")]
        public int ExpiresIn { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("ext_expires_in")]
        public int ExtExpiresIn { get; set; }
        /// <summary>
        /// The associated access token.
        /// </summary>
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeAccessToken() { return !AccessToken.IsNullOrEmpty(); }
        /// <summary>
        /// The associated ID token.
        /// </summary>
        [JsonProperty("id_token")]
        public string IdToken { get; set; }
        [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeIdToken() { return !IdToken.IsNullOrEmpty(); }
        /// <summary>
        /// The associated refresh token.
        /// </summary>
        [JsonProperty("refresh_token")]
        public string RefreshToken { get; set; }
        [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeRefreshToken() { return !RefreshToken.IsNullOrEmpty(); }

        /// <summary>
        /// The scopes this token is valid for.
        /// </summary>
        [JsonIgnore]
        public List<string> Scopes { get; set; }

        [JsonProperty("scope")]
        protected string Scope
        {
            get
            {
                return Scopes == null ? string.Empty : string.Join(" ", Scopes);
            }
            set
            {
                Scopes = value.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries).ToList();
            }
        }
        /// <summary>
        /// The resource this token is valid for.
        /// </summary>
        [JsonProperty("resource")]
        public string Resource { get; set; }
        [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeResource() { return !Resource.IsNullOrEmpty(); }

        /// <summary>
        /// The <see cref="DateTime"/> that the token will expire on.
        /// </summary>
        [JsonIgnore]
        public DateTime? ExpiresOn { get; set; }
        /// <summary>
        /// A dummy property for the serializer to set and get.
        /// </summary>
        [JsonProperty("expires_on")]
        protected int? ExpiresOnInt
        {
            get
            {
                return ExpiresOn.HasValue ? ExpiresOn.Value.ToUnixTimestamp() : (int?)null;
            }
            set
            {
                if (value == default)
                {
                    ExpiresOn = null;
                }
                else
                {
                    ExpiresOn = value.Value.ToDateTime();
                }
            }
        }
        [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeExpiresOnInt() { return ExpiresOnInt != null; }

        /// <summary>
        /// The <see cref="DateTime"/> that the token should not be used before.
        /// </summary>
        [JsonIgnore]
        public DateTime? NotBefore { get; set; }
        /// <summary>
        /// A dummy property for the serializer to set and get.
        /// </summary>
        [JsonProperty("not_before")]
        protected int? NotBeforeInt
        {
            get
            {
                return NotBefore.HasValue ? ExpiresOn.Value.ToUnixTimestamp() : (int?)null;
            }
            set
            {
                if (value == default)
                {
                    NotBefore = null;
                }
                else
                {
                    NotBefore = value.Value.ToDateTime();
                }
            }
        }
        [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeNotBeforeInt() { return NotBeforeInt != null; }

        /// <summary>
        /// Initializes a new instance of the <see cref="TokenResponse"/> class.
        /// </summary>
        public TokenResponse()
        {
        }

        internal TokenResponse(TokenResponse token, DateTime dateTimeRetrieved)
        {
            Type = token.Type;
            AccessToken = token.AccessToken;

            ExpiresIn = Math.Max(0, token.ExpiresIn - ((int)(DateTime.UtcNow - dateTimeRetrieved).TotalSeconds));
            ExtExpiresIn = Math.Max(0, token.ExtExpiresIn - ((int)(DateTime.UtcNow - dateTimeRetrieved).TotalSeconds));
        }

        /// <summary>
        /// Returns this as an <see cref="IApplicationTokenResponse"/>.
        /// </summary>
        /// <returns></returns>
        public IApplicationTokenResponse AsApplicationTokenResponse()
        {
            return this;
        }

        /// <summary>
        /// Returns this as an <see cref="IUserTokenResponse"/>.
        /// </summary>
        /// <returns></returns>
        public IUserTokenResponse AsUserTokenResponse()
        {
            return this;
        }
    }
}
