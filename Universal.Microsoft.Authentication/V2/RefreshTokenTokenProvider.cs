﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Universal.Microsoft.Authentication.V2
{
    /// <summary>
    /// A <see cref="ITokenProvider"/> that uses a refresh token.
    /// </summary>
    public class RefreshTokenTokenProvider : ITokenProvider, IDisposable
    {
        private TokenClient TokenClient { get; set; }
        /// <summary>
        /// Gets a <see cref="DateTimeOffset"/> indicating when the last access token was retrieved.
        /// </summary>
        public DateTimeOffset? LastRetrieved { get; private set; }
        /// <summary>
        /// Gets the most recent <see cref="TokenResponse"/>.
        /// </summary>
        public TokenResponse TokenResponse { get; private set; }
        private string Tenant { get; set; }
        private string ClientId { get; set; }
        private string ClientSecret { get; set; }
        /// <summary>
        /// Gets the current value of the refresh token.
        /// </summary>
        public string RefreshToken { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="RefreshTokenTokenProvider"/> class.
        /// </summary>
        /// <param name="tenant"></param>
        /// <param name="clientId"></param>
        /// <param name="refreshToken"></param>
        public RefreshTokenTokenProvider(string tenant, string clientId, string refreshToken) : this(tenant, clientId, null, refreshToken) { }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="tenant"></param>
        /// <param name="clientId"></param>
        /// <param name="clientSecret"></param>
        /// <param name="refreshToken"></param>
        public RefreshTokenTokenProvider(string tenant, string clientId, string clientSecret, string refreshToken)
        {
            if (tenant == null)
            {
                throw new ArgumentNullException(nameof(tenant));
            }

            if (clientId == null)
            {
                throw new ArgumentNullException(nameof(clientId));
            }

            if (refreshToken == null)
            {
                throw new ArgumentNullException(nameof(refreshToken));
            }

            Tenant = tenant;
            ClientId = clientId;
            ClientSecret = clientSecret;
            RefreshToken = refreshToken;
            TokenClient = new TokenClient();
        }

        /// <summary>
        /// Gets a token.
        /// </summary>
        /// <returns></returns>
        public async Task<TokenResponse> GetTokenAsync()
        {
            return await GetTokenAsync(default).ConfigureAwait(false);
        }

        /// <summary>
        /// Gets a token.
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<TokenResponse> GetTokenAsync(CancellationToken cancellationToken)
        {
            if (LastRetrieved == null || (DateTimeOffset.Now - LastRetrieved.Value).TotalMinutes > 45)
            {
                TokenRequest tokenRequest = TokenRequest.ForRefreshTokenFlow(Tenant, ClientId, RefreshToken);
                if (ClientSecret != null)
                {
                    tokenRequest.ClientSecret = ClientSecret;
                }
                TokenResponse = await TokenClient.GetTokenAsync(tokenRequest, cancellationToken).ConfigureAwait(false);
                LastRetrieved = DateTimeOffset.Now;
                RefreshToken = TokenResponse.RefreshToken;
            }

            return TokenResponse;
        }

        public void Dispose()
        {
            ((IDisposable)TokenClient).Dispose();
        }
    }
}
