﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Reflection;
using Universal.Common;
using UriBuilder = Universal.Common.UriBuilder;

namespace Universal.Microsoft.Authentication.V2
{
    /// <summary>
    /// Class to assist with constructing valid <see cref="Uri"/> for the authorize endpoint.
    /// </summary>
    public class AuthorizeUriBuilder
    {
        /// <summary>
        /// The {tenant} value in the path of the request can be used to control who can sign into the application. The allowed values are common, organizations, consumers, and tenant identifiers. For more detail, see protocol basics.
        /// </summary>
        public string Tenant { get; set; }
        /// <summary>
        /// The Application (client) ID that the Azure portal – App registrations experience assigned to your app.
        /// </summary>
        [QueryParameter("client_id")]
        public string ClientId { get; set; }
        private ISet<string> mResponseTypes { get; set; } = new HashSet<string>();
        /// <summary>
        /// Must include code for the authorization code flow. Can also include id_token or token if using the hybrid flow.
        /// </summary>
        [QueryParameter("response_type")]
        public string ResponseType
        {
            get
            {
                if (mResponseTypes != null && mResponseTypes.Any())
                {
                    return string.Join(" ", mResponseTypes);
                }
                else
                {
                    return null;
                }
            }
            set
            {
                if (value != null)
                {
                    mResponseTypes = IEnumerableExtensions.ToHashSet(value.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries));
                }
                else
                {
                    mResponseTypes.Clear();
                }
            }
        }
        /// <summary>
        /// The redirect_uri of your app, where authentication responses can be sent and received by your app. It must exactly match one of the redirect_uris you registered in the portal, except it must be url encoded. For native &amp; mobile apps, you should use one of the recommended values - https://login.microsoftonline.com/common/oauth2/nativeclient (for apps using embedded browsers) or http://localhost (for apps that use system browsers).
        /// </summary>
        [QueryParameter("redirect_uri")]
        public string RedirectUri { get; set; }
        private ISet<string> mScopes { get; set; } = new HashSet<string>();
        /// <summary>
        /// A space-separated list of scopes that you want the user to consent to. For the /authorize leg of the request, this can cover multiple resources, allowing your app to get consent for multiple web APIs you want to call.
        /// </summary>
        [QueryParameter("scope")]
        public string Scope
        {
            get
            {
                if (mScopes != null && mScopes.Any())
                {
                    return string.Join(" ", mScopes);
                }
                else
                {
                    return null;
                }
            }
            set
            {
                if (value != null)
                {
                    mScopes = IEnumerableExtensions.ToHashSet(value.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries));
                }
                else
                {
                    mScopes.Clear();
                }
            }
        }

        /// <summary>
        /// Specifies the method that should be used to send the resulting token back to your app. Can be one of the following:
        ///
        /// - query
        /// - fragment
        /// - form_post
        ///
        /// query provides the code as a query string parameter on your redirect URI.If you're requesting an ID token using the implicit flow, you can't use query as specified in the OpenID spec.If you're requesting just the code, you can use query, fragment, or form_post. form_post executes a POST containing the code to your redirect URI.
        /// </summary>
        [QueryParameter("response_mode")]
        public string ResponseMode { get; set; }
        /// <summary>
        /// A value included in the request that will also be returned in the token response. It can be a string of any content that you wish. A randomly generated unique value is typically used for preventing cross-site request forgery attacks. The value can also encode information about the user's state in the app before the authentication request occurred, such as the page or view they were on.
        /// </summary>
        [QueryParameter("state")]
        public string State { get; set; }

        /// <summary>
        /// Indicates the type of user interaction that is required. The only valid values at this time are login, none, consent, and select_account.
        /// </summary>
        [QueryParameter("prompt")]
        public string Prompt { get; set; }
        /// <summary>
        /// Can be used to pre-fill the username/email address field of the sign-in page for the user, if you know their username ahead of time. Often apps will use this parameter during re-authentication, having already extracted the username from a previous sign-in using the preferred_username claim.
        /// </summary>
        [QueryParameter("login_hint")]
        public string LoginHint { get; set; }
        /// <summary>
        /// If included, it will skip the email-based discovery process that user goes through on the sign-in page, leading to a slightly more streamlined user experience - for example, sending them to their federated identity provider. Often apps will use this parameter during re-authentication, by extracting the tid from a previous sign-in. If the tid claim value is 9188040d-6c67-4c5b-b112-36a304b66dad, you should use domain_hint=consumers. Otherwise, use domain_hint=organizations.
        /// </summary>
        [QueryParameter("domain_hint")]
        public string DomainHint { get; set; }

        /// <summary>
        /// Used to secure authorization code grants via Proof Key for Code Exchange (PKCE). Required if code_challenge_method is included. For more information, see the PKCE RFC. This is now recommended for all application types - both public and confidential clients - and required by the Microsoft identity platform for single page apps using the authorization code flow.
        /// </summary>
        [QueryParameter("code_challenge")]
        public string CodeChallenge { get; set; }
        /// <summary>
        /// The method used to encode the code_verifier for the code_challenge parameter. This SHOULD be S256, but the spec allows the use of plain if for some reason the client cannot support SHA256.
        /// </summary>
        [QueryParameter("code_challenge_method")]
        public string CodeChallengeMethod { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="AuthorizeUriBuilder"/> class.
        /// </summary>
        public AuthorizeUriBuilder()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AuthorizeUriBuilder"/> class.
        /// </summary>
        /// <param name="tenant"></param>
        /// <param name="clientId"></param>
        /// <param name="responseType"></param>
        /// <param name="redirectUri"></param>
        /// <param name="scope"></param>
        public AuthorizeUriBuilder(string tenant, string clientId, string responseType, string redirectUri, string scope) : this()
        {
            Tenant = tenant;
            ClientId = clientId;
            ResponseType = responseType;
            RedirectUri = redirectUri;
            Scope = scope;
        }

        /// <summary>
        /// Sets the tenant to 'common'.
        /// </summary>
        /// <returns></returns>
        public AuthorizeUriBuilder WithCommonTenant()
        {
            return WithTenant("common");
        }

        /// <summary>
        /// Sets the tenant to 'organizations'.
        /// </summary>
        /// <returns></returns>
        public AuthorizeUriBuilder WithOrganizationsTenant()
        {
            return WithTenant("organizations");
        }

        /// <summary>
        /// Sets the tenant to 'consumers'.
        /// </summary>
        /// <returns></returns>
        public AuthorizeUriBuilder WithConsumersTenant()
        {
            return WithTenant("consumers");
        }

        protected AuthorizeUriBuilder WithTenant(string tenant)
        {
            Tenant = tenant;
            return this;
        }

        /// <summary>
        /// Adds the 'code' response type.
        /// </summary>
        /// <returns></returns>
        public AuthorizeUriBuilder AddCodeResponseType()
        {
            return AddResponseType("code");
        }

        /// <summary>
        /// Adds the 'id_token' response type.
        /// </summary>
        /// <returns></returns>
        public AuthorizeUriBuilder AddIdTokenResponseType()
        {
            return AddResponseType("id_token");
        }

        /// <summary>
        /// Adds the 'token' response type.
        /// </summary>
        /// <returns></returns>
        public AuthorizeUriBuilder AddTokenResponseType()
        {
            return AddResponseType("token");
        }

        protected AuthorizeUriBuilder AddResponseType(string responseType)
        {
            mResponseTypes.Add(responseType);
            return this;
        }

        /// <summary>
        /// Adds all supported Open ID scopes.
        /// </summary>
        /// <returns></returns>
        public AuthorizeUriBuilder AddOpenIdScopes()
        {
            return AddOpenIdScope()
                .AddEmailScope()
                .AddProfileScope()
                .AddOfflineAccessScope();
        }

        /// <summary>
        /// Adds the 'openid' scope.
        /// </summary>
        /// <returns></returns>
        public AuthorizeUriBuilder AddOpenIdScope()
        {
            return AddScope("openid");
        }

        /// <summary>
        /// Adds the 'email' scope.
        /// </summary>
        /// <returns></returns>
        public AuthorizeUriBuilder AddEmailScope()
        {
            return AddScope("email");
        }

        /// <summary>
        /// Adds the 'profile' scope.
        /// </summary>
        /// <returns></returns>
        public AuthorizeUriBuilder AddProfileScope()
        {
            return AddScope("profile");
        }

        /// <summary>
        /// Adds the 'offline_access' scope.
        /// </summary>
        /// <returns></returns>
        public AuthorizeUriBuilder AddOfflineAccessScope()
        {
            return AddScope("offline_access");
        }

        /// <summary>
        /// Adds a scope, prefixing 'https://vault.azure.net/'.
        /// </summary>
        /// <param name="scope"></param>
        /// <returns></returns>
        public AuthorizeUriBuilder AddAzureKeyVaultScope(string scope)
        {
            return AddScope($"https://vault.azure.net/{scope}");
        }

        /// <summary>
        /// Adds scopes, prefixing 'https://vault.azure.net/'.
        /// </summary>
        /// <param name="scopes"></param>
        /// <returns></returns>
        public AuthorizeUriBuilder AddAzureKeyVaultScopes(params string[] scopes)
        {
            return AddScopes(scopes.Select(x => $"https://vault.azure.net/{x}").ToArray());
        }

        /// <summary>
        /// Adds a scope, prefixing 'https://graph.microsoft.com/'.
        /// </summary>
        /// <param name="scope"></param>
        /// <returns></returns>
        public AuthorizeUriBuilder AddMicrosoftGraphScope(string scope)
        {
            return AddScope($"https://graph.microsoft.com/{scope}");
        }

        /// <summary>
        /// Adds scopes, prefixing 'https://graph.microsoft.com/'.
        /// </summary>
        /// <param name="scopes"></param>
        /// <returns></returns>
        public AuthorizeUriBuilder AddMicrosoftGraphScopes(params string[] scopes)
        {
            return AddScopes(scopes.Select(x => $"https://graph.microsoft.com/{x}").ToArray());
        }

        /// <summary>
        /// Adds a scope, prefixing 'https://outlook.office.com/'.
        /// </summary>
        /// <param name="scope"></param>
        /// <returns></returns>
        public AuthorizeUriBuilder AddOffice365MailScope(string scope)
        {
            return AddScope($"https://outlook.office.com/{scope}");
        }

        /// <summary>
        /// Adds scopes, prefixing 'https://outlook.office.com/'.
        /// </summary>
        /// <param name="scopes"></param>
        /// <returns></returns>
        public AuthorizeUriBuilder AddOffice365MailScope(params string[] scopes)
        {
            return AddScopes(scopes.Select(x => $"https://outlook.office.com/{x}").ToArray());
        }

        /// <summary>
        /// Adds the specified scope.
        /// </summary>
        /// <param name="scope"></param>
        /// <returns></returns>
        public AuthorizeUriBuilder AddScope(string scope)
        {
            mScopes.Add(scope);
            return this;
        }

        /// <summary>
        /// Adds the specified scopes.
        /// </summary>
        /// <param name="scopes"></param>
        /// <returns></returns>
        public AuthorizeUriBuilder AddScopes(params string[] scopes)
        {
            mScopes.AddRange(scopes);
            return this;
        }

        /// <summary>
        /// Sets the response mode to 'query'.
        /// </summary>
        /// <returns></returns>
        public AuthorizeUriBuilder WithQueryResponseMode()
        {
            return WithResponseMode("query");
        }

        /// <summary>
        /// Sets the response mode to 'fragment'.
        /// </summary>
        /// <returns></returns>
        public AuthorizeUriBuilder WithFragmentResponseMode()
        {
            return WithResponseMode("fragment");
        }

        /// <summary>
        /// Sets the response mode to 'form_post'.
        /// </summary>
        /// <returns></returns>
        public AuthorizeUriBuilder WithFormPostResponseMode()
        {
            return WithResponseMode("form_post");
        }

        protected AuthorizeUriBuilder WithResponseMode(string responseMode)
        {
            ResponseMode = responseMode;
            return this;
        }

        /// <summary>
        /// Gets the <see cref="Uri"/> constructed by this <see cref="AuthorizeUriBuilder"/>.
        /// </summary>
        public Uri Uri
        {
            get
            {
                UriBuilder uriBuilder = new UriBuilder($"https://login.microsoftonline.com/{WebUtility.UrlEncode(Tenant)}/oauth2/v2.0/authorize");

                foreach (FieldInfo fieldInfo in GetType().GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance).Where(x => x.GetCustomAttribute<QueryParameterAttribute>() != null))
                {
                    QueryParameterAttribute queryParameterAttribute = fieldInfo.GetCustomAttribute<QueryParameterAttribute>();

                    if (fieldInfo.GetValue(this) != null)
                    {
                        string name = queryParameterAttribute.Name ?? fieldInfo.Name;
                        string value = fieldInfo.GetValue(this).ToString();

                        uriBuilder.AddQuery(name, value);
                    }
                }

                foreach (PropertyInfo propertyInfo in GetType().GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance).Where(x => x.GetCustomAttribute<QueryParameterAttribute>() != null))
                {
                    QueryParameterAttribute queryParameterAttribute = propertyInfo.GetCustomAttribute<QueryParameterAttribute>();

                    if (propertyInfo.GetValue(this) != null)
                    {
                        string name = queryParameterAttribute.Name ?? propertyInfo.Name;
                        string value = propertyInfo.GetValue(this).ToString();

                        uriBuilder.AddQuery(name, value);
                    }
                }

                return uriBuilder.Uri;
            }
        }
    }
}