﻿using System;
using System.Net.Http;
using Universal.Common.Net.Http;

namespace Universal.Microsoft.Authentication.V2
{
    public class AuthenticationException : HttpException<AuthenticationErrorResponse>
    {
        public AuthenticationException()
        {
        }

        public AuthenticationException(string message) : base(message)
        {
        }

        public AuthenticationException(string message, Exception innerException) : base(message, innerException)
        {
        }

        public AuthenticationException(HttpResponseMessage httpResponseMessage) : base(
            (int)httpResponseMessage.StatusCode,
            httpResponseMessage.ReasonPhrase,
            httpResponseMessage.Content != null ? AuthenticationErrorResponse.FromString(httpResponseMessage.Content.ReadAsStringAsync().GetAwaiter().GetResult()) : null)
        {
        }
    }
}
