﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using Universal.Common.Serialization;

namespace Universal.Microsoft.Authentication.V2
{
    public class AuthenticationErrorResponse : JsonSerializable<AuthenticationErrorResponse>
    {
        [JsonProperty("error")]
        public string Error { get; set; }
        [JsonProperty("error_description")]
        public string ErrorDescription { get; set; }
        [JsonProperty("error_codes")]
        public List<int> ErrorCodes { get; set; }
        [JsonProperty("timestamp")]
        public DateTime Timestamp { get; set; }
        [JsonProperty("trace_id")]
        public string TraceId { get; set; }
        [JsonProperty("correlation_id")]
        public string CorrelationId { get; set; }
    }
}
