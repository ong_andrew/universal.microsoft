﻿using Microsoft.Bot.Connector.DirectLine;
using System;

namespace Universal.Microsoft.Bot
{
    public class MessageReceivedArgs : EventArgs
    {
        public ActivitySet ActivitySet { get; }

        public MessageReceivedArgs(ActivitySet aActivitySet)
        {
            ActivitySet = aActivitySet;
        }
    }
}
