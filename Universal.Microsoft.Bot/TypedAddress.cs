﻿using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.Dialogs.Internals;

namespace Universal.Microsoft.Bot
{
    public class TypedAddress : IAddress
    {
        public BotStoreType BotStoreType { get; set; }

        public string BotId { get; set; }
        public string ChannelId { get; set; }
        public string UserId { get; set; }
        public string ConversationId { get; set; }
        public string ServiceUrl { get; set; }

        public TypedAddress(string aBotId, string aChannelId, string aUserId, string aConversationId, string aServiceUrl, BotStoreType aBotStoreType)
        {
            BotId = aBotId;
            ChannelId = aChannelId;
            ConversationId = aConversationId;
            UserId = aUserId;
            ServiceUrl = aServiceUrl;

            BotStoreType = aBotStoreType;
        }

        public Address ToAddress()
        {
            return new Address(BotId, ChannelId, UserId, ConversationId, ServiceUrl);
        }
    }
}
