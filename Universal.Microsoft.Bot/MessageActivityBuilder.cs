﻿using AdaptiveCards;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;
using System.Collections.Generic;

namespace Universal.Microsoft.Bot
{
    public class MessageActivityBuilder : ContextDependentComponent<IDialogContext>
    {
        protected IMessageActivity mMessageActivity;
        public IMessageActivity ToMessageActivity() { return mMessageActivity; }

        public MessageActivityBuilder(IDialogContext context) : base(context)
        {
            mContext = context;
        }

        public MessageActivityBuilder New()
        {
            mMessageActivity = mContext.MakeMessage();

            return this;
        }

        public MessageActivityBuilder Text(string text)
        {
            mMessageActivity.Text = text;

            return this;
        }

        public MessageActivityBuilder AddSuggestedAction(CardAction cardAction)
        {
            if (mMessageActivity.SuggestedActions == null)
            {
                mMessageActivity.SuggestedActions = new SuggestedActions();
            }

            if (mMessageActivity.SuggestedActions.Actions == null)
            {
                mMessageActivity.SuggestedActions.Actions = new List<CardAction>();
            }

            mMessageActivity.SuggestedActions.Actions.Add(cardAction);

            return this;
        }

        public MessageActivityBuilder AddSuggestedActions(ICollection<CardAction> actions)
        {
            foreach (CardAction action in actions)
            {
                AddSuggestedAction(action);
            }

            return this;
        }

        public MessageActivityBuilder AddHeroCard(HeroCard heroCard)
        {
            if (mMessageActivity.Attachments == null)
            {
                mMessageActivity.Attachments = new List<Attachment>();
            }

            mMessageActivity.Attachments.Add(heroCard.ToAttachment());

            return this;
        }

        public MessageActivityBuilder AddAdaptiveCard(AdaptiveCard adaptiveCard)
        {
            if (mMessageActivity.Attachments == null)
            {
                mMessageActivity.Attachments = new List<Attachment>();
            }

            mMessageActivity.Attachments.Add(new Attachment() { Content = adaptiveCard.ToJson(), ContentType = "application/vnd.microsoft.card.adaptive" });

            return this;
        }
    }
}
