﻿using Microsoft.Bot.Builder.Dialogs;

namespace Universal.Microsoft.Bot
{
    public abstract class ContextDependentComponent<T> where T : IDialogContext
    {
        protected T mContext;

        public ContextDependentComponent(T context)
        {
            mContext = context;
        }
    }
}
