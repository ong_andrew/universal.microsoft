﻿namespace Universal.Microsoft.Bot
{
    public static class AttachmentExtensions
    {
        public static global::Microsoft.Bot.Connector.Attachment ToConnectorAttachment(this global::Microsoft.Bot.Connector.DirectLine.Attachment aAttachment)
        {
            return new global::Microsoft.Bot.Connector.Attachment() { Content = aAttachment.Content, ContentType = aAttachment.ContentType, ContentUrl = aAttachment.ContentUrl, Name = aAttachment.Name, ThumbnailUrl = aAttachment.ThumbnailUrl };
        }

        public static global::Microsoft.Bot.Connector.DirectLine.Attachment ToDirectLineAttachment(this global::Microsoft.Bot.Connector.Attachment aAttachment)
        {
            return new global::Microsoft.Bot.Connector.DirectLine.Attachment() { Content = aAttachment.Content, ContentType = aAttachment.ContentType, ContentUrl = aAttachment.ContentUrl, Name = aAttachment.Name, ThumbnailUrl = aAttachment.ThumbnailUrl };
        }
    }
}
