﻿namespace Universal.Microsoft.Bot
{
    public static class ConversationReferenceExtensions
    {
        public static global::Microsoft.Bot.Connector.ConversationReference ToConnectorConversationReference(this global::Microsoft.Bot.Connector.DirectLine.ConversationReference aConversationReference)
        {
            return new global::Microsoft.Bot.Connector.ConversationReference()
            {
                ActivityId = aConversationReference.ActivityId,
                Bot = aConversationReference.Bot.ToConnectorChannelAccount(),
                ChannelId = aConversationReference.ChannelId,
                Conversation = aConversationReference.Conversation.ToConnectorConversationAccount(),
                ServiceUrl = aConversationReference.ServiceUrl,
                User = aConversationReference.User.ToConnectorChannelAccount()
            };
        }

        public static global::Microsoft.Bot.Connector.DirectLine.ConversationReference ToDirectLineConversationReference(this global::Microsoft.Bot.Connector.ConversationReference aConversationReference)
        {
            return new global::Microsoft.Bot.Connector.DirectLine.ConversationReference()
            {
                ActivityId = aConversationReference.ActivityId,
                Bot = aConversationReference.Bot.ToDirectLineChannelAccount(),
                ChannelId = aConversationReference.ChannelId,
                Conversation = aConversationReference.Conversation.ToDirectLineConversationAccount(),
                ServiceUrl = aConversationReference.ServiceUrl,
                User = aConversationReference.User.ToDirectLineChannelAccount()
            };
        }
    }
}
