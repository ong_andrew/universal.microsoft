﻿using System.Linq;

namespace Universal.Microsoft.Bot
{
    public static class SuggestedActionsExtensions
    {
        public static global::Microsoft.Bot.Connector.SuggestedActions ToConnectorSuggestedActions(this global::Microsoft.Bot.Connector.DirectLine.SuggestedActions mSuggestedActions)
        {
            return new global::Microsoft.Bot.Connector.SuggestedActions()
            {
                Actions = (from action in mSuggestedActions.Actions select action.ToConnectorCardAction()).ToList(),
                To = (from suggestion in mSuggestedActions.To select suggestion).ToList()
            };
        }

        public static global::Microsoft.Bot.Connector.DirectLine.SuggestedActions ToDirectLineSuggestedActions(this global::Microsoft.Bot.Connector.SuggestedActions mSuggestedActions)
        {
            return new global::Microsoft.Bot.Connector.DirectLine.SuggestedActions()
            {
                Actions = (from action in mSuggestedActions.Actions select action.ToDirectLineCardAction()).ToList(),
                To = (from suggestion in mSuggestedActions.To select suggestion).ToList()
            };
        }
    }
}
