﻿using System.Collections.Generic;
using Universal.Common.Extensions;

namespace Universal.Microsoft.Bot
{
    public static class StringExtensions
    {
        private static readonly Dictionary<string, string> mDefaultReplacementsForCharactersDisallowedByAzure = new Dictionary<string, string>() { { "/", "|s|" }, { @"\", "|b|" }, { "#", "|h|" }, { "?", "|q|" } };
        private static readonly Dictionary<string, string> mDefaultInverseReplacementsForCharactersDisallowedByAzure = mDefaultReplacementsForCharactersDisallowedByAzure.Inverse();

        public static string SanitizeForAzureKeys(this string input, Dictionary<string, string> replacements = null)
        {
            Dictionary<string, string> replacementMap = replacements ?? mDefaultReplacementsForCharactersDisallowedByAzure;
            
            return input.Trim().Replace(replacementMap);
        }

        public static string DesanitizeForAzureKeys(this string input, Dictionary<string, string> replacements = null)
        {
            Dictionary<string, string> replacementMap = replacements ?? mDefaultInverseReplacementsForCharactersDisallowedByAzure;
            
            return input.Trim().Replace(replacementMap);
        }
    }
}
