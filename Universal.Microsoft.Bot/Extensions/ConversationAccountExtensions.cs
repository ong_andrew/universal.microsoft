﻿namespace Universal.Microsoft.Bot
{
    public static class ConversationAccountExtensions
    {
        public static global::Microsoft.Bot.Connector.ConversationAccount ToConnectorConversationAccount(this global::Microsoft.Bot.Connector.DirectLine.ConversationAccount aConversationAccount)
        {
            return new global::Microsoft.Bot.Connector.ConversationAccount() { Id = aConversationAccount.Id, IsGroup = aConversationAccount.IsGroup, Name = aConversationAccount.Name };
        }

        public static global::Microsoft.Bot.Connector.DirectLine.ConversationAccount ToDirectLineConversationAccount(this global::Microsoft.Bot.Connector.ConversationAccount aConversationAccount)
        {
            return new global::Microsoft.Bot.Connector.DirectLine.ConversationAccount() { Id = aConversationAccount.Id, IsGroup = aConversationAccount.IsGroup, Name = aConversationAccount.Name };
        }
    }
}
