﻿namespace Universal.Microsoft.Bot
{
    public static class CardActionExtensions
    {
        public static global::Microsoft.Bot.Connector.CardAction ToConnectorCardAction(this global::Microsoft.Bot.Connector.DirectLine.CardAction mCardAction)
        {
            return new global::Microsoft.Bot.Connector.CardAction
            {
                Image = mCardAction.Image,
                Title = mCardAction.Title,
                Type = mCardAction.Type,
                Value = mCardAction.Value
            };
        }

        public static global::Microsoft.Bot.Connector.DirectLine.CardAction ToDirectLineCardAction(this global::Microsoft.Bot.Connector.CardAction mCardAction)
        {
            return new global::Microsoft.Bot.Connector.DirectLine.CardAction
            {
                Image = mCardAction.Image,
                Title = mCardAction.Title,
                Type = mCardAction.Type,
                Value = mCardAction.Value
            };
        }
    }
}
