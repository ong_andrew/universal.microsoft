﻿using System.Linq;

namespace Universal.Microsoft.Bot
{
    public static class ActivityExtensions
    {
        public static global::Microsoft.Bot.Connector.Activity ToConnectorActivity(this global::Microsoft.Bot.Connector.DirectLine.Activity aActivity)
        {
            //global::Microsoft.Bot.Connector.Activity result = (global::Microsoft.Bot.Connector.Activity)JsonConvert.DeserializeObject(JsonConvert.SerializeObject(aActivity));
            global::Microsoft.Bot.Connector.Activity result = new global::Microsoft.Bot.Connector.Activity()
            {
                Action = aActivity.Action,
                AttachmentLayout = aActivity.AttachmentLayout,
                Attachments = (from attachment in aActivity.Attachments select attachment.ToConnectorAttachment()).ToList(),
                ChannelData = aActivity.ChannelData,
                ChannelId = aActivity.ChannelId,
                Code = aActivity.Code,
                Conversation = aActivity.Conversation.ToConnectorConversationAccount(),
                Entities = (from entity in aActivity.Entities select entity.ToConnectorEntity()).ToList(),
                From = aActivity.From.ToConnectorChannelAccount(),
                HistoryDisclosed = aActivity.HistoryDisclosed,
                Id = aActivity.Id,
                InputHint = aActivity.InputHint,
                Locale = aActivity.Locale,
                LocalTimestamp = aActivity.LocalTimestamp,
                MembersAdded = (from member in aActivity.MembersAdded select member.ToConnectorChannelAccount()).ToList(),
                MembersRemoved = (from member in aActivity.MembersRemoved select member.ToConnectorChannelAccount()).ToList(),
                Name = aActivity.Name,
                Properties = aActivity.Properties,
                Recipient = aActivity.Recipient.ToConnectorChannelAccount(),
                RelatesTo = aActivity.RelatesTo.ToConnectorConversationReference(),
                ReplyToId = aActivity.ReplyToId,
                ServiceUrl = aActivity.ServiceUrl,
                Speak = aActivity.Speak,
                SuggestedActions = aActivity.SuggestedActions.ToConnectorSuggestedActions(),
                Summary = aActivity.Summary,
                Text = aActivity.Text,
                TextFormat = aActivity.TextFormat,
                Timestamp = aActivity.Timestamp,
                TopicName = aActivity.TopicName,
                Type = aActivity.Type,
                Value = aActivity.Value
            };

            return result;
        }

        public static global::Microsoft.Bot.Connector.DirectLine.Activity ToDirectLineActivity(this global::Microsoft.Bot.Connector.Activity aActivity)
        {
            //global::Microsoft.Bot.Connector.DirectLine.Activity result = (global::Microsoft.Bot.Connector.DirectLine.Activity)JsonConvert.DeserializeObject(JsonConvert.SerializeObject(aActivity));
            global::Microsoft.Bot.Connector.DirectLine.Activity result = new global::Microsoft.Bot.Connector.DirectLine.Activity()
            {
                Action = aActivity.Action,
                AttachmentLayout = aActivity.AttachmentLayout,
                Attachments = (from attachment in aActivity.Attachments select attachment.ToDirectLineAttachment()).ToList(),
                ChannelData = aActivity.ChannelData,
                ChannelId = aActivity.ChannelId,
                Code = aActivity.Code,
                Conversation = aActivity.Conversation.ToDirectLineConversationAccount(),
                Entities = (from entity in aActivity.Entities select entity.ToDirectLineEntity()).ToList(),
                From = aActivity.From.ToDirectLineChannelAccount(),
                HistoryDisclosed = aActivity.HistoryDisclosed,
                Id = aActivity.Id,
                InputHint = aActivity.InputHint,
                Locale = aActivity.Locale,
                LocalTimestamp = aActivity.LocalTimestamp,
                MembersAdded = (from member in aActivity.MembersAdded select member.ToDirectLineChannelAccount()).ToList(),
                MembersRemoved = (from member in aActivity.MembersRemoved select member.ToDirectLineChannelAccount()).ToList(),
                Name = aActivity.Name,
                Properties = aActivity.Properties,
                Recipient = aActivity.Recipient.ToDirectLineChannelAccount(),
                RelatesTo = aActivity.RelatesTo.ToDirectLineConversationReference(),
                ReplyToId = aActivity.ReplyToId,
                ServiceUrl = aActivity.ServiceUrl,
                Speak = aActivity.Speak,
                SuggestedActions = aActivity.SuggestedActions.ToDirectLineSuggestedActions(),
                Summary = aActivity.Summary,
                Text = aActivity.Text,
                TextFormat = aActivity.TextFormat,
                Timestamp = aActivity.Timestamp?.UtcDateTime,
                TopicName = aActivity.TopicName,
                Type = aActivity.Type,
                Value = aActivity.Value
            };

            return result;
        }
    }
}