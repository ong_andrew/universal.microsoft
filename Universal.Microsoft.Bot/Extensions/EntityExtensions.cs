﻿namespace Universal.Microsoft.Bot
{
    public static class EntityExtensions
    {
        public static global::Microsoft.Bot.Connector.Entity ToConnectorEntity(this global::Microsoft.Bot.Connector.DirectLine.Entity aEntity)
        {
            return new global::Microsoft.Bot.Connector.Entity() { Properties = aEntity.Properties, Type = aEntity.Type };
        }

        public static global::Microsoft.Bot.Connector.DirectLine.Entity ToDirectLineEntity(this global::Microsoft.Bot.Connector.Entity aEntity)
        {
            return new global::Microsoft.Bot.Connector.DirectLine.Entity() { Properties = aEntity.Properties, Type = aEntity.Type };
        }
    }
}
