﻿namespace Universal.Microsoft.Bot
{
    public static class ChannelAccountExtensions
    {
        public static global::Microsoft.Bot.Connector.ChannelAccount ToConnectorChannelAccount(this global::Microsoft.Bot.Connector.DirectLine.ChannelAccount aChannelAccount)
        {
            return new global::Microsoft.Bot.Connector.ChannelAccount() { Id = aChannelAccount.Id, Name = aChannelAccount.Name };
        }

        public static global::Microsoft.Bot.Connector.DirectLine.ChannelAccount ToDirectLineChannelAccount(this global::Microsoft.Bot.Connector.ChannelAccount aChannelAccount)
        {
            return new global::Microsoft.Bot.Connector.DirectLine.ChannelAccount() { Id = aChannelAccount.Id, Name = aChannelAccount.Name };
        }
    }
}
