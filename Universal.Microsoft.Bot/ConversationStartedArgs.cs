﻿using System;

namespace Universal.Microsoft.Bot
{
    public class ConversationStartedArgs : EventArgs
    {
        public string ConversationId { get; }

        public ConversationStartedArgs(string aConversationId)
        {
            ConversationId = aConversationId;
        }
    }
}
