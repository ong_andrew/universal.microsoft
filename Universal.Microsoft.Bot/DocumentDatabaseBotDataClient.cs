﻿using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.Dialogs.Internals;
using Microsoft.Bot.Connector;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Universal.Microsoft.Bot
{
    public class DocumentDatabaseBotDataClient
    {
        protected DocumentClient mDocumentClient;
        protected string mDatabaseId;
        protected string mCollectionId;

        public DocumentDatabaseBotDataClient(string aDocumentDatabaseServiceEndpoint, string aDocumentDatabaseKey, string aDatabaseId = "botdb", string aCollectionId = "botcollection")
        {
            mDocumentClient = new DocumentClient(new Uri(aDocumentDatabaseServiceEndpoint), aDocumentDatabaseKey);
            mDatabaseId = aDatabaseId;
            mCollectionId = aCollectionId;
        }

        public async Task CreateIfNotExistsAsync()
        {
            await CreateDatabaseIfNotExistsAsync();
            await CreateCollectionIfNotExistsAsync();
        }

        public async Task<List<TypedAddress>> GetAddressesAsync(int aTop = 0, Func<dynamic, bool> predicate = null)
        {
            List<TypedAddress> result = new List<TypedAddress>();

            ResourceResponse<DocumentCollection> response = await mDocumentClient.ReadDocumentCollectionAsync(UriFactory.CreateDocumentCollectionUri(mDatabaseId, mCollectionId));
            var query = mDocumentClient.CreateDocumentQuery(response.Resource.SelfLink);

            List<string> documentIds;

            if (aTop == 0)
            {
                if (predicate != null)
                {
                    documentIds = query.Where(predicate).Select(x => x.Id).Cast<string>().ToList();
                }
                else
                {
                    documentIds = query.Select(x => x.Id).ToList();
                }
            }
            else
            {
                if (predicate != null)
                {
                    documentIds = query.Where(predicate).Select(x => x.Id).Take(aTop).Cast<string>().ToList();
                }
                else
                {
                    documentIds = query.Take(aTop).Select(x => x.Id).ToList();
                }
            }
            
            foreach (string documentId in documentIds)
            {
                result.Add(GetTypedAddressFromEntityKey(documentId));
            }

            return result;
        } 

        public async Task<BotData> GetUserDataAsync(IAddress aAddress)
        {
            return await GetBotDataAsync(aAddress, BotStoreType.BotUserData);
        }

        public async Task<BotData> GetUserDataOrDefaultAsync(IAddress aAddress)
        {
            BotData result;

            try
            {
                result = await GetUserDataAsync(aAddress);
            }
            catch
            {
                result = new BotData();
            }

            return result;
        }

        public async Task<BotData> GetConversationDataAsync(IAddress aAddress)
        {
            return await GetBotDataAsync(aAddress, BotStoreType.BotConversationData);
        }

        public async Task<BotData> GetConversationDataOrDefaultAsync(IAddress aAddress)
        {
            BotData result;

            try
            {
                result = await GetConversationDataAsync(aAddress);
            }
            catch
            {
                result = new BotData();
            }

            return result;
        }

        public async Task<BotData> GetPrivateConversationDataAsync(IAddress aAddress)
        {
            return await GetBotDataAsync(aAddress, BotStoreType.BotPrivateConversationData);
        }

        public async Task<BotData> GetPrivateConversationDataOrDefaultAsync(IAddress aAddress)
        {
            BotData result;

            try
            {
                result = await GetPrivateConversationDataAsync(aAddress);
            }
            catch
            {
                result = new BotData();
            }

            return result;
        }

        public async Task<BotData> GetBotDataAsync(TypedAddress aAddress)
        {
            return await GetBotDataAsync(aAddress, aAddress.BotStoreType);
        }

        public async Task<BotData> GetBotDataOrDefaultAsync(TypedAddress aAddress)
        {
            BotData result;

            try
            {
                result = await GetBotDataAsync(aAddress);
            }
            catch
            {
                result = new BotData();
            }

            return result;
        }

        protected async Task<BotData> GetBotDataAsync(IAddress aAddress, BotStoreType aBotStoreType)
        {
            ResourceResponse<Document> response = await mDocumentClient.ReadDocumentAsync(UriFactory.CreateDocumentUri(mDatabaseId, mCollectionId, GetEntityKey(aAddress, aBotStoreType)));
            DocDbBotDataEntity entity = (dynamic)response.Resource;

            return new BotData(response?.Resource.ETag, entity?.Data);
        }

        public async Task SetUserDataAsync(IAddress aAddress, BotData aBotData)
        {
            await SetBotDataAsync(aAddress, BotStoreType.BotUserData, aBotData);
        }

        public async Task SetConversationDataAsync(IAddress aAddress, BotData aBotData)
        {
            await SetBotDataAsync(aAddress, BotStoreType.BotConversationData, aBotData);
        }

        public async Task SetPrivateConversationDataDataAsync(IAddress aAddress, BotData aBotData)
        {
            await SetBotDataAsync(aAddress, BotStoreType.BotPrivateConversationData, aBotData);
        }

        public async Task SetBotDataAsync(TypedAddress aAddress, BotData aBotData)
        {
            await SetBotDataAsync(aAddress, aAddress.BotStoreType, aBotData);
        }

        public async Task SetBotDataAsync(IAddress aAddress, BotStoreType aBotStoreType, BotData aBotData)
        {
            RequestOptions requestOptions = new RequestOptions()
            {
                AccessCondition = new AccessCondition()
                {
                    Type = AccessConditionType.IfMatch,
                    Condition = aBotData.ETag
                }
            };

            DocDbBotDataEntity entity = new DocDbBotDataEntity(aAddress, aBotStoreType, aBotData);
            string entityKey = GetEntityKey(aAddress, aBotStoreType);

            if (string.IsNullOrEmpty(aBotData.ETag))
            {
                await mDocumentClient.CreateDocumentAsync(UriFactory.CreateDocumentCollectionUri(mDatabaseId, mCollectionId), entity, requestOptions);
            }
            else if (aBotData.ETag == "*")
            {
                if (aBotData.Data != null)
                {
                    await mDocumentClient.UpsertDocumentAsync(UriFactory.CreateDocumentCollectionUri(mDatabaseId, mCollectionId), entity, requestOptions);
                }
                else
                {
                    await mDocumentClient.DeleteDocumentAsync(UriFactory.CreateDocumentUri(mDatabaseId, mCollectionId, entityKey), requestOptions);
                }
            }
            else
            {
                if (aBotData.Data != null)
                {
                    await mDocumentClient.ReplaceDocumentAsync(UriFactory.CreateDocumentUri(mDatabaseId, mCollectionId, entityKey), entity, requestOptions);
                }
                else
                {
                    await mDocumentClient.DeleteDocumentAsync(UriFactory.CreateDocumentUri(mDatabaseId, mCollectionId, entityKey), requestOptions);
                }
            }
        }

        public async Task DeleteBotDataAsync(TypedAddress aAddress)
        {
            await DeleteBotDataAsync(aAddress, aAddress.BotStoreType);
        }

        protected async Task DeleteBotDataAsync(IAddress aAddress, BotStoreType aBotStoreType)
        {
            await mDocumentClient.DeleteDocumentAsync(UriFactory.CreateDocumentUri(mDatabaseId, mCollectionId, GetEntityKey(aAddress, aBotStoreType)));
        }

        public static string GetEntityKey(TypedAddress aTypedAddress)
        {
            return GetEntityKey(aTypedAddress, aTypedAddress.BotStoreType);
        }

        public static string GetEntityKey(IAddress aAddress, BotStoreType aBotStoreType)
        {
            switch (aBotStoreType)
            {
                case BotStoreType.BotConversationData:
                    return $"{aAddress.ChannelId}:conversation{aAddress.ConversationId.SanitizeForAzureKeys()}";

                case BotStoreType.BotUserData:
                    return $"{aAddress.ChannelId}:user{aAddress.UserId.SanitizeForAzureKeys()}";

                case BotStoreType.BotPrivateConversationData:
                    return $"{aAddress.ChannelId}:private{aAddress.ConversationId.SanitizeForAzureKeys()}:{aAddress.UserId.SanitizeForAzureKeys()}";

                default:
                    throw new ArgumentException("Unsupported bot store type!");
            }
        }

        public static TypedAddress GetTypedAddressFromEntityKey(string aEntityKey, string aBotId = "", string aServiceUrl = "")
        {
            string[] parts = aEntityKey.Split(new string[] { ":" }, StringSplitOptions.RemoveEmptyEntries);
            string channelId = parts[0];

            if (aEntityKey.Contains(":private"))
            {
                string conversationId = parts[1].Substring("private".Length).DesanitizeForAzureKeys();
                string userId = parts[2].DesanitizeForAzureKeys();

                if (channelId == "msteams")
                {
                    conversationId = $"{parts[1]}:{parts[2]}".Substring("private".Length).DesanitizeForAzureKeys();
                    userId = $"{parts[3]}:{parts[4]}".DesanitizeForAzureKeys();
                }

                return new TypedAddress(aBotId, channelId, userId, conversationId, aServiceUrl, BotStoreType.BotPrivateConversationData);
            }
            else if (aEntityKey.Contains(":user"))
            {
                string userId = parts[1].Substring("user".Length).DesanitizeForAzureKeys();

                if (channelId == "msteams")
                {
                    userId = $"{parts[1]}:{parts[2]}".Substring("user".Length).DesanitizeForAzureKeys();
                }

                return new TypedAddress(aBotId, channelId, userId, "", aServiceUrl, BotStoreType.BotUserData);
            }
            else if (aEntityKey.Contains(":conversation"))
            {
                string conversationId = parts[1].Substring("conversation".Length).DesanitizeForAzureKeys();

                if (channelId == "msteams")
                {
                    conversationId = $"{parts[1]}:{parts[2]}".Substring("conversation".Length).DesanitizeForAzureKeys();
                }

                return new TypedAddress(aBotId, channelId, "", conversationId, aServiceUrl, BotStoreType.BotConversationData);
            }
            else
            {
                throw new Exception($"Not a vald address: {aEntityKey}");
            }
        }

        protected async Task CreateDatabaseIfNotExistsAsync()
        {
            try
            {
                await mDocumentClient.ReadDatabaseAsync(UriFactory.CreateDatabaseUri(mDatabaseId));
            }
            catch (DocumentClientException e)
            {
                if (e.StatusCode == HttpStatusCode.NotFound)
                {
                    await mDocumentClient.CreateDatabaseAsync(new Database { Id = mDatabaseId });
                }
                else
                {
                    throw;
                }
            }
        }

        protected async Task CreateCollectionIfNotExistsAsync()
        {
            try
            {
                await mDocumentClient.ReadDocumentCollectionAsync(UriFactory.CreateDocumentCollectionUri(mDatabaseId, mCollectionId));
            }
            catch (DocumentClientException e)
            {
                if (e.StatusCode == System.Net.HttpStatusCode.NotFound)
                {
                    await mDocumentClient.CreateDocumentCollectionAsync(UriFactory.CreateDatabaseUri(mDatabaseId), new DocumentCollection { Id = mCollectionId });
                }
                else
                {
                    throw;
                }
            }
        }
    }
    
    internal class DocDbBotDataEntity
    {
        public DocDbBotDataEntity() { }

        internal DocDbBotDataEntity(IAddress key, BotStoreType botStoreType, BotData botData)
        {
            this.Id = DocumentDatabaseBotDataClient.GetEntityKey(key, botStoreType);
            this.BotId = key.BotId;
            this.ChannelId = key.ChannelId;
            this.ConversationId = key.ConversationId;
            this.UserId = key.UserId;
            this.Data = botData.Data;
        }

        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }
        [JsonProperty(PropertyName = "botId")]
        public string BotId { get; set; }
        [JsonProperty(PropertyName = "channelId")]
        public string ChannelId { get; set; }
        [JsonProperty(PropertyName = "conversationId")]
        public string ConversationId { get; set; }
        [JsonProperty(PropertyName = "userId")]
        public string UserId { get; set; }
        [JsonProperty(PropertyName = "data")]
        public object Data { get; set; }
    }
}
