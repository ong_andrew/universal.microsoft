﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Bot.Connector.DirectLine;
using System.Threading;
using WebSocketSharp;
using Newtonsoft.Json;
using System.IO;

namespace Universal.Microsoft.Bot
{
    public delegate void ConversationStartedEventHandler(object sender, ConversationStartedArgs e);
    public delegate void MessageReceivedEventHandler(object sender, MessageReceivedArgs e);

    public class DirectLineConversation : IDisposable
    {
        private string mDirectLineSecret;

        private DirectLineClient mDirectLineClient;
        private Conversation mConversation;
        private int mPollingInterval;

        private List<Activity> mActivitiesProcessed;
        private List<Activity> mActivitiesIncomingQueue;
        private List<Activity> mActivitiesOutgoingQueue;

        Thread mListenerThread;
        Thread mIncomingQueueThread;
        Thread mOutgoingQueueThread;

        private bool mRun;
        
        public event ConversationStartedEventHandler ConversationStarted;
        protected virtual void OnConversationStarted(ConversationStartedArgs e)
        {
            ConversationStarted?.Invoke(this, e);
        }
        
        public event MessageReceivedEventHandler MessageReceived;
        protected virtual void OnMessageReceived(MessageReceivedArgs e)
        {
            MessageReceived?.Invoke(this, e);
        }

        public DirectLineConversation(string aDirectLineSecret, int aPollingInterval = 500)
        {
            mDirectLineSecret = aDirectLineSecret;
            mPollingInterval = aPollingInterval;

            mRun = false;

            mDirectLineClient = null;
            mConversation = null;

            mActivitiesProcessed = new List<Activity>();
            mActivitiesIncomingQueue = new List<Activity>();
            mActivitiesOutgoingQueue = new List<Activity>();
        }

        public List<Activity> GetActivities()
        {
            List<Activity> result = new List<Activity>();
            
            lock (mActivitiesProcessed)
            {
                result.AddRange(mActivitiesProcessed);
            }
            
            return result;
        }

        public void Start()
        {
            if (mRun)
            {
                throw new Exception("Conversation already started.");
            }
            mRun = true;

            mDirectLineClient = new DirectLineClient(mDirectLineSecret);
            mConversation = mDirectLineClient.Conversations.StartConversation();

            if (mConversation == null)
            {
                throw new Exception("Unable to start conversation.");
            }

            ConversationStartedArgs conversationStartedArgs = new ConversationStartedArgs(mConversation.ConversationId);
            OnConversationStarted(conversationStartedArgs);

            mListenerThread = new Thread(ListenSocket);
            mIncomingQueueThread = new Thread(ProcessIncomingQueue);
            mOutgoingQueueThread = new Thread(ProcessOutgoingQueue);

            mListenerThread.Start();
            mIncomingQueueThread.Start();
            mOutgoingQueueThread.Start();
        }

        public async Task StartAsync()
        {
            if (mRun)
            {
                throw new Exception("Conversation already started.");
            }
            mRun = true;

            DirectLineClient directLineClient = new DirectLineClient(mDirectLineSecret);
            Conversation conversation = await directLineClient.Conversations.StartConversationAsync();

            ConversationStartedArgs e = new ConversationStartedArgs(conversation.ConversationId);
            OnConversationStarted(e);

            mListenerThread = new Thread(ListenSocket);
            mIncomingQueueThread = new Thread(ProcessIncomingQueue);
            mOutgoingQueueThread = new Thread(ProcessOutgoingQueue);

            mListenerThread.Start();
            mIncomingQueueThread.Start();
            mOutgoingQueueThread.Start();
        }

        public void Send(string aText, string aUserId = "UserId", string aUserName = "UserName")
        {
            Send(new Activity() { Type = ActivityTypes.Message, Text = aText, From = new ChannelAccount(aUserId, aUserName) });
        }

        public void Send(Activity aActivity)
        {
            lock (mActivitiesOutgoingQueue)
            {
                mActivitiesOutgoingQueue.Add(aActivity);
            }
        }

        public void Stop()
        {
            mRun = false;

            mListenerThread.Join();
            mIncomingQueueThread.Join();
            mOutgoingQueueThread.Join();
        }

        private void ListenSocket()
        {
            using (WebSocket webSocket = new WebSocket(mConversation.StreamUrl))
            {
                webSocket.OnMessage += (o, e) =>
                {
                    ActivitySet activitySet = JsonConvert.DeserializeObject<ActivitySet>(e.Data);

                    lock (mActivitiesIncomingQueue)
                    {
                        if (activitySet != null)
                        {
                            MessageReceivedArgs messageReceivedArgs = new MessageReceivedArgs(activitySet);
                            OnMessageReceived(messageReceivedArgs);

                            foreach (Activity activity in activitySet.Activities)
                            {
                                mActivitiesIncomingQueue.Add(activity);
                            }
                        }
                    }
                };
            webSocket.Connect();

                while (webSocket.IsAlive)
                {
                    if (!mRun)
                    {
                        Console.SetOut(new StreamWriter(Stream.Null));
                        webSocket.Close();
                        Console.SetOut(new StreamWriter(Console.OpenStandardOutput()) { AutoFlush = true });
                    }
                }
            }
        }

        private void ProcessIncomingQueue()
        {
            while (mRun)
            {
                lock (mActivitiesProcessed)
                {
                    lock (mActivitiesIncomingQueue)
                    {
                        mActivitiesProcessed.AddRange(mActivitiesIncomingQueue);
                        mActivitiesIncomingQueue.Clear();
                    }
                }

                Thread.Sleep(mPollingInterval);
            }
        }

        private void ProcessOutgoingQueue()
        {
            while (mRun)
            {
                lock (mActivitiesOutgoingQueue)
                {
                    foreach (Activity activity in mActivitiesOutgoingQueue)
                    {
                        ResourceResponse resourceResponse = mDirectLineClient.Conversations.PostActivity(mConversation.ConversationId, activity);
                    }

                    mActivitiesOutgoingQueue.Clear();
                }

                Thread.Sleep(mPollingInterval);
            }
        }

        public void Dispose()
        {
            Stop();
        }

        public bool IsActive()
        {
            return mRun;
        }
    }
}
