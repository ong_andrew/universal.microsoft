﻿using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Rendering;
using System.Collections.Generic;
using Universal.Common;

namespace Universal.Microsoft.AspNetCore.Components.Web
{
    /// <summary>
    /// Represents empty space of some specified size.
    /// </summary>
    public class Space : ComponentBase
    {
        /// <summary>
        /// A component that represents spacing.
        /// </summary>
        public Space() : base()
        {
        }

        /// <summary>
        /// Sets the width in pixels for this space component.
        /// </summary>
        [Parameter]
        public int? Width
        {
            set
            {
                if (value != null)
                {
                    WidthSpecification = $"{value.Value}px";
                }
                else
                {
                    WidthSpecification = null;
                }
            }
        }
        /// <summary>
        /// Sets the height in pixels for this space component.
        /// </summary>
        [Parameter]
        public int? Height
        {
            set
            {
                if (value != null)
                {
                    HeightSpecification = $"{value.Value}px";
                }
                else
                {
                    HeightSpecification = null;
                }
            }
        }
        /// <summary>
        /// Sets the width specification as a string.
        /// </summary>
        [Parameter]
        public string WidthSpecification { get; set; }
        /// <summary>
        /// Sets the height specification as a string.
        /// </summary>
        [Parameter]
        public string HeightSpecification { get; set; }
        /// <summary>
        /// The display style to use. 
        /// Defaults to 'inline-block' if <see cref="WidthSpecification"/> is set and not overriden by passing this parameter.
        /// </summary>
        [Parameter]
        public string Display { get; set; }

        private string Style
        {
            get
            {
                List<string> parts = new List<string>();

                if (!WidthSpecification.IsNullOrEmpty())
                {
                    parts.Add($"width: {WidthSpecification};");
                }

                if (!HeightSpecification.IsNullOrEmpty())
                {
                    parts.Add($"height: {HeightSpecification};");
                }

                if (!Display.IsNullOrEmpty())
                {
                    parts.Add($"display: {Display};");
                }
                else if (!WidthSpecification.IsNullOrEmpty())
                {
                    parts.Add($"display: inline-block;");
                }

                return string.Join(" ", parts);
            }
        }

        protected override void BuildRenderTree(RenderTreeBuilder builder)
        {
            builder.OpenElement(0, "div");
            builder.AddAttribute(1, "style", Style);
            builder.CloseElement();
        }
    }
}
