﻿using System;
using System.Text.Json.Serialization;

namespace Universal.Microsoft.AspNetCore.Components.Web
{
    /// <summary>
    /// Represents an error event.
    /// https://developer.mozilla.org/en-US/docs/Web/API/ErrorEvent
    /// </summary>
    public class ErrorEventArgs : EventArgs
    {
        /// <summary>
        /// Gets or sets the message property.
        /// </summary>
        [JsonPropertyName("message")]
        public string Message { get; set; }
        /// <summary>
        /// Gets or sets the filename property.
        /// </summary>
        [JsonPropertyName("filename")]
        public string FileName { get; set; }
        /// <summary>
        /// Gets or sets the lineno property.
        /// </summary>
        [JsonPropertyName("lineno")]
        public int LineNumber { get; set; }
        /// <summary>
        /// Gets or sets the colno property.
        /// </summary>
        [JsonPropertyName("colno")]
        public int ColumnNumber { get; set; }
    }
}
