﻿using Microsoft.AspNetCore.Components.Web;

namespace Universal.Microsoft.AspNetCore.Components.Web
{
    /// <summary>
    /// Common values for <see cref="KeyboardEventArgs.Key"/>.
    /// </summary>
    public static partial class Keys
    {
        public const string Alt = "Alt";
        public const string ArrowDown = "ArrowDown";
        public const string ArrowLeft = "ArrowLeft";
        public const string ArrowRight = "ArrowRight";
        public const string ArrowUp = "ArrowUp";
        public const string CapsLock = "CapsLock";
        public const string ContextMenu = "ContextMenu";
        public const string Control = "Control";
        public const string Delete = "Delete";
        public const string End = "End";
        public const string Enter = "Enter";
        public const string Escape = "Escape";
        public const string Home = "Home";
        public const string Insert = "Insert";
        public const string Meta = "Meta";
        public const string NumLock = "NumLock";
        public const string PageDown = "PageDown";
        public const string PageUp = "PageUp";
        public const string Pause = "Pause";
        public const string PrintScreen = "PrintScreen";
        public const string ScrollLock = "ScrollLock";
        public const string Shift = "Shift";
        public const string Tab = "Tab";
    }
}
