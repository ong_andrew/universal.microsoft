﻿using Microsoft.AspNetCore.Components;

namespace Universal.Microsoft.AspNetCore.Components.Web
{
    /// <summary>
    /// Represents an overlay.
    /// </summary>
    public interface IOverlay : IComponent
    {
        /// <summary>
        /// Shows the overlay.
        /// </summary>
        void Show();
        /// <summary>
        /// Hides the overlay.
        /// </summary>
        void Hide();
    }
}
