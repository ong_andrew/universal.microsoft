﻿window.UniversalMicrosoftAspNetCoreComponentsWeb = {};

UniversalMicrosoftAspNetCoreComponentsWeb.AddErrorEventListener = function (dotNetObjectReference) {
    window.addEventListener("error", async (eventArgs) => {
        dotNetObjectReference.invokeMethodAsync("RaiseError", { message: eventArgs.message, filename: eventArgs.filename, lineno: eventArgs.lineno, colno: eventArgs.colno });
    });
}

UniversalMicrosoftAspNetCoreComponentsWeb.ObserveIntersection = function (dotNetObjectReference, targetElement, options) {
    let intersectionObserver = new IntersectionObserver(async (eventArgs) => {
        let cleanedEventArgs = {
            isIntersecting: eventArgs[0].isIntersecting
        };
        try {
            await dotNetObjectReference.invokeMethodAsync("OnIntersectionObserved", cleanedEventArgs);
        }
        catch {
            intersectionObserver.unobserve(targetElement);
        }
    }, options);
    intersectionObserver.observe(targetElement);
}

UniversalMicrosoftAspNetCoreComponentsWeb.SetTitle = function (title) {
    document.title = title;
}

UniversalMicrosoftAspNetCoreComponentsWeb.StoreDotNetObjectReference = function (key, dotNetObjectReference) {
    if (!window.dotNetObjectReferences) {
        window.dotNetObjectReferences = {};
    }
    window.dotNetObjectReferences[key] = dotNetObjectReference;
}

UniversalMicrosoftAspNetCoreComponentsWeb.GetDotNetObjectReference = function (key) {
    return window.dotNetObjectReferences[key];
}

UniversalMicrosoftAspNetCoreComponentsWeb.DeleteDotNetObjectReference = function (key) {
    delete window.dotNetObjectReferences[key];
}

UniversalMicrosoftAspNetCoreComponentsWeb.SaveBase64StringAsFile = function (fileName, mediaType, base64String) {
    let link = document.createElement("a");
    link.download = fileName;
    link.href = `data:${mediaType};base64,` + base64String;
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
}