﻿using Microsoft.JSInterop;
using System;

namespace Universal.Microsoft.AspNetCore.Components.Web
{
    /// <summary>
    /// Represents a class that can 
    /// </summary>
    public class ErrorEventListener
    {
        /// <summary>
        /// Raised when the window's onerror event fires.
        /// </summary>
        public event EventHandler<ErrorEventArgs> OnError;

        [JSInvokable]
        public void RaiseError(ErrorEventArgs eventArgs)
        {
            OnError?.Invoke(this, eventArgs);
        }
    }
}
