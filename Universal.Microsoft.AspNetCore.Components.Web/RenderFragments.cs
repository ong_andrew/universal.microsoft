﻿using Microsoft.AspNetCore.Components;

namespace Universal.Microsoft.AspNetCore.Components.Web
{
    /// <summary>
    /// Reusable <see cref="RenderFragment"/> that avoid the overhead of <see cref="IComponent"/>.
    /// </summary>
    public static class RenderFragments
    {
        /// <summary>
        /// Renders horizontal space with the given width in pixels.
        /// </summary>
        /// <param name="width"></param>
        /// <param name="display"></param>
        /// <returns></returns>
        public static RenderFragment HorizontalSpace(int width, string display = null)
        {
            return HorizontalSpace($"{width}px", display);
        }

        /// <summary>
        /// Renders horizontal space with the given width.
        /// </summary>
        /// <param name="widthSpecification"></param>
        /// <param name="display"></param>
        /// <returns></returns>
        public static RenderFragment HorizontalSpace(string widthSpecification, string display = null) 
        {
            return builder =>
            {
                builder.OpenElement(0, "div");
                builder.AddAttribute(1, "style", $"width: {widthSpecification}; display: {display ?? "inline-block"};");
                builder.CloseElement();
            };
        }

        /// <summary>
        /// Renders vertical space with the given height in pixels.
        /// </summary>
        /// <param name="height"></param>
        /// <param name="display"></param>
        /// <returns></returns>
        public static RenderFragment VerticalSpace(int height, string display = null)
        {
            return VerticalSpace($"{height}px", display);
        }

        /// <summary>
        /// Renders vertical space with the given height.
        /// </summary>
        /// <param name="heightSpecification"></param>
        /// <param name="display"></param>
        /// <returns></returns>
        public static RenderFragment VerticalSpace(string heightSpecification, string display = null)
        {
            return builder =>
            {
                builder.OpenElement(0, "div");
                builder.AddAttribute(1, "style", $"height: {heightSpecification};" + (display != null ? $" display: {display};" : string.Empty));
                builder.CloseElement();
            };
        }
    }
}
