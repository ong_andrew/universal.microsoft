﻿using Microsoft.AspNetCore.Components;
using System.Collections.Specialized;

namespace Universal.Microsoft.AspNetCore.Components.Web
{
    /// <summary>
    /// Extension methods for the <see cref="NavigationManager"/> class.
    /// </summary>
    public static class NavigationManagerExtensions
    {
        /// <summary>
        /// Gets the query parameters from the current URI as a <see cref="NameValueCollection"/>.
        /// </summary>
        /// <param name="navigationManager"></param>
        /// <returns></returns>
        public static NameValueCollection GetQueryParameters(this NavigationManager navigationManager)
        {
            return new Universal.Common.UriBuilder(navigationManager.Uri).QueryParameters;
        }
    }
}
