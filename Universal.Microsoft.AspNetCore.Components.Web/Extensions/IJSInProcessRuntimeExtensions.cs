﻿using Microsoft.JSInterop;
using System;
using Universal.Microsoft.JSInterop;

namespace Universal.Microsoft.AspNetCore.Components.Web
{
    /// <summary>
    /// Extensions for the <see cref="IJSInProcessRuntime"/> interface.
    /// </summary>
    public static class IJSInProcessRuntimeExtensions
    {
        /// <summary>
        /// Adds an <see cref="ErrorEventListener" /> to the window.
        /// </summary>
        /// <param name="jsInProcessRuntime"></param>
        /// <returns></returns>
        public static ErrorEventListener AddErrorEventListener(this IJSInProcessRuntime jsInProcessRuntime)
        {
            ErrorEventListener errorEventListener = new ErrorEventListener();

            jsInProcessRuntime.InvokeVoid("UniversalMicrosoftAspNetCoreComponentsWeb.AddErrorEventListener", DotNetObjectReference.Create(errorEventListener));

            return errorEventListener;
        }

        /// <summary>
        /// Equivalent to window.alert(message).
        /// </summary>
        /// <param name="jsInProcessRuntime"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public static void Alert(this IJSInProcessRuntime jsInProcessRuntime, string message)
        {
            jsInProcessRuntime.InvokeVoid("window.alert", message);
        }

        /// <summary>
        /// Equivalent to window.history.back().
        /// </summary>
        /// <param name="jsInProcessRuntime"></param>
        /// <returns></returns>
        public static void Back(this IJSInProcessRuntime jsInProcessRuntime)
        {
            jsInProcessRuntime.InvokeVoid("window.history.back");
        }

        /// <summary>
        /// Gets the IANA timezone.
        /// </summary>
        /// <param name="jsInProcessRuntime"></param>
        /// <returns></returns>
        public static string GetTimezone(this IJSInProcessRuntime jsInProcessRuntime)
        {
            return jsInProcessRuntime.GetTimeZone();
        }

        /// <summary>
        /// Gets the IANA timezone.
        /// </summary>
        /// <param name="jsInProcessRuntime"></param>
        /// <returns></returns>
        public static string GetTimeZone(this IJSInProcessRuntime jsInProcessRuntime)
        {
            return jsInProcessRuntime.Evaluate<string>("Intl.DateTimeFormat().resolvedOptions().timeZone");
        }

        /// <summary>
        /// Sets the document's title.
        /// </summary>
        /// <param name="jsInProcessRuntime"></param>
        /// <param name="title"></param>
        /// <returns></returns>
        public static void SetTitle(this IJSInProcessRuntime jsInProcessRuntime, string title)
        {
            jsInProcessRuntime.InvokeVoid("UniversalMicrosoftAspNetCoreComponentsWeb.SetTitle", title);
        }

        /// <summary>
        /// Stores the <see cref="DotNetObjectReference{TValue}"/> in JS memory by automatically generating a key that can be used to retrieve it.
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="jsInProcessRuntime"></param>
        /// <param name="dotNetObjectReference"></param>
        /// <returns></returns>
        public static string StoreDotNetObjectReference<TValue>(this IJSInProcessRuntime jsInProcessRuntime, DotNetObjectReference<TValue> dotNetObjectReference)
            where TValue : class
        {
            string key = Guid.NewGuid().ToString();
            jsInProcessRuntime.StoreDotNetObjectReference(key, dotNetObjectReference);
            return key;
        }

        /// <summary>
        /// Stores the <see cref="DotNetObjectReference{TValue}"/> in JS memory.
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="jsInProcessRuntime"></param>
        /// <param name="key"></param>
        /// <param name="dotNetObjectReference"></param>
        /// <returns></returns>
        public static void StoreDotNetObjectReference<TValue>(this IJSInProcessRuntime jsInProcessRuntime, string key, DotNetObjectReference<TValue> dotNetObjectReference)
            where TValue : class
        {
            jsInProcessRuntime.InvokeVoid("UniversalMicrosoftAspNetCoreComponentsWeb.StoreDotNetObjectReference", key, dotNetObjectReference);
        }

        /// <summary>
        /// Deletes a previously stored reference.
        /// </summary>
        /// <param name="jsInProcessRuntime"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static void DeleteDotNotObjectReference(this IJSInProcessRuntime jsInProcessRuntime, string key)
        {
            jsInProcessRuntime.InvokeVoid("UniversalMicrosoftAspNetCoreComponentsWeb.DeleteDotNetObjectReference", key);
        }

        /// <summary>
        /// Equivalent to window.open(url, windowName, windowFeatures).
        /// </summary>
        /// <param name="jsInProcessRuntime"></param>
        /// <param name="url"></param>
        /// <param name="windowName"></param>
        /// <param name="windowFeatures"></param>
        /// <returns></returns>
        public static void Open(this IJSInProcessRuntime jsInProcessRuntime, string url, string windowName = null, string windowFeatures = null)
        {
            jsInProcessRuntime.InvokeVoid("window.open", url, windowName, windowFeatures);
        }

        /// <summary>
        /// Sends the byte array to the client browser and simulates a link click to download the content as a file.
        /// </summary>
        /// <param name="jsInProcessRuntime"></param>
        /// <param name="fileName"></param>
        /// <param name="mediaType"></param>
        /// <param name="bytes"></param>
        /// <returns></returns>
        public static void SaveBytesAsFile(this IJSInProcessRuntime jsInProcessRuntime, string fileName, string mediaType, byte[] bytes)
        {
            jsInProcessRuntime.InvokeVoid("UniversalMicrosoftAspNetCoreComponentsWeb.SaveBase64StringAsFile", fileName, mediaType, Convert.ToBase64String(bytes));
        }
    }
}
