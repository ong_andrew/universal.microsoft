﻿using Microsoft.JSInterop;
using System;
using System.Text;
using System.Threading.Tasks;
using Universal.Microsoft.JSInterop;

namespace Universal.Microsoft.AspNetCore.Components.Web
{
    /// <summary>
    /// Extension methods for <see cref="IJSRuntime"/>.
    /// </summary>
    public static class IJSRuntimeExtensions
    {
        /// <summary>
        /// Adds an <see cref="ErrorEventListener" /> to the window.
        /// </summary>
        /// <param name="jsRuntime"></param>
        /// <returns></returns>
        public static async ValueTask<ErrorEventListener> AddErrorEventListenerAsync(this IJSRuntime jsRuntime)
        {
            ErrorEventListener errorEventListener = new ErrorEventListener();

            await jsRuntime.InvokeVoidAsync("UniversalMicrosoftAspNetCoreComponentsWeb.AddErrorEventListener", DotNetObjectReference.Create(errorEventListener)).ConfigureAwait(false);

            return errorEventListener;
        }

        /// <summary>
        /// Adds a script tag with the specified source to the end of the document body.
        /// </summary>
        /// <param name="jsRuntime"></param>
        /// <param name="type"></param>
        /// <param name="source"></param>
        /// <returns></returns>
        public static async ValueTask AddScriptAsync(this IJSRuntime jsRuntime, string type, string source)
        {
            if (!await jsRuntime.CheckScriptExistsAsync(type, source))
            {
                JSInterop.TaskCompletionSource<bool> taskCompletionSource = new JSInterop.TaskCompletionSource<bool>();
                var taskCompletionSourceDotNetObjectReference = DotNetObjectReference.Create(taskCompletionSource);
                string key = await jsRuntime.StoreDotNetObjectReferenceAsync(taskCompletionSourceDotNetObjectReference);
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.AppendLine("let scriptElement = document.createElement(\"script\");");
                stringBuilder.AppendLine($"scriptElement.type = \"{type}\";");
                stringBuilder.AppendLine($"scriptElement.src = \"{source}\"");
                stringBuilder.AppendLine($"scriptElement.onload = async () => {{ await UniversalMicrosoftAspNetCoreComponentsWeb.GetDotNetObjectReference(\"{key}\").invokeMethodAsync(\"SetResultJS\", true); }}");
                stringBuilder.AppendLine($"scriptElement.onerror = async () => {{ await UniversalMicrosoftAspNetCoreComponentsWeb.GetDotNetObjectReference(\"{key}\").invokeMethodAsync(\"SetResultJS\", false); }}");
                stringBuilder.AppendLine("document.body.appendChild(scriptElement);");
                string javascriptSource = stringBuilder.ToString();
                await jsRuntime.EvaluateAsync(javascriptSource);
                await taskCompletionSource.Task;
                await jsRuntime.DeleteDotNotObjectReferenceAsync(key);
                taskCompletionSourceDotNetObjectReference.Dispose();
            }
        }

        private static ValueTask<bool> CheckScriptExistsAsync(this IJSRuntime jsRuntime, string type, string source)
        {
            return jsRuntime.EvaluateAsync<bool>($"Array.from(document.querySelectorAll(\"script\")).some(x => x.type == \"{type}\" && x.getAttribute(\"src\") == \"{source}\")");
        }

        /// <summary>
        /// Equivalent to window.alert(message).
        /// </summary>
        /// <param name="jsRuntime"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public static ValueTask AlertAsync(this IJSRuntime jsRuntime, string message)
        {
            return jsRuntime.InvokeVoidAsync("window.alert", message);
        }

        /// <summary>
        /// Equivalent to window.history.back().
        /// </summary>
        /// <param name="jsRuntime"></param>
        /// <returns></returns>
        public static ValueTask BackAsync(this IJSRuntime jsRuntime)
        {
            return jsRuntime.InvokeVoidAsync("window.history.back");
        }

        /// <summary>
        /// Gets the IANA timezone.
        /// </summary>
        /// <param name="jsRuntime"></param>
        /// <returns></returns>
        public static async ValueTask<string> GetTimezoneAsync(this IJSRuntime jsRuntime)
        {
            return await jsRuntime.GetTimeZoneAsync();
        }

        /// <summary>
        /// Gets the IANA timezone.
        /// </summary>
        /// <param name="jsRuntime"></param>
        /// <returns></returns>
        public static async ValueTask<string> GetTimeZoneAsync(this IJSRuntime jsRuntime)
        {
            return await jsRuntime.EvaluateAsync<string>("Intl.DateTimeFormat().resolvedOptions().timeZone");
        }

        /// <summary>
        /// Sets the document's title.
        /// </summary>
        /// <param name="jsRuntime"></param>
        /// <param name="title"></param>
        /// <returns></returns>
        public static async ValueTask SetTitleAsync(this IJSRuntime jsRuntime, string title)
        {
            await jsRuntime.InvokeVoidAsync("UniversalMicrosoftAspNetCoreComponentsWeb.SetTitle", title);
        }

        /// <summary>
        /// Stores the <see cref="DotNetObjectReference{TValue}"/> in JS memory by automatically generating a key that can be used to retrieve it.
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="jsRuntime"></param>
        /// <param name="dotNetObjectReference"></param>
        /// <returns></returns>
        public static async ValueTask<string> StoreDotNetObjectReferenceAsync<TValue>(this IJSRuntime jsRuntime, DotNetObjectReference<TValue> dotNetObjectReference)
            where TValue : class
        {
            string key = Guid.NewGuid().ToString();
            await jsRuntime.StoreDotNetObjectReferenceAsync(key, dotNetObjectReference);
            return key;
        }

        /// <summary>
        /// Stores the <see cref="DotNetObjectReference{TValue}"/> in JS memory.
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="jsRuntime"></param>
        /// <param name="key"></param>
        /// <param name="dotNetObjectReference"></param>
        /// <returns></returns>
        public static ValueTask StoreDotNetObjectReferenceAsync<TValue>(this IJSRuntime jsRuntime, string key, DotNetObjectReference<TValue> dotNetObjectReference)
            where TValue : class
        {
            return jsRuntime.InvokeVoidAsync("UniversalMicrosoftAspNetCoreComponentsWeb.StoreDotNetObjectReference", key, dotNetObjectReference);
        }

        /// <summary>
        /// Deletes a previously stored reference.
        /// </summary>
        /// <param name="jsRuntime"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static ValueTask DeleteDotNotObjectReferenceAsync(this IJSRuntime jsRuntime, string key)
        {
            return jsRuntime.InvokeVoidAsync("UniversalMicrosoftAspNetCoreComponentsWeb.DeleteDotNetObjectReference", key);
        }

        /// <summary>
        /// Equivalent to window.open(url, windowName, windowFeatures).
        /// </summary>
        /// <param name="jsRuntime"></param>
        /// <param name="url"></param>
        /// <param name="windowName"></param>
        /// <param name="windowFeatures"></param>
        /// <returns></returns>
        public static ValueTask OpenAsync(this IJSRuntime jsRuntime, string url, string windowName = null, string windowFeatures = null)
        {
            return jsRuntime.InvokeVoidAsync("window.open", url, windowName, windowFeatures);
        }

        /// <summary>
        /// Sends the byte array to the client browser and simulates a link click to download the content as a file.
        /// </summary>
        /// <param name="jsRuntime"></param>
        /// <param name="fileName"></param>
        /// <param name="mediaType"></param>
        /// <param name="bytes"></param>
        /// <returns></returns>
        public static ValueTask SaveBytesAsFileAsync(this IJSRuntime jsRuntime, string fileName, string mediaType, byte[] bytes)
        {
            return jsRuntime.InvokeVoidAsync("UniversalMicrosoftAspNetCoreComponentsWeb.SaveBase64StringAsFile", fileName, mediaType, Convert.ToBase64String(bytes));
        }
    }
}
