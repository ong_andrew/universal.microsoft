﻿using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Rendering;
using Microsoft.JSInterop;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Universal.Microsoft.AspNetCore.Components.Web
{
    /// <summary>
    /// A div that can interact by observing intersections.
    /// </summary>
    public class IntersectionObserver : ComponentBase
    {
        private DotNetObjectReference<IntersectionObserver> mDotNetObjectReference { get; set; }

        [Inject]
        private IJSRuntime jsRuntime { get; set; }
        private ElementReference Reference { get; set; }

        /// <summary>
        /// Callback to invoke when the intersection is observed.
        /// </summary>
        [Parameter]
        public EventCallback<EventArgs> IntersectionObserved { get; set; }
        /// <summary>
        /// Child content to render.
        /// </summary>
        [Parameter]
        public RenderFragment ChildContent { get; set; }
        /// <summary>
        /// Options to pass to the JavaScript IntersectionObserver constructor.
        /// </summary>
        [Parameter]
        public Dictionary<string, object> Options { get; set; }
        [Parameter]
        public string Class { get; set; }
        [Parameter]
        public string Style { get; set; }
        [Parameter(CaptureUnmatchedValues = true)]
        public Dictionary<string, object> Attributes { get; set; }

        protected override async Task OnAfterRenderAsync(bool firstRender)
        {
            await base.OnAfterRenderAsync(firstRender);
            if (mDotNetObjectReference == null)
            {
                mDotNetObjectReference = DotNetObjectReference.Create(this);
                await jsRuntime.InvokeVoidAsync("UniversalMicrosoftAspNetCoreComponentsWeb.ObserveIntersection", mDotNetObjectReference, Reference, Options ?? new Dictionary<string, object>());
            }
        }

        [JSInvokable]
        public async Task OnIntersectionObserved(EventArgs eventArgs)
        {
            await IntersectionObserved.InvokeAsync(eventArgs);
        }

        public void Dispose()
        {
            mDotNetObjectReference?.Dispose();
            mDotNetObjectReference = null;
        }

        public class EventArgs : System.EventArgs
        {
            public bool IsIntersecting { get; set; }
        }

        protected override void BuildRenderTree(RenderTreeBuilder builder)
        {
            builder.OpenElement(0, "div");
            builder.AddAttribute(1, "class", Class);
            builder.AddAttribute(2, "style", Style);
            builder.AddMultipleAttributes(3, Attributes);
            builder.AddElementReferenceCapture(4, elementReference => Reference = elementReference);
            builder.AddContent(5, ChildContent);
            builder.CloseElement();
        }
    }
}
