﻿namespace Universal.Microsoft.AspNetCore.Components.Web
{
	public static partial class Keys 
	{
		public const string F1 = "F1";
		public const string F2 = "F2";
		public const string F3 = "F3";
		public const string F4 = "F4";
		public const string F5 = "F5";
		public const string F6 = "F6";
		public const string F7 = "F7";
		public const string F8 = "F8";
		public const string F9 = "F9";
		public const string F10 = "F10";
		public const string F11 = "F11";
		public const string F12 = "F12";
	}
}