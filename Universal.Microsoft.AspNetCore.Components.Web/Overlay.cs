﻿using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Rendering;
using System.Collections.Generic;

namespace Universal.Microsoft.AspNetCore.Components.Web
{
    /// <summary>
    /// Reprsents a overlay.
    /// </summary>
    public class Overlay : ComponentBase, IOverlay
    {
        /// <summary>
        /// Component that displays information in a full screen overlay.
        /// </summary>
        public Overlay() : base()
        {
            IsShowing = false;
        }

        /// <summary>
        /// Gets or sets the z-index for this overlay. Defaults to 1.
        /// </summary>
        [Parameter]
        public string ZIndex { get; set; } = "1";
        /// <summary>
        /// Gets or set the child content to render in the overlay.
        /// </summary>
        [Parameter]
        public RenderFragment ChildContent { get; set; }
        private bool mIsShowing;
        /// <summary>
        /// Gets or sets a value that indicates if the overlay is showing.
        /// </summary>
        [Parameter]
        public bool IsShowing
        {
            get => mIsShowing;
            set
            {
                if (mIsShowing != value)
                {
                    mIsShowing = value;
                    StateHasChanged();
                }
            }
        }
        /// <summary>
        /// Gets or sets additional style parameters to include.
        /// </summary>
        [Parameter]
        public string Style { get; set; }
        /// <summary>
        /// Gets or sets the additional attributes to add onto the element.
        /// </summary>
        [Parameter(CaptureUnmatchedValues = true)]
        public Dictionary<string, object> Attributes { get; set; }

        private string Display
        {
            get => IsShowing ? "block" : "none";
        }

        /// <summary>
        /// Shows the overlay.
        /// </summary>
        public void Show()
        {
            IsShowing = true;
        }

        /// <summary>
        /// Hides the overlay.
        /// </summary>
        public void Hide()
        {
            IsShowing = false;
        }

        protected override void BuildRenderTree(RenderTreeBuilder builder)
        {
            builder.OpenElement(0, "div");
            builder.AddAttribute(1, "style", $"position: fixed; width: 100%; height: 100%; left: 0px; top: 0px; z-index: {ZIndex}; display: {Display}; {Style}");
            builder.AddMultipleAttributes(2, Attributes);
            builder.AddContent(3, ChildContent);
            builder.CloseElement();
        }
    }
}
