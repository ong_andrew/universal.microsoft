﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;

namespace Universal.Microsoft.Authentication.V2
{
    [TestClass]
    [TestCategory("TokenClient")]
    public class TokenClientTests
    {
        [TestMethod]
        public async Task CanGetToken()
        {
            using (TokenClient tokenClient = new TokenClient())
            {
                TokenRequest tokenRequest = TokenRequest.ForClientCredentialsFlow("redmarble.ai", "0319bcec-0427-455d-8aae-a6bc45e1b7ea", "N0iW.5Dui04KfyRd.mc1GORDO7~7440CY_");
                tokenRequest.AddMicrosoftGraphScope(".default");
                TokenResponse tokenResponse = await tokenClient.GetTokenAsync(tokenRequest);
                Assert.IsNotNull(tokenResponse.AccessToken);
            }
        }

        /*
        [TestMethod]
        public async Task CanGetTokenManual()
        {
            using (TokenClient tokenClient = new TokenClient())
            {
                TokenRequest tokenRequest = TokenRequest.ForAuthorizationCodeFlow("common", "f3f1207e-d28d-48cb-af2f-1f1b9875e7a3", "0.AUEAM4AMFGNp9kK1NzhQ4yTGJn4g8fON0stIry8fG5h156NBAC0.AQABAAIAAAD--DLA3VO7QrddgJg7WevrwKQH1jtfinelJfFALcS23PwdKCD-f6pa0ai3prq8j9adOkhlMVkI9S8RnQ4LIbCfd68m78ofCqns5QCqtUnKzVTFoljrVV6vCltQyofgUbZFl3i3MPSmg62zKEIX0c_fhjHvoMvLLpSa0MBSzrLVQGtwwZqvP3H6GsmhdNacgcI_uDiyDaxe8gzsLnnbPbnQ1ih-QUUjQRAdPdkJ0J-OncNmajOXaL6_Je3DwzfmK_wsGwfOeK9olg1B0nEuvIBDgg57z0GTZz-GGqr84UB4sH4riP8brg8Cd-FbcPiYe2AYPSjlOvHXlzHYKYvClW13lvp_INMCtuoDrR_Jd_TuDrz9GIw_nQw0HVdcw4AfJ7MLyjyq1mwsX8G4H_un6EeGnwZ1GuN7fzG3mG0xwQfzAt1uukD1xqXQz81JuZcdyoXR8vwiQI1R6_mpzlj75NIRDoCylvJOfxSoW2qHIT_sug8ALf_eEwaj7QQ4ataN9ZBoAzv5wwwI5LT4YdexjNO2ZKqPcF9hbIQkgD22iyOv9mbqBQ5lRSzWzFoC5e0P72Vdf35JFeF_rdvMC7wxu2sTuRk7DfulcGiSDbGIeAPkCYhNFrxq_gNFC52ImbawKl8eFhb0V9b7v8P8jlpR8o-Aynadq2BWGHGDy_1oyuo8U5D04ywXWrZCk7O6YKXRl1b_uAolAYVfzf6PJ-q7IMCxIAA", "https://localhost:1234");
                tokenRequest.ClientSecret = "XDK_ryb0xMWB2_F2L3-h-WgLx5xS7YK5l6";
                TokenResponse tokenResponse = await tokenClient.GetTokenAsync(tokenRequest);
                Assert.IsNotNull(tokenResponse.AccessToken);
            }
        }
        */
    }
}
