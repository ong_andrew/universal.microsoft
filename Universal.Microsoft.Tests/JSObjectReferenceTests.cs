﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Universal.Microsoft.JSInterop
{
    [TestClass]
    [TestCategory("JSObjectReference")]
    public class JSObjectReferenceTests
    {
        [TestMethod]
        public void CanSerialize()
        {
            JSObjectReference jsObjectReference = new JSObjectReference()
            {
                Id = Guid.NewGuid().ToString()
            };

            string serialized = System.Text.Json.JsonSerializer.Serialize(jsObjectReference);
            Assert.IsTrue(serialized.Contains("$type"));
            Assert.IsTrue(serialized.Contains("Id"));
        }

        [TestMethod]
        public void CanDeserialize()
        {
            JSObjectReference jsObjectReference = System.Text.Json.JsonSerializer.Deserialize<JSObjectReference>("{\"Id\": \"Test\"}");
            Assert.AreEqual("Test", jsObjectReference.Id);

            string guid = Guid.NewGuid().ToString();
            string serialized = System.Text.Json.JsonSerializer.Serialize(new JSObjectReference()
            {
                Id = guid
            });

            jsObjectReference = System.Text.Json.JsonSerializer.Deserialize<JSObjectReference>(serialized);
            Assert.AreEqual(guid, jsObjectReference.Id);
        }
    }
}
