﻿using Microsoft.Graph;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;

namespace Universal.Microsoft.Authentication.V2
{
    [TestClass]
    [TestCategory("GraphAuthenticationProvider")]
    public class GraphAuthenticationProviderTests
    {
        [TestMethod]
        public async Task CanAuthenticateAgainstGraphServicePassive()
        {
            PassiveTokenProvider tokenProvider = new PassiveTokenProvider("redmarble.ai", "0319bcec-0427-455d-8aae-a6bc45e1b7ea", "N0iW.5Dui04KfyRd.mc1GORDO7~7440CY_");

            GraphAuthenticationProvider graphAuthenticationProvider = new GraphAuthenticationProvider(tokenProvider);

            GraphServiceClient client = new GraphServiceClient(graphAuthenticationProvider);

            IGraphServiceUsersCollectionPage users = await client.Users.Request().GetAsync();

            Assert.IsNotNull(users);
            Assert.AreNotEqual(0, users.Count, "Non-zero number of users expected.");
        }

        [TestMethod]
        public async Task CanAuthenticateAgainstGraphServiceProactive()
        {
            ProactiveTokenProvider tokenProvider = new ProactiveTokenProvider("redmarble.ai", "0319bcec-0427-455d-8aae-a6bc45e1b7ea", "N0iW.5Dui04KfyRd.mc1GORDO7~7440CY_");

            GraphAuthenticationProvider graphAuthenticationProvider = new GraphAuthenticationProvider(tokenProvider);

            GraphServiceClient client = new GraphServiceClient(graphAuthenticationProvider);

            IGraphServiceUsersCollectionPage users = await client.Users.Request().GetAsync();

            Assert.IsNotNull(users);
            Assert.AreNotEqual(0, users.Count, "Non-zero number of users expected.");
        }
    }
}
