using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;

namespace Universal.Microsoft.Authentication.V2
{
    [TestClass]
    [TestCategory("PassiveTokenProvider")]
    public class PassiveTokenProviderTests
    {
        [TestMethod]
        public async Task CanRetrieveToken()
        {
            PassiveTokenProvider tokenProvider = new PassiveTokenProvider("redmarble.ai", "0319bcec-0427-455d-8aae-a6bc45e1b7ea", "N0iW.5Dui04KfyRd.mc1GORDO7~7440CY_");
            TokenResponse token = await tokenProvider.GetTokenAsync();

            Assert.IsNotNull(token);
        }

        [TestMethod]
        public async Task RetrievedTokenExpiryCorrectlyUpdated()
        {
            PassiveTokenProvider tokenProvider = new PassiveTokenProvider("redmarble.ai", "0319bcec-0427-455d-8aae-a6bc45e1b7ea", "N0iW.5Dui04KfyRd.mc1GORDO7~7440CY_");
            TokenResponse token = await tokenProvider.GetTokenAsync();

            int expiresIn = token.ExpiresIn;

            Assert.IsTrue(expiresIn > 5, $"ExpiresIn ({token.ExpiresIn}) for a new token must have at least 5 seconds of validity.");

            await Task.Delay(3000);

            TokenResponse tokenAfter = await tokenProvider.GetTokenAsync();

            int expiresInAfter = tokenAfter.ExpiresIn;
            Assert.AreEqual(token.AccessToken, tokenAfter.AccessToken, "The access tokens should be the same if the token is valid.");
            Assert.AreNotEqual(tokenAfter, token);
            Assert.AreNotEqual(expiresInAfter, expiresIn);

            Assert.IsTrue(expiresIn - expiresInAfter >= 3, $"ExpiresIn ({expiresIn}) for the original token must be at least 3 greater than ExpiresIn ({expiresInAfter}) for the token retrieved 3 seconds later.");
        }
    }
}
