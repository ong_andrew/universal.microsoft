﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using System.Linq;

namespace Universal.Microsoft.CognitiveServices.Luis.V2
{
    [TestClass]
    [TestCategory("QueryResponse")]
    public class QueryResponseTests
    {
        [TestMethod]
        public void CanDeserializeKnownResponse()
        {
            string rawJson = File.ReadAllText(@"Resources\Sample LUIS Query Response.json");
            QueryResponse queryResponse = QueryResponse.FromString(rawJson);
            Assert.AreNotEqual(0, queryResponse.Intents.Count());
        }
    }
}
