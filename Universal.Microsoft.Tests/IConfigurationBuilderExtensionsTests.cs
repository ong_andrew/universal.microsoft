﻿using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace Microsoft.Extensions.Configuration
{
    [TestClass]
    [TestCategory("IConfigurationBuilderExtensions")]
    public class IConfigurationBuilderExtensionsTests
    {
        [TestMethod]
        public void ThrowsExceptionWhenMarkedRequired()
        {
            Assert.ThrowsException<InvalidOperationException>(() => new ConfigurationBuilder().AddJsonEmbeddedResource(GetType().Assembly, "Does Not Exist", optional: false).Build());
        }

        [TestMethod]
        public void DoesNotThrowExceptionWhenMarkedOptional()
        {
            new ConfigurationBuilder()
                .AddJsonEmbeddedResource(GetType().Assembly, "Does Not Exist", optional: true)
                .Build();
        }

        [TestMethod]
        public void GetsJsonConfig()
        {
            IConfiguration configuration = new ConfigurationBuilder()
                .AddJsonEmbeddedResource(GetType().Assembly, "Universal.Microsoft.Tests.Resources.AppSettings.json")
                .Build();

            Assert.AreEqual("42", configuration["Test"]);
        }
    }
}
