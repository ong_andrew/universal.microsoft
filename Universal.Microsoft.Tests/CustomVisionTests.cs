﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Universal.Common;
using UriBuilder = Universal.Common.UriBuilder;

namespace Universal.Microsoft.CognitiveServices.CustomVision.V2
{
    [TestClass]
    [TestCategory("CustomVision")]
    public class CustomVisionTests
    {
        private byte[] ImageBytes;

        [TestInitialize]
        public void Initialize()
        {
            ImageBytes = File.ReadAllBytes("../../../Resources/TestImageSign1029.jpg");
        }

        [TestMethod]
        public async Task CanPredict()
        {
            using (CustomVisionClient client =  CustomVisionClient.CreatePredictionClient("1e6c37a784a846838058b98afe2f4a77"))
            {
                PredictImageResponse response = await client.PredictImageAsync("dbbfb06e-2ae3-4de4-813d-446efc5f1a77", ImageBytes);
                Assert.AreEqual("dbbfb06e-2ae3-4de4-813d-446efc5f1a77", response.Project);
                Assert.IsTrue(response.Predictions.Count > 0);
            }
        }

        [TestMethod]
        public async Task CanPredictWithNoStore()
        {
            using (CustomVisionClient client = CustomVisionClient.CreatePredictionClient("1e6c37a784a846838058b98afe2f4a77"))
            {
                PredictImageResponse response = await client.PredictImageWithNoStoreAsync("dbbfb06e-2ae3-4de4-813d-446efc5f1a77", ImageBytes);
                Assert.AreEqual("dbbfb06e-2ae3-4de4-813d-446efc5f1a77", response.Project);
                Assert.IsTrue(response.Predictions.Count > 0);
            }
        }

        [TestMethod]
        public async Task CanPredictUrl()
        {
            using (CustomVisionClient client = CustomVisionClient.CreatePredictionClient("1e6c37a784a846838058b98afe2f4a77"))
            {
                PredictImageResponse response = await client.PredictImageUrlAsync("dbbfb06e-2ae3-4de4-813d-446efc5f1a77", new PredictImageUrlRequest() { Url = "https://drive.google.com/a/redmarble.ai/uc?authuser=1&id=1u_T-oGCwBatUDr6r6mZm6htcVpUFWuHV&export=download" });
                Assert.AreEqual("dbbfb06e-2ae3-4de4-813d-446efc5f1a77", response.Project);
                Assert.IsTrue(response.Predictions.Count > 0);
            }
        }

        [TestMethod]
        public async Task CanPredictUrlWithNoStore()
        {
            using (CustomVisionClient client = CustomVisionClient.CreatePredictionClient("1e6c37a784a846838058b98afe2f4a77"))
            {
                PredictImageResponse response = await client.PredictImageUrlWithNoStoreAsync("dbbfb06e-2ae3-4de4-813d-446efc5f1a77", new PredictImageUrlRequest() { Url = "https://drive.google.com/a/redmarble.ai/uc?authuser=1&id=1u_T-oGCwBatUDr6r6mZm6htcVpUFWuHV&export=download" });
                Assert.AreEqual("dbbfb06e-2ae3-4de4-813d-446efc5f1a77", response.Project);
                Assert.IsTrue(response.Predictions.Count > 0);
            }
        }

        [TestMethod]
        public async Task ThrowsException()
        {
            using (CustomVisionClient client = CustomVisionClient.CreatePredictionClient("1e6c37a784a846838058b98afe2f4a77"))
            {
                await Assert.ThrowsExceptionAsync<CustomVisionException>(async () =>
                {
                    PredictImageResponse response = await client.PredictImageAsync("dbbfb06e-2ae3-4de4-813d-446efc5f1a77", new byte[1]);
                });
            }
        }

        [TestMethod]
        public async Task ExtraHeadersAreOkay()
        {
            using (CustomVisionClient client = new CustomVisionClient("ae57d834e644404c9b7187ca223981cd", "1e6c37a784a846838058b98afe2f4a77"))
            {
                PredictImageResponse response = await client.PredictImageUrlAsync("dbbfb06e-2ae3-4de4-813d-446efc5f1a77", new PredictImageUrlRequest() { Url = "https://drive.google.com/a/redmarble.ai/uc?authuser=1&id=1u_T-oGCwBatUDr6r6mZm6htcVpUFWuHV&export=download" });
                Assert.AreEqual("dbbfb06e-2ae3-4de4-813d-446efc5f1a77", response.Project);
                Assert.IsTrue(response.Predictions.Count > 0);
            }
        }

        [TestMethod]
        public async Task CanGetDomains()
        {
            using (CustomVisionClient client = CustomVisionClient.CreateTrainingClient("ae57d834e644404c9b7187ca223981cd"))
            {
                GetDomainsResponse domains = await client.GetDomainsAsync();
                Assert.IsNotNull(domains);
                Assert.IsTrue(domains.Count > 0);

                string domainId = domains.Random().Id;
                GetDomainResponse domain = await client.GetDomainAsync(domainId);
                Assert.IsNotNull(domain);
                Assert.AreEqual(domainId, domain.Id);
            }
        }

        [TestMethod]
        public async Task CanGetProjectsAndIterationsAndExport()
        {
            using (CustomVisionClient client = CustomVisionClient.CreateTrainingClient("ae57d834e644404c9b7187ca223981cd"))
            {
                GetProjectsResponse projects = await client.GetProjectsAsync();
                Assert.IsNotNull(projects);
                Assert.IsTrue(projects.Count > 0);

                string projectId = projects.Random().Id;
                GetProjectResponse project = await client.GetProjectAsync(projectId);
                Assert.IsNotNull(project);
                Assert.AreEqual(projectId, project.Id);

                GetIterationsResponse iterations = await client.GetIterationsAsync(projectId);
                Assert.IsNotNull(iterations);
                Assert.IsTrue(iterations.Count > 0);

                string iterationId = iterations.Random().Id;
                GetIterationResponse iteration = await client.GetIterationAsync(projectId, iterationId);
                Assert.IsNotNull(iteration);
                Assert.AreEqual(iterationId, iteration.Id);

                GetIterationPerformanceResponse iterationPerformance = await client.GetIterationPerformanceAsync(projectId, iterationId);
                Assert.IsNotNull(iterationPerformance);
                Assert.IsTrue(iterationPerformance.PerTagPerformance.Count > 0);

                try
                {
                    await client.ExportIterationAsync(projectId, iterationId, Platforms.CoreML);
                    GetExportsResponse exports = await client.GetExportsAsync(projectId, iterationId);
                    Assert.IsNotNull(exports);
                    Assert.IsTrue(exports.Any(x => x.Platform == Platforms.CoreML));
                }
                catch (CustomVisionException e)
                {
                    Assert.AreEqual("BadRequestExportAlreadyInProgress", e.Content.Code);
                }
            }
        }

        [TestMethod]
        public async Task CanGetProjectsIterationsAndTags()
        {
            using (CustomVisionClient client = CustomVisionClient.CreateTrainingClient("ae57d834e644404c9b7187ca223981cd"))
            {
                GetProjectsResponse projects = await client.GetProjectsAsync();
                Assert.IsNotNull(projects);
                Assert.IsTrue(projects.Count > 0);

                string projectId = projects.Random().Id;

                GetTagsResponse tags = await client.GetTagsAsync(projectId);
                Assert.IsNotNull(tags);
                Assert.IsTrue(tags.Count > 0);

                Console.WriteLine(tags);

                string tagId = tags.Random().Id;
                GetTagResponse tag = await client.GetTagAsync(projectId, tagId);
                Assert.IsNotNull(tag);
            }
        }

        [TestMethod]
        public async Task CanQuickTest()
        {
            using (CustomVisionClient client = CustomVisionClient.CreateTrainingClient("ae57d834e644404c9b7187ca223981cd"))
            {
                QuickTestImageResponse response = await client.QuickTestImageAsync("dbbfb06e-2ae3-4de4-813d-446efc5f1a77", ImageBytes);
                Assert.AreEqual("dbbfb06e-2ae3-4de4-813d-446efc5f1a77", response.Project);
                Assert.IsTrue(response.Predictions.Count > 0);

                response = await client.QuickTestImageUrlAsync("dbbfb06e-2ae3-4de4-813d-446efc5f1a77", new QuickTestImageUrlRequest() { Url = "https://drive.google.com/a/redmarble.ai/uc?authuser=1&id=1u_T-oGCwBatUDr6r6mZm6htcVpUFWuHV&export=download" });
                Assert.AreEqual("dbbfb06e-2ae3-4de4-813d-446efc5f1a77", response.Project);
                Assert.IsTrue(response.Predictions.Count > 0);
            }
        }

        [TestMethod]
        public async Task CanGetImages()
        {
            using (CustomVisionClient client = CustomVisionClient.CreateTrainingClient("ae57d834e644404c9b7187ca223981cd"))
            {
                GetImagesByIdsResponse images = await client.GetImagesByIdsAsync("dbbfb06e-2ae3-4de4-813d-446efc5f1a77");

                int untaggedImageCount = await client.GetUntaggedImageCountAsync("dbbfb06e-2ae3-4de4-813d-446efc5f1a77");
                GetUntaggedImagesResponse untaggedImages = await client.GetUntaggedImagesAsync("dbbfb06e-2ae3-4de4-813d-446efc5f1a77");

                if (untaggedImageCount >= 50)
                {
                    Assert.IsTrue(untaggedImageCount >= untaggedImages.Count && untaggedImages.Count > 0);
                }
                else
                {
                    Assert.AreEqual(untaggedImageCount, untaggedImages.Count);
                }

                int taggedImageCount = await client.GetTaggedImageCountAsync("dbbfb06e-2ae3-4de4-813d-446efc5f1a77");
                GetTaggedImagesResponse taggedImages = await client.GetTaggedImagesAsync("dbbfb06e-2ae3-4de4-813d-446efc5f1a77");

                if (taggedImageCount >= 50)
                {
                    Assert.IsTrue(taggedImageCount >= taggedImages.Count && taggedImages.Count > 0);
                }
                else
                {
                    Assert.AreEqual(taggedImageCount, taggedImages.Count);
                }
            }
        }

        [TestMethod]
        public async Task CanUploadAndTagImage()
        {
            using (CustomVisionClient client = CustomVisionClient.CreateTrainingClient("ae57d834e644404c9b7187ca223981cd"))
            {
                CreateImagesFromDataResponse createImagesFromDataResponse = await client.CreateImagesFromDataAsync("dbbfb06e-2ae3-4de4-813d-446efc5f1a77", new List<byte[]>() { ImageBytes });
                
                Assert.IsTrue(createImagesFromDataResponse.IsBatchSuccessful);
                Assert.AreEqual(1, createImagesFromDataResponse.Images.Count);

                string imageId = createImagesFromDataResponse.Images.First().ImageData.Id;
                GetTagsResponse tags = await client.GetTagsAsync("dbbfb06e-2ae3-4de4-813d-446efc5f1a77");
                List<string> tagIds = tags.Random(3).Select(x => x.Id).ToList();

                CreateImageTagsRequest createImageTagsRequest = new CreateImageTagsRequest();
                createImageTagsRequest.Tags.Add(new CreateImageTagsRequest.Tag()
                {
                    ImageId = imageId,
                    TagId = tagIds[0]
                });
                createImageTagsRequest.Tags.Add(new CreateImageTagsRequest.Tag()
                {
                    ImageId = imageId,
                    TagId = tagIds[1]
                });
                createImageTagsRequest.Tags.Add(new CreateImageTagsRequest.Tag()
                {
                    ImageId = imageId,
                    TagId = tagIds[2]
                });

                CreateImageTagsResponse createImageTagsResponse = await client.CreateImageTagsAsync("dbbfb06e-2ae3-4de4-813d-446efc5f1a77", createImageTagsRequest);
                Assert.AreEqual(3, createImageTagsResponse.Created.Count + createImageTagsResponse.Duplicated.Count + createImageTagsResponse.Exceeded.Count);
                
                await client.DeleteImagesAsync("dbbfb06e-2ae3-4de4-813d-446efc5f1a77", new string[] { imageId });
            }
        }

        [TestMethod]
        public async Task CanTrainAndDeleteIteration()
        {
            using (CustomVisionClient client = CustomVisionClient.CreateTrainingClient("ae57d834e644404c9b7187ca223981cd"))
            {
                string projectId = "c307126c-deec-4219-b050-3d755bf45b6f";
                GetTagsResponse tags = await client.GetTagsAsync(projectId);
                
                CreateImagesFromDataResponse imagesResponse = await client.CreateImagesFromDataAsync(projectId, new byte[][] { File.ReadAllBytes(@"Resources\TestImageSign1029.jpg") }, new string[] { tags.First().Id });

                Assert.IsTrue(imagesResponse.IsBatchSuccessful);

                string imageId = imagesResponse.Images.First().ImageData.Id;
                GetIterationsResponse iterations = await client.GetIterationsAsync(projectId);
                TrainProjectResponse trainProjectResponse = await client.TrainProjectAsync(projectId);
                GetIterationsResponse newIterations = await client.GetIterationsAsync(projectId);

                Assert.IsTrue(newIterations.Count > iterations.Count);
                await client.DeleteImagesAsync(projectId, new string[] { imageId });
                await client.DeleteIterationAsync(projectId, trainProjectResponse.Id);

                GetIterationsResponse finalIterations = await client.GetIterationsAsync(projectId);
                Assert.AreEqual(finalIterations.Count, iterations.Count);
            }
        }

        [TestMethod]
        public void UriBuilderHandlesArrays()
        {
            UriGenerator uriGenerator = new UriGenerator("testhost.com");
            UriBuilder uriBuilder = uriGenerator.CreateImagesFromData("proj", new string[] { "apples", "oranges" });

            Console.WriteLine(uriBuilder.ToString());
        }
    }
}

namespace Universal.Microsoft.CognitiveServices.CustomVision.V3
{
    [TestClass]
    [TestCategory("CustomVision")]
    public class CustomVisionTests
    {
        private byte[] ImageBytes;

        [TestInitialize]
        public void Initialize()
        {
            ImageBytes = File.ReadAllBytes("../../../Resources/TestImageSign1029.jpg");
        }

        [TestMethod]
        public async Task CanClassify()
        {
            using (CustomVisionClient client = CustomVisionClient.CreatePredictionClient("1e6c37a784a846838058b98afe2f4a77"))
            {
                ClassifyImageResponse response = await client.ClassifyImageAsync("697a667b-49b4-4472-baea-0e7e65c373a2", "Iteration2", ImageBytes);
                Assert.AreEqual("697a667b-49b4-4472-baea-0e7e65c373a2", response.Project);
                Assert.IsTrue(response.Predictions.Count > 0);
            }
        }

        [TestMethod]
        public async Task CanClassifyWithNoStore()
        {
            using (CustomVisionClient client = CustomVisionClient.CreatePredictionClient("1e6c37a784a846838058b98afe2f4a77"))
            {
                ClassifyImageResponse response = await client.ClassifyImageWithNoStoreAsync("697a667b-49b4-4472-baea-0e7e65c373a2", "Iteration2", ImageBytes);
                Assert.AreEqual("697a667b-49b4-4472-baea-0e7e65c373a2", response.Project);
                Assert.IsTrue(response.Predictions.Count > 0);
            }
        }

        [TestMethod]
        public async Task CanClassifyUrl()
        {
            using (CustomVisionClient client = CustomVisionClient.CreatePredictionClient("1e6c37a784a846838058b98afe2f4a77"))
            {
                ClassifyImageResponse response = await client.ClassifyImageUrlAsync("697a667b-49b4-4472-baea-0e7e65c373a2", "Iteration2", "https://drive.google.com/a/redmarble.ai/uc?authuser=1&id=1u_T-oGCwBatUDr6r6mZm6htcVpUFWuHV&export=download");
                Assert.AreEqual("697a667b-49b4-4472-baea-0e7e65c373a2", response.Project);
                Assert.IsTrue(response.Predictions.Count > 0);
            }
        }

        [TestMethod]
        public async Task CanClassifyUrlWithNoStore()
        {
            using (CustomVisionClient client = CustomVisionClient.CreatePredictionClient("1e6c37a784a846838058b98afe2f4a77"))
            {
                ClassifyImageResponse response = await client.ClassifyImageUrlWithNoStoreAsync("697a667b-49b4-4472-baea-0e7e65c373a2", "Iteration2", "https://drive.google.com/a/redmarble.ai/uc?authuser=1&id=1u_T-oGCwBatUDr6r6mZm6htcVpUFWuHV&export=download");
                Assert.AreEqual("697a667b-49b4-4472-baea-0e7e65c373a2", response.Project);
                Assert.IsTrue(response.Predictions.Count > 0);
            }
        }

        [TestMethod]
        public async Task CanGetIterations()
        {
            using (CustomVisionClient client = CustomVisionClient.CreateTrainingClient("ae57d834e644404c9b7187ca223981cd"))
            {
                GetIterationsResponse response = await client.GetIterationsAsync("dbbfb06e-2ae3-4de4-813d-446efc5f1a77");
                Assert.AreNotEqual(0, response.Count);
            }
        }

        [TestMethod]
        public async Task CanGetIteration()
        {
            using (CustomVisionClient client = CustomVisionClient.CreateTrainingClient("ae57d834e644404c9b7187ca223981cd"))
            {
                GetIterationResponse response = await client.GetIterationAsync("dbbfb06e-2ae3-4de4-813d-446efc5f1a77", "0419f475-3bea-4cc7-ad25-d38f67b7996c");
                Assert.AreEqual("dbbfb06e-2ae3-4de4-813d-446efc5f1a77", response.ProjectId);
                Assert.AreEqual("0419f475-3bea-4cc7-ad25-d38f67b7996c", response.Id);
            }
        }
    }
}