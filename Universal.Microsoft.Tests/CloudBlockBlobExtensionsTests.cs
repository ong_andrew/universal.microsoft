﻿using Microsoft.Azure.Storage;
using Microsoft.Azure.Storage.Auth;
using Microsoft.Azure.Storage.Blob;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using System.Threading.Tasks;
using Universal.Microsoft.Azure.Storage.Blob.Extensions;

namespace Universal.Microsoft.Azure.Storage.Blob
{
    [TestClass]
    [TestCategory("CloudBlockBlobExtensions")]
    public class CloudBlockBlobExtensionsTests
    {
        public static CloudStorageAccount CloudStorageAccount { get; set; }

        [TestInitialize]
        public void Initialize()
        {
            CloudStorageAccount = new CloudStorageAccount(new StorageCredentials("cloudstoragetests", "Jj9YkNPgjmz31Gb1QED6Bi+1W28vIfYZSm+rKfVaxLT3y91/9fga0la7V8eZhgkxSX/kCbBJ1zbVxCQYxS+Pow=="), "core.windows.net", true);
        } 

        [TestMethod]
        public async Task CanCreateAndDeleteBlob()
        {
            byte[] bytes = new byte[] { 70, 79, 50 };
            CloudBlobClient cloudBlobClient = CloudStorageAccount.CreateCloudBlobClient();
            CloudBlobContainer cloudBlobContainer = cloudBlobClient.GetContainerReference("test");
            await cloudBlobContainer.CreateIfNotExistsAsync(BlobContainerPublicAccessType.Off, cloudBlobClient.DefaultRequestOptions, null);
            CloudBlockBlob cloudBlockBlob = cloudBlobContainer.GetBlockBlobReference("test.exe");
            await cloudBlockBlob.UploadFromByteArrayAsync(bytes);

            byte[] downloadedBytes = await cloudBlockBlob.DownloadToByteArrayAsync();
            Assert.IsTrue(bytes.SequenceEqual(downloadedBytes));
        }
    }
}
