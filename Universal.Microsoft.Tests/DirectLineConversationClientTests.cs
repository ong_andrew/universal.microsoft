﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Universal.Microsoft.Bot.Connector.DirectLine
{
    [TestClass]
    [TestCategory("DirectLineConversationClient")]
    public class DirectLineConversationClientTests
    {
        [TestMethod]
        public async Task CanStartNewConversation()
        {
            int messagesReceived = 0;
            using (DirectLineConversationClient directLineConversationClient = new DirectLineConversationClient("7EKgiyxjIsE.7VKgxw5zfB5SuCWEhpUa_Vtma_CV0e3Rd6A1bOopg2w"))
            {
                directLineConversationClient.OnActivityAvailable += (sender, eventArgs) =>
                {
                    messagesReceived++;
                    Console.WriteLine(JsonConvert.SerializeObject(eventArgs.Activity));
                };

                await directLineConversationClient.StartConversationAsync();

                await Task.Delay(5000);

                await directLineConversationClient.CloseAsync();
            }

            Assert.AreNotEqual(0, messagesReceived);
        }

        [TestMethod]
        public async Task CanStartNewConversationUsingHttp()
        {
            int messagesReceived = 0;
            using (DirectLineConversationClient directLineConversationClient = new DirectLineConversationClient("7EKgiyxjIsE.7VKgxw5zfB5SuCWEhpUa_Vtma_CV0e3Rd6A1bOopg2w", false))
            {
                directLineConversationClient.OnActivityAvailable += (sender, eventArgs) =>
                {
                    messagesReceived++;
                    Console.WriteLine(JsonConvert.SerializeObject(eventArgs.Activity));
                };

                await directLineConversationClient.StartConversationAsync();

                await Task.Delay(5000);

                await directLineConversationClient.CloseAsync();
            }

            Assert.AreNotEqual(0, messagesReceived);
        }
    }
}
