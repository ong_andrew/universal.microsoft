﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.IO;
using System.Linq;

namespace Universal.Microsoft.Azure.Search
{
    [TestClass]
    [TestCategory("WebApiSkillResponse")]
    public class WebApiSkillResponseTests
    {
        [TestMethod]
        public void CanDeserialize()
        {
            WebApiSkillResponse webApiSkillResponse = WebApiSkillResponse.FromString(File.ReadAllText("Resources/WebApiSkillResponse.json"));
            Assert.IsTrue(new string[] { "b5", "a1", "c3" }.SequenceEqual(webApiSkillResponse.Select(x => x.RecordId)));
            Assert.AreEqual((JValue)5, ((dynamic)webApiSkillResponse.ElementAt(0).Data["contractDate"]).day);
            Assert.IsTrue(webApiSkillResponse.Last().Errors.Any());
        }

        [TestMethod]
        public void CanDeserializeTyped()
        {
            WebApiSkillResponse<ContractOutput> webApiSkillResponse = WebApiSkillResponse<ContractOutput>.FromString(File.ReadAllText("Resources/WebApiSkillResponse.json"));
            Assert.IsTrue(new string[] { "b5", "a1", "c3" }.SequenceEqual(webApiSkillResponse.Select(x => x.RecordId)));
            Assert.AreEqual(5, webApiSkillResponse.ElementAt(0).Data.ContractDate.Day);
            Assert.IsTrue(webApiSkillResponse.Last().Errors.Any());
        }

        public class ContractOutput
        {
            [JsonProperty("contractDate")]
            public Date ContractDate { get; set; }

            public class Date
            {
                [JsonProperty("day")]
                public int Day { get; set; }
                [JsonProperty("month")]
                public int Month { get; set; }
                [JsonProperty("year")]
                public int Year { get; set; }
            }
        }
    }
}
