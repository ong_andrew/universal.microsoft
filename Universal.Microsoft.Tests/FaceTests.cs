﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Universal.Microsoft.CognitiveServices.Face.V1
{
    [TestClass]
    [TestCategory("Face")]
    public class FaceTests
    {
        private const string SubscriptionKey = "cee59358624a47cb8ead5006d9e07854";
        private const string Host = Hosts.SoutheastAsia;

        [TestInitialize]
        [TestCleanup]
        public async Task DeleteTestingGroups()
        {
            using (FaceClient faceClient = new FaceClient(SubscriptionKey, Host))
            {
                try
                {
                    await faceClient.DeletePersonGroupAsync("test");
                }
                catch { }
                try
                {
                    await faceClient.DeleteFaceListAsync("test");
                }
                catch { }
            }
        }

        [TestMethod]
        public async Task CanCreateAndDeletePersonGroup()
        {
            using (FaceClient faceClient = new FaceClient(SubscriptionKey, Host))
            {
                await faceClient.CreatePersonGroupAsync("test", new CreatePersonGroupRequest() { Name = "Testerman" });
                await faceClient.DeletePersonGroupAsync("test");
            }
        }

        [TestMethod]
        public async Task CanCreateAndDeletePersonGroupPerson()
        {
            using (FaceClient faceClient = new FaceClient(SubscriptionKey, Host))
            {
                await faceClient.CreatePersonGroupAsync("test", new CreatePersonGroupRequest() { Name = "Red Marble AI", UserData = "People from Red Marble AI." });

                CreatePersonGroupPersonResponse response = await faceClient.CreatePersonGroupPersonAsync("test", new CreatePersonGroupPersonRequest() { Name = "David Timm" });
                Assert.IsNotNull(response.PersonId);
                await faceClient.DeletePersonGroupPersonAsync("test", response.PersonId);

                await faceClient.DeletePersonGroupAsync("test");
            }
        }

        [TestMethod]
        public async Task CanCreateAndDeletePersonGroupPersonFace()
        {
            using (FaceClient faceClient = new FaceClient(SubscriptionKey, Host))
            {
                await faceClient.CreatePersonGroupAsync("test", new CreatePersonGroupRequest() { Name = "Red Marble AI", UserData = "People from Red Marble AI." });

                CreatePersonGroupPersonResponse createPersonResponse = await faceClient.CreatePersonGroupPersonAsync("test", new CreatePersonGroupPersonRequest() { Name = "David Timm" });
                Assert.IsNotNull(createPersonResponse.PersonId);

                CreatePersonGroupPersonFaceResponse createFaceResponse = await faceClient.CreatePersonGroupPersonFaceAsync("test", createPersonResponse.PersonId, File.ReadAllBytes(@"Resources\David Timm.jpg"));
                Assert.IsNotNull(createFaceResponse.PersistedFaceId);

                await faceClient.DeletePersonGroupPersonAsync("test", createPersonResponse.PersonId);

                await faceClient.DeletePersonGroupAsync("test");
            }
        }

        [TestMethod]
        public async Task CanDetectAndIdentifyPerson()
        {
            using (FaceClient faceClient = new FaceClient(SubscriptionKey, Host))
            {
                await faceClient.CreatePersonGroupAsync("test", new CreatePersonGroupRequest() { Name = "Red Marble AI", UserData = "People from Red Marble AI." });

                CreatePersonGroupPersonResponse createPersonResponse = await faceClient.CreatePersonGroupPersonAsync("test", new CreatePersonGroupPersonRequest() { Name = "David Timm" });
                Assert.IsNotNull(createPersonResponse.PersonId);

                CreatePersonGroupPersonFaceResponse createFaceResponse = await faceClient.CreatePersonGroupPersonFaceAsync("test", createPersonResponse.PersonId, File.ReadAllBytes(@"Resources\David Timm.jpg"));
                Assert.IsNotNull(createFaceResponse.PersistedFaceId);

                await faceClient.TrainPersonGroupAsync("test");

                string status = (await faceClient.GetPersonGroupTrainingStatus("test")).Status;
                while (status == TrainingStatuses.NotStarted || status == TrainingStatuses.Running)
                {
                    await Task.Delay(2000);
                    status = (await faceClient.GetPersonGroupTrainingStatus("test")).Status;
                }

                Assert.AreEqual(TrainingStatuses.Succeeded, status, "Training did not succeed.");

                DetectResponse detectResponse = await faceClient.DetectAsync(File.ReadAllBytes(@"Resources\David Timm.jpg"), true, true, new List<string>() { FaceAttributes.Age, FaceAttributes.Emotion, FaceAttributes.FacialHair, FaceAttributes.Gender, FaceAttributes.Glasses, FaceAttributes.HeadPose, FaceAttributes.Smile, FaceAttributes.Hair, FaceAttributes.Accessories, FaceAttributes.Blur, FaceAttributes.Occlusion, FaceAttributes.Noise, FaceAttributes.Makeup, FaceAttributes.Exposure });
                Assert.IsNotNull(detectResponse, "Detect response was null.");
                Assert.IsTrue(detectResponse.Any(), "No results returned in response.");
                Assert.IsNotNull(detectResponse.First().FaceId, "No Face ID returned in response.");

                IdentifyResponse identifyResponse = await faceClient.IdentifyAsync(new IdentifyRequest()
                {
                    FaceIds = new List<string>() { detectResponse.First().FaceId },
                    PersonGroupId = "test"
                });

                Assert.AreEqual(createPersonResponse.PersonId, identifyResponse.First().Candidates.First().PersonId);

                await faceClient.DeletePersonGroupPersonAsync("test", createPersonResponse.PersonId);

                await faceClient.DeletePersonGroupAsync("test");
            }
        }

        [TestMethod]
        public async Task CanCreateAndDeleteFaceList()
        {
            using (FaceClient faceClient = new FaceClient(SubscriptionKey, Host))
            {
                await faceClient.CreateFaceListAsync("test", new CreateFaceListRequest() { Name = "Testerman" });
                await faceClient.DeleteFaceListAsync("test");
            }
        }

        [TestMethod]
        public async Task CanCreateGetAndListFaceList()
        {
            using (FaceClient faceClient = new FaceClient(SubscriptionKey, Host))
            {
                await faceClient.CreateFaceListAsync("test", new CreateFaceListRequest() { Name = "Testerman" });
                Assert.IsTrue((await faceClient.ListFaceListsAsync()).Any(x => x.Name == "Testerman" && x.FaceListId == "test"));
                Assert.AreEqual("Testerman", (await faceClient.GetFaceListAsync("test")).Name);
                await faceClient.DeleteFaceListAsync("test");
            }
        }
    }
}
