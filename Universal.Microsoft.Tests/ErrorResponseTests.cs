﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;

namespace Universal.Microsoft.Authentication.V2
{
    [TestClass]
    [TestCategory("ErrorResponse")]
    public class ErrorResponseTests
    {
        [TestMethod]
        public void CanDeserializeResponses()
        {
            AuthenticationErrorResponse errorResponse = AuthenticationErrorResponse.FromString(@"{""error"":""invalid_grant"",""error_description"":""AADSTS9002313: Invalid request. Request is malformed or invalid.\r\nTrace ID: 460c6540-6523-4dc5-aa24-102b5ffa2700\r\nCorrelation ID: b11d145f-77d4-4314-b29f-30ca310dc26c\r\nTimestamp: 2019-02-13 23:05:48Z"",""error_codes"":[9002313],""timestamp"":""2019-02-13 23:05:48Z"",""trace_id"":""460c6540-6523-4dc5-aa24-102b5ffa2700"",""correlation_id"":""b11d145f-77d4-4314-b29f-30ca310dc26c""}");

            Assert.AreEqual("invalid_grant", errorResponse.Error);
            Assert.IsTrue(errorResponse.ErrorDescription.Contains("Invalid request."));
            Assert.AreEqual("b11d145f-77d4-4314-b29f-30ca310dc26c", errorResponse.CorrelationId);
            Assert.AreEqual(new DateTime(2019, 2, 13, 23, 5, 48), errorResponse.Timestamp);
            Assert.AreEqual("460c6540-6523-4dc5-aa24-102b5ffa2700", errorResponse.TraceId);
            Assert.AreEqual(1, errorResponse.ErrorCodes.Count());
            Assert.AreEqual(9002313, errorResponse.ErrorCodes.First());
        }
    }
}
