﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Universal.Microsoft.Authentication.V2
{
    [TestClass]
    [TestCategory("AuthorizeUriBuilder")]
    public class AuthorizeUriBuilderTests
    {
        [TestMethod]
        public void CanBuildAuthorizeUri()
        {
            AuthorizeUriBuilder authorizeUriBuilder = new AuthorizeUriBuilder()
            {
                ClientId = "f3f1207e-d28d-48cb-af2f-1f1b9875e7a3",
                RedirectUri = "https://localhost:1234"
            }
                .WithCommonTenant()
                .AddOpenIdScopes()
                .AddMicrosoftGraphScopes("user.read", "sites.read.all")
                .AddCodeResponseType();

            Uri uri = authorizeUriBuilder.Uri;
            Console.WriteLine(uri);
            Assert.AreEqual("https", uri.Scheme);
            Assert.AreEqual("login.microsoftonline.com", uri.Host);
            Assert.AreEqual("/common/oauth2/v2.0/authorize", uri.PathAndQuery.Split(new string[] { "?" }, StringSplitOptions.RemoveEmptyEntries)[0]);
        }
    }
}
