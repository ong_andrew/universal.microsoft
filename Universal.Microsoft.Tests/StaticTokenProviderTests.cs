﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;

namespace Universal.Microsoft.Authentication.V2
{
    [TestClass]
    [TestCategory("StaticTokenProvider")]
    public class StaticTokenProviderTests
    {
        [TestMethod]
        public async Task CanRetrieveToken()
        {
            StaticTokenProvider tokenProvider = new StaticTokenProvider("abc");
            TokenResponse token = await tokenProvider.GetTokenAsync();
            Assert.AreEqual("abc", token.AccessToken);
        }
    }
}
