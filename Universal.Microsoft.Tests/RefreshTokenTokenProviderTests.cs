﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;

namespace Universal.Microsoft.Authentication.V2
{
    [TestClass]
    [TestCategory("RefreshTokenTokenProvider")]
    public class RefreshTokenTokenProviderTests
    {
        [TestMethod]
        public async Task CanGetToken()
        {
            ITokenProvider tokenProvider = new RefreshTokenTokenProvider("downergroup.com", "f3f1207e-d28d-48cb-af2f-1f1b9875e7a3", "XDK_ryb0xMWB2_F2L3-h-WgLx5xS7YK5l6", "0.AQoA4sZL-u3A7EOdbRRefjahpX4g8fON0stIry8fG5h156MKAAE.AgABAAAAAAD--DLA3VO7QrddgJg7WevrAgDs_wQA9P85cZcr5bXWvIXN8uM-Ml00A9xj6nIlsc3B41yoL_w2R_qU5g4dY_DbUJ1yxwQUqMl6ATTWX4Bt22Da4Jyx606FtS56DpiS290QTAW7zmBEq2GmRBzwQRJXd404vb_WpSuq207z3P4W6Ceo7OmpZ9jl9IgcH8pZtId75nBXL38zxnm-Aa1g3PdN80EER-42e5dk_9aibE92Kna_x4RFBM0uTZE1n9cegywM4wO-379tKHPUmMVzddw14y_Fmv26XJavHFHiQnIIOMsWxX2PBcaq51LM29KT8tLCnGfgATlTqThSQDI1IiN0_M9jkvxMBfO8oHZfihKoTbgRZy5ClmOVQSGEaBY4_VJueSsSe538MuYJaAySNIev6sy6fJBmrfKTgfdlzoZHGP957jv3KmUcNfHC9tcH610ROZctzOz4Mt1BhvaoyuNaYUPM6TbyLDmz3xDNUk0oLNKaQ4drO2Vjdbf2DecIq1j86auxd_HquBAJsYSg4iXs0LEC6X5Nvy3xaIApOEYeemxYwbX5z2kP5R_Mwd9BqBDjmGGRGlBf6uPjghyrET1jsJopPVHuDqYMRgp6l-dMnU0Bers4mi9zUf4oiiGDUiADk3qE5lDOpslobSWxfBQMtuBgKuITSkgJkYZp03fEJEu2ZQjE2FTWbLphsTCaUP-I55xq4CIYo2B40A5oj2PyIElTgdetUM1yrxXUHttc11xU6DHKXFbV3oRV763w10IshLX98ZraSV_spt_X_PbIs6IDgx1ME2At9PRPqqX4jiiInfMJW7DpJ0SekJDvZRpThA7wZsBm05qHF_yC7ytpe3njp8ch0bOTCs3433E4XS1SsRxZ-bcZv_x7yyJwuW1eCRal1xGoTsXbkIH2ZOTieoPOBuikO9I54Say7PD9GYsl8bV5oTKrxxBnWN09aC1zz409zxZQtUt6OZiXwpjcUij-oU4Z1syp40qfNn_vphXsBFEWEUkYMCJnYuWpIxwL1GDOlVBfp_B3hRT5U5mQC2spqOHxC6HklOdE9pzkXZnivdPt-IG_2JgIrkw");
            TokenResponse tokenResponse = await tokenProvider.GetTokenAsync();
            Assert.IsNotNull(tokenResponse);
            Assert.IsNotNull(tokenResponse.AccessToken);
            Assert.IsNotNull(tokenResponse.RefreshToken);
        }
    }
}
