﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;

namespace Universal.Microsoft.Authentication.V2
{
    [TestClass]
    [TestCategory("ApplicationTokenProvider")]
    public class ApplicationTokenProviderTests
    {
        [TestMethod]
        public async Task CanRetrieveToken()
        {
            ApplicationTokenProvider tokenProvider = new ApplicationTokenProvider("redmarble.ai", "0319bcec-0427-455d-8aae-a6bc45e1b7ea", "N0iW.5Dui04KfyRd.mc1GORDO7~7440CY_");
            TokenResponse token = await tokenProvider.GetTokenAsync();

            Assert.IsNotNull(token);

            Assert.AreNotEqual(token.AccessToken, (await tokenProvider.GetTokenAsync()).AccessToken);
        }
    }
}
