﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Universal.Microsoft.CognitiveServices.Luis.V2
{
    [TestClass]
    [TestCategory("Luis")]
    public class LuisTests
    {
        private const string SubscriptionKey = "96130df169654be3a17a90b7967b06da";

        [TestMethod]
        public async Task CanQuery()
        {
            using (LuisClient client = LuisClient.CreateEndpointClient(SubscriptionKey))
            {
                QueryResponse response = await client.QueryAsync("8f6af84b-ecff-4f7b-9cd7-739f0e78f25d", "hello");

                Assert.AreEqual("HelloWorld", response.TopScoringIntent.Intent);

                response = await client.QueryAsync("8f6af84b-ecff-4f7b-9cd7-739f0e78f25d", "hello", staging: true);

                Assert.AreEqual("HelloWorldStaging", response.TopScoringIntent.Intent);
            }
        }

        [TestMethod]
        public async Task CanQueryVerbose()
        {
            using (LuisClient client = LuisClient.CreateEndpointClient(SubscriptionKey))
            {
                QueryResponse response = await client.QueryAsync("8f6af84b-ecff-4f7b-9cd7-739f0e78f25d", "hello");
                Assert.AreEqual(0, response.Intents.Count());
                response = await client.QueryAsync("8f6af84b-ecff-4f7b-9cd7-739f0e78f25d", "hello",  verbose: true);
                Assert.AreNotEqual(0, response.Intents.Count());
            }
        }

        [TestMethod]
        public async Task CanCreateAndDeleteApplication()
        {
            using (LuisClient client = LuisClient.CreateApiClient(SubscriptionKey))
            {
                GetApplicationsListResponse applications = await client.GetApplicationsListAsync();

                var application = applications.FirstOrDefault(x => x.Name == "Dummy");

                if (application != null)
                {
                    await client.DeleteApplicationAsync(application.Id);
                }

                AddApplicationRequest addApplicationRequest = new AddApplicationRequest()
                {
                    Name = "Dummy",
                    Culture = "en-us",
                    Description = "Description",
                    Domain = "Others",
                    InitialVersion = "1.0",
                    UsageScenario = "Test"
                };

                string applicationId = await client.AddApplicationAsync(addApplicationRequest);

                Assert.IsNotNull(applicationId);

                GetApplicationInfoResponse applicationInfo = await client.GetApplicationInfoAsync(applicationId);

                Assert.AreEqual("Dummy", applicationInfo.Name);
                Assert.AreEqual("en-us", applicationInfo.Culture);
                Assert.AreEqual("Description", applicationInfo.Description);
                Assert.AreEqual("Test", applicationInfo.UsageScenario);
                Assert.AreEqual(1, applicationInfo.VersionsCount);
                Assert.AreEqual("Others", applicationInfo.Domain);

                await client.DeleteApplicationAsync(applicationId);
            }  
        }

        [TestMethod]
        public async Task CanGetCultures()
        {
            using (LuisClient client = LuisClient.CreateApiClient(SubscriptionKey))
            {
                GetLuisApplicationCulturesListResponse cultures = await client.GetLuisApplicationCulturesListAsync();
                Assert.IsTrue(cultures.Count > 0);
                Assert.IsTrue(cultures.Any(x => x.Code.Equals("en-us", StringComparison.InvariantCultureIgnoreCase)));
            }
        }

        [TestMethod]
        public async Task CanDownloadQueryLogs()
        {
            using (LuisClient luisClient = LuisClient.CreateApiClient(SubscriptionKey))
            {
                AddApplicationRequest addApplicationRequest = new AddApplicationRequest()
                {
                    Name = "QueryLogTest",
                    Culture = "en-us",
                    Description = "QueryLogTest",
                    Domain = "Others",
                    InitialVersion = "1.0",
                    UsageScenario = "Test"
                };

                string applicationId = await luisClient.AddApplicationAsync(addApplicationRequest);

                string queryLogs = await luisClient.DownloadApplicationQueryLogs(applicationId, true);

                Assert.IsNotNull(queryLogs);

                await luisClient.DeleteApplicationAsync(applicationId);
            }
        }

        [TestMethod]
        public async Task CanManipulateVersions()
        {
            using (LuisClient client = LuisClient.CreateApiClient(SubscriptionKey))
            {
                GetApplicationsListResponse applications = await client.GetApplicationsListAsync();

                var application = applications.FirstOrDefault(x => x.Name == "VersionTesting");

                if (application != null)
                {
                    await client.DeleteApplicationAsync(application.Id);
                }

                AddApplicationRequest addApplicationRequest = new AddApplicationRequest()
                {
                    Name = "VersionTesting",
                    Culture = "en-us",
                    Description = "VersionTesting",
                    Domain = "Others",
                    InitialVersion = "1.0",
                    UsageScenario = "Test"
                };

                string applicationId = await client.AddApplicationAsync(addApplicationRequest);

                var versions = await client.GetApplicationVersionsListAsync(applicationId);
                Assert.IsTrue(versions.Count > 0);

                var applicationVersion = versions.First();
                Assert.AreEqual("1.0", applicationVersion.Version);

                var alternativeApplicationVersion = await client.GetApplicationVersionAsync(applicationId, "1.0");

                Assert.AreEqual(applicationVersion.AssignedEndpointKey, alternativeApplicationVersion.AssignedEndpointKey);
                Assert.AreEqual(applicationVersion.CreatedDateTime, alternativeApplicationVersion.CreatedDateTime);
                Assert.AreEqual(applicationVersion.EndpointHitsCount, alternativeApplicationVersion.EndpointHitsCount);
                Assert.AreEqual(applicationVersion.EndpointUrl, alternativeApplicationVersion.EndpointUrl);
                Assert.AreEqual(applicationVersion.EntitiesCount, alternativeApplicationVersion.EntitiesCount);
                Assert.AreEqual(applicationVersion.IntentsCount, alternativeApplicationVersion.IntentsCount);
                Assert.AreEqual(applicationVersion.LastModifiedDateTime, alternativeApplicationVersion.LastModifiedDateTime);
                Assert.AreEqual(applicationVersion.LastPublishedDateTime, alternativeApplicationVersion.LastPublishedDateTime);
                Assert.AreEqual(applicationVersion.LastTrainedDateTime, alternativeApplicationVersion.LastTrainedDateTime);
                Assert.AreEqual(applicationVersion.TrainingStatus, alternativeApplicationVersion.TrainingStatus);
                Assert.AreEqual(applicationVersion.Version, alternativeApplicationVersion.Version);

                await client.DeleteApplicationAsync(applicationId);
            }
        }

        [TestMethod]
        public async Task ApplicationExportsCorrectly()
        {
            using (LuisClient luisClient = LuisClient.CreateApiClient(SubscriptionKey))
            {
                AddApplicationRequest addApplicationRequest = new AddApplicationRequest()
                {
                    Name = "ExportVersionTesting",
                    Culture = "en-us",
                    Description = "ExportVersionTesting",
                    Domain = "Others",
                    InitialVersion = "1.0",
                    UsageScenario = "Test"
                };

                string applicationId = await luisClient.AddApplicationAsync(addApplicationRequest);

                var application = await luisClient.ExportApplicationVersionAsync(applicationId, "1.0");

                Assert.AreEqual("ExportVersionTesting", application.Name);
                Assert.AreEqual("1.0", application.VersionId);
                Assert.AreEqual("ExportVersionTesting", application.Description);

                await luisClient.DeleteApplicationAsync(applicationId);
            }
        }

        [TestMethod]
        public async Task TrainingAndPublishingWorks()
        {
            using (LuisClient client = LuisClient.CreateApiClient(SubscriptionKey))
            {
                GetApplicationsListResponse applications = await client.GetApplicationsListAsync();

                var application = applications.FirstOrDefault(x => x.Name == "TrainingTest");

                if (application != null)
                {
                    await client.DeleteApplicationAsync(application.Id);
                }

                AddApplicationRequest addApplicationRequest = new AddApplicationRequest()
                {
                    Name = "TrainingTest",
                    Culture = "en-us",
                    Description = "TrainingTest",
                    Domain = "Others",
                    InitialVersion = "1.0",
                    UsageScenario = "Test"
                };

                string applicationId = await client.AddApplicationAsync(addApplicationRequest);

                TrainApplicationVersionResponse trainResponse = await client.TrainApplicationVersionAsync(applicationId, "1.0");
                GetVersionTrainingStatusResponse trainingStatuses = await client.GetVersionTrainingStatusAsync(applicationId, "1.0");

                Assert.IsNotNull(trainingStatuses);

                trainingStatuses = await client.PollUntilTrainingCompletedAsync(applicationId, "1.0");
                
                Assert.IsTrue(trainingStatuses.All(x => x.Details.Status == TrainingStatuses.Success), $"Could not train application.");

                PublishApplicationResponse publishResponse = await client.PublishApplicationAsync(applicationId, new PublishApplicationRequest() { VersionId = "1.0", IsStaging = false, Region = Regions.WestUS });

                Assert.IsFalse(publishResponse.IsStaging);
                Assert.AreEqual(Regions.WestUS, publishResponse.EndpointRegion);

                await client.DeleteApplicationAsync(applicationId);
            }
        }

        [TestMethod]
        public async Task ResolutionReturnsCorrectInformation()
        {
            using (LuisClient client = LuisClient.CreateApiClient("8b247c29d082464aa6edbbc4f62508e6"))
            {
                QueryResponse response = await client.QueryAsync("a6d67f74-b5b1-4f81-9b59-2e6d46e2a4b4", "invoice status 2015 6123412345");

                IEnumerable<string> resolutions = response.Entities.Where(x => x.Type == "FinancialYear").Select(x => x.Resolution.AsListResolution()).SelectMany(x => x.Values);

                Assert.IsTrue(resolutions.Contains("2015"));

                resolutions = response.ResolveListEntity("FinancialYear");

                Assert.IsTrue(resolutions.Contains("2015"));
            }
        }

        [TestMethod]
        public async Task CanClone()
        {
            using (LuisClient luisClient = LuisClient.CreateApiClient(SubscriptionKey))
            {
                AddApplicationRequest addApplicationRequest = new AddApplicationRequest()
                {
                    Name = "CloneTesting",
                    Culture = "en-us",
                    Description = "CloneTesting",
                    Domain = "Others",
                    InitialVersion = "1.0",
                    UsageScenario = "Test"
                };

                string applicationId = await luisClient.AddApplicationAsync(addApplicationRequest);

                string newVersion = await luisClient.CloneVersionAsync(applicationId, "1.0", new CloneVersionRequest() { Version = "2.0" });

                Assert.AreEqual("2.0", newVersion);

                ExportApplicationVersionResponse oldApplication = await luisClient.ExportApplicationVersionAsync(applicationId, "1.0");
                ExportApplicationVersionResponse newApplication = await luisClient.ExportApplicationVersionAsync(applicationId, "2.0");

                Assert.AreEqual(oldApplication.Name, newApplication.Name);
                Assert.AreNotEqual(oldApplication.VersionId, newApplication.VersionId);

                await luisClient.DeleteApplicationAsync(applicationId);
            }
        }

        [TestMethod]
        public async Task ContainsClosedList()
        {
            using (LuisClient luisClient = LuisClient.CreateApiClient("33b7b7c274d44c8eb7114ff170a6c06d"))
            {
                ExportApplicationVersionResponse response = await luisClient.ExportApplicationVersionAsync("2a28ab39-3af0-4651-a93c-5ce55eaf1749", "2.0");
                Assert.IsTrue(response.ClosedLists.Count > 0);
            }
        }

        [TestMethod]
        public void CanDeserializeQueryResponse()
        { 
            string responseString =
@"{
  ""query"": ""close work order W0226001 dave did the work from 21 July 2017 10:00am to 21 July 2017 2pm for 2 hours"",
  ""topScoringIntent"": {
    ""intent"": ""Query Document"",
    ""score"": 0.9063454
  },
  ""intents"": [
    {
      ""intent"": ""Query Document"",
      ""score"": 0.9063454
    },
    {
      ""intent"": ""None"",
      ""score"": 0.254976422
    },
    {
      ""intent"": ""Query Time"",
      ""score"": 0.0186578333
    },
    {
      ""intent"": ""Query Material"",
      ""score"": 0.01049312
    },
    {
      ""intent"": ""Query Purchasing Limit"",
      ""score"": 0.00451416243
    },
    {
      ""intent"": ""Joke"",
      ""score"": 0.00386184547
    },
    {
      ""intent"": ""Query Vendor"",
      ""score"": 0.00202378351
    },
    {
      ""intent"": ""Change Language"",
      ""score"": 0.0001897138
    },
    {
      ""intent"": ""Help"",
      ""score"": 6.821429E-05
    },
    {
      ""intent"": ""Feedback"",
      ""score"": 6.580262E-05
    },
    {
      ""intent"": ""Greeting"",
      ""score"": 1.0785418E-06
    }
  ],
  ""entities"": [
    {
      ""entity"": ""from 21 july 2017 10:00am to 21 july 2017 2pm"",
      ""type"": ""builtin.datetimeV2.datetimerange"",
      ""startIndex"": 44,
      ""endIndex"": 88,
      ""resolution"": {
        ""values"": [
          {
            ""timex"": ""(2017-07-21T10:00,2017-07-21T14,PT4H)"",
            ""type"": ""datetimerange"",
            ""start"": ""2017-07-21 10:00:00"",
            ""end"": ""2017-07-21 14:00:00""
          }
        ]
      }
    },
    {
      ""entity"": ""2 hours"",
      ""type"": ""builtin.datetimeV2.duration"",
      ""startIndex"": 94,
      ""endIndex"": 100,
      ""resolution"": {
        ""values"": [
          {
            ""timex"": ""PT2H"",
            ""type"": ""duration"",
            ""value"": ""7200""
          }
        ]
      }
    },
    {
      ""entity"": ""work order"",
      ""type"": ""Document Type"",
      ""startIndex"": 6,
      ""endIndex"": 15,
      ""resolution"": {
        ""values"": [
          ""Work Order""
        ]
      }
    },
    {
      ""entity"": ""dave"",
      ""type"": ""First Name"",
      ""startIndex"": 26,
      ""endIndex"": 29,
      ""score"": 0.89003396
    },
    {
      ""entity"": ""w0226001"",
      ""type"": ""Identifier"",
      ""startIndex"": 17,
      ""endIndex"": 24,
      ""score"": 0.996203244
    },
    {
      ""entity"": ""close"",
      ""type"": ""Query Hint"",
      ""startIndex"": 0,
      ""endIndex"": 4,
      ""resolution"": {
        ""values"": [
          ""Close""
        ]
      }
    }
  ]
}";

            QueryResponse response = QueryResponse.FromString(responseString);
            Assert.AreEqual("close work order W0226001 dave did the work from 21 July 2017 10:00am to 21 July 2017 2pm for 2 hours", response.Query);
            Assert.AreEqual(11, response.Intents.Count);
            Assert.AreEqual(6, response.Entities.Count);
        }
    }
}
