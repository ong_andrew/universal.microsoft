﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Linq;

namespace Universal.Microsoft.Azure.Search
{
    [TestClass]
    [TestCategory("WebApiSkillRequest")]
    public class WebApiSkillRequestTests
    {
        [TestMethod]
        public void CanDeserialize()
        {
            WebApiSkillRequest webApiSkillRequest = WebApiSkillRequest.FromString(File.ReadAllText("Resources/WebApiSkillRequest.json"));

            Assert.IsNotNull(webApiSkillRequest);
            Assert.AreEqual(3, webApiSkillRequest.Count());
            Assert.IsTrue(new string[] { "a1", "b5", "c3" }.SequenceEqual(webApiSkillRequest.Select(x => x.RecordId)));
            Assert.IsTrue(webApiSkillRequest.All(x => x.Data.ContainsKey("contractText")));
            Assert.AreEqual("This is a contract that was issues on November 3, 2017 and that involves... ", webApiSkillRequest.ElementAt(0).Data["contractText"]);
            Assert.AreEqual("In the City of Seattle, WA on February 5, 2018 there was a decision made...", webApiSkillRequest.ElementAt(1).Data["contractText"]);
            Assert.IsNull(webApiSkillRequest.ElementAt(2).Data["contractText"]);
        }

        [TestMethod]
        public void CanDeserializeTyped()
        {
            WebApiSkillRequest<ContractData> webApiSkillRequest = WebApiSkillRequest<ContractData>.FromString(File.ReadAllText("Resources/WebApiSkillRequest.json"));

            Assert.IsNotNull(webApiSkillRequest);
            Assert.AreEqual(3, webApiSkillRequest.Count());
            Assert.IsTrue(new string[] { "a1", "b5", "c3" }.SequenceEqual(webApiSkillRequest.Select(x => x.RecordId)));
            Assert.AreEqual("This is a contract that was issues on November 3, 2017 and that involves... ", webApiSkillRequest.ElementAt(0).Data.ContractText);
            Assert.AreEqual("In the City of Seattle, WA on February 5, 2018 there was a decision made...", webApiSkillRequest.ElementAt(1).Data.ContractText);
            Assert.IsNull(webApiSkillRequest.ElementAt(2).Data.ContractText);
        }

        public class ContractData
        {
            [JsonProperty("contractText")]
            public string ContractText { get; set; }
        }
    }
}
