﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace Universal.Microsoft.CognitiveServices.SpeakerRecognition.V1
{
    [TestClass]
    [TestCategory("SpeakerRecognition")]
    public class SpeakerRecognitionTests
    {
        private const string SubscriptionKey = "8433a0b2972443f6aaaca4086d2b02b9";
        private byte[] AudioBytes;

        [TestInitialize]
        public void Initialize()
        {
            AudioBytes = File.ReadAllBytes("../../../Resources/f7d47191-a9d2-4337-a1f2-2a6c0f12d1f0.wav");
        }

        [TestMethod]
        public async Task CanGetVerificationPhrases()
        {
            using (SpeakerRecognitionClient client = new SpeakerRecognitionClient(SubscriptionKey))
            {
                ListAllSupportedVerificationPhrasesResponse response = await client.ListAllSupportedVerificationPhrasesAsync();

                Assert.IsTrue(response.Count > 0);

                Console.WriteLine(response);
            }
        }

        [TestMethod]
        public async Task WrongKeyThrowsException()
        {
            using (SpeakerRecognitionClient client = new SpeakerRecognitionClient("asdf"))
            {
                await Assert.ThrowsExceptionAsync<SpeakerRecognitionException>(async() => { await client.ListAllSupportedVerificationPhrasesAsync(); });
            }
        }

        [TestMethod]
        public async Task CanCreateAndDeleteVerificationProfile()
        {
            using (SpeakerRecognitionClient client = new SpeakerRecognitionClient(SubscriptionKey))
            {
                CreateVerificationProfileResponse response = await client.CreateVerificationProfileAsync(new CreateVerificationProfileRequest() { Locale = Locales.EnUS });

                string verificationProfileId = response.VerificationProfileId;
                Assert.IsNotNull(verificationProfileId);

                await client.DeleteVerificationProfileAsync(verificationProfileId);
            }
        }

        [TestMethod]
        public async Task CanGetAllProfiles()
        {
            using (SpeakerRecognitionClient client = new SpeakerRecognitionClient(SubscriptionKey))
            {
                GetAllVerificationProfilesResponse response = await client.GetAllVerificationProfilesAsync();

                Console.WriteLine(response);

                GetAllIdentificationProfilesResponse otherResponse = await client.GetAllIdentificationProfilesAsync();

                Console.WriteLine(otherResponse);
            }
        }

        [TestMethod]
        public async Task CanCreateAndGetVerificationProfile()
        {
            using (SpeakerRecognitionClient client = new SpeakerRecognitionClient(SubscriptionKey))
            {
                CreateVerificationProfileResponse response = await client.CreateVerificationProfileAsync(new CreateVerificationProfileRequest() { Locale = Locales.EnUS });

                string verificationProfileId = response.VerificationProfileId;
                Assert.IsNotNull(verificationProfileId);

                GetVerificationProfileResponse getVerificationProfileResponse = await client.GetVerificationProfileAsync(verificationProfileId);

                Assert.IsNotNull(getVerificationProfileResponse);
                Assert.AreEqual(EnrollmentStatuses.Enrolling, getVerificationProfileResponse.EnrollmentStatus);
                Assert.AreEqual(0, getVerificationProfileResponse.EnrollmentsCount);
                Assert.IsTrue(getVerificationProfileResponse.RemainingEnrollmentsCount > 0);

                await client.DeleteVerificationProfileAsync(verificationProfileId);
            }
        }

        [TestMethod]
        public async Task CanCreateAndGetIdentificationProfile()
        {
            using (SpeakerRecognitionClient client = new SpeakerRecognitionClient(SubscriptionKey))
            {
                CreateIdentificationProfileResponse response = await client.CreateIdentificationProfileAsync(new CreateIdentificationProfileRequest() { Locale = Locales.EnUS });

                string identificationProfileId = response.IdentificationProfileId;
                Assert.IsNotNull(identificationProfileId);

                GetIdentificationProfileResponse getIdentificationProfileResponse = await client.GetIdentificationProfileAsync(identificationProfileId);

                Assert.IsNotNull(getIdentificationProfileResponse);
                Assert.AreEqual(EnrollmentStatuses.Enrolling, getIdentificationProfileResponse.EnrollmentStatus);
                Assert.AreEqual(0, getIdentificationProfileResponse.EnrollmentSpeechTime);
                Assert.IsTrue(getIdentificationProfileResponse.RemainingEnrollmentSpeechTime > 0);

                await client.DeleteIdentificationProfileAsync(identificationProfileId);
            }
        }

        [TestMethod]
        public async Task CanEnrollIdentificationProfile()
        {
            using (SpeakerRecognitionClient client = new SpeakerRecognitionClient(SubscriptionKey))
            {
                CreateIdentificationProfileResponse response = await client.CreateIdentificationProfileAsync(new CreateIdentificationProfileRequest() { Locale = Locales.EnUS });

                string identificationProfileId = response.IdentificationProfileId;
                Assert.IsNotNull(identificationProfileId);

                CreateIdentificationProfileEnrollmentResponse createIdentificationProfileEnrollmentResponse = await client.CreateIdentificationProfileEnrollmentAsync(identificationProfileId, AudioBytes, true);

                GetOperationStatusResponse getOperationStatusResponse = await client.PollOperationUntilCompleteAsync(createIdentificationProfileEnrollmentResponse);

                Assert.AreEqual(OperationStatuses.Succeeded, getOperationStatusResponse.Status);

                await client.DeleteIdentificationProfileAsync(identificationProfileId);
            }
        }

        [TestMethod]
        public async Task CanResetVerificationProfile()
        {
            using (SpeakerRecognitionClient client = new SpeakerRecognitionClient(SubscriptionKey))
            {
                CreateVerificationProfileResponse response = await client.CreateVerificationProfileAsync(new CreateVerificationProfileRequest() { Locale = Locales.EnUS });

                string verificationProfileId = response.VerificationProfileId;
                Assert.IsNotNull(verificationProfileId);

                await client.ResetVerificationProfileEnrollmentsAsync(verificationProfileId);

                await client.DeleteVerificationProfileAsync(verificationProfileId);
            }
        }

        [TestMethod]
        public async Task CanCreateVerificationEnrollment()
        {
            using (SpeakerRecognitionClient client = new SpeakerRecognitionClient(SubscriptionKey))
            {
                CreateVerificationProfileResponse response = await client.CreateVerificationProfileAsync(new CreateVerificationProfileRequest() { Locale = Locales.EnUS });

                string verificationProfileId = response.VerificationProfileId;
                Assert.IsNotNull(verificationProfileId);

                Assert.IsNotNull(AudioBytes);
                CreateVerificationProfileEnrollmentResponse enrollmentResponse = await client.CreateVerificationProfileEnrollmentAsync(verificationProfileId, AudioBytes);

                Assert.AreEqual(1, enrollmentResponse.EnrollmentsCount);
                Assert.AreEqual(EnrollmentStatuses.Enrolling, enrollmentResponse.EnrollmentStatus);

                await client.DeleteVerificationProfileAsync(verificationProfileId);
            }
        }

        [TestMethod]
        public async Task CanIdentify()
        {
            using (SpeakerRecognitionClient client = new SpeakerRecognitionClient(SubscriptionKey))
            {
                CreateIdentificationProfileResponse response = await client.CreateIdentificationProfileAsync(new CreateIdentificationProfileRequest() { Locale = Locales.EnUS });

                string identificationProfileId = response.IdentificationProfileId;
                Assert.IsNotNull(identificationProfileId);

                string enrollmentStatus;
                do
                {
                    CreateIdentificationProfileEnrollmentResponse createIdentificationProfileEnrollmentResponse = await client.CreateIdentificationProfileEnrollmentAsync(identificationProfileId, AudioBytes, true);

                    GetOperationStatusResponse getOperationStatusResponse = await client.PollOperationUntilCompleteAsync(createIdentificationProfileEnrollmentResponse);

                    Assert.AreEqual(OperationStatuses.Succeeded, getOperationStatusResponse.Status);
                    enrollmentStatus = getOperationStatusResponse.ProcessingResult.AsEnrollmentResult().EnrollmentStatus;
                    await Task.Delay(500);
                } while (enrollmentStatus != EnrollmentStatuses.Enrolled);

                IdentifyResponse identifyResponse = await client.IdentifyAsync(new List<string>() { identificationProfileId }, AudioBytes, true);

                GetOperationStatusResponse identifyOperationResponse = await client.PollOperationUntilCompleteAsync(identifyResponse);

                Assert.AreEqual(OperationStatuses.Succeeded, identifyOperationResponse.Status);
                Assert.AreEqual(identificationProfileId, identifyOperationResponse.ProcessingResult.AsIdentificationResult().IdentificationProfileId);

                await client.DeleteIdentificationProfileAsync(identificationProfileId);
            }
        }

        [TestMethod]
        public async Task CanVerify()
        {
            using (SpeakerRecognitionClient client = new SpeakerRecognitionClient(SubscriptionKey))
            {
                CreateVerificationProfileResponse response = await client.CreateVerificationProfileAsync(new CreateVerificationProfileRequest() { Locale = Locales.EnUS });

                string verificationProfileId = response.VerificationProfileId;
                Assert.IsNotNull(verificationProfileId);

                Assert.IsNotNull(AudioBytes);
                CreateVerificationProfileEnrollmentResponse enrollmentResponse = await client.CreateVerificationProfileEnrollmentAsync(verificationProfileId, AudioBytes);

                Assert.AreEqual(1, enrollmentResponse.EnrollmentsCount);
                Assert.AreEqual(EnrollmentStatuses.Enrolling, enrollmentResponse.EnrollmentStatus);

                enrollmentResponse = await client.CreateVerificationProfileEnrollmentAsync(verificationProfileId, AudioBytes);

                Assert.AreEqual(2, enrollmentResponse.EnrollmentsCount);
                Assert.AreEqual(EnrollmentStatuses.Enrolling, enrollmentResponse.EnrollmentStatus);

                enrollmentResponse = await client.CreateVerificationProfileEnrollmentAsync(verificationProfileId, AudioBytes);

                Assert.AreEqual(3, enrollmentResponse.EnrollmentsCount);

                GetVerificationProfileResponse getVerificationProfileResponse;

                do
                {
                    getVerificationProfileResponse = await client.GetVerificationProfileAsync(verificationProfileId);
                    
                    await Task.Delay(1000);
                }
                while (getVerificationProfileResponse.EnrollmentStatus != EnrollmentStatuses.Enrolled);

                Assert.AreEqual(EnrollmentStatuses.Enrolled, getVerificationProfileResponse.EnrollmentStatus);

                VerifyResponse verificationResponse = await client.VerifyAsync(verificationProfileId, AudioBytes);

                Assert.AreEqual(VerifyResults.Accept, verificationResponse.Result);

                await client.DeleteVerificationProfileAsync(verificationProfileId);
            }
        }
    }
}
