﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Web;

namespace Universal.Microsoft.Authentication.V2
{
    [TestClass]
    [TestCategory("AuthenticationClient")]
    public class AuthenticationClientTests
    {
        [TestMethod]
        public void CanCreateAuthorizationUris()
        {
            using (AuthenticationClient authenticationClient = new AuthenticationClient("common", "f8eae6d2-a151-4dc9-8bec-7896064994e9", "fciSPVCRG7])fbewQ4394$;", "https://goldiedevelopment.azurewebsites.net/api/Authentication/code"))
            {
                Uri uri = authenticationClient.CreateAuthorizationCodeUri(new List<string>() { "openid", "offline_access", "email", "profile", "https://graph.microsoft.com/user.read", "https://graph.microsoft.com/mail.read" });
                Assert.AreEqual("https", uri.Scheme);
                Assert.AreEqual("login.microsoftonline.com", uri.Host);
                Assert.IsTrue(uri.PathAndQuery.Contains("openid"));
                Assert.IsTrue(uri.PathAndQuery.Contains("offline_access"));
                Assert.IsTrue(uri.PathAndQuery.Contains("email"));
                Assert.IsTrue(uri.PathAndQuery.Contains("profile"));
                Assert.IsTrue(uri.PathAndQuery.Contains("/common/"));

                Assert.IsTrue(uri.PathAndQuery.Contains("f8eae6d2-a151-4dc9-8bec-7896064994e9"));

                Assert.IsTrue(uri.PathAndQuery.Contains(HttpUtility.UrlEncode("https://graph.microsoft.com/user.read")));
                Assert.IsTrue(uri.PathAndQuery.Contains(HttpUtility.UrlEncode("https://graph.microsoft.com/mail.read")));
            }
        }
    }
}
