﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace Universal.Microsoft.Bot.Connector.DirectLine
{
    [TestClass]
    [TestCategory("DirectLineClient")]
    public class DirectLineClientTests
    {
        [TestMethod]
        public async Task Works()
        {
            using (DirectLineClient directLineClient = new DirectLineClient("7EKgiyxjIsE.7VKgxw5zfB5SuCWEhpUa_Vtma_CV0e3Rd6A1bOopg2w"))
            {
                Conversation conversation = await directLineClient.GenerateConversationTokenAsync(new GenerateConversationTokenRequest()
                {
                    User = new GenerateConversationTokenRequest.UserInfo()
                    {
                        Id = "andrew.ong@oceanagold.com",
                        Name = "Andrew Ong"
                    }
                });

                Assert.IsNull(conversation.StreamUrl);
                Assert.IsNotNull(conversation.ConversationId);

                string conversationId = conversation.ConversationId;

                conversation = await directLineClient.StartConversationWithTokenAsync(conversation.Token);
                Assert.AreEqual(conversationId, conversation.ConversationId);

                ResourceResponse resourceResponse = await directLineClient.PostActivityWithTokenAsync(conversation.Token, conversation.ConversationId, new Activity()
                {
                    Type = ActivityTypes.Message,
                    From = new ChannelAccount()
                    {
                        Id = "andrew.ong@oceanagold.com",
                        Name = "Andrew Ong"
                    },
                    Text = "Hi!"
                });

                Assert.IsNotNull(resourceResponse.Id);
            }
        }

        [TestMethod]
        public async Task CanUploadFile()
        {
            using (DirectLineClient directLineClient = new DirectLineClient("7EKgiyxjIsE.7VKgxw5zfB5SuCWEhpUa_Vtma_CV0e3Rd6A1bOopg2w"))
            {
                Conversation conversation = await directLineClient.GenerateConversationTokenAsync(new GenerateConversationTokenRequest()
                {
                    User = new GenerateConversationTokenRequest.UserInfo()
                    {
                        Id = "andrew.ong@oceanagold.com",
                        Name = "Andrew Ong"
                    }
                });

                Assert.IsNull(conversation.StreamUrl);
                Assert.IsNotNull(conversation.ConversationId);

                string conversationId = conversation.ConversationId;

                conversation = await directLineClient.StartConversationWithTokenAsync(conversation.Token);
                Assert.AreEqual(conversationId, conversation.ConversationId);

                ResourceResponse resourceResponse = await directLineClient.UploadFilesAsync(conversation.ConversationId, "andrew.ong@oceanagold.com", null, new List<(string, Stream)> { ("abc.txt", new MemoryStream(new byte[] { 68, 67, 42 })) }, CancellationToken.None);

                Assert.IsNotNull(resourceResponse.Id);
            }
        }
    }
}
