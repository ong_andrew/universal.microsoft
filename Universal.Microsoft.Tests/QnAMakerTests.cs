﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Universal.Microsoft.CognitiveServices.QnAMaker.V4
{
    [TestClass]
    [TestCategory("QnAMaker")]
    public class QnAMakerTests
    {
        public const string SubscriptionKey = "47d83570b1974049b413894ba76a9d17";
        
        [TestMethod]
        public async Task CanCreateAndDeleteKnowledgebase()
        {
            using (QnAMakerClient client = QnAMakerClient.CreateApiClient(SubscriptionKey))
            {
                CreateKnowledgebaseResponse response = await client.CreateKnowledgebaseAsync(new CreateKnowledgebaseRequest()
                {
                    Name = "Create Delete Test"
                });

                GetOperationDetailsResponse operationResponse = await client.PollOperationUntilCompleteAsync(response);

                Assert.AreEqual(OperationStates.Succeeded, operationResponse.OperationState);

                string knowledgebaseId = operationResponse.ResourceLocation.Split(new string[] { "/" }, StringSplitOptions.RemoveEmptyEntries)[1];
                Assert.IsNotNull(knowledgebaseId);

                await client.DeleteKnowledgebaseAsync(knowledgebaseId);
            }
        }

        [TestMethod]
        public async Task CanQueryAsync()
        {
            using (QnAMakerClient client = QnAMakerClient.CreateEndpointClient("abbf8927-9606-4602-951f-0a61e463416a", "qnamakerunittests.azurewebsites.net"))
            {
                GenerateAnswerRequest generateAnswerRequest = new GenerateAnswerRequest("hi");

                Assert.AreEqual("Hello world!", (await client.GenerateAnswerAsync("59623d6e-cae1-46f1-a702-839c31672957", generateAnswerRequest)).Answers.First().Answer);
            }
        }

        [TestMethod]
        public async Task CanQueryWithManualKeyAsync()
        {
            using (QnAMakerClient client = QnAMakerClient.CreateEndpointClient("WRONG KEY", "qnamakerunittests.azurewebsites.net"))
            {
                GenerateAnswerRequest generateAnswerRequest = new GenerateAnswerRequest("hi");

                await Assert.ThrowsExceptionAsync<QnAMakerException>(async () => { await client.GenerateAnswerAsync("59623d6e-cae1-46f1-a702-839c31672957", generateAnswerRequest); });

                Assert.AreEqual("Hello world!", (await client.GenerateAnswerAsync("59623d6e-cae1-46f1-a702-839c31672957", "abbf8927-9606-4602-951f-0a61e463416a", "qnamakerunittests.azurewebsites.net", generateAnswerRequest)).Answers.First().Answer);
            }
        }

        [TestMethod]
        public async Task WrongKeyThrowsException()
        {
            using (QnAMakerClient client = QnAMakerClient.CreateApiClient("ASDF"))
            {
                await Assert.ThrowsExceptionAsync<QnAMakerException>(async() => await client.CreateKnowledgebaseAsync(new CreateKnowledgebaseRequest()
                {
                    Name = "Create Delete Test"
                }));
            }
        }

        [TestMethod]
        public async Task CanPublishUpdateQueryAndDownloadSampleKnowledgebase()
        {
            using (QnAMakerClient client = QnAMakerClient.CreateApiClient(SubscriptionKey))
            {
                CreateKnowledgebaseRequest request = new CreateKnowledgebaseRequest()
                {
                    Name = "Publish Query Test",
                    QnAs = new List<CreateKnowledgebaseRequest.QnAItem>()
                    {
                        new CreateKnowledgebaseRequest.QnAItem()
                        {
                            Id = 0,
                            Questions = new List<string>()
                            {
                                "What is your name?",
                                "Who are you?"
                            },
                            Answer = "Andrew"
                        }
                    }
                };

                CreateKnowledgebaseResponse response = await client.CreateKnowledgebaseAsync(request);

                GetOperationDetailsResponse operationResponse = await client.PollOperationUntilCompleteAsync(response);

                Assert.AreEqual(OperationStates.Succeeded, operationResponse.OperationState, operationResponse.ToString());
                    
                string knowledgebaseId = operationResponse.ResourceLocation.Split(new string[] { "/" }, StringSplitOptions.RemoveEmptyEntries)[1];
                Assert.IsNotNull(knowledgebaseId);

                operationResponse = await client.PollOperationUntilCompleteAsync(await client.UpdateKnowledgebaseAsync(knowledgebaseId, new UpdateKnowledgebaseRequest()
                    {
                        Add = new UpdateKnowledgebaseRequest.AddContent()
                        {
                            QnAList = new List<UpdateKnowledgebaseRequest.QnAItem>()
                            {
                                new UpdateKnowledgebaseRequest.QnAItem()
                                {
                                    Id = 1,
                                    Answer = "Hello world!",
                                    Questions = new List<string>()
                                    {
                                        "Hi",
                                        "Hello"
                                    }
                                }
                            }
                        }
                    }));

                Assert.AreEqual(OperationStates.Succeeded, operationResponse.OperationState, operationResponse.ToString());

                await client.PublishKnowledgebaseAsync(knowledgebaseId);

                GetKnowledgebaseDetailsResponse knowledgebaseDetails = await client.GetKnowledgebaseDetailsAsync(knowledgebaseId);

                string host = new Uri(knowledgebaseDetails.HostName).Host;

                Assert.IsNotNull(host);

                GetEndpointKeysResponse keyResponse = await client.GetEndpointKeysAsync();

                Assert.IsNotNull(keyResponse.PrimaryEndpointKey);

                GenerateAnswerRequest generateAnswerRequest = new GenerateAnswerRequest("hi");

                Assert.AreEqual("Hello world!", (await client.GenerateAnswerAsync(knowledgebaseId, keyResponse.PrimaryEndpointKey, host, generateAnswerRequest)).Answers.First().Answer);

                DownloadKnowledgebaseResponse download = await client.DownloadKnowledgebaseAsync(knowledgebaseId, Environments.Production);
                Assert.AreEqual(2, download.QnADocuments.Count);

                await client.DeleteKnowledgebaseAsync(knowledgebaseId);
            }
        }
    }
}