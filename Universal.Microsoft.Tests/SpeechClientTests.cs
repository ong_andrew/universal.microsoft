﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using System.Threading.Tasks;

namespace Universal.Microsoft.CognitiveServices.Speech.V1
{
    [TestClass]
    [TestCategory("SpeechClient")]
    public class SpeechClientTests
    {
        private const string Region = Regions.WestUS;
        private const string EndpointId = "3d4c8895-31ba-4dbd-844c-ea0741589764";
        private const string SubscriptionKey = "edda15c78392454b9278fb6433a0ea4d";

        [TestMethod]
        public async Task CanRecognizeInteractive()
        {
            using (SpeechClient speechClient = new SpeechClient(Region, SubscriptionKey))
            {
                RecognizeResponse recognizeResponse = await speechClient.RecognizeInteractiveAsync(File.ReadAllBytes(@"Resources\022632_74eeb440-dd93-4fc2-ae2e-61e2033955f1.wav"), EndpointId);
                Assert.AreEqual("Success", recognizeResponse.RecognitionStatus);
            }
        }

        [TestMethod]
        public async Task CanRecognizeDictation()
        {
            using (SpeechClient speechClient = new SpeechClient(Region, SubscriptionKey))
            {
                RecognizeResponse recognizeResponse = await speechClient.RecognizeDictationAsync(File.ReadAllBytes(@"Resources\022632_74eeb440-dd93-4fc2-ae2e-61e2033955f1.wav"), EndpointId, default);
                Assert.AreEqual("Success", recognizeResponse.RecognitionStatus);
            }
        }
    }
}
