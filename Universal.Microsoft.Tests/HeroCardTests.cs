﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using Universal.Microsoft.Cards.Hero;

namespace Universal.Microsoft.Cards
{
    [TestClass]
    [TestCategory("HeroCard")]
    public class HeroCardTests
    {
        [TestMethod]
        public void CanDeserialize()
        {
            HeroCard heroCard = HeroCard.FromString(SampleHeroCard);
            Assert.AreEqual("Seattle Center Monorail", heroCard.Title);
            Assert.AreEqual("Seattle Center Monorail", heroCard.Subtitle);
            Assert.AreEqual("The Seattle Center Monorail is an elevated train line between Seattle Center (near the Space Needle) and downtown Seattle. It was built for the 1962 World's Fair. Its original two trains, completed in 1961, are still in service.", heroCard.Text);

            Assert.IsNotNull(heroCard.Images);
            Assert.AreEqual(1, heroCard.Images.Count());
            Assert.AreEqual("https://upload.wikimedia.org/wikipedia/commons/thumb/4/49/Seattle_monorail01_2008-02-25.jpg/1024px-Seattle_monorail01_2008-02-25.jpg", heroCard.Images.First().Url);
            Assert.AreEqual(2, heroCard.Buttons.Count());
            Assert.AreEqual(ActionTypes.OpenUrl, heroCard.Buttons.First().Type);
            Assert.AreEqual("Official website", heroCard.Buttons.First().Title);
            Assert.AreEqual("https://www.seattlemonorail.com", heroCard.Buttons.First().Value);
            Assert.AreEqual(ActionTypes.OpenUrl, heroCard.Buttons.Last().Type);
            Assert.AreEqual("Wikipeda page", heroCard.Buttons.Last().Title);
            Assert.AreEqual("https://en.wikipedia.org/wiki/Seattle_Center_Monorail", heroCard.Buttons.Last().Value);
        }

        [TestMethod]
        public void SerializationIgnoresNulls()
        {
            HeroCard heroCard = HeroCard.FromString(SampleHeroCard);
            Assert.IsFalse(heroCard.ToString().Contains("null"));
        }

        [TestMethod]
        public void SurviesSerialization()
        {
            HeroCard originalHeroCard = HeroCard.FromString(SampleHeroCard);
            string serializedOriginalHeroCard = originalHeroCard.ToString();
            HeroCard heroCard = HeroCard.FromString(serializedOriginalHeroCard);

            Assert.AreEqual("Seattle Center Monorail", heroCard.Title);
            Assert.AreEqual("Seattle Center Monorail", heroCard.Subtitle);
            Assert.AreEqual("The Seattle Center Monorail is an elevated train line between Seattle Center (near the Space Needle) and downtown Seattle. It was built for the 1962 World's Fair. Its original two trains, completed in 1961, are still in service.", heroCard.Text);

            Assert.IsNotNull(heroCard.Images);
            Assert.AreEqual(1, heroCard.Images.Count());
            Assert.AreEqual("https://upload.wikimedia.org/wikipedia/commons/thumb/4/49/Seattle_monorail01_2008-02-25.jpg/1024px-Seattle_monorail01_2008-02-25.jpg", heroCard.Images.First().Url);
            Assert.AreEqual(2, heroCard.Buttons.Count());
            Assert.AreEqual(ActionTypes.OpenUrl, heroCard.Buttons.First().Type);
            Assert.AreEqual("Official website", heroCard.Buttons.First().Title);
            Assert.AreEqual("https://www.seattlemonorail.com", heroCard.Buttons.First().Value);
            Assert.AreEqual(ActionTypes.OpenUrl, heroCard.Buttons.Last().Type);
            Assert.AreEqual("Wikipeda page", heroCard.Buttons.Last().Title);
            Assert.AreEqual("https://en.wikipedia.org/wiki/Seattle_Center_Monorail", heroCard.Buttons.Last().Value);
        }

        private const string SampleHeroCard = @"{
     ""title"": ""Seattle Center Monorail"",
     ""subtitle"": ""Seattle Center Monorail"",
     ""text"": ""The Seattle Center Monorail is an elevated train line between Seattle Center (near the Space Needle) and downtown Seattle. It was built for the 1962 World's Fair. Its original two trains, completed in 1961, are still in service."",
     ""images"": [
       {
         ""url"":""https://upload.wikimedia.org/wikipedia/commons/thumb/4/49/Seattle_monorail01_2008-02-25.jpg/1024px-Seattle_monorail01_2008-02-25.jpg""
       }
     ],
    ""buttons"": [
      {
         ""type"": ""openUrl"",
         ""title"": ""Official website"",
         ""value"": ""https://www.seattlemonorail.com""
       },
      {
        ""type"": ""openUrl"",
        ""title"": ""Wikipeda page"",
        ""value"": ""https://en.wikipedia.org/wiki/Seattle_Center_Monorail""
       }
     ]
   }";
    }
}
