﻿using Microsoft.AspNetCore.Mvc;
using System.Net.Http;
using System.Threading.Tasks;

namespace Universal.Microsoft.AspNetCore.Mvc
{
    /// <summary>
    /// Represents an <see cref="ActionResult"/> that when execute writes a multipart response to the stream.
    /// </summary>
    public class MultipartContentResult : MultipartContent, IActionResult
    {
        /// <summary>
        /// Creates a new instance of the <see cref="MultipartContentResult"/> class.
        /// </summary>
        public MultipartContentResult() : base() { }
        /// <summary>
        /// Creates a new instance of the <see cref="MultipartContentResult"/> class.
        /// </summary>
        /// <param name="subtype"></param>
        public MultipartContentResult(string subtype) : base(subtype) { }
        /// <summary>
        /// Creates a new instance of the <see cref="MultipartContentResult"/> class.
        /// </summary>
        /// <param name="subtype"></param>
        /// <param name="boundary"></param>
        public MultipartContentResult(string subtype, string boundary) :  base(subtype, boundary) { }

        /// <summary>
        /// <inheritdoc />
        /// </summary>
        /// <param name="actionContext"></param>
        /// <returns></returns>
        public async Task ExecuteResultAsync(ActionContext actionContext)
        {
            actionContext.HttpContext.Response.ContentLength = Headers.ContentLength;
            actionContext.HttpContext.Response.ContentType = Headers.ContentType.ToString();

            await CopyToAsync(actionContext.HttpContext.Response.Body);
        }
    }
}
