﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.Net.Http.Headers;
using System.IO;
using System.Threading.Tasks;
using Universal.Common;

namespace Universal.Microsoft.AspNetCore.Mvc.Formatters
{
    /// <summary>
    /// <see cref="InputFormatter"/> that formats requests with no content type or content type "text/plain" to a string.
    /// </summary>
    public class StringRequestBodyFormatter : InputFormatter
    {
        public StringRequestBodyFormatter()
        {
            SupportedMediaTypes.Add(new MediaTypeHeaderValue(MediaTypes.Text.Plain.ToString()));
        }
        
        public override bool CanRead(InputFormatterContext context)
        {
            string contentType = context.HttpContext.Request.ContentType;
            if (string.IsNullOrEmpty(contentType) || contentType == MediaTypes.Text.Plain)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public override async Task<InputFormatterResult> ReadRequestBodyAsync(InputFormatterContext context)
        {
            HttpRequest httpRequest = context.HttpContext.Request;
            string contentType = context.HttpContext.Request.ContentType;

            if (string.IsNullOrEmpty(contentType) || contentType == MediaTypes.Text.Plain)
            {
                using (StreamReader streamReader = new StreamReader(httpRequest.Body))
                {
                    return await InputFormatterResult.SuccessAsync(await streamReader.ReadToEndAsync());
                }
            }
            else
            {
                return await InputFormatterResult.FailureAsync();
            }
        }
    }
}
