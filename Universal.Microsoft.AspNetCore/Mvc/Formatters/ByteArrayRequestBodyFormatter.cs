﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.Net.Http.Headers;
using System.IO;
using System.Threading.Tasks;
using Universal.Common;

namespace Universal.Microsoft.AspNetCore.Mvc.Formatters
{
    /// <summary>
    /// <see cref="InputFormatter"/> that formats requests with content type "application/octet-stream" to a byte array.
    /// </summary>
    public class ByteArrayRequestBodyFormatter : InputFormatter
    {
        public ByteArrayRequestBodyFormatter()
        {
            SupportedMediaTypes.Add(new MediaTypeHeaderValue(MediaTypes.Application.OctetStream.ToString()));
        }

        public override bool CanRead(InputFormatterContext context)
        {
            string contentType = context.HttpContext.Request.ContentType;
            if (contentType == MediaTypes.Application.OctetStream)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public override async Task<InputFormatterResult> ReadRequestBodyAsync(InputFormatterContext context)
        {
            HttpRequest httpRequest = context.HttpContext.Request;
            string contentType = context.HttpContext.Request.ContentType;

            if (contentType == MediaTypes.Application.OctetStream)
            {
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    await httpRequest.Body.CopyToAsync(memoryStream);
                    return await InputFormatterResult.SuccessAsync(memoryStream.ToArray());
                }
            }
            else
            {
                return await InputFormatterResult.FailureAsync();
            }
        }
    }
}
