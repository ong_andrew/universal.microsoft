﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.ActionConstraints;
using System;
using System.Linq;

namespace Universal.Microsoft.AspNetCore.Mvc
{
    /// <summary>
    /// <see cref="IActionConstraint"/> that specifies that the action should be matched only when all the query parameters are present.
    /// </summary>
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false)]
    public class QueryParametersAttribute : Attribute, IActionConstraint
    {
        /// <summary>
        /// The constraint order.
        /// </summary>
        public int Order { get; private set; }

        private string[] mKeys;

        /// <summary>
        /// Specifies the query parameters that are required.
        /// </summary>
        /// <param name="keys"></param>
        public QueryParametersAttribute(params string[] keys) : this(0, keys) { }
        /// <summary>
        /// Specifies the query parameters that are required.
        /// </summary>
        /// <param name="order"></param>
        /// <param name="keys"></param>
        public QueryParametersAttribute(int order, params string[] keys)
        {
            Order = order;
            mKeys = keys;
        }

        public bool Accept(ActionConstraintContext context)
        {
            IQueryCollection query = context.RouteContext.HttpContext.Request.Query;
            return mKeys.All(key => query.ContainsKey(key));
        }
    }
}
