﻿using Microsoft.AspNetCore.Mvc;
using System;
using Universal.Microsoft.AspNetCore.Mvc.Formatters;

namespace Universal.Microsoft.AspNetCore.Mvc
{
    /// <summary>
    /// Extension methods for <see cref="MvcOptions"/>.
    /// </summary>
    public static class MvcOptionsExtensions
    {
        /// <summary>
        /// Adds a <see cref="StringRequestBodyFormatter"/> and <see cref="ByteArrayRequestBodyFormatter"/> to the input formatter collection used by MVC.
        /// </summary>
        /// <param name="MvcOptions"></param>
        public static void AddRawRequestBodyInputFormatters(this MvcOptions MvcOptions)
        {
            MvcOptions.InputFormatters.Insert(0, new StringRequestBodyFormatter());
            MvcOptions.InputFormatters.Insert(1, new ByteArrayRequestBodyFormatter());
        }
    }
}

namespace Universal.Microsoft.AspNetCore.Mvc.Extensions
{
    /// <summary>
    /// Extension methods for <see cref="MvcOptions"/>.
    /// </summary>
    [Obsolete("Use the equivalent in the base namespace.")]
    public static class MvcOptionsExtensions
    {
        /// <summary>
        /// Adds a <see cref="StringRequestBodyFormatter"/> and <see cref="ByteArrayRequestBodyFormatter"/> to the input formatter collection used by MVC.
        /// </summary>
        /// <param name="MvcOptions"></param>
        [Obsolete("Use the equivalent in the base namespace.")]
        public static void AddRawRequestBodyInputFormatters(this MvcOptions MvcOptions)
        {
            MvcOptions.InputFormatters.Insert(0, new StringRequestBodyFormatter());
            MvcOptions.InputFormatters.Insert(1, new ByteArrayRequestBodyFormatter());
        }
    }
}
