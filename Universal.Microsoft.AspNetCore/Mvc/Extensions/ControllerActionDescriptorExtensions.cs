﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using Universal.Common.Reflection;

namespace Universal.Microsoft.AspNetCore.Mvc
{
    /// <summary>
    /// Extensions for the <see cref="ControllerActionDescriptor"/> class.
    /// </summary>
    public static class ControllerActionDescriptorExtensions
    {
        /// <summary>
        /// Enumerates the attributes on the controller and action method.
        /// </summary>
        /// <param name="controllerActionDescriptor"></param>
        /// <returns></returns>
        public static IEnumerable<Attribute> GetAttributes(this ControllerActionDescriptor controllerActionDescriptor)
        {
            foreach (Attribute attribute in controllerActionDescriptor.GetControllerAttributes().Concat(controllerActionDescriptor.GetActionAttributes()))
            {
                yield return attribute;
            }
        }

        /// <summary>
        /// Enumerates the attributes on the controller and action method of the given type.
        /// </summary>
        /// <typeparam name="TAttribute"></typeparam>
        /// <param name="controllerActionDescriptor"></param>
        /// <returns></returns>
        public static IEnumerable<TAttribute> GetAttributes<TAttribute>(this ControllerActionDescriptor controllerActionDescriptor)
            where TAttribute : Attribute
        {
            foreach (TAttribute attribute in controllerActionDescriptor.GetControllerAttributes<TAttribute>().Concat(controllerActionDescriptor.GetActionAttributes<TAttribute>()))
            {
                yield return attribute;
            }
        }

        /// <summary>
        /// Returns the attributes on the given <see cref="ControllerActionDescriptor"/>'s action.
        /// </summary>
        /// <param name="controllerActionDescriptor"></param>
        /// <returns></returns>
        public static IEnumerable<Attribute> GetActionAttributes(this ControllerActionDescriptor controllerActionDescriptor)
        {
            foreach (Attribute attribute in controllerActionDescriptor.MethodInfo.GetAttributes())
            {
                yield return attribute;
            }
        }

        /// <summary>
        /// Returns the attributes on the given <see cref="ControllerActionDescriptor"/>'s action.
        /// </summary>
        /// <param name="controllerActionDescriptor"></param>
        /// <returns></returns>
        public static IEnumerable<TAttribute> GetActionAttributes<TAttribute>(this ControllerActionDescriptor controllerActionDescriptor)
            where TAttribute : Attribute
        {
            foreach (TAttribute attribute in controllerActionDescriptor.MethodInfo.GetAttributes<TAttribute>())
            {
                yield return attribute;
            }
        }

        /// <summary>
        /// Returns the attributes on the given <see cref="ControllerActionDescriptor"/>'s controller.
        /// </summary>
        /// <param name="controllerActionDescriptor"></param>
        /// <returns></returns>
        public static IEnumerable<Attribute> GetControllerAttributes(this ControllerActionDescriptor controllerActionDescriptor)
        {
            foreach (Attribute attribute in controllerActionDescriptor.ControllerTypeInfo.GetAttributes())
            {
                yield return attribute;
            }
        }

        /// <summary>
        /// Returns the attributes on the given <see cref="ControllerActionDescriptor"/>'s controller.
        /// </summary>
        /// <param name="controllerActionDescriptor"></param>
        /// <returns></returns>
        public static IEnumerable<TAttribute> GetControllerAttributes<TAttribute>(this ControllerActionDescriptor controllerActionDescriptor)
            where TAttribute : Attribute
        {
            foreach (TAttribute attribute in controllerActionDescriptor.ControllerTypeInfo.GetAttributes<TAttribute>())
            {
                yield return attribute;
            }
        }

        /// <summary>
        /// Returns true if the controller or action is marked with an <see cref="AuthorizeAttribute"/>.
        /// Returns false if the controller is marked with an <see cref="AuthorizeAttribute"/> but the action is marked with an <see cref="AllowAnonymousAttribute"/>.
        /// If controllers or actions are annotated with both attributes, the behavior of this method is arbitrary.
        /// </summary>
        /// <param name="controllerActionDescriptor"></param>
        /// <returns></returns>
        public static bool RequiresAuthorization(this ControllerActionDescriptor controllerActionDescriptor)
        {
            bool controllerHasAuthorizeAttribute = controllerActionDescriptor.GetControllerAttributes<AuthorizeAttribute>().Any();
            bool actionHasAuthorizeAttribute = controllerActionDescriptor.GetActionAttributes<AuthorizeAttribute>().Any();
            bool actionHasAllowAnonymousAttribute = controllerActionDescriptor.GetActionAttributes<AllowAnonymousAttribute>().Any();

            if (actionHasAuthorizeAttribute)
            {
                return true;
            }
            else if (controllerHasAuthorizeAttribute)
            {
                if (actionHasAllowAnonymousAttribute)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }

            return false;
        }
    }
}


namespace Universal.Microsoft.AspNetCore.Mvc.Extensions
{
    /// <summary>
    /// Extensions for the <see cref="ControllerActionDescriptor"/> class.
    /// </summary>
    [Obsolete("Use the equivalent in the base namespace.")]
    public static class ControllerActionDescriptorExtensions
    {
        /// <summary>
        /// Enumerates the attributes on the controller and action method.
        /// </summary>
        /// <param name="controllerActionDescriptor"></param>
        /// <returns></returns>
        [Obsolete("Use the equivalent in the base namespace.")]
        public static IEnumerable<Attribute> GetAttributes(this ControllerActionDescriptor controllerActionDescriptor)
        {
            foreach (Attribute attribute in controllerActionDescriptor.GetControllerAttributes().Concat(controllerActionDescriptor.GetActionAttributes()))
            {
                yield return attribute;
            }
        }

        /// <summary>
        /// Enumerates the attributes on the controller and action method of the given type.
        /// </summary>
        /// <typeparam name="TAttribute"></typeparam>
        /// <param name="controllerActionDescriptor"></param>
        /// <returns></returns>
        [Obsolete("Use the equivalent in the base namespace.")]
        public static IEnumerable<TAttribute> GetAttributes<TAttribute>(this ControllerActionDescriptor controllerActionDescriptor)
            where TAttribute : Attribute
        {
            foreach (TAttribute attribute in controllerActionDescriptor.GetControllerAttributes<TAttribute>().Concat(controllerActionDescriptor.GetActionAttributes<TAttribute>()))
            {
                yield return attribute;
            }
        }

        /// <summary>
        /// Returns the attributes on the given <see cref="ControllerActionDescriptor"/>'s action.
        /// </summary>
        /// <param name="controllerActionDescriptor"></param>
        /// <returns></returns>
        [Obsolete("Use the equivalent in the base namespace.")]
        public static IEnumerable<Attribute> GetActionAttributes(this ControllerActionDescriptor controllerActionDescriptor)
        {
            foreach (Attribute attribute in controllerActionDescriptor.MethodInfo.GetAttributes())
            {
                yield return attribute;
            }
        }

        /// <summary>
        /// Returns the attributes on the given <see cref="ControllerActionDescriptor"/>'s action.
        /// </summary>
        /// <param name="controllerActionDescriptor"></param>
        /// <returns></returns>
        [Obsolete("Use the equivalent in the base namespace.")]
        public static IEnumerable<TAttribute> GetActionAttributes<TAttribute>(this ControllerActionDescriptor controllerActionDescriptor)
            where TAttribute : Attribute
        {
            foreach (TAttribute attribute in controllerActionDescriptor.MethodInfo.GetAttributes<TAttribute>())
            {
                yield return attribute;
            }
        }

        /// <summary>
        /// Returns the attributes on the given <see cref="ControllerActionDescriptor"/>'s controller.
        /// </summary>
        /// <param name="controllerActionDescriptor"></param>
        /// <returns></returns>
        [Obsolete("Use the equivalent in the base namespace.")]
        public static IEnumerable<Attribute> GetControllerAttributes(this ControllerActionDescriptor controllerActionDescriptor)
        {
            foreach (Attribute attribute in controllerActionDescriptor.ControllerTypeInfo.GetAttributes())
            {
                yield return attribute;
            }
        }

        /// <summary>
        /// Returns the attributes on the given <see cref="ControllerActionDescriptor"/>'s controller.
        /// </summary>
        /// <param name="controllerActionDescriptor"></param>
        /// <returns></returns>
        [Obsolete("Use the equivalent in the base namespace.")]
        public static IEnumerable<TAttribute> GetControllerAttributes<TAttribute>(this ControllerActionDescriptor controllerActionDescriptor)
            where TAttribute : Attribute
        {
            foreach (TAttribute attribute in controllerActionDescriptor.ControllerTypeInfo.GetAttributes<TAttribute>())
            {
                yield return attribute;
            }
        }

        /// <summary>
        /// Returns true if the controller or action is marked with an <see cref="AuthorizeAttribute"/>.
        /// Returns false if the controller is marked with an <see cref="AuthorizeAttribute"/> but the action is marked with an <see cref="AllowAnonymousAttribute"/>.
        /// If controllers or actions are annotated with both attributes, the behavior of this method is arbitrary.
        /// </summary>
        /// <param name="controllerActionDescriptor"></param>
        /// <returns></returns>
        [Obsolete("Use the equivalent in the base namespace.")]
        public static bool RequiresAuthorization(this ControllerActionDescriptor controllerActionDescriptor)
        {
            bool controllerHasAuthorizeAttribute = controllerActionDescriptor.GetControllerAttributes<AuthorizeAttribute>().Any();
            bool actionHasAuthorizeAttribute = controllerActionDescriptor.GetActionAttributes<AuthorizeAttribute>().Any();
            bool actionHasAllowAnonymousAttribute = controllerActionDescriptor.GetActionAttributes<AllowAnonymousAttribute>().Any();

            if (actionHasAuthorizeAttribute)
            {
                return true;
            }
            else if (controllerHasAuthorizeAttribute)
            {
                if (actionHasAllowAnonymousAttribute)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }

            return false;
        }
    }
}
