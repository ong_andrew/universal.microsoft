﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.Extensions.Primitives;
using System;
using System.Collections.Generic;
using System.Net.Http;

namespace Universal.Microsoft.AspNetCore
{
    /// <summary>
    /// Extensions for the <see cref="HttpRequest"/> class.
    /// </summary>
    public static class HttpRequestExtensions
    {
        /// <summary>
        /// Converts an ASP.NET Core <see cref="HttpRequest"/> to a <see cref="HttpRequestMessage"/>.
        /// </summary>
        /// <param name="HttpRequest"></param>
        /// <returns></returns>
        public static HttpRequestMessage ToHttpRequestMessage(this HttpRequest HttpRequest)
        {
            HttpRequestMessage httpRequestMessage = new HttpRequestMessage();
            var requestMethod = HttpRequest.Method;
            if (!HttpMethods.IsGet(requestMethod) && !HttpMethods.IsHead(requestMethod) && !HttpMethods.IsDelete(requestMethod) && !HttpMethods.IsTrace(requestMethod))
            {
                StreamContent streamContent = new StreamContent(HttpRequest.Body);
                httpRequestMessage.Content = streamContent;
            }

            foreach (KeyValuePair<string, StringValues> header in HttpRequest.Headers)
            {
                if (!httpRequestMessage.Headers.TryAddWithoutValidation(header.Key, header.Value.ToArray()) && httpRequestMessage.Content != null)
                {
                    httpRequestMessage.Content?.Headers.TryAddWithoutValidation(header.Key, header.Value.ToArray());
                }
            }

            Uri uri = new Uri(HttpRequest.GetDisplayUrl());
            httpRequestMessage.Headers.Host = uri.Authority;
            httpRequestMessage.RequestUri = uri;
            httpRequestMessage.Method = new HttpMethod(HttpRequest.Method);

            return httpRequestMessage;
        }
    }
}


namespace Universal.Microsoft.AspNetCore.Extensions
{
    /// <summary>
    /// Extensions for the <see cref="HttpRequest"/> class.
    /// </summary>
    [Obsolete("Use the equivalent in the base namespace.")]
    public static class HttpRequestExtensions
    {
        /// <summary>
        /// Converts an ASP.NET Core <see cref="HttpRequest"/> to a <see cref="HttpRequestMessage"/>.
        /// </summary>
        /// <param name="HttpRequest"></param>
        /// <returns></returns>
        [Obsolete("Use the equivalent in the base namespace.")]
        public static HttpRequestMessage ToHttpRequestMessage(this HttpRequest HttpRequest)
        {
            HttpRequestMessage httpRequestMessage = new HttpRequestMessage();
            var requestMethod = HttpRequest.Method;
            if (!HttpMethods.IsGet(requestMethod) && !HttpMethods.IsHead(requestMethod) && !HttpMethods.IsDelete(requestMethod) && !HttpMethods.IsTrace(requestMethod))
            {
                StreamContent streamContent = new StreamContent(HttpRequest.Body);
                httpRequestMessage.Content = streamContent;
            }
            
            foreach (KeyValuePair<string, StringValues> header in HttpRequest.Headers)
            {
                if (!httpRequestMessage.Headers.TryAddWithoutValidation(header.Key, header.Value.ToArray()) && httpRequestMessage.Content != null)
                {
                    httpRequestMessage.Content?.Headers.TryAddWithoutValidation(header.Key, header.Value.ToArray());
                }
            }

            Uri uri = new Uri(HttpRequest.GetDisplayUrl());
            httpRequestMessage.Headers.Host = uri.Authority;
            httpRequestMessage.RequestUri = uri;
            httpRequestMessage.Method = new HttpMethod(HttpRequest.Method);

            return httpRequestMessage;
        }
    }
}
