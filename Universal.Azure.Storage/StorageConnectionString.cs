﻿using System;
using System.Collections.Generic;
using System.Linq;
using Universal.Common.Collections;

namespace Universal.Azure.Storage
{
    /// <summary>
    /// Class to parse connection strings for Azure Storage.
    /// </summary>
    public class StorageConnectionString
    {
        private IDictionary<string, string> Dictionary { get; set; } = new SparseDictionary<string, string>();

        /// <summary>
        /// Returns the value of AccountName.
        /// </summary>
        public string AccountName
        {
            get => Dictionary["AccountName"];
        }

        /// <summary>
        /// Returns the value of AccountKey.
        /// </summary>
        public string AccountKey
        {
            get => Dictionary["AccountKey"];
        }

        /// <summary>
        /// Returns the value of DefaultEndpointsProtocol.
        /// </summary>
        public string DefaultEndpointsProtocol
        {
            get => Dictionary["DefaultEndpointsProtocol"];
        }

        /// <summary>
        /// Returns the value of EndpointSuffix.
        /// </summary>
        public string EndpointSuffix
        {
            get => Dictionary["EndpointSuffix"];
        }

        /// <summary>
        /// Gets the value of the specified key.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public string this[string key]
        {
            get
            {
                return Dictionary[key];
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="StorageConnectionString"/> from a connection string.
        /// </summary>
        /// <param name="connectionString"></param>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="FormatException"></exception>
        public StorageConnectionString(string connectionString)
        {
            if (connectionString == null)
            {
                throw new ArgumentNullException(nameof(connectionString));
            }

            string[] parts = connectionString.Split(new string[] { ";" }, StringSplitOptions.None).Select(x => x.Trim()).ToArray();
            foreach (string part in parts)
            {
                string[] subparts = part.Split(new string[] { "=" }, StringSplitOptions.None);
                if (subparts.Length < 2)
                {
                    throw new FormatException("Invalid connection string format.");
                }

                Dictionary[subparts[0].Trim()] = string.Join("=", subparts.Skip(1)).Trim();
            }
        }

        public override string ToString()
        {
            return string.Join(";", Dictionary.Select(x => $"{x.Key}={x.Value}"));
        }
    }
}
