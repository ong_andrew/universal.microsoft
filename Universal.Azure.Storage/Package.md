# Universal.Azure.Storage

## Quickstart
### StorageConnectionString
Utility for parsing connection strings.

```csharp
StorageConnectionString connectionString = new StorageConnectionString("...");
Console.WriteLine(connectionString.AccountName); // Get a property.
Console.WriteLine(connectionString["CustomKey"]); // Get a property with no structured getter.
```