﻿namespace Universal.Microsoft.CognitiveServices
{
    /// <summary>
    /// Available regions for the Microsoft Cognitive Services endpoints.
    /// Not all services are available in all regions.
    /// </summary>
    public static class Regions
    {
        public const string AustraliaEast = "australiaeast";
        public const string BrazilSouth = "brazilsouth";
        public const string CanadaCentral = "canadacentral";
        public const string CentralIndia = "centralindia";
        public const string EastAsia = "eastasia";
        public const string EastUS = "eastus";
        public const string EastUS2 = "eastus2";
        public const string FraceCentral = "francecentral";
        public const string JapanEast = "japaneast";
        public const string JapanWest = "japanwest";
        public const string KoreaCentral = "koreacentral";
        public const string NorthCentralUS = "northcentralus";
        public const string NorthEurope = "northeurope";
        public const string SouthCentralUS = "southcentralus";
        public const string SoutheastAsia = "southeastasia";
        public const string UKSouth = "uksouth";
        public const string WestCentralUS = "westcentralus";
        public const string WestEurope = "westeurope";
        public const string WestUS = "westus";
        public const string WestUS2 = "westus2";
        public const string USCentral = "uscentral";
    }
}
