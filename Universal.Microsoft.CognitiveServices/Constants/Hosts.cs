﻿namespace Universal.Microsoft.CognitiveServices
{
    /// <summary>
    /// Available hosts for the Microsoft Cognitive Services endpoints.
    /// Not all services are available in all regions.
    /// </summary>
    public static class Hosts
    {
        public const string AustraliaEast = "australiaeast.api.cognitive.microsoft.com";
        public const string BrazilSouth = "brazilsouth.api.cognitive.microsoft.com";
        public const string CanadaCentral = "canadacentral.api.cognitive.microsoft.com";
        public const string CentralIndia = "centralindia.api.cognitive.microsoft.com";
        public const string EastAsia = "eastasia.api.cognitive.microsoft.com";
        public const string EastUS = "eastus.api.cognitive.microsoft.com";
        public const string EastUS2 = "eastus2.api.cognitive.microsoft.com";
        public const string FraceCentral = "francecentral.api.cognitive.microsoft.com";
        public const string JapanEast = "japaneast.api.cognitive.microsoft.com";
        public const string JapanWest = "japanwest.api.cognitive.microsoft.com";
        public const string KoreaCentral = "koreacentral.api.cognitive.microsoft.com";
        public const string NorthCentralUS = "northcentralus.api.cognitive.microsoft.com";
        public const string NorthEurope = "northeurope.api.cognitive.microsoft.com";
        public const string SouthCentralUS = "southcentralus.api.cognitive.microsoft.com";
        public const string SoutheastAsia = "southeastasia.api.cognitive.microsoft.com";
        public const string UKSouth = "uksouth.api.cognitive.microsoft.com";
        public const string WestCentralUS = "westcentralus.api.cognitive.microsoft.com";
        public const string WestEurope = "westeurope.api.cognitive.microsoft.com";
        public const string WestUS = "westus.api.cognitive.microsoft.com";
        public const string WestUS2 = "westus2.api.cognitive.microsoft.com";
        public const string USCentral = "uscentral.api.cognitive.microsoft.com";
    }
}