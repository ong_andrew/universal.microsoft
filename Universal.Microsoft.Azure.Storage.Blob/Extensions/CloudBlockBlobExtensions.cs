﻿using Microsoft.Azure.Storage.Blob;
using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace Universal.Microsoft.Azure.Storage.Blob
{
    /// <summary>
    /// Extension methods for the <see cref="CloudBlockBlob"/> class.
    /// </summary>
    public static class CloudBlockBlobExtensions
    {
        /// <summary>
        /// Downloads the given <see cref="CloudBlockBlob"/> and returns the output as a byte array.
        /// </summary>
        /// <param name="CloudBlockBlob"></param>
        /// <returns></returns>
        public static byte[] DownloadToByteArray(this CloudBlockBlob CloudBlockBlob)
        {
            using (MemoryStream memoryStream = new MemoryStream())
            {
                CloudBlockBlob.DownloadToStream(memoryStream);

                return memoryStream.ToArray();
            }
        }

        /// <summary>
        /// Downloads the given <see cref="CloudBlockBlob"/> and returns the output as a byte array.
        /// </summary>
        /// <param name="CloudBlockBlob"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public static async Task<byte[]> DownloadToByteArrayAsync(this CloudBlockBlob CloudBlockBlob, CancellationToken cancellationToken = default)
        {
            using (MemoryStream memoryStream = new MemoryStream())
            {
                await CloudBlockBlob.DownloadToStreamAsync(memoryStream, cancellationToken).ConfigureAwait(false);

                return memoryStream.ToArray();
            }
        }

        /// <summary>
        /// Uploads the given byte array.
        /// </summary>
        /// <param name="CloudBlockBlob"></param>
        /// <param name="buffer"></param>
        public static void UploadFromByteArray(this CloudBlockBlob CloudBlockBlob, byte[] buffer)
        {
            CloudBlockBlob.UploadFromByteArray(buffer, 0, buffer.Length);
        }

        /// <summary>
        /// Uploads the given byte array.
        /// </summary>
        /// <param name="CloudBlockBlob"></param>
        /// <param name="buffer"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public static async Task UploadFromByteArrayAsync(this CloudBlockBlob CloudBlockBlob, byte[] buffer, CancellationToken cancellationToken = default)
        {
            await CloudBlockBlob.UploadFromByteArrayAsync(buffer, 0, buffer.Length, cancellationToken).ConfigureAwait(false);
        }
    }
}

namespace Universal.Microsoft.Azure.Storage.Blob.Extensions
{
    /// <summary>
    /// Extension methods for the <see cref="CloudBlockBlob"/> class.
    /// </summary>
    [Obsolete("Use the equivalent in the base namespace.")]
    public static class CloudBlockBlobExtensions
    {
        /// <summary>
        /// Downloads the given <see cref="CloudBlockBlob"/> and returns the output as a byte array.
        /// </summary>
        /// <param name="CloudBlockBlob"></param>
        /// <returns></returns>
        [Obsolete("Use the equivalent in the base namespace.")]
        public static byte[] DownloadToByteArray(this CloudBlockBlob CloudBlockBlob)
        {
            using (MemoryStream memoryStream = new MemoryStream())
            {
                CloudBlockBlob.DownloadToStream(memoryStream);

                return memoryStream.ToArray();
            }
        }

        /// <summary>
        /// Downloads the given <see cref="CloudBlockBlob"/> and returns the output as a byte array.
        /// </summary>
        /// <param name="CloudBlockBlob"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        [Obsolete("Use the equivalent in the base namespace.")]
        public static async Task<byte[]> DownloadToByteArrayAsync(this CloudBlockBlob CloudBlockBlob, CancellationToken cancellationToken = default)
        {
            using (MemoryStream memoryStream = new MemoryStream())
            {
                await CloudBlockBlob.DownloadToStreamAsync(memoryStream, cancellationToken).ConfigureAwait(false);

                return memoryStream.ToArray();
            }
        }

        /// <summary>
        /// Uploads the given byte array.
        /// </summary>
        /// <param name="CloudBlockBlob"></param>
        /// <param name="buffer"></param>
        [Obsolete("Use the equivalent in the base namespace.")]
        public static void UploadFromByteArray(this CloudBlockBlob CloudBlockBlob, byte[] buffer)
        {
            CloudBlockBlob.UploadFromByteArray(buffer, 0, buffer.Length);
        }

        /// <summary>
        /// Uploads the given byte array.
        /// </summary>
        /// <param name="CloudBlockBlob"></param>
        /// <param name="buffer"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        [Obsolete("Use the equivalent in the base namespace.")]
        public static async Task UploadFromByteArrayAsync(this CloudBlockBlob CloudBlockBlob, byte[] buffer, CancellationToken cancellationToken = default)
        {
            await CloudBlockBlob.UploadFromByteArrayAsync(buffer, 0, buffer.Length, cancellationToken).ConfigureAwait(false);
        }
    }
}
