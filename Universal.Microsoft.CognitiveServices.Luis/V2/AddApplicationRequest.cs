﻿using Newtonsoft.Json;
using Universal.Common.Serialization;

namespace Universal.Microsoft.CognitiveServices.Luis.V2
{
    public class AddApplicationRequest : JsonSerializable<AddApplicationRequest>
    {
        [JsonProperty("name")]
        public string Name;
        [JsonProperty("description")]
        public string Description;
        [JsonProperty("culture")]
        public string Culture;
        [JsonProperty("usageScenario")]
        public string UsageScenario;
        [JsonProperty("domain")]
        public string Domain;
        [JsonProperty("initialVersionId")]
        public string InitialVersion;
    }
}