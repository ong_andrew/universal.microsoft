﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using Universal.Common.Serialization;

namespace Universal.Microsoft.CognitiveServices.Luis.V2
{
    public class GetVersionTrainingStatusResponse : JsonCollectionSerializable<GetVersionTrainingStatusResponse, GetVersionTrainingStatusResponse.VersionTrainingStatus>
    {
        public GetVersionTrainingStatusResponse()
        {
            Collection = new List<VersionTrainingStatus>();
        }

        public class VersionTrainingStatus
        {
            [JsonProperty("modelId")]
            public string ModelId;
            [JsonProperty("details")]
            public TrainingDetails Details;

            public VersionTrainingStatus()
            {
                Details = new TrainingDetails();
            }
        }

        public class TrainingDetails
        {
            [JsonProperty("statusId")]
            public int StatusId;
            [JsonProperty("status")]
            public string Status;
            [JsonProperty("exampleCount")]
            public int ExampleCount;
            [JsonProperty("trainingDateTime")]
            public DateTime? TrainingDateTime;
            [JsonProperty("failureReason")]
            public string FailureReason;
        }
    }
}