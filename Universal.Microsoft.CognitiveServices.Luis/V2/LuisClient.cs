﻿using Newtonsoft.Json;
using System;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Universal.Common.Net.Http;

namespace Universal.Microsoft.CognitiveServices.Luis.V2
{
    /// <summary>
    /// Client for interacting with the Microsoft LUIS V2 service.
    /// </summary>
    public class LuisClient : HttpServiceClient
    {
        protected UriGenerator mUriGenerator;
        protected string mSubscriptionKey;
        protected string mEndpointKey;
        protected string mEndpointHost;

        /// <summary>
        /// Initializes a new instance of the <see cref="LuisClient"/> class.
        /// </summary>
        /// <param name="subscriptionKey">Subscription key which provides access to this API. Found in your Cognitive Services accounts.</param>
        /// <param name="endpointKey">Key that provides access to the query endpoint.</param>
        /// <param name="host">The base URL for the service, defaults to "westus.api.cognitive.microsoft.com".</param>
        /// <param name="endpointHost">The base URL for queries, defaults to "westus.api.cognitive.microsoft.com".</param>
        public LuisClient(string subscriptionKey, string endpointKey, string host = Hosts.WestUS, string endpointHost = Hosts.WestUS)
        {
            mUriGenerator = new UriGenerator(host);
            mSubscriptionKey = subscriptionKey;
            mEndpointKey = endpointKey;
            mEndpointHost = endpointHost;
        }

        /// <summary>
        /// Creates a new instance of the <see cref="LuisClient"/> which is suitable for managing the LUIS service via HTTP APIs.
        /// </summary>
        /// <param name="subscriptionKey"></param>
        /// <param name="host"></param>
        /// <returns></returns>
        public static LuisClient CreateApiClient(string subscriptionKey, string host = Hosts.WestUS)
        {
            return new LuisClient(subscriptionKey, null, host);
        }

        /// <summary>
        /// Creates a new instance of the <see cref="LuisClient"/> which is suitable for querying the model endpoints.
        /// </summary>
        /// <param name="endpointKey"></param>
        /// <param name="endpointHost"></param>
        /// <returns></returns>
        public static LuisClient CreateEndpointClient(string endpointKey, string endpointHost = Hosts.WestUS)
        {
            return new LuisClient(null, endpointKey, endpointHost: endpointHost);
        }

        protected override HttpClient CreateHttpClient()
        {
            HttpClient httpClient = base.CreateHttpClient();
            if (mSubscriptionKey != null)
            {
                httpClient.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", mSubscriptionKey);
            }
            return httpClient;
        }

        protected override Task HandleNonSuccessCodeAsync(HttpResponseMessage httpResponseMessage, CancellationToken cancellationToken)
        {
            try
            {
                throw new LuisException(httpResponseMessage);
            }
            catch (JsonSerializationException)
            {
                throw new HttpException(httpResponseMessage);
            }
        }

        /// <summary>
        /// Creates a new LUIS app.
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<string> AddApplicationAsync(AddApplicationRequest request, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Post, mUriGenerator.Apps(), new JsonContent(request.ToString()), cancellationToken: cancellationToken).ConfigureAwait(false))
            {
                return (await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false)).Replace("\"", "");
            }
        }

        /// <summary>
        /// Creates a new version equivalent to the current snapshot of the selected application version.
        /// </summary>
        /// <param name="appId">The application ID.</param>
        /// <param name="versionId">The application version ID.</param>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<string> CloneVersionAsync(string appId, string versionId, CloneVersionRequest request, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Post, mUriGenerator.VersionsClone(appId, versionId), new JsonContent(request.ToString()), cancellationToken: cancellationToken).ConfigureAwait(false))
            {
                return (await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false)).Replace("\"", "");
            }
        }

        /// <summary>
        /// Deletes an application.
        /// </summary>
        /// <param name="appId">The application ID.</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<DeleteApplicationResponse> DeleteApplicationAsync(string appId, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Delete, mUriGenerator.Apps(appId), cancellationToken: cancellationToken).ConfigureAwait(false))
            {
                return DeleteApplicationResponse.FromString(await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
        }

        /// <summary>
        /// Gets the query logs of the past month for the application.
        /// </summary>
        /// <param name="appId">The application ID.</param>
        /// <param name="includeResponse">indicate whether to include the response column in the csv file.</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<string> DownloadApplicationQueryLogs(string appId, bool? includeResponse = null, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Get, mUriGenerator.DownloadQueryLogs(appId, includeResponse), cancellationToken: cancellationToken).ConfigureAwait(false))
            {
                return await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false);
            }
        }

        /// <summary>
        /// Exports a LUIS application to JSON format.
        /// </summary>
        /// <param name="appId">The application version ID.</param>
        /// <param name="versionId">The application ID.</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ExportApplicationVersionResponse> ExportApplicationVersionAsync(string appId, string versionId, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Get, mUriGenerator.ExportApplicationVersion(appId, versionId), cancellationToken: cancellationToken).ConfigureAwait(false))
            {
                return ExportApplicationVersionResponse.FromString(await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
        }

        /// <summary>
        /// Gets the application info.
        /// </summary>
        /// <param name="appId">The application ID.</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<GetApplicationInfoResponse> GetApplicationInfoAsync(string appId, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Get, mUriGenerator.Apps(appId), cancellationToken: cancellationToken).ConfigureAwait(false))
            {
                return GetApplicationInfoResponse.FromString(await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
        }

        /// <summary>
        /// Gets the supported LUIS application cultures.
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<GetLuisApplicationCulturesListResponse> GetLuisApplicationCulturesListAsync(CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Get, mUriGenerator.Cultures(), cancellationToken: cancellationToken).ConfigureAwait(false))
            {
                return GetLuisApplicationCulturesListResponse.FromString(await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
        }

        /// <summary>
        /// Lists all of the user applications.
        /// </summary>
        /// <param name="skip">The number of entries to skip. Default value is 0.</param>
        /// <param name="take">The number of entries to return. Maximum page size is 500. Default is 100.</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<GetApplicationsListResponse> GetApplicationsListAsync(int? skip = null, int? take = null, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Get, mUriGenerator.GetApplicationsList(skip, take), cancellationToken: cancellationToken).ConfigureAwait(false))
            {
                return GetApplicationsListResponse.FromString(await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
        }

        /// <summary>
        /// Gets the task info.
        /// </summary>
        /// <param name="appId">The application ID.</param>
        /// <param name="versionId">The task version ID.</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<GetApplicationVersionResponse> GetApplicationVersionAsync(string appId, string versionId, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Get, mUriGenerator.Versions(appId, versionId).AddSegment(""), cancellationToken: cancellationToken))
            {
                return GetApplicationVersionResponse.FromString(await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
        }

        /// <summary>
        /// Gets the application versions info.
        /// </summary>
        /// <param name="appId">The application ID.</param>
        /// <param name="skip">The number of entries to skip. Default value is 0.</param>
        /// <param name="take">The number of entries to return. Maximum page size is 500. Default is 100.</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<GetApplicationVersionsListResponse> GetApplicationVersionsListAsync(string appId, int? skip = null, int? take = null, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Get, mUriGenerator.GetVersionsList(appId, skip, take), cancellationToken: cancellationToken).ConfigureAwait(false))
            {
                return GetApplicationVersionsListResponse.FromString(await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
        }

        /// <summary>
        /// Gets the training status of all models (intents and entities) for the specified LUIS app. 
        /// You must call the train API to train the LUIS app before you call this API to get training status.
        /// </summary>
        /// <param name="appId">The application ID.</param>
        /// <param name="versionId">The task version ID.</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<GetVersionTrainingStatusResponse> GetVersionTrainingStatusAsync(string appId, string versionId, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Get, mUriGenerator.VersionsTrain(appId, versionId), cancellationToken: cancellationToken).ConfigureAwait(false))
            {
                return GetVersionTrainingStatusResponse.FromString(await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
        }

        /// <summary>
        /// Polls the GetVersionTrainingStatus endpoint until the training is complete.
        /// </summary>
        /// <param name="appId"></param>
        /// <param name="versionId"></param>
        /// <param name="delay"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<GetVersionTrainingStatusResponse> PollUntilTrainingCompletedAsync(string appId, string versionId, int delay = 1000, CancellationToken cancellationToken = default)
        {
            GetVersionTrainingStatusResponse response;
            do
            {
                using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Get, mUriGenerator.VersionsTrain(appId, versionId), cancellationToken: cancellationToken).ConfigureAwait(false))
                {
                    response = GetVersionTrainingStatusResponse.FromString(await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false));
                }

                await Task.Delay(delay).ConfigureAwait(false);
            } while (response.Any(x => x.Details.Status == TrainingStatuses.InProgress || x.Details.Status == TrainingStatuses.Queued));

            return response;
        }

        /// <summary>
        /// Publishes a specific version of the application.
        /// </summary>
        /// <param name="appId">The application version ID.</param>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<PublishApplicationResponse> PublishApplicationAsync(string appId, PublishApplicationRequest request, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Post, mUriGenerator.AppsPublish(appId), new JsonContent(request.ToString()), cancellationToken: cancellationToken).ConfigureAwait(false))
            {
                return PublishApplicationResponse.FromString(await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
        }

        /// <summary>
        /// Sends a training request for a version of a specified LUIS app.
        /// This POST request initiates a request asynchronously. To determine whether the training request is successful, submit a GET request to get training status.
        /// </summary>
        /// <param name="appId">The application ID.</param>
        /// <param name="versionId">The task version ID.</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<TrainApplicationVersionResponse> TrainApplicationVersionAsync(string appId, string versionId, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Post, mUriGenerator.VersionsTrain(appId, versionId), cancellationToken: cancellationToken).ConfigureAwait(false))
            {
                return TrainApplicationVersionResponse.FromString(await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
        }

        /// <summary>
        /// Gets the published endpoint predictions for the given query. The current maximum query size is 500 characters.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="query"></param>
        /// <param name="endpointKey"></param>
        /// <param name="endpointHost"></param>
        /// <param name="staging"></param>
        /// <param name="verbose"></param>
        /// <param name="timezoneOffset"></param>
        /// <param name="bingSpellCheckSubscriptionKey"></param>
        /// <param name="log"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<QueryResponse> QueryAsync(string id, string query, string endpointKey = null, string endpointHost = null, bool staging = false, bool? verbose = null, int? timezoneOffset = null, string bingSpellCheckSubscriptionKey = null, bool? log = null, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Get, mUriGenerator.QueryApp(endpointHost ?? mEndpointHost, id, endpointKey ?? mEndpointKey ?? mSubscriptionKey, staging, verbose, bingSpellCheckSubscriptionKey != null, bingSpellCheckSubscriptionKey, timezoneOffset, log, query), cancellationToken: cancellationToken).ConfigureAwait(false))
            {
                return QueryResponse.FromString(await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
        }
    }
}
