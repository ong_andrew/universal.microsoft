﻿using UriBuilder = Universal.Common.UriBuilder;

namespace Universal.Microsoft.CognitiveServices.Luis.V2
{
    public class UriGenerator
    {
        protected string mHost;

        public UriGenerator(string host)
        {
            mHost = host;
        }

        public UriBuilder Base()
        {
            return new UriBuilder("https", mHost);
        }

        public UriBuilder Base(string path)
        {
            UriBuilder uriBuilder = Base();
            uriBuilder.Path = path;
            return uriBuilder;
        }
        
        public UriBuilder Apps()
        {
            return Base("/luis/api/v2.0/apps");
        }

        public UriBuilder Apps(string appId)
        {
            return Apps().AddSegment(appId);
        }

        public UriBuilder GetApplicationsList(int? skip = null, int? take = null)
        {
            UriBuilder uriBuilder = Apps();
            
            if (skip != null)
            {
                uriBuilder.AddQuery("skip", skip.Value);
            }
            if (take != null)
            {
                uriBuilder.AddQuery("take", take.Value);
            }

            return uriBuilder;
        }

        public UriBuilder AppsPublish(string appId)
        {
            return Apps(appId).AddSegment("publish");
        }

        public UriBuilder Cultures()
        {
            return Apps().AddSegment("cultures");
        }

        public UriBuilder DownloadQueryLogs(string appId, bool? includeResponse = null)
        {
            UriBuilder uriBuilder = Apps(appId).AddSegment("querylogs");

            if (includeResponse != null)
            {
                uriBuilder.AddQuery("includeResponse", includeResponse.Value);
            }

            return uriBuilder;
        }

        public UriBuilder QueryApp(string host, string id, string subscriptionKey, bool staging, bool? verbose, bool? spellCheck, string bingSpellCheckSubscriptionKey, int? timezoneOffset, bool? log, string query)
        {
            UriBuilder uriBuilder = new UriBuilder("https", host);
            uriBuilder.AddSegments("luis", "v2.0", "apps", id);
            uriBuilder.AddQuery("subscription-key", subscriptionKey);
            if (staging)
            {
                uriBuilder.AddQuery("staging", staging);
            }

            if (verbose != null)
            {
                uriBuilder.AddQuery("verbose", verbose.Value);
            }

            if (spellCheck != null)
            {
                uriBuilder.AddQuery("spellCheck", spellCheck.Value);
                uriBuilder.AddQuery("bing-spell-check-subscription-key", bingSpellCheckSubscriptionKey);
            }

            if (log != null)
            {
                uriBuilder.AddQuery("log", log.Value);
            }

            if (timezoneOffset != null)
            {
                uriBuilder.AddQuery("timezoneOffset", timezoneOffset.Value);
            }
            uriBuilder.AddQuery("q", query);
            return uriBuilder;
        }

        public UriBuilder Versions(string appId)
        {
            return Apps(appId).AddSegment("versions");
        }

        public UriBuilder Versions(string appId, string versionId)
        {
            return Versions(appId).AddSegment(versionId);
        }

        public UriBuilder ExportApplicationVersion(string appId, string versionId)
        {
            return Versions(appId, versionId).AddSegment("export");
        }

        public UriBuilder GetVersionsList(string appId, int? skip = null, int? take = null)
        {
            UriBuilder uriBuilder = Versions(appId);

            if (skip != null)
            {
                uriBuilder.AddQuery("skip", skip.Value);
            }
            if (take != null)
            {
                uriBuilder.AddQuery("take", take.Value);
            }

            return uriBuilder;
        }

        public UriBuilder VersionsTrain(string appId, string versionId)
        {
            return Versions(appId, versionId).AddSegment("train");
        }

        public UriBuilder VersionsClone(string appId, string versionId)
        {
            return Versions(appId, versionId).AddSegment("clone");
        }
    }
}
