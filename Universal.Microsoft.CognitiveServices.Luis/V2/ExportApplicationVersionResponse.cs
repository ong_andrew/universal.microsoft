﻿using Newtonsoft.Json;
using System.Collections.Generic;
using Universal.Common.Serialization;

namespace Universal.Microsoft.CognitiveServices.Luis.V2
{
    public class ExportApplicationVersionResponse : JsonSerializable<ExportApplicationVersionResponse>
    {
        [JsonProperty("luis_schema_version")]
        public string LuisSchemaVersion { get; set; }
        [JsonProperty("versionId")]
        public string VersionId { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("desc")]
        public string Description { get; set; }
        [JsonProperty("culture")]
        public string Culture { get; set; }

        [JsonProperty("intents")]
        public List<Intent> Intents { get; set; }
        [JsonProperty("entities")]
        public List<Entity> Entities { get; set; }

        [JsonProperty("composites")]
        public List<Composite> Composites { get; set; }
        [JsonProperty("closedLists")]
        public List<ClosedList> ClosedLists { get; set; }

        [JsonProperty("patternAnyEntities")]
        public List<PatternAnyEntity> PatternAnyEntities { get; set; }

        [JsonProperty("regex_entities")]
        public List<RegexEntity> RegexEntities { get; set; }

        [JsonProperty("prebuiltEntities")]
        public List<PrebuiltEntity> PrebuiltEntities { get; set; }

        [JsonProperty("model_features")]
        public List<ModelFeature> ModelFeatures { get; set; }
        [JsonProperty("regex_features")]
        public List<RegexFeature> RegexFeatures { get; set; }
        [JsonProperty("patterns")]
        public List<PatternItem> Patterns { get; set; }
        [JsonProperty("utterances")]
        public List<Utterance> Utterances { get; set; }

        public ExportApplicationVersionResponse()
        {
            Intents = new List<Intent>();
            Entities = new List<Entity>();
            Composites = new List<Composite>();
            ClosedLists = new List<ClosedList>();
            PatternAnyEntities = new List<PatternAnyEntity>();
            RegexEntities = new List<RegexEntity>();
            PrebuiltEntities = new List<PrebuiltEntity>();
            ModelFeatures = new List<ModelFeature>();
            RegexFeatures = new List<RegexFeature>();
            Patterns = new List<PatternItem>();
            Utterances = new List<Utterance>();
        }

        public class Intent
        {
            [JsonProperty("name")]
            public string Name { get; set; }
            [JsonProperty("inherits")]
            public Inherits Inherits { get; set; }
        }

        public class Inherits
        {
            [JsonProperty("domain_name")]
            public string DomainName { get; set; }
            [JsonProperty("model_name")]
            public string ModelName { get; set; }
        }

        public class Entity
        {
            [JsonProperty("name")]
            public string Name;
            [JsonProperty("roles")]
            public List<string> Roles;
            [JsonProperty("children")]
            public List<string> Children;

            public Entity()
            {
                Roles = new List<string>();
                Children = new List<string>();
            }
        }

        public class Composite
        {
            [JsonProperty("name")]
            public string Name;
            [JsonProperty("roles")]
            public List<string> Roles;
            [JsonProperty("children")]
            public List<string> Children;

            public Composite()
            {
                Roles = new List<string>();
                Children = new List<string>();
            }
        }

        public class ClosedList
        {
            [JsonProperty("name")]
            public string Name { get; set; }
            [JsonProperty("sublists")]
            public List<Sublist> Sublists { get; set; }
            [JsonProperty("roles")]
            public List<string> Roles;

            public ClosedList()
            {
                Roles = new List<string>();
                Sublists = new List<Sublist>();
            }
        }

        public class Sublist
        {
            [JsonProperty("canonicalForm")]
            public string CanonicalForm { get; set; }
            [JsonProperty("list")]
            public List<string> List { get; set; }

            public Sublist()
            {
                List = new List<string>();
            }
        }

        public class PatternAnyEntity
        {
            [JsonProperty("name")]
            public string Name { get; set; }
            [JsonProperty("explicitList")]
            public List<string> ExplicitList { get; set; }
            [JsonProperty("roles")]
            public List<string> Roles;

            public PatternAnyEntity()
            {
                Roles = new List<string>();
                ExplicitList = new List<string>();
            }
        }

        public class RegexEntity
        {
            [JsonProperty("name")]
            public string Name { get; set; }
            [JsonProperty("regexPattern")]
            public string RegexPattern { get; set; }
            [JsonProperty("roles")]
            public List<string> Roles;

            public RegexEntity()
            {
                Roles = new List<string>();
            }
        }

        public class PrebuiltEntity
        {
            [JsonProperty("name")]
            public string Name { get; set; }
            [JsonProperty("roles")]
            public List<string> Roles;

            public PrebuiltEntity()
            {
                Roles = new List<string>();
            }
        }

        public class PatternItem
        {
            [JsonProperty("pattern")]
            public string Pattern { get; set; }
            [JsonProperty("intent")]
            public string Intent { get; set; }
        }

        public class Utterance
        {
            [JsonProperty("text")]
            public string Text { get; set; }
            [JsonProperty("intent")]
            public string Intent { get; set; }
            [JsonProperty("entities")]
            public List<UtteranceEntity> Entities { get; set; }

            public Utterance()
            {
                Entities = new List<UtteranceEntity>();
            }
        }

        public class UtteranceEntity
        {
            [JsonProperty("entity")]
            public string Entity { get; set; }
            [JsonProperty("startPos")]
            public int StartPosition { get; set; }
            [JsonProperty("endPos")]
            public int EndPosition { get; set; }
        }

        public class ModelFeature
        {
            [JsonProperty("name")]
            public string Name { get; set; }
            [JsonProperty("mode")]
            public bool Mode { get; set; }
            [JsonProperty("words")]
            public string Words { get; set; }
            [JsonProperty("activated")]
            public bool Activated { get; set; }
        }

        public class RegexFeature
        {
            [JsonProperty("name")]
            public string Name { get; set; }
            [JsonProperty("pattern")]
            public string Pattern { get; set; }
            [JsonProperty("activated")]
            public bool Activated { get; set; }
        }
    }
}
