﻿using Newtonsoft.Json;
using Universal.Common.Serialization;

namespace Universal.Microsoft.CognitiveServices.Luis.V2
{
    public class CloneVersionRequest : JsonSerializable<CloneVersionRequest>
    {
        [JsonProperty("version")]
        public string Version { get; set; }
    }
}