﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Universal.Microsoft.CognitiveServices.Luis.V2
{
    public static class QueryResponseExtensions
    {
        /// <summary>
        /// Returns a list of canonical values as resolved by LUIS for all entities of a given type.
        /// </summary>
        /// <param name="QueryResponse"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public static IEnumerable<string> ResolveListEntity(this QueryResponse @QueryResponse, string type)
        {
            return @QueryResponse.Entities.Where(x => x.Type == type).Select(x => x.Resolution.AsListResolution()).SelectMany(x => x.Values);
        }

        /// <summary>
        /// Returns a list of DateTimeV2 entities in the <see cref="QueryResponse"/>.
        /// </summary>
        /// <param name="QueryResponse"></param>
        /// <returns></returns>
        public static IEnumerable<QueryResponse.EntityItem> GetDateTimeV2Entities(this QueryResponse @QueryResponse)
        {
            return QueryResponse.Entities.Where(x => x.IsDateTimeV2Entity());
        }

        /// <summary>
        /// Returns true if
        /// </summary>
        /// <param name="EntityItem"></param>
        /// <returns></returns>
        public static bool IsDateTimeV2Entity(this QueryResponse.EntityItem EntityItem)
        {
            return EntityItem.Type.StartsWith("builtin.datetimeV2", StringComparison.InvariantCultureIgnoreCase);
        }
    }
}