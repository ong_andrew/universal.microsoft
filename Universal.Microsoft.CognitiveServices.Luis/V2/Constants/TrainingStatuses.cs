﻿namespace Universal.Microsoft.CognitiveServices.Luis.V2
{
    public static class TrainingStatuses
    {
        public const string Fail = "Fail";
        public const string InProgress = "InProgress";
        public const string Success = "Success";
        public const string UpToDate = "UpToDate";
        public const string Queued = "Queued";
    }
}