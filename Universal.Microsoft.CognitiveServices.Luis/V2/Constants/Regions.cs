﻿namespace Universal.Microsoft.CognitiveServices.Luis.V2
{
    public class Regions
    {
        public const string AustraliaEast = "australiaeast";
        public const string BrazilSouth = "brazilsouth";
        public const string CanadaCentral = "canadacentral";
        public const string CentralIndia = "centralindia";
        public const string EastAsia = "eastasia";
        public const string EastUS = "eastus";
        public const string EastUS2 = "eastus2";
        public const string JapanEast = "japaneast";
        public const string NorthEurope = "northeurope";
        public const string SouthCentralUS = "southcentralus";
        public const string SoutheastAsia = "southeastasia";
        public const string UKSouth = "uksouth";
        public const string WestCentralUS = "westcentralus";
        public const string WestEurope = "westeurope";
        public const string WestUS = "westus";
        public const string WestUS2 = "westus2";
    }
}