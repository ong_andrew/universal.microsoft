﻿namespace Universal.Microsoft.CognitiveServices.Luis.V2
{
    public static class EntityTypes
    {
        public static class BuiltIn
        {
            public const string Number = "builtin.number";
            public const string Ordinal = "builtin.ordinal";
            public const string Temperature = "builtin.temperature";
            public const string Dimension = "builtin.dimension";
            public const string Money = "builtin.money";
            public const string Age = "builtin.age";
            public const string Percentage = "builtin.percentage";

            public static class DateTimeV2
            {
                public const string Date = "builtin.datetimeV2.date";
                public const string DateTime = "builtin.datetimeV2.datetime";
                public const string Time = "builtin.datetimeV2.time";
                public const string DateRange = "builtin.datetimeV2.daterange";
                public const string TimeRange = "builtin.datetimeV2.timerange";
                public const string DateTimeRange = "builtin.datetimeV2.datetimerange";
                public const string Duration = "builtin.datetimeV2.duration";
                public const string Set = "builtin.datetimeV2.set";
            }
        }
    }
}
