﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using Universal.Common.Serialization;

namespace Universal.Microsoft.CognitiveServices.Luis.V2
{
    public class GetApplicationsListResponse : JsonCollectionSerializable<GetApplicationsListResponse, GetApplicationsListResponse.Application>
    {
        public GetApplicationsListResponse()
        {
            Collection = new List<Application>();
        }

        public class Application
        {
            [JsonProperty("id")]
            public string Id;
            [JsonProperty("name")]
            public string Name;
            [JsonProperty("description")]
            public string Description;
            [JsonProperty("culture")]
            public string Culture;
            [JsonProperty("usageScenario")]
            public string UsageScenario;
            [JsonProperty("domain")]
            public string Domain;
            [JsonProperty("versionsCount")]
            public int VersionsCount;
            [JsonProperty("createdDateTime")]
            public DateTime CreatedDateTime;
            [JsonProperty("endpoints")]
            public EndpointsData Endpoints;
            [JsonProperty("endpointHitsCount")]
            public int EndpointHitsCount;
            [JsonProperty("activeVersion")]
            public string ActiveVersion { get; set; }
            [JsonProperty("ownerEmail")]
            public string OwnerEmail { get; set; }

            public class EndpointsData
            {
                [JsonProperty("PRODUCTION")]
                public Endpoint Production { get; set; }
                [JsonProperty(PropertyName = "STAGING")]
                public Endpoint Staging;
            }

            public class Endpoint
            {
                [JsonProperty("versionId")]
                public string VersionId;
                [JsonProperty("isStaging")]
                public bool IsStaging;
                [JsonProperty("endpointUrl")]
                public string EndpointUrl;
                [JsonProperty("endpointRegion")]
                public string EndpointRegion;
                [JsonProperty("assignedEndpointKey")]
                public string AssignedEndpointKey;
                [JsonProperty("publishedDateTime")]
                public DateTime? PublishedDateTime;
            }
        }
    }
}
