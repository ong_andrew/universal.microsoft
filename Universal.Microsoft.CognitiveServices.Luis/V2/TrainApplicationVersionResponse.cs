﻿using Newtonsoft.Json;
using Universal.Common.Serialization;

namespace Universal.Microsoft.CognitiveServices.Luis.V2
{
    public class TrainApplicationVersionResponse : JsonSerializable<TrainApplicationVersionResponse>
    {
        [JsonProperty("statusId")]
        public int StatusId;
        [JsonProperty("status")]
        public string Status;
    }
}
