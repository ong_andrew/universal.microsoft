﻿using System;
using System.Net.Http;
using Universal.Common.Net.Http;

namespace Universal.Microsoft.CognitiveServices.Luis.V2
{
    public class LuisException : HttpException<LuisErrorResponse>
    {
        public LuisException()
        {
        }

        public LuisException(string message) : base(message)
        {
        }

        public LuisException(string message, Exception innerException) : base(message, innerException)
        {
        }

        public LuisException(HttpResponseMessage httpResponseMessage) : base(
            (int)httpResponseMessage.StatusCode,
            httpResponseMessage.ReasonPhrase,
            httpResponseMessage.Content != null ? LuisErrorResponse.FromString(httpResponseMessage.Content.ReadAsStringAsync().GetAwaiter().GetResult()) : null)
        {
        }
    }
}