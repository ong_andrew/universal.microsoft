﻿using Newtonsoft.Json;
using Universal.Common.Serialization;

namespace Universal.Microsoft.CognitiveServices.Luis.V2
{
    public class PublishApplicationResponse : JsonSerializable<PublishApplicationResponse>
    {
        [JsonProperty("endpointUrl")]
        public string EndpointUrl { get; set; }
        [JsonProperty("subscription-key")]
        public string SubscriptionKey { get; set; }
        [JsonProperty("endpointRegion")]
        public string EndpointRegion { get; set; }
        [JsonProperty("isStaging")]
        public bool IsStaging { get; set; }
    }
}
