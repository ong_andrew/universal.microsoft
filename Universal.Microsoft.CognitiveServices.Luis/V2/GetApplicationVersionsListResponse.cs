﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using Universal.Common.Serialization;

namespace Universal.Microsoft.CognitiveServices.Luis.V2
{
    public class GetApplicationVersionsListResponse : JsonCollectionSerializable<GetApplicationVersionsListResponse, GetApplicationVersionsListResponse.ApplicationVersion>
    {
        public GetApplicationVersionsListResponse()
        {
            Collection = new List<ApplicationVersion>();
        }

        public class ApplicationVersion
        {
            [JsonProperty("version")]
            public string Version { get; set; }
            [JsonProperty("createdDateTime")]
            public DateTime? CreatedDateTime { get; set; }
            [JsonProperty("lastModifiedDateTime")]
            public DateTime? LastModifiedDateTime { get; set; }
            [JsonProperty("lastTrainedDateTime")]
            public DateTime? LastTrainedDateTime { get; set; }
            [JsonProperty("lastPublishedDateTime")]
            public DateTime? LastPublishedDateTime { get; set; }
            [JsonProperty("endpointUrl")]
            public string EndpointUrl { get; set; }
            [JsonProperty("assignedEndpointKey")]
            public EndpointKey AssignedEndpointKey { get; set; }
            [JsonProperty("intentsCount")]
            public int IntentsCount { get; set; }
            [JsonProperty("entitiesCount")]
            public int EntitiesCount { get; set; }
            [JsonProperty("endpointHitsCount")]
            public int EndpointHitsCount { get; set; }
            [JsonProperty("trainingStatus")]
            public string TrainingStatus { get; set; }

            public class EndpointKey
            {
                [JsonProperty("SubscriptionKey")]
                public string SubscriptionKey;
                [JsonProperty("SubscriptionName")]
                public string SubscriptionName;
                [JsonProperty("SubscriptionRegion")]
                public string SubscriptionRegion;
            }
        }
    }
}
