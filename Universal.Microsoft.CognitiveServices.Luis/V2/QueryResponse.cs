﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using Universal.Common.Serialization;

namespace Universal.Microsoft.CognitiveServices.Luis.V2
{
    public class QueryResponse : JsonSerializable<QueryResponse>
    {
        [JsonProperty("query")]
        public string Query { get; set; }
        [JsonProperty("topScoringIntent")]
        public IntentItem TopScoringIntent { get; set; }
        [JsonProperty("intents")]
        public List<IntentItem> Intents { get; set; }
        [JsonProperty("entities")]
        public List<EntityItem> Entities { get; set; }
        [JsonProperty("compositeEntities")]
        public List<CompositeEntity> CompositeEntities { get; set; }
        [JsonProperty("sentimentAnalysis")]
        public SentimentAnalysisResult SentimentAnalysis { get; set; }

        public QueryResponse()
        {
            TopScoringIntent = new IntentItem();
            Intents = new List<IntentItem>();
            Entities = new List<EntityItem>();
            CompositeEntities = new List<CompositeEntity>();
            SentimentAnalysis = new SentimentAnalysisResult();
        }

        public class IntentItem
        {
            [JsonProperty("intent")]
            public string Intent { get; set; }
            [JsonProperty("score")]
            public double Score { get; set; }
        }

        public class SentimentAnalysisResult
        {
            [JsonProperty("label")]
            public string Label { get; set; }
            [JsonProperty("score")]
            public float Score { get; set; }
        }

        public class EntityItem
        {
            [JsonProperty("type")]
            public string Type { get; set; }
            [JsonProperty("entity")]
            public string Entity { get; set; }
            [JsonProperty("startIndex")]
            public int StartIndex { get; set; }
            [JsonProperty("endIndex")]
            public int EndIndex { get; set; }
            [JsonProperty("score")]
            public float Score { get; set; }
            [JsonProperty("resolution")]
            public Resolution Resolution { get; set; }

            public EntityItem()
            {
                Resolution = new Resolution();
            }
        }

        public class CompositeEntity
        {
            [JsonProperty("parentType")]
            public string ParentType { get; set; }
            [JsonProperty("value")]
            public string Value { get; set; }
            [JsonProperty("children")]
            public List<Child> Children { get; set; }

            public CompositeEntity()
            {
                Children = new List<Child>();
            }
        }

        public class Child
        {
            [JsonProperty("type")]
            public string Type { get; set; }
            [JsonProperty("value")]
            public string Value { get; set; }
        }

        public class Resolution : JsonSerializable<Resolution>, IListResolution, IDateTimeV2Resolution
        {
            List<string> IListResolution.Values
            {
                get
                {
                    return Values.Cast<string>().ToList();
                }
            }

            List<DateTimeValue> IDateTimeV2Resolution.Values
            {
                get
                {
                    return Values.Select(x => DateTimeValue.FromString(x.ToString())).ToList();
                }
            }

            [JsonProperty("values")]
            public List<object> Values { get; set; }
            [JsonProperty("value")]
            public string Value { get; set; }
            [JsonProperty("unit")]
            public string Unit { get; set; }

            public Resolution()
            {
                Values = new List<object>();
            }

            public IListResolution AsListResolution()
            {
                return this;
            }

            public IDateTimeV2Resolution AsDateTimeV2Resolution()
            {
                return this;
            }
        }

        public interface IListResolution
        {
            List<string> Values { get; }
        }

        public interface IDateTimeV2Resolution
        {
            List<DateTimeValue> Values { get; }
        }

        public class DateTimeValue : JsonSerializable<DateTimeValue>
        {
            [JsonProperty("timex")]
            public string Timex { get; set; }
            [JsonProperty("type")]
            public string Type { get; set; }
            [JsonProperty("value")]
            public string Value { get; set; }
            [JsonProperty("Mod")]
            public string Mod { get; set; }
            [JsonProperty("start")]
            public string Start { get; set; }
            [JsonProperty("end")]
            public string End { get; set; }
        }
    }
}
