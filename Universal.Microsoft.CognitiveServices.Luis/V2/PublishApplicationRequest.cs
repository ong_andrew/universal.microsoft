﻿using Newtonsoft.Json;
using Universal.Common.Serialization;

namespace Universal.Microsoft.CognitiveServices.Luis.V2
{
    public class PublishApplicationRequest : JsonSerializable<PublishApplicationRequest>
    {
        [JsonProperty("versionId")]
        public string VersionId { get; set; }
        [JsonProperty("isStaging")]
        public bool IsStaging { get; set; }
        [JsonProperty("region")]
        public string Region { get; set; }
    }
}