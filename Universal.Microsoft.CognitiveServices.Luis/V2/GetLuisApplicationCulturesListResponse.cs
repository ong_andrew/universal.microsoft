﻿using Newtonsoft.Json;
using System.Collections.Generic;
using Universal.Common.Serialization;

namespace Universal.Microsoft.CognitiveServices.Luis.V2
{
    public class GetLuisApplicationCulturesListResponse : JsonCollectionSerializable<GetLuisApplicationCulturesListResponse, GetLuisApplicationCulturesListResponse.Culture>
    {
        public GetLuisApplicationCulturesListResponse()
        {
            Collection = new List<Culture>();
        }

        public class Culture
        {
            [JsonProperty("name")]
            public string Name { get; set; }
            [JsonProperty("code")]
            public string Code { get; set; }
        }
    }
}