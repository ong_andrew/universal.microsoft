﻿using Azure.Storage.Blobs;
using System;

namespace Universal.Azure.Storage.Blobs
{
    /// <summary>
    /// Extension methods for the <see cref="BlobServiceClient"/> class.
    /// </summary>
    public static class BlobServiceClientExtensions
    {
        /// <summary>
        /// Gets a <see cref="BlobClient"/> from a <see cref="BlobServiceClient"/> and a <see cref="Uri"/> to the blob.
        /// </summary>
        /// <param name="blobServiceClient"></param>
        /// <param name="blobUri"></param>
        /// <returns></returns>
        public static BlobClient GetBlobClient(this BlobServiceClient blobServiceClient, Uri blobUri)
        {
            BlobClient blobClient = new BlobClient(blobUri);
            return blobServiceClient.GetBlobContainerClient(blobClient.BlobContainerName).GetBlobClient(blobClient.Name);
        }
    }
}
