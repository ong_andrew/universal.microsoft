﻿using Microsoft.JSInterop;
using System.Threading.Tasks;

namespace Universal.Microsoft.JSInterop
{
    /// <summary>
    /// Extensions for <see cref="IJSRuntime"/>.
    /// </summary>
    public static class IJSRuntimeExtensions
    {
        /// <summary>
        /// Evaluates the given JavaScript expression.
        /// </summary>
        /// <param name="jsRuntime"></param>
        /// <param name="expression"></param>
        /// <returns></returns>
        public static ValueTask EvaluateAsync(this IJSRuntime jsRuntime, string expression)
        {
            return jsRuntime.InvokeVoidAsync("eval", expression);
        }

        /// <summary>
        /// Evaluates the given JavaScript expression.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="jsRuntime"></param>
        /// <param name="expression"></param>
        /// <returns></returns>
        public static ValueTask<T> EvaluateAsync<T>(this IJSRuntime jsRuntime, string expression)
        {
            return jsRuntime.InvokeAsync<T>("eval", expression);
        }
    }
}
