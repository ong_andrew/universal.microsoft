﻿#if NETSTANDARD2_0
using System;
using System.Text.Json.Serialization;

namespace Universal.Microsoft.JSInterop
{
    /// <summary>
    /// Represents a reference to a JavaScript object.
    /// </summary>
    [JsonConverter(typeof(JSObjectReferenceJsonConverter))]
    public struct JSObjectReference : IDisposable
    {
        /// <summary>
        /// Raised when this object is to be disposed.
        /// </summary>
        public event Action<JSObjectReference> Disposed;

        /// <summary>
        /// Gets or sets the ID of the referenced object.
        /// </summary>
        public string Id { get; set; }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }

        /// <summary>
        /// Called when this item is to be disposed.
        /// </summary>
        void IDisposable.Dispose()
        {
            Disposed?.Invoke(this);
        }
    }
}
#endif