﻿#if NETSTANDARD2_0
using System;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Universal.Microsoft.JSInterop
{
    internal class JSObjectReferenceJsonConverter : JsonConverter<JSObjectReference>
    {
        public override JSObjectReference Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
        {
            if (reader.TokenType != JsonTokenType.StartObject)
            {
                throw new JsonException();
            }

            StringComparison stringComparison = options.PropertyNameCaseInsensitive ? StringComparison.InvariantCultureIgnoreCase : StringComparison.InvariantCulture;

            JSObjectReference jsObjectReference = new JSObjectReference();

            while (reader.Read())
            {
                if (reader.TokenType == JsonTokenType.EndObject)
                {
                    return jsObjectReference;
                }
                else if (reader.TokenType == JsonTokenType.PropertyName)
                {
                    string propertyName = reader.GetString();

                    reader.Read();
                    if (propertyName.Equals(ConvertPropertyName(nameof(JSObjectReference.Id), options), stringComparison))
                    {
                        jsObjectReference.Id = reader.GetString();
                    }
                }
            }

            throw new JsonException();
        }

        public override void Write(Utf8JsonWriter writer, JSObjectReference value, JsonSerializerOptions options)
        {
            writer.WriteStartObject();
            writer.WriteString(ConvertPropertyName("$type", options), typeof(JSObjectReference).Name);
            writer.WriteString(ConvertPropertyName(nameof(JSObjectReference.Id), options), value.Id);
            writer.WriteEndObject();
        }

        private string ConvertPropertyName(string propertyName, JsonSerializerOptions options)
        {
            if (options.PropertyNamingPolicy != null)
            {
                return options.PropertyNamingPolicy.ConvertName(propertyName);
            }
            else
            {
                return propertyName;
            }
        }
    }
}
#endif