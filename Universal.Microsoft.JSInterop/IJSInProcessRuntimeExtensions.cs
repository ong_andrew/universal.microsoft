﻿using Microsoft.JSInterop;

namespace Universal.Microsoft.JSInterop
{
    /// <summary>
    /// Extensions for the <see cref="IJSInProcessRuntime"/> interface.
    /// </summary>
    public static class IJSInProcessRuntimeExtensions
    {        
        /// <summary>
        /// Evaluates the given JavaScript expression.
        /// </summary>
        /// <param name="jsInProcessRuntime"></param>
        /// <param name="expression"></param>
        /// <returns></returns>
        public static void Evaluate(this IJSInProcessRuntime jsInProcessRuntime, string expression)
        {
            jsInProcessRuntime.InvokeVoid("eval", expression);
        }

        /// <summary>
        /// Evaluates the given JavaScript expression.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="jsInProcessRuntime"></param>
        /// <param name="expression"></param>
        /// <returns></returns>
        public static T Evaluate<T>(this IJSInProcessRuntime jsInProcessRuntime, string expression)
        {
            return jsInProcessRuntime.Invoke<T>("eval", expression);
        }
    }
}
