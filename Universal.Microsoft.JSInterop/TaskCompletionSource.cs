﻿using Microsoft.JSInterop;

namespace Universal.Microsoft.JSInterop
{
    /// <summary>
    /// A <see cref="System.Threading.Tasks.TaskCompletionSource{TResult}"/> that supports JS interop.
    /// </summary>
    /// <typeparam name="TResult"></typeparam>
    public class TaskCompletionSource<TResult> : System.Threading.Tasks.TaskCompletionSource<TResult>
    {
        /// <summary>
        /// An <see cref="JSInvokableAttribute"/>-applied <see cref="System.Threading.Tasks.TaskCompletionSource{TResult}.SetResult"/>.
        /// </summary>
        /// <param name="result"></param>
        [JSInvokable]
        public void SetResultJS(TResult result)
        {
            SetResult(result);
        }
    }
}
