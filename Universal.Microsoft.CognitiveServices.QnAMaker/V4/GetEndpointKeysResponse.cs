﻿using Newtonsoft.Json;
using Universal.Common.Serialization;

namespace Universal.Microsoft.CognitiveServices.QnAMaker.V4
{
    public class GetEndpointKeysResponse : JsonSerializable<GetEndpointKeysResponse>
    {
        [JsonProperty("primaryEndpointKey")]
        public string PrimaryEndpointKey { get; set; }
        [JsonProperty("secondaryEndpointKey")]
        public string SecondaryEndpointKey { get; set; }
    }
}
