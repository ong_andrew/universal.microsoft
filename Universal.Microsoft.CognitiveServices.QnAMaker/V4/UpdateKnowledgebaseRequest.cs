﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Universal.Common;
using Universal.Common.Serialization;

namespace Universal.Microsoft.CognitiveServices.QnAMaker.V4
{
    public class UpdateKnowledgebaseRequest : JsonSerializable<UpdateKnowledgebaseRequest>
    {
        [JsonProperty("add")]
        public AddContent Add { get; set; }
        [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeAdd() { return Add != null; }
        [JsonProperty("delete")]
        public DeleteContent Delete { get; set; }
        [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeDelete() { return Delete != null; }
        [JsonProperty("update")]
        public UpdateContent Update { get; set; }
        [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeUpdate() { return Update != null; }

        public class AddContent
        {
            [JsonProperty("qnaList")]
            public List<QnAItem> QnAList { get; set; }
            [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeQnAList() { return QnAList != null && QnAList.Any(); }
            [JsonProperty("urls")]
            public List<string> Urls { get; set; }
            [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeUrls() { return Urls != null && Urls.Any(); }
            [JsonProperty("files")]
            public List<File> Files { get; set; }
            [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeFiles() { return Files != null && Files.Any(); }

            public AddContent()
            {
                QnAList = new List<QnAItem>();
                Urls = new List<string>();
                Files = new List<File>();
            }
        }

        public class QnAItem
        {
            [JsonProperty("id")]
            public int Id { get; set; }
            [JsonProperty("answer")]
            public string Answer { get; set; }
            [JsonProperty("source")]
            public string Source { get; set; }
            [JsonProperty("questions")]
            public List<string> Questions { get; set; }
            [JsonProperty("metadata")]
            public List<Metadata> Metadata { get; set; }
            [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeMetadata() { return Metadata != null && Metadata.Any(); }

            public QnAItem()
            {
                Questions = new List<string>();
                Metadata = new List<Metadata>();
            }
        }

        public class Metadata
        {
            [JsonProperty("name")]
            public string Name { get; set; }
            [JsonProperty("value")]
            public string Value { get; set; }
        }

        public class File
        {
            [JsonProperty("fileName")]
            public string FileName { get; set; }
            [JsonProperty("fileUri")]
            public string FileUri { get; set; }
        }

        public class DeleteContent
        {
            [JsonProperty("ids")]
            public List<int> Ids { get; set; }
            [JsonProperty("sources")]
            public List<string> Sources { get; set; }

            public DeleteContent()
            {
                Ids = new List<int>();
                Sources = new List<string>();
            }
        }

        public class UpdateContent
        {
            [JsonProperty("name")]
            public string Name { get; set; }
            [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeName() { return !Name.IsNullOrEmpty(); }
            [JsonProperty("qnaList")]
            public List<QnAItemUpdate> QnAList { get; set; }
            [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeQnAList() { return QnAList != null && QnAList.Any(); }
            [JsonProperty("urls")]
            public List<string> Urls { get; set; }
            [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeUrls() { return Urls != null && Urls.Any(); }

            public UpdateContent()
            {
                QnAList = new List<QnAItemUpdate>();
                Urls = new List<string>();
            }
        }

        public class QnAItemUpdate
        {
            [JsonProperty("id")]
            public int Id { get; set; }
            [JsonProperty("answer")]
            public string Answer { get; set; }
            [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeAnswer() { return !Answer.IsNullOrEmpty(); }
            [JsonProperty("source")]
            public string Source { get; set; }
            [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeSource() { return !Source.IsNullOrEmpty(); }
            [JsonProperty("questions")]
            public QuestionUpdate Questions { get; set; }
            [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeQuestions() { return Questions != null && (Questions.ShouldSerializeAdd() || Questions.ShouldSerializeDelete()); }
            [JsonProperty("metadata")]
            public MetadataUpdate Metadata { get; set; }
            [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeMetadata() { return Metadata != null && (Metadata.ShouldSerializeAdd() || Metadata.ShouldSerializeDelete()); }

            public QnAItemUpdate()
            {
                Questions = new QuestionUpdate();
                Metadata = new MetadataUpdate();
            }
        }

        public class QuestionUpdate
        {
            [JsonProperty("add")]
            public List<string> Add { get; set; }
            [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeAdd() { return Add != null && Add.Any(); }
            [JsonProperty("delete")]
            public List<string> Delete { get; set; }
            [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeDelete() { return Delete != null && Delete.Any(); }

            public QuestionUpdate()
            {
                Add = new List<string>();
                Delete = new List<string>();
            }
        }

        public class MetadataUpdate
        {
            [JsonProperty("add")]
            public List<Metadata> Add { get; set; }
            [EditorBrowsable(EditorBrowsableState.Never)]public bool ShouldSerializeAdd() { return Add != null && Add.Any(); }
            [JsonProperty("delete")]
            public List<Metadata> Delete { get; set; }
            [EditorBrowsable(EditorBrowsableState.Never)] public bool ShouldSerializeDelete() { return Delete != null && Delete.Any(); }

            public MetadataUpdate()
            {
                Add = new List<Metadata>();
                Delete = new List<Metadata>();
            }
        }
    }
}
