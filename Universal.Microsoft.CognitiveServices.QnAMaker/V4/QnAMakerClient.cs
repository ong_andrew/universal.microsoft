﻿using Newtonsoft.Json;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Universal.Common;
using Universal.Common.Net.Http;

namespace Universal.Microsoft.CognitiveServices.QnAMaker.V4
{
    /// <summary>
    /// Client for interacting with the Microsoft QnA Maker V4 service.
    /// </summary>
    public class QnAMakerClient : HttpServiceClient
    {
        protected UriGenerator mUriGenerator;
        protected string mSubscriptionKey;
        protected string mEndpointKey;
        protected string mEndpointHost;

        /// <summary>
        /// Initializes a new instance of the <see cref="QnAMakerClient"/> class.
        /// </summary>
        /// <param name="subscriptionKey">Subscription key which provides access to this API. Found in your Cognitive Services accounts.</param>
        /// <param name="endpointKey"></param>
        /// <param name="endpointHost"></param>
        /// <param name="apiHost">The base URL for the service, defaults to "westus.api.cognitive.microsoft.com".</param>
        public QnAMakerClient(string subscriptionKey, string endpointKey, string endpointHost, string apiHost = Hosts.WestUS)
        {
            mUriGenerator = new UriGenerator(apiHost);
            mSubscriptionKey = subscriptionKey;
            mEndpointKey = endpointKey;
            mEndpointHost = endpointHost;
        }

        /// <summary>
        /// Creates a new <see cref="QnAMakerClient"/> that can be used to call backend APIs.
        /// </summary>
        /// <param name="subscriptionKey"></param>
        /// <param name="apiHost"></param>
        /// <returns></returns>
        public static QnAMakerClient CreateApiClient(string subscriptionKey, string apiHost = Hosts.WestUS)
        {
            return new QnAMakerClient(subscriptionKey, null, null, apiHost);
        }

        /// <summary>
        /// Creates a new <see cref="QnAMakerClient"/> that can be used to query a knowledgebase endpoint.
        /// </summary>
        /// <param name="endpointKey"></param>
        /// <param name="endpointHost"></param>
        /// <returns></returns>
        public static QnAMakerClient CreateEndpointClient(string endpointKey, string endpointHost)
        {
            return new QnAMakerClient(null, endpointKey, endpointHost);
        }
        
        protected override Task PreProcessHttpRequestMessageAsync(HttpRequestMessage httpRequestMessage, CancellationToken cancellationToken)
        {
            if (!mSubscriptionKey.IsNullOrEmpty())
            {
                httpRequestMessage.Headers.Add("Ocp-Apim-Subscription-Key", mSubscriptionKey);
            }

            return Task.CompletedTask;
        }

        protected override Task HandleNonSuccessCodeAsync(HttpResponseMessage httpResponseMessage, CancellationToken cancellationToken)
        {
            try
            {
                throw new QnAMakerException(httpResponseMessage);
            }
            catch (JsonSerializationException)
            {
                throw new HttpException(httpResponseMessage);
            }
        }

        /// <summary>
        /// Asynchronous operation to create a new knowledgebase.
        /// </summary>
        /// <param name="requestBody"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<CreateKnowledgebaseResponse> CreateKnowledgebaseAsync(string requestBody, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await PostAsync(
                mUriGenerator.CreateKnowledgebase(),
                new StringContent(requestBody, Encoding.UTF8, MediaTypes.Application.Json),
                cancellationToken: cancellationToken
            ).ConfigureAwait(false))
            {
                return CreateKnowledgebaseResponse.FromString(await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
        }

        /// <summary>
        /// Asynchronous operation to create a new knowledgebase.
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<CreateKnowledgebaseResponse> CreateKnowledgebaseAsync(CreateKnowledgebaseRequest request, CancellationToken cancellationToken = default)
        {
            return await CreateKnowledgebaseAsync(request.ToString()).ConfigureAwait(false);
        }

        /// <summary>
        /// Deletes the knowledgebase and all its data.
        /// </summary>
        /// <param name="knowledgebaseId"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task DeleteKnowledgebaseAsync(string knowledgebaseId, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await DeleteAsync(
                mUriGenerator.Knowledgebases(knowledgebaseId),
                cancellationToken: cancellationToken
            ).ConfigureAwait(false))
            {
            }
        }

        /// <summary>
        /// Download alterations from runtime.
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<DownloadAlterationsResponse> DownloadAlterationsAsync(CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await GetAsync(
                mUriGenerator.Alterations(),
                cancellationToken: cancellationToken
            ).ConfigureAwait(false))
            {
                return DownloadAlterationsResponse.FromString(await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
        }

        /// <summary>
        /// Download the knowledgebase.
        /// </summary>
        /// <param name="id">Knowledgebase id</param>
        /// <param name="environment">Specifies whether environment is Test or Prod. See <see cref="Environments" />.</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<DownloadKnowledgebaseResponse> DownloadKnowledgebaseAsync(string id, string environment, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await GetAsync(
                mUriGenerator.Knowledgebases(id, environment),
                cancellationToken: cancellationToken
            ).ConfigureAwait(false))
            {
                return DownloadKnowledgebaseResponse.FromString(await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
        }

        /// <summary>
        /// Generates an answer from a given knowledgebase using the host and key specified on creation of the <see cref="QnAMakerClient"/>.
        /// </summary>
        /// <param name="id">The GUID for your knowledge base.</param>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<GenerateAnswerResponse> GenerateAnswerAsync(string id, GenerateAnswerRequest request, CancellationToken cancellationToken = default)
        {
            return await GenerateAnswerAsync(id, mEndpointKey, mEndpointHost, request, cancellationToken: cancellationToken).ConfigureAwait(false);
        }

        /// <summary>
        /// Generates an answer from a given knowledgebase using the host and key details as specified.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="endpointKey"></param>
        /// <param name="endpointHost">The hostname of the endpoint deployed in your Azure subscription. For example qnamaker.azurewebsites.net.</param>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<GenerateAnswerResponse> GenerateAnswerAsync(string id, string endpointKey, string endpointHost, GenerateAnswerRequest request, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(
                HttpMethod.Post,
                mUriGenerator.GenerateAnswer(endpointHost, id),
                new JsonContent(request.ToString()),
                (httpRequestMessage) => 
                {
                    httpRequestMessage.Headers.Authorization = new AuthenticationHeaderValue("EndpointKey", endpointKey);
                },
                cancellationToken: cancellationToken
            ).ConfigureAwait(false))
            {
                return GenerateAnswerResponse.FromString(await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
        }

        /// <summary>
        /// Gets endpoint keys for an endpoint
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<GetEndpointKeysResponse> GetEndpointKeysAsync(CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await GetAsync(
                mUriGenerator.EndpointKeys(),
                cancellationToken: cancellationToken
            ).ConfigureAwait(false))
            {
                return GetEndpointKeysResponse.FromString(await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
        }

        /// <summary>
        /// Gets details of a specific knowledgebase.
        /// </summary>
        /// <param name="id">Knowledgebase id.</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<GetKnowledgebaseDetailsResponse> GetKnowledgebaseDetailsAsync(string id, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await GetAsync(
                mUriGenerator.Knowledgebases(id),
                cancellationToken: cancellationToken
            ).ConfigureAwait(false))
            {
                return GetKnowledgebaseDetailsResponse.FromString(await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
        }

        /// <summary>
        /// Gets all knowledgebases for a user.
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<GetKnowledgebasesForUserResponse> GetKnowledgebasesForUserAsync(CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await GetAsync(
                mUriGenerator.Knowledgebases(),
                cancellationToken: cancellationToken
            ).ConfigureAwait(false))
            {
                return GetKnowledgebasesForUserResponse.FromString(await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
        }

        /// <summary>
        /// Gets details of a specific long running operation.
        /// </summary>
        /// <param name="id">Operation id.</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<GetOperationDetailsResponse> GetOperationDetailsAsync(string id, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await GetAsync(
                mUriGenerator.Operations(id),
                cancellationToken: cancellationToken
            ).ConfigureAwait(false))
            {
                return GetOperationDetailsResponse.FromString(await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
        }

        /// <summary>
        /// Polls an operation until it completes.
        /// </summary>
        /// <param name="id">The operation ID.</param>
        /// <param name="delay">The polling interval in milliseconds.</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<GetOperationDetailsResponse> PollOperationUntilCompleteAsync(string id, int delay = 1000, CancellationToken cancellationToken = default)
        {
            GetOperationDetailsResponse response;

            do
            {
                response = await GetOperationDetailsAsync(id, cancellationToken).ConfigureAwait(false);

                await Task.Delay(delay).ConfigureAwait(false);
            }
            while (response.OperationState == OperationStates.NotStarted || response.OperationState == OperationStates.Running);

            return response;
        }

        /// <summary>
        ///  Polls an operation until it completes.
        /// </summary>
        /// <param name="operation"></param>
        /// <param name="delay">The polling interval in milliseconds.</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<GetOperationDetailsResponse> PollOperationUntilCompleteAsync(IOperation operation, int delay = 1000, CancellationToken cancellationToken = default)
        {
            return await PollOperationUntilCompleteAsync(operation.OperationId, delay, cancellationToken).ConfigureAwait(false);
        }

        /// <summary>
        /// Publishes all changes in test index of a knowledgebase to its prod index.
        /// </summary>
        /// <param name="id">Knowledgebase id</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task PublishKnowledgebaseAsync(string id, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await PostAsync(
                mUriGenerator.Knowledgebases(id),
                cancellationToken: cancellationToken
            ).ConfigureAwait(false))
            {
            }
        }

        /// <summary>
        /// Re-generates an endpoint key.
        /// </summary>
        /// <param name="keyType">type of Key</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<RefreshEndpointKeysResponse> RefreshEndpointKeys(string keyType, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await PatchAsync(
                mUriGenerator.EndpointKeys(keyType),
                cancellationToken: cancellationToken
            ).ConfigureAwait(false))
            {
                return RefreshEndpointKeysResponse.FromString(await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
        }

        /// <summary>
        /// Replace alterations data.
        /// </summary>
        /// <param name="requestBody"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task ReplaceAlterationsAsync(string requestBody, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await PutAsync(
                mUriGenerator.Alterations(),
                new StringContent(requestBody, Encoding.UTF8, MediaTypes.Application.Json),
                cancellationToken: cancellationToken
            ).ConfigureAwait(false))
            {
            }
        }

        /// <summary>
        /// Replace alterations data.
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task ReplaceAlterationsAsync(ReplaceAlterationsRequest request, CancellationToken cancellationToken = default)
        {
            await ReplaceAlterationsAsync(request.ToString(), cancellationToken).ConfigureAwait(false);
        }

        /// <summary>
        /// Replace knowledgebase contents.
        /// </summary>
        /// <param name="id">Knowledgebase id</param>
        /// <param name="requestBody"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task ReplaceKnowledgebaseAsync(string id, string requestBody, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await PutAsync(
                mUriGenerator.Knowledgebases(id),
                new StringContent(requestBody, Encoding.UTF8, MediaTypes.Application.Json),
                cancellationToken: cancellationToken
            ).ConfigureAwait(false))
            {
            }
        }

        /// <summary>
        /// Asynchronous operation to modify a knowledgebase.
        /// </summary>
        /// <param name="id">Knowledgebase id</param>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task ReplaceKnowledgebaseAsync(string id, ReplaceKnowledgebaseRequest request, CancellationToken cancellationToken = default)
        {
            await ReplaceKnowledgebaseAsync(id, request.ToString(), cancellationToken).ConfigureAwait(false);
        }

        /// <summary>
        /// Asynchronous operation to modify a knowledgebase.
        /// </summary>
        /// <param name="id">Knowledgebase id</param>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<UpdateKnowledgebaseResponse> UpdateKnowledgebaseAsync(string id, UpdateKnowledgebaseRequest request, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await PatchAsync(
                mUriGenerator.Knowledgebases(id),
                new JsonContent(request.ToString()),
                cancellationToken: cancellationToken
            ).ConfigureAwait(false))
            {
                return UpdateKnowledgebaseResponse.FromString(await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
        }
    }
}
