﻿using System;
using System.Net.Http;
using Universal.Common.Net.Http;

namespace Universal.Microsoft.CognitiveServices.QnAMaker.V4
{
    /// <summary>
    /// Represents an error returned by the QnA Maker service.
    /// </summary>
    public class QnAMakerException : HttpException<QnAMakerErrorResponse>
    {
        public QnAMakerException()
        {
        }

        public QnAMakerException(string message) : base(message)
        {
        }

        public QnAMakerException(string message, Exception innerException) : base(message, innerException)
        {
        }

        public QnAMakerException(HttpResponseMessage httpResponseMessage) : base(
            (int)httpResponseMessage.StatusCode,
            httpResponseMessage.ReasonPhrase,
            httpResponseMessage.Content != null ? QnAMakerErrorResponse.FromString(httpResponseMessage.Content.ReadAsStringAsync().GetAwaiter().GetResult()) : null)
        {
        }
    }
}
