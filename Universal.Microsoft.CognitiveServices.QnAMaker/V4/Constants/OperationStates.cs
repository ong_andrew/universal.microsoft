﻿namespace Universal.Microsoft.CognitiveServices.QnAMaker.V4
{
    /// <summary>
    /// Possible values returned in the operation state field by the QnA Maker API.
    /// </summary>
    public static class OperationStates
    {
        public const string Failed = "Failed";
        public const string NotStarted = "NotStarted";
        public const string Running = "Running";
        public const string Succeeded = "Succeeded";
    }
}