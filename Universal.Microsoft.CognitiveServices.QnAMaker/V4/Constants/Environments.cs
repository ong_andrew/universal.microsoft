﻿namespace Universal.Microsoft.CognitiveServices.QnAMaker.V4
{
    public static class Environments
    {
        public const string Production = "Prod";
        public const string Test = "Test";
    }
}