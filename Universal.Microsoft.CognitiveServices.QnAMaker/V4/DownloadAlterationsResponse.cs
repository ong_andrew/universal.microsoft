﻿using Newtonsoft.Json;
using System.Collections.Generic;
using Universal.Common.Serialization;

namespace Universal.Microsoft.CognitiveServices.QnAMaker.V4
{
    public class DownloadAlterationsResponse : JsonSerializable<DownloadAlterationsResponse>
    {
        [JsonProperty("wordAlterations")]
        public List<WordAlteration> WordAlterations { get; set; }

        public DownloadAlterationsResponse()
        {
            WordAlterations = new List<WordAlteration>();
        }

        public class WordAlteration
        {
            [JsonProperty("alterations")]
            public List<string> Alterations { get; set; }

            public WordAlteration()
            {
                Alterations = new List<string>();
            }
        }
    }
}
