﻿using Newtonsoft.Json;
using System.Collections.Generic;
using Universal.Common.Serialization;

namespace Universal.Microsoft.CognitiveServices.QnAMaker.V4
{
    public class GenerateAnswerResponse : JsonSerializable<GenerateAnswerResponse>
    {
        [JsonProperty("answers")]
        public List<AnswerItem> Answers { get; set; }
        [JsonProperty("debugInfo")]
        public object DebugInfo { get; set; }

        public GenerateAnswerResponse()
        {
            Answers = new List<AnswerItem>();
        }

        public class AnswerItem
        {
            [JsonProperty("score")]
            public float Score { get; set; }
            [JsonProperty("Id")]
            public int Id { get; set; }
            [JsonProperty("answer")]
            public string Answer { get; set; }
            [JsonProperty("source")]
            public string Source { get; set; }
            [JsonProperty("questions")]
            public List<string> Questions { get; set; }
            [JsonProperty("metadata")]
            public List<Metadata> Metadata { get; set; }
            public Context Context { get; set; }

            public AnswerItem()
            {
                Questions = new List<string>();
                Metadata = new List<Metadata>();
            }
        }

        public class Metadata
        {
            [JsonProperty("name")]
            public string Name { get; set; }
            [JsonProperty("value")]
            public string Value { get; set; }
        }


        public class Context
        {
            [JsonProperty("isContextOnly")]
            public bool IsContextOnly { get; set; }
            [JsonProperty("prompts")]
            public List<Prompt> Prompts { get; set; }

            public Context()
            {
                Prompts = new List<Prompt>();
            }
        }

        public class Prompt
        {
            [JsonProperty("displayOrder")]
            public int DisplayOrder { get; set; }
            [JsonProperty("qnaId")]
            public int QnAaId { get; set; }
            [JsonProperty("qna")]
            public object QnA { get; set; }
            [JsonProperty("displayText")]
            public string DisplayText { get; set; }
        }
    }
}
