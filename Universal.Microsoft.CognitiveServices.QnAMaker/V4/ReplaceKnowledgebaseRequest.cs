﻿using Newtonsoft.Json;
using System.Collections.Generic;
using Universal.Common.Serialization;

namespace Universal.Microsoft.CognitiveServices.QnAMaker.V4
{
    public class ReplaceKnowledgebaseRequest : JsonSerializable<ReplaceKnowledgebaseRequest>
    {
        [JsonProperty("qnAList")]
        public List<QnAItem> QnAs { get; set; }

        public ReplaceKnowledgebaseRequest()
        {
            QnAs = new List<QnAItem>();
        }

        public class QnAItem
        {
            [JsonProperty("id")]
            public int Id { get; set; }
            [JsonProperty("answer")]
            public string Answer { get; set; }
            [JsonProperty("source")]
            public string Source { get; set; }
            [JsonProperty("questions")]
            public List<string> Questions { get; set; }
            [JsonProperty("metadata")]
            public List<Metadata> Metadata { get; set; }

            public QnAItem()
            {
                Questions = new List<string>();
                Metadata = new List<Metadata>();
            }
        }

        public class Metadata
        {
            [JsonProperty("name")]
            public string Name { get; set; }
            [JsonProperty("value")]
            public string Value { get; set; }
        }
    }
}
