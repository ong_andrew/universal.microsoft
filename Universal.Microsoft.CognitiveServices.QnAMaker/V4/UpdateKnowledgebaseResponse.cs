﻿using Newtonsoft.Json;
using System;
using Universal.Common.Serialization;

namespace Universal.Microsoft.CognitiveServices.QnAMaker.V4
{
    public class UpdateKnowledgebaseResponse : JsonSerializable<UpdateKnowledgebaseResponse>, IOperation
    {
        [JsonProperty("operationState")]
        public string OperationState { get; set; }
        [JsonProperty("createdTimestamp")]
        public DateTime CreatedTimestamp { get; set; }
        [JsonProperty("lastActionTimestamp")]
        public DateTime LastActionTimestamp { get; set; }
        [JsonProperty("userId")]
        public string UserId { get; set; }
        [JsonProperty("operationId")]
        public string OperationId { get; set; }
    }
}
