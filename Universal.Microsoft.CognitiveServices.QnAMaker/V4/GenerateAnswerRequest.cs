﻿using Newtonsoft.Json;
using System.Collections.Generic;
using Universal.Common.Serialization;

namespace Universal.Microsoft.CognitiveServices.QnAMaker.V4
{
    public class GenerateAnswerRequest : JsonSerializable<GenerateAnswerRequest>
    {
        [JsonProperty("question")]
        public string Question { get; set; }
        [JsonProperty("top")]
        public int Top { get; set; }
        [JsonProperty("userId")]
        public string UserId { get; set; }
        [JsonProperty("strictFilters")]
        public List<Metadata> StrictFilters { get; set; }

        public GenerateAnswerRequest()
        {
            Top = 1;
            StrictFilters = new List<Metadata>();
        }

        public GenerateAnswerRequest(string question)
        {
            Top = 1;
            StrictFilters = new List<Metadata>();
            Question = question;
        }

        public class Metadata
        {
            [JsonProperty("name")]
            public string Name { get; set; }
            [JsonProperty("value")]
            public string Value { get; set; }
        }
    }
}
