﻿using Newtonsoft.Json;
using System.Collections.Generic;
using Universal.Common.Serialization;

namespace Universal.Microsoft.CognitiveServices.QnAMaker.V4
{
    public class ReplaceAlterationsRequest : JsonSerializable<ReplaceAlterationsRequest>
    {
        [JsonProperty("wordAlterations")]
        public List<WordAlteration> WordAlterations { get; set; }

        public ReplaceAlterationsRequest()
        {
            WordAlterations = new List<WordAlteration>();
        }

        public class WordAlteration
        {
            [JsonProperty("alterations")]
            public List<string> Alterations { get; set; }

            public WordAlteration()
            {
                Alterations = new List<string>();
            }
        }
    }

}
