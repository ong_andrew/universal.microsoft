﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using Universal.Common.Serialization;

namespace Universal.Microsoft.CognitiveServices.QnAMaker.V4
{
    public class GetOperationDetailsResponse : JsonSerializable<GetOperationDetailsResponse>, IOperation
    {
        [JsonProperty("operationState")]
        public string OperationState { get; set; }
        [JsonProperty("createdTimestamp")]
        public DateTime CreatedTimestamp { get; set; }
        [JsonProperty("lastActionTimestamp")]
        public DateTime LastActionTimestamp { get; set; }
        [JsonProperty("resourceLocation")]
        public string ResourceLocation { get; set; }
        [JsonProperty("userId")]
        public string UserId { get; set; }
        [JsonProperty("operationId")]
        public string OperationId { get; set; }
        [JsonProperty("errorResponse")]
        public ErrorResponseItem ErrorResponse { get; set; }

        public class ErrorResponseItem
        {
            public Error Error { get; set; }
        }

        public class Error
        {
            [JsonProperty("code")]
            public string Code { get; set; }
            [JsonProperty("message")]
            public string Message { get; set; }
            [JsonProperty("details")]
            public List<Detail> Details { get; set; }

            public Error()
            {
                Details = new List<Detail>();
            }
        }

        public class Detail
        {
            [JsonProperty("code")]
            public string Code { get; set; }
            [JsonProperty("message")]
            public string Message { get; set; }
            [JsonProperty("target")]
            public string Target { get; set; }
        }

        public GetOperationDetailsResponse()
        {
            ErrorResponse = new GetOperationDetailsResponse.ErrorResponseItem();
        }
    }
}