﻿using Newtonsoft.Json;
using System.Collections.Generic;
using Universal.Common.Serialization;

namespace Universal.Microsoft.CognitiveServices.QnAMaker.V4
{
    /// <summary>
    /// Represents an error response returned by the QnA Maker service.
    /// </summary>
    public class QnAMakerErrorResponse : JsonSerializable<QnAMakerErrorResponse>
    {
        [JsonProperty("error")]
        public ErrorDetails Error { get; set; }

        public class ErrorDetails
        {
            [JsonProperty("code")]
            public string Code { get; set; }
            [JsonProperty("message")]
            public string Message { get; set; }
            [JsonProperty("details")]
            public List<Detail> Details { get; set; }

            public ErrorDetails()
            {
                Details = new List<Detail>();
            }
        }

        public class Detail
        {
            [JsonProperty("code")]
            public string Code { get; set; }
            [JsonProperty("message")]
            public string Message { get; set; }
            [JsonProperty("target")]
            public string Target { get; set; }
        }

        public QnAMakerErrorResponse()
        {
            Error = new ErrorDetails();
        }
    }
}
