﻿using System;
using UriBuilder = Universal.Common.UriBuilder;

namespace Universal.Microsoft.CognitiveServices.QnAMaker.V4
{
    public class UriGenerator
    {
        protected string mHost;

        public UriGenerator(string host)
        {
            mHost = host;
        }

        public UriBuilder NewBuilder()
        {
            return new UriBuilder("https", mHost);
        }

        public UriBuilder NewBuilder(string path)
        {
            UriBuilder uriBuilder = NewBuilder();
            uriBuilder.Path = path;
            return uriBuilder;
        }

        public Uri CreateKnowledgebase()
        {
            UriBuilder builder = new UriBuilder(Knowledgebases());
            builder.AddSegment("create");
            return builder.Uri;
        }

        public Uri GenerateAnswer(string host, string id)
        {
            UriBuilder builder = new UriBuilder("https", host);
            builder.AddSegments("qnamaker", "knowledgebases", id, "generateAnswer");
            return builder.Uri;
        }

        public Uri Alterations()
        {
            return NewBuilder("/qnamaker/v4.0/alterations").Uri;
        }

        public Uri Knowledgebases()
        {
            return NewBuilder("/qnamaker/v4.0/knowledgebases").Uri;
        }

        public Uri Knowledgebases(string id)
        {
            UriBuilder builder = new UriBuilder(Knowledgebases());
            builder.AddSegment(id);
            return builder.Uri;
        }

        public Uri Knowledgebases(string id, string environment)
        {
            UriBuilder builder = new UriBuilder(Knowledgebases());
            builder.AddSegments(id, environment, "qna");
            return builder.Uri;
        }

        public Uri Operations()
        {
            return NewBuilder("/qnamaker/v4.0/operations").Uri;
        }

        public Uri Operations(string id)
        {
            UriBuilder builder = new UriBuilder(Operations());
            builder.AddSegment(id);
            return builder.Uri;
        }

        public Uri EndpointKeys()
        {
            return NewBuilder("/qnamaker/v4.0/endpointkeys").Uri;
        }

        public Uri EndpointKeys(string keyType)
        {
            UriBuilder builder = new UriBuilder(EndpointKeys());
            builder.AddSegment(keyType);
            return builder.Uri;
        }
    }
}
