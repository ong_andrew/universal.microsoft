﻿namespace Universal.Microsoft.CognitiveServices.QnAMaker.V4
{
    /// <summary>
    /// Represents a long running operation that can be polled through the QnA Maker APIs.
    /// </summary>
    public interface IOperation
    {
        string OperationId { get; }
    }
}
