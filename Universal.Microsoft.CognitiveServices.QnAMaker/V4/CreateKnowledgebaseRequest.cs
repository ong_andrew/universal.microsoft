﻿using Newtonsoft.Json;
using System.Collections.Generic;
using Universal.Common.Serialization;

namespace Universal.Microsoft.CognitiveServices.QnAMaker.V4
{
    public class CreateKnowledgebaseRequest : JsonSerializable<CreateKnowledgebaseRequest>
    {
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("qnaList")]
        public List<QnAItem> QnAs { get; set; }
        [JsonProperty("urls")]
        public List<string> Urls { get; set; }
        [JsonProperty("files")]
        public List<File> Files { get; set; }

        public CreateKnowledgebaseRequest()
        {
            QnAs = new List<QnAItem>();
            Urls = new List<string>();
            Files = new List<File>();
        }

        public class QnAItem
        {
            [JsonProperty("id")]
            public int Id { get; set; }
            [JsonProperty("answer")]
            public string Answer { get; set; }
            [JsonProperty("source")]
            public string Source { get; set; }
            [JsonProperty("questions")]
            public List<string> Questions { get; set; }
            [JsonProperty("metadata")]
            public List<Metadata> Metadata { get; set; }

            public QnAItem()
            {
                Questions = new List<string>();
                Metadata = new List<Metadata>();
            }
        }

        public class Metadata
        {
            [JsonProperty("name")]
            public string Name { get; set; }
            [JsonProperty("value")]
            public string Value { get; set; }
        }

        public class File
        {
            [JsonProperty("fileName")]
            public string FileName { get; set; }
            [JsonProperty("fileUri")]
            public string FileUri { get; set; }
        }
    }
    
    
}






