﻿namespace Universal.Microsoft.Cards.Adaptive
{
    public class ActionTypes
    {
        /// <summary>
        /// When invoked, show the given url either by launching it in an external web browser or showing within an embedded web browser.
        /// </summary>
        public const string OpenUrl = "Action.OpenUrl";
        /// <summary>
        /// Defines an AdaptiveCard which is shown to the user when the button or link is clicked.
        /// </summary>
        public const string ShowCard = "Action.ShowCard";
        /// <summary>
        /// Gathers input fields, merges with optional data field, and sends an event to the client. It is up to the client to determine how this data is processed. For example: With BotFramework bots, the client would send an activity through the messaging medium to the bot.
        /// </summary>
        public const string Submit = "Action.Submit";
        /// <summary>
        /// An action that toggles the visibility of associated card elements.
        /// </summary>
        public const string ToggleVisibility = "Action.ToggleVisibility";
    }
}
