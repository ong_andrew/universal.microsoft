﻿using Newtonsoft.Json;
using Universal.Common.Serialization;

namespace Universal.Microsoft.Cards.Hero
{
    [JsonObject(ItemNullValueHandling = NullValueHandling.Ignore)]
    public class Image : JsonSerializable<Image>
    {
        [JsonProperty("url")]
        public string Url { get; set; }
        [JsonProperty("alt")]
        public string Alt { get; set; }
        [JsonProperty("tap")]
        public Action Tap { get; set; }
    }
}
