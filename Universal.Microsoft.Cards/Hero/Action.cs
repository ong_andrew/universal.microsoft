﻿using Newtonsoft.Json;
using Universal.Common.Serialization;

namespace Universal.Microsoft.Cards.Hero
{
    [JsonObject(ItemNullValueHandling = NullValueHandling.Ignore)]
    public class Action : JsonSerializable<Action>
    {
        [JsonProperty("type")]
        public string Type { get; set; }
        [JsonProperty("title")]
        public string Title { get; set; }
        [JsonProperty("value")]
        public object Value { get; set; }
        [JsonProperty("displayText")]
        public string DisplayText { get; set; }
        [JsonProperty("text")]
        public string Text { get; set; }
    }
}
