﻿namespace Universal.Microsoft.Cards.Hero
{
    public static class ActionTypes
    {
        /// <summary>
        /// Initiates a phone call.
        /// </summary>
        public const string Call = "call";
        /// <summary>
        /// Downloads a file.
        /// </summary>
        public const string DownloadFile = "downloadFile";
        /// <summary>
        /// Sends a message to the bot (from the user who clicked the button or tapped the card). This message (from user to bot) is visible to all conversation participants.
        /// </summary>
        public const string ImBack = "imBack";
        /// <summary>
        /// Sends a message and payload to the bot (from the user who clicked the button or tapped the card). This message is not visible.
        /// </summary>
        public const string Invoke = "invoke";
        /// <summary>
        /// Sends a message and payload to the bot (from the user who clicked the button or tapped the card) and sends a separate message to the chat stream.
        /// </summary>
        public const string MessageBack = "messageBack";
        /// <summary>
        /// Opens a URL in the default browser.
        /// </summary>
        public const string OpenUrl = "openUrl";
        /// <summary>
        ///
        /// </summary>
        public const string Payment = "payment";
        /// <summary>
        /// Plays audio.
        /// </summary>
        public const string PlayAudio = "playAudio";
        /// <summary>
        /// Plays a video.
        /// </summary>
        public const string PlayVideo = "playVideo";
        /// <summary>
        /// Sends a message to the bot, and may not post a visible response in the chat.
        /// </summary>
        public const string PostBack = "postBack";
        /// <summary>
        /// Displays an image.
        /// </summary>
        public const string ShowImage = "showImage";
        /// <summary>
        /// Initiates OAuth flow, allowing bots to connect with secure services.
        /// </summary>
        public const string Signin = "signin";
    }
}
