﻿using Newtonsoft.Json;
using Universal.Common.Serialization;

namespace Universal.Microsoft.Cards.Hero
{
    /// <summary>
    /// A card that typically contains a single large image, one or more buttons and text.
    /// </summary>
    [JsonObject(ItemNullValueHandling = NullValueHandling.Ignore)]
    public class HeroCard : JsonSerializable<HeroCard>
    {
        /// <summary>
        /// Title of the card.
        /// </summary>
        [JsonProperty("title")]
        public string Title { get; set; }
        /// <summary>
        /// Subtitle of the card.
        /// </summary>
        [JsonProperty("subtitle")]
        public string Subtitle { get; set; }
        /// <summary>
        /// Text appears just below the subtitle.
        /// </summary>
        [JsonProperty("text")]
        public string Text { get; set; }
        /// <summary>
        /// Image displayed at top of card.
        /// </summary>
        [JsonProperty("images")]
        public Image[] Images { get; set; }
        /// <summary>
        /// Set of actions applicable to the current card.
        /// </summary>
        [JsonProperty("tap")]
        public Action Tap { get; set; }
        /// <summary>
        /// This action will be activated when the user taps on the card itself.
        /// </summary>
        [JsonProperty("buttons")]
        public Action[] Buttons { get; set; }
    }
}
