﻿using Newtonsoft.Json;
using System;
using Universal.Common.Serialization;

namespace Universal.Microsoft.CognitiveServices.SpeakerRecognition.V1
{
    public class GetVerificationProfileResponse : JsonSerializable<GetVerificationProfileResponse>
    {
        /// <summary>
        /// Id of the speaker verification profile.
        /// </summary>
        [JsonProperty("verificationProfileId")]
        public string VerificationProfileId { get; set; }
        /// <summary>
        /// Language locale of the speaker verification profile. See <see cref="Locales"/>.
        /// </summary>
        [JsonProperty("locale")]
        public string Locale { get; set; }
        /// <summary>
        /// Speaker enrollments count.
        /// </summary>
        [JsonProperty("enrollmentsCount")]
        public int EnrollmentsCount { get; set; }
        /// <summary>
        /// Remaining number of required enrollments if enrollmentStatus == Enrolling.
        /// </summary>
        [JsonProperty("remainingEnrollmentsCount")]
        public int RemainingEnrollmentsCount { get; set; }
        /// <summary>
        /// Created date of the speaker verification profile.
        /// </summary>
        [JsonProperty("createdDateTime")]
        public DateTime CreatedDateTime { get; set; }
        /// <summary>
        /// Last date of usage for this profile.
        /// </summary>
        [JsonProperty("lastActionDateTime")]
        public DateTime LastActionDateTime { get; set; }
        /// <summary>
        /// Speaker verification profile enrollment status. See <see cref="EnrollmentStatuses"/>.
        /// </summary>
        [JsonProperty("enrollmentStatus")]
        public string EnrollmentStatus { get; set; }
    }
}
