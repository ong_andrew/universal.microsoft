﻿using Newtonsoft.Json;
using Universal.Common.Serialization;

namespace Universal.Microsoft.CognitiveServices.SpeakerRecognition.V1
{
    public class CreateVerificationProfileEnrollmentResponse : JsonSerializable<CreateVerificationProfileEnrollmentResponse>
    {
        /// <summary>
        /// The current enrollment status. See <see cref="EnrollmentStatuses"/>.
        /// </summary>
        [JsonProperty("enrollmentStatus")]
        public string EnrollmentStatus;
        /// <summary>
        /// The number of enrollments performed.
        /// </summary>
        [JsonProperty("enrollmentsCount")]
        public int EnrollmentsCount;
        /// <summary>
        /// The number of enrollments required for the profile to be usable.
        /// </summary>
        [JsonProperty("remainingEnrollments")]
        public int RemainingEnrollments;
        /// <summary>
        /// The detected phrase.
        /// </summary>
        [JsonProperty("phrase")]
        public string Phrase;
    }
}
