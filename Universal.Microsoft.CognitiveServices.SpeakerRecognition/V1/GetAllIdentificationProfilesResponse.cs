﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using Universal.Common.Serialization;

namespace Universal.Microsoft.CognitiveServices.SpeakerRecognition.V1
{
    public class GetAllIdentificationProfilesResponse : JsonSerializable<GetAllIdentificationProfilesResponse>, IList<GetAllIdentificationProfilesResponse.IdentificationProfile>
    {
        protected List<IdentificationProfile> IdentificationProfiles { get; set; }

        public int Count => ((IList<IdentificationProfile>)IdentificationProfiles).Count;

        public bool IsReadOnly => ((IList<IdentificationProfile>)IdentificationProfiles).IsReadOnly;

        public IdentificationProfile this[int index] { get => ((IList<IdentificationProfile>)IdentificationProfiles)[index]; set => ((IList<IdentificationProfile>)IdentificationProfiles)[index] = value; }

        public GetAllIdentificationProfilesResponse()
        {
            IdentificationProfiles = new List<IdentificationProfile>();
        }

        public class IdentificationProfile
        {
            /// <summary>
            /// Id of the speaker identification profile.
            /// </summary>
            [JsonProperty("identificationProfileId")]
            public string IdentificationProfileId { get; set; }
            /// <summary>
            /// Language locale of the speaker identification profile. See <see cref="Locales"/>.
            /// </summary>
            [JsonProperty("locale")]
            public string Locale { get; set; }
            /// <summary>
            /// Total number of useful speech detected in all enrollment audio files provided (seconds).
            /// </summary>
            [JsonProperty("enrollmentSpeechTime")]
            public double EnrollmentSpeechTime { get; set; }
            /// <summary>
            /// Remaining number of speech needed for a successful enrollment (seconds).
            /// </summary>
            [JsonProperty("remainingEnrollmentSpeechTime")]
            public double RemainingEnrollmentSpeechTime { get; set; }
            /// <summary>
            /// Created date of the speaker identification profile.
            /// </summary>
            [JsonProperty("createdDateTime")]
            public DateTime CreatedDateTime { get; set; }
            /// <summary>
            /// Last date of usage for this profile.
            /// </summary>
            [JsonProperty("lastActionDateTime")]
            public DateTime LastActionDateTime { get; set; }
            /// <summary>
            /// Speaker identification profile enrollment status. See <see cref="EnrollmentStatuses"/>.
            /// </summary>
            [JsonProperty("enrollmentStatus")]
            public string EnrollmentStatus { get; set; }
        }

        public int IndexOf(IdentificationProfile item)
        {
            return ((IList<IdentificationProfile>)IdentificationProfiles).IndexOf(item);
        }

        public void Insert(int index, IdentificationProfile item)
        {
            ((IList<IdentificationProfile>)IdentificationProfiles).Insert(index, item);
        }

        public void RemoveAt(int index)
        {
            ((IList<IdentificationProfile>)IdentificationProfiles).RemoveAt(index);
        }

        public void Add(IdentificationProfile item)
        {
            ((IList<IdentificationProfile>)IdentificationProfiles).Add(item);
        }

        public void Clear()
        {
            ((IList<IdentificationProfile>)IdentificationProfiles).Clear();
        }

        public bool Contains(IdentificationProfile item)
        {
            return ((IList<IdentificationProfile>)IdentificationProfiles).Contains(item);
        }

        public void CopyTo(IdentificationProfile[] array, int arrayIndex)
        {
            ((IList<IdentificationProfile>)IdentificationProfiles).CopyTo(array, arrayIndex);
        }

        public bool Remove(IdentificationProfile item)
        {
            return ((IList<IdentificationProfile>)IdentificationProfiles).Remove(item);
        }

        public IEnumerator<IdentificationProfile> GetEnumerator()
        {
            return ((IList<IdentificationProfile>)IdentificationProfiles).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IList<IdentificationProfile>)IdentificationProfiles).GetEnumerator();
        }
    }
}
