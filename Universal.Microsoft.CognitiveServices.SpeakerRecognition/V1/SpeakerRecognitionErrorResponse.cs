﻿using Newtonsoft.Json;
using Universal.Common.Serialization;

namespace Universal.Microsoft.CognitiveServices.SpeakerRecognition.V1
{
    public class SpeakerRecognitionErrorResponse : JsonSerializable<SpeakerRecognitionErrorResponse>
    {
        [JsonProperty("error")]
        public ErrorDetails Error { get; set; }

        public class ErrorDetails
        {
            [JsonProperty("code")]
            public string Code { get; set; }
            [JsonProperty("message")]
            public string Message { get; set; }
        }

        public SpeakerRecognitionErrorResponse()
        {
            Error = new ErrorDetails();
        }
    }
}
