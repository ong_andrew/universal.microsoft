﻿namespace Universal.Microsoft.CognitiveServices.SpeakerRecognition.V1
{
    public static class Confidences
    {
        /// <summary>
        /// The confidence of the verification is low.
        /// </summary>
        public const string Low = "Low";
        /// <summary>
        /// The confidence of the verification is normal.
        /// </summary>
        public const string Normal = "Normal";
        /// <summary>
        /// The confidence of the verification is high.
        /// </summary>
        public const string High = "High";
    }
}