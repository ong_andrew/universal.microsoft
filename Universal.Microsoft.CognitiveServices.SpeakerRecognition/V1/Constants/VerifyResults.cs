﻿namespace Universal.Microsoft.CognitiveServices.SpeakerRecognition.V1
{
    public static class VerifyResults
    {
        /// <summary>
        /// The verification is accepted.
        /// </summary>
        public const string Accept = "Accept";
        /// <summary>
        /// The verification is rejected.
        /// </summary>
        public const string Reject = "Reject";
    }
}