﻿namespace Universal.Microsoft.CognitiveServices.SpeakerRecognition.V1
{
    /// <summary>
    /// List of locales supported by the Microsoft Speaker Recognition service.
    /// </summary>
    public static class Locales
    {
        public const string EnUS = "en-US";
        public const string EsES = "es-ES";
        public const string FrFR = "fr-FR";
        public const string ZhCN = "zh-CN";
    }
}
