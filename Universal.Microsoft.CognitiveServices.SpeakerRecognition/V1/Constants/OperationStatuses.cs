﻿namespace Universal.Microsoft.CognitiveServices.SpeakerRecognition.V1
{
    public static class OperationStatuses
    {
        public const string Failed = "failed";
        public const string NotStarted = "notstarted";
        public const string Running = "running";
        public const string Succeeded = "succeeded";
    }
}