﻿namespace Universal.Microsoft.CognitiveServices.SpeakerRecognition.V1
{
    public static class EnrollmentStatuses
    {
        /// <summary>
        /// The profile is currently enrolled and is ready for verification.
        /// </summary>
        public const string Enrolled = "Enrolled";
        /// <summary>
        /// The profile is currently enrolling and is not ready for verification.
        /// </summary>
        public const string Enrolling = "Enrolling";
        /// <summary>
        /// The profile is currently training and is not ready for verification.
        /// </summary>
        public const string Training = "Training";
    }
}