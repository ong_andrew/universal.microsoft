﻿using System.Collections;
using System.Collections.Generic;
using Universal.Common.Serialization;

namespace Universal.Microsoft.CognitiveServices.SpeakerRecognition.V1
{
    public class ListAllSupportedVerificationPhrasesResponse : JsonSerializable<ListAllSupportedVerificationPhrasesResponse>, IList<ListAllSupportedVerificationPhrasesResponse.PhraseItem>
    {
        public PhraseItem this[int index] { get => ((IList<PhraseItem>)PhraseItems)[index]; set => ((IList<PhraseItem>)PhraseItems)[index] = value; }

        public int Count => ((IList<PhraseItem>)PhraseItems).Count;

        public bool IsReadOnly => ((IList<PhraseItem>)PhraseItems).IsReadOnly;

        protected List<PhraseItem> PhraseItems { get; set; }

        public ListAllSupportedVerificationPhrasesResponse()
        {
            PhraseItems = new List<PhraseItem>();
        }

        public void Add(PhraseItem item)
        {
            ((IList<PhraseItem>)PhraseItems).Add(item);
        }

        public void Clear()
        {
            ((IList<PhraseItem>)PhraseItems).Clear();
        }

        public bool Contains(PhraseItem item)
        {
            return ((IList<PhraseItem>)PhraseItems).Contains(item);
        }

        public void CopyTo(PhraseItem[] array, int arrayIndex)
        {
            ((IList<PhraseItem>)PhraseItems).CopyTo(array, arrayIndex);
        }

        public IEnumerator<PhraseItem> GetEnumerator()
        {
            return ((IList<PhraseItem>)PhraseItems).GetEnumerator();
        }

        public int IndexOf(PhraseItem item)
        {
            return ((IList<PhraseItem>)PhraseItems).IndexOf(item);
        }

        public void Insert(int index, PhraseItem item)
        {
            ((IList<PhraseItem>)PhraseItems).Insert(index, item);
        }

        public bool Remove(PhraseItem item)
        {
            return ((IList<PhraseItem>)PhraseItems).Remove(item);
        }

        public void RemoveAt(int index)
        {
            ((IList<PhraseItem>)PhraseItems).RemoveAt(index);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IList<PhraseItem>)PhraseItems).GetEnumerator();
        }

        public class PhraseItem
        {
            public string Phrase { get; set; }
        }
    }
}
