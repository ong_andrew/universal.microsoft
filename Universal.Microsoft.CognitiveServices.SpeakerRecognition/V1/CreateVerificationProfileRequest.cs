﻿using Newtonsoft.Json;
using Universal.Common.Serialization;

namespace Universal.Microsoft.CognitiveServices.SpeakerRecognition.V1
{
    public class CreateVerificationProfileRequest : JsonSerializable<CreateVerificationProfileRequest>
    {
        /// <summary>
        /// Locale for language of the new speaker verification profile. See <see cref="Locales"/>.
        /// </summary>
        [JsonProperty("locale")]
        public string Locale { get; set; }
    }
}
