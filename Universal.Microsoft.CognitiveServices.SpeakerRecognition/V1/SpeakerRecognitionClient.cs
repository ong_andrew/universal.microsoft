﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Universal.Common.Net.Http;

namespace Universal.Microsoft.CognitiveServices.SpeakerRecognition.V1
{
    /// <summary>
    /// Client for interacting with the Microsoft Speaker Recognition APIs.
    /// </summary>
    public class SpeakerRecognitionClient : HttpServiceClient
    {
        protected string mSubscriptionKey;
        protected UriGenerator mUriGenerator;

        protected override Task HandleNonSuccessCodeAsync(HttpResponseMessage httpResponseMessage, CancellationToken cancellationToken)
        {
            try
            {
                throw new SpeakerRecognitionException(httpResponseMessage);
            }
            catch (JsonSerializationException)
            {
                throw new HttpException(httpResponseMessage);
            }
        }

        protected override HttpClient CreateHttpClient()
        {
            HttpClient httpClient = base.CreateHttpClient();
            httpClient.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", mSubscriptionKey);
            return httpClient;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SpeakerRecognitionClient"/> class.
        /// </summary>
        /// <param name="subscriptionKey"></param>
        /// <param name="host">The base URL for the service, defaults to "westus.api.cognitive.microsoft.com".</param>
        public SpeakerRecognitionClient(string subscriptionKey, string host = Hosts.WestUS)
        {
            mSubscriptionKey = subscriptionKey;
            mUriGenerator = new UriGenerator(host);
        }

        /// <summary>
        /// Create a new speaker identification profile with specified locale.
        /// </summary>
        /// <param name="requestBody"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<CreateIdentificationProfileResponse> CreateIdentificationProfileAsync(string requestBody, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Post, mUriGenerator.IdentificationProfiles(), new JsonContent(requestBody), cancellationToken: cancellationToken).ConfigureAwait(false))
            {
                return CreateIdentificationProfileResponse.FromString(await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
        }

        /// <summary>
        /// Create a new speaker identification profile with specified locale.
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<CreateIdentificationProfileResponse> CreateIdentificationProfileAsync(CreateIdentificationProfileRequest request, CancellationToken cancellationToken = default)
        {
            return await CreateIdentificationProfileAsync(request.ToString(), cancellationToken: cancellationToken).ConfigureAwait(false);
        }

        /// <summary>
        /// Enrollment for speaker identification is text-independent, which means that there are no restrictions on what the speaker says in the audio. The speaker's voice is recorded, and a number of features are extracted to form a unique voiceprint.
        /// </summary>
        /// <param name="id">ID of speaker identification profile. GUID returned from Identification Profile - Create Profile API</param>
        /// <param name="audioBytes"></param>
        /// <param name="shortAudio">Instruct the service to waive the recommended minimum audio limit needed for enrollment. Set value to “true” to force enrollment using any audio length (min. 1 second).</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<CreateIdentificationProfileEnrollmentResponse> CreateIdentificationProfileEnrollmentAsync(string id, byte[] audioBytes, bool shortAudio = false, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Post, mUriGenerator.EnrollIdentificationProfile(id, shortAudio), new ByteArrayContent(audioBytes), cancellationToken: cancellationToken).ConfigureAwait(false))
            {
                return new CreateIdentificationProfileEnrollmentResponse() { OperationLocation = httpResponseMessage.Headers.GetValues("Operation-Location").First() };
            }
        }

        /// <summary>
        /// Create a new speaker verification profile with specific locale.
        /// </summary>
        /// <param name="requestBody"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<CreateVerificationProfileResponse> CreateVerificationProfileAsync(string requestBody, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Post, mUriGenerator.VerificationProfiles(), new JsonContent(requestBody), cancellationToken: cancellationToken).ConfigureAwait(false))
            {
                return CreateVerificationProfileResponse.FromString(await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
        }

        /// <summary>
        /// Create a new speaker verification profile with specific locale.
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<CreateVerificationProfileResponse> CreateVerificationProfileAsync(CreateVerificationProfileRequest request, CancellationToken cancellationToken = default)
        {
            return await CreateVerificationProfileAsync(request.ToString(), cancellationToken: cancellationToken).ConfigureAwait(false);
        }

        /// <summary>
        /// Enrollment for speaker verification is text-dependent, which means speaker need to choose a specific phrase to use in both enrollment and verification. List of supported phrases can be found in Verification Phrase - List All Supported Verification Phrases.
        /// </summary>
        /// <param name="id">ID of speaker verification profile. GUID returned from Verification Profile - Create Profile API</param>
        /// <param name="audioBytes">Audio file should be at least 1-second-long and no longer than 15 seconds.</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<CreateVerificationProfileEnrollmentResponse> CreateVerificationProfileEnrollmentAsync(string id, byte[] audioBytes, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Post, mUriGenerator.EnrollVerificationProfile(id), new ByteArrayContent(audioBytes), cancellationToken: cancellationToken).ConfigureAwait(false))
            {
                return CreateVerificationProfileEnrollmentResponse.FromString(await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
        }

        /// <summary>
        /// Deletes both speaker identification profile and all associated enrollments permanently from the service.
        /// </summary>
        /// <param name="id">ID of speaker identification profile. GUID returned from Identification Profile - Create Profile API</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task DeleteIdentificationProfileAsync(string id, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Delete, mUriGenerator.IdentificationProfiles(id), cancellationToken: cancellationToken).ConfigureAwait(false))
            {
            }
        }

        /// <summary>
        /// Deletes both speaker verification profile and all associated enrollments permanently from the service.
        /// </summary>
        /// <param name="id">ID of speaker verification profile. It should be a GUID.</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task DeleteVerificationProfileAsync(string id, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Delete, mUriGenerator.VerificationProfiles(id), cancellationToken: cancellationToken).ConfigureAwait(false))
            {
            }
        }

        /// <summary>
        /// Get all speaker identification profiles within the subscription.
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<GetAllIdentificationProfilesResponse> GetAllIdentificationProfilesAsync(CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Get, mUriGenerator.IdentificationProfiles(), cancellationToken: cancellationToken).ConfigureAwait(false))
            {
                return GetAllIdentificationProfilesResponse.FromString(await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
        }

        /// <summary>
        /// Get all speaker verification profiles within the subscription.
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<GetAllVerificationProfilesResponse> GetAllVerificationProfilesAsync(CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Get, mUriGenerator.VerificationProfiles(), cancellationToken: cancellationToken).ConfigureAwait(false))
            {
                return GetAllVerificationProfilesResponse.FromString(await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
        }

        /// <summary>
        /// Get a speaker identification profile by identificationProfileId.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<GetIdentificationProfileResponse> GetIdentificationProfileAsync(string id, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Get, mUriGenerator.IdentificationProfiles(id), cancellationToken: cancellationToken).ConfigureAwait(false))
            {
                return GetIdentificationProfileResponse.FromString(await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
        }

        /// <summary>
        /// Get operation status or result. The operation should be created by Speaker Recognition - Identification or Identification Profile - Create Enrollment. And the URL should be retrieved from Operation-Location header of initial POST 202 response
        /// </summary>
        /// <param name="id">The operation Id, created by Speaker Recognition - Identification or Identification Profile - Create Enrollment.</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<GetOperationStatusResponse> GetOperationStatusAsync(string id, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Get, mUriGenerator.Operations(id), cancellationToken: cancellationToken).ConfigureAwait(false))
            {
                return GetOperationStatusResponse.FromString(await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
        }

        /// <summary>
        /// Get a speaker verification profile by verificationProfileId
        /// </summary>
        /// <param name="id">ID of speaker verification profile. It should be a GUID.</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<GetVerificationProfileResponse> GetVerificationProfileAsync(string id, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Get, mUriGenerator.VerificationProfiles(id), cancellationToken: cancellationToken).ConfigureAwait(false))
            {
                return GetVerificationProfileResponse.FromString(await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
        }

        /// <summary>
        /// To automatically identify who is speaking given a group of speakers.
        /// </summary>
        /// <param name="ids"></param>
        /// <param name="audioBytes"></param>
        /// <param name="shortAudio">Instruct the service to waive the recommended minimum audio limit needed for identification. Set value to “true” to force identification using any audio length (min. 1 second).</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<IdentifyResponse> IdentifyAsync(IEnumerable<string> ids, byte[] audioBytes, bool shortAudio = false, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Post, mUriGenerator.Identify(ids, shortAudio), new ByteArrayContent(audioBytes), cancellationToken: cancellationToken).ConfigureAwait(false))
            {
                return new IdentifyResponse() { OperationLocation = httpResponseMessage.Headers.GetValues("Operation-Location").First() };
            }
        }

        /// <summary>
        /// Returns the list of supported verification phrases that can be used for Verification Profile - Create Enrollment and Speaker Recognition - Verification.
        /// </summary>
        /// <param name="locale">Locale for the language when retrieving verification phrases.</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<ListAllSupportedVerificationPhrasesResponse> ListAllSupportedVerificationPhrasesAsync(string locale = Locales.EnUS, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Get, mUriGenerator.VerificationPhrases(locale), cancellationToken: cancellationToken).ConfigureAwait(false))
            {
                return ListAllSupportedVerificationPhrasesResponse.FromString(await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
        }

        /// <summary>
        /// Polls the given operation until it is complete.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="delay"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<GetOperationStatusResponse> PollOperationUntilCompleteAsync(string id, int delay = 1000, CancellationToken cancellationToken = default)
        {
            GetOperationStatusResponse response;

            do
            {
                response = await GetOperationStatusAsync(id, cancellationToken).ConfigureAwait(false);

                await Task.Delay(delay).ConfigureAwait(false);
            }
            while (response.Status.Equals(OperationStatuses.NotStarted, StringComparison.InvariantCultureIgnoreCase)|| response.Status.Equals(OperationStatuses.Running, StringComparison.InvariantCultureIgnoreCase));

            return response;
        }
        
        public async Task<GetOperationStatusResponse> PollOperationUntilCompleteAsync(IOperation operation, int delay = 1000, CancellationToken cancellationToken = default)
        {
            return await PollOperationUntilCompleteAsync(operation.OperationId, delay, cancellationToken).ConfigureAwait(false);
        }

        /// <summary>
        /// Deletes all enrollments associated with the given speaker identification profile permanently from the service.
        /// </summary>
        /// <param name="id">ID of speaker identification profile.GUID returned from Identification Profile - Create Profile API</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task ResetIdentificationProfileEnrollmentsAsync(string id, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Post, mUriGenerator.ResetIdentificationProfile(id), cancellationToken: cancellationToken).ConfigureAwait(false))
            {
            }
        }

        /// <summary>
        /// Deletes all enrollments associated with the given speaker’s verification profile permanently from the service.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task ResetVerificationProfileEnrollmentsAsync(string id, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Post, mUriGenerator.ResetVerificationProfile(id), cancellationToken: cancellationToken).ConfigureAwait(false))
            {
            }
        }

        /// <summary>
        ///  To automatically verify and authenticate users using their voice or speech.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="audioBytes"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<VerifyResponse> VerifyAsync(string id, byte[] audioBytes, CancellationToken cancellationToken = default)
        {
            using (HttpResponseMessage httpResponseMessage = await ExecuteAsync(HttpMethod.Post, mUriGenerator.Verify(id), new ByteArrayContent(audioBytes), cancellationToken: cancellationToken).ConfigureAwait(false))
            {
                return VerifyResponse.FromString(await httpResponseMessage.Content.ReadAsStringAsync().ConfigureAwait(false));
            }
        }
    }
}
