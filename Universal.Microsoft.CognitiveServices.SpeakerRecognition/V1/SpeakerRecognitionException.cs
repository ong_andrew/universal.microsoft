﻿using System;
using System.Net.Http;
using Universal.Common.Net.Http;

namespace Universal.Microsoft.CognitiveServices.SpeakerRecognition.V1
{
    public class SpeakerRecognitionException : HttpException<SpeakerRecognitionErrorResponse>
    {
        public SpeakerRecognitionException()
        {
        }

        public SpeakerRecognitionException(string message) : base(message)
        {
        }

        public SpeakerRecognitionException(string message, Exception innerException) : base(message, innerException)
        {
        }

        public SpeakerRecognitionException(HttpResponseMessage httpResponseMessage) : base(
            (int)httpResponseMessage.StatusCode, 
            httpResponseMessage.ReasonPhrase, 
            httpResponseMessage.Content != null ? SpeakerRecognitionErrorResponse.FromString(httpResponseMessage.Content.ReadAsStringAsync().GetAwaiter().GetResult()) : null)
        {
        }
    }
}
