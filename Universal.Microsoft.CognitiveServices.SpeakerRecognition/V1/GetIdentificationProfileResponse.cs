﻿using Newtonsoft.Json;
using System;
using Universal.Common.Serialization;

namespace Universal.Microsoft.CognitiveServices.SpeakerRecognition.V1
{
    public class GetIdentificationProfileResponse : JsonSerializable<GetIdentificationProfileResponse>
    {
        /// <summary>
        /// Id of the speaker identification profile.
        /// </summary>
        [JsonProperty("identificationProfileId")]
        public string IdentificationProfileId { get; set; }
        /// <summary>
        /// Language locale of the speaker identification profile.
        /// </summary>
        [JsonProperty("locale")]
        public string Locale { get; set; }
        /// <summary>
        /// Total number of useful speech detected in all enrollment audio files provided (seconds).
        /// </summary>
        [JsonProperty("enrollmentSpeechTime")]
        public double EnrollmentSpeechTime { get; set; }
        /// <summary>
        /// Remaining number of speech needed for a successful enrollment (seconds).
        /// </summary>
        [JsonProperty("remainingEnrollmentSpeechTime")]
        public double RemainingEnrollmentSpeechTime { get; set; }
        /// <summary>
        /// Created date of the speaker identification profile.
        /// </summary>
        [JsonProperty("createdDateTime")]
        public DateTime CreatedDateTime { get; set; }
        /// <summary>
        /// Last date of usage for this profile.
        /// </summary>
        [JsonProperty("lastActionDateTime")]
        public DateTime LastActionDateTime { get; set; }
        /// <summary>
        /// Speaker identification profile enrollment status
        /// </summary>
        [JsonProperty("enrollmentStatus")]
        public string EnrollmentStatus { get; set; }
    }
}
