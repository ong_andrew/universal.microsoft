﻿using Newtonsoft.Json;
using Universal.Common.Serialization;

namespace Universal.Microsoft.CognitiveServices.SpeakerRecognition.V1
{
    public class CreateVerificationProfileResponse : JsonSerializable<CreateVerificationProfileResponse>
    {
        /// <summary>
        /// Id of the created speaker verification profile.
        /// </summary>
        [JsonProperty("verificationProfileId")]
        public string VerificationProfileId { get; set; }
    }
}
