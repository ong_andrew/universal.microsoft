﻿using System;
using System.Collections.Generic;
using UriBuilder = Universal.Common.UriBuilder;

namespace Universal.Microsoft.CognitiveServices.SpeakerRecognition.V1
{
    public class UriGenerator
    {
        protected string mHost;

        public UriGenerator(string host)
        {
            mHost = host;
        }

        public UriBuilder NewBuilder()
        {
            return new UriBuilder("https", mHost);
        }

        public UriBuilder NewBuilder(string path)
        {
            UriBuilder uriBuilder = NewBuilder();
            uriBuilder.Path = path;
            return uriBuilder;
        }
        
        public Uri IdentificationProfiles()
        {
            return NewBuilder("/spid/v1.0/identificationProfiles").Uri;
        }

        public Uri IdentificationProfiles(string id)
        {
            UriBuilder builder = new UriBuilder(IdentificationProfiles());
            builder.AddSegment(id);
            return builder.Uri;
        }

        public Uri ResetIdentificationProfile(string id)
        {
            UriBuilder builder = new UriBuilder(IdentificationProfiles(id));
            builder.AddSegment("reset");

            return builder.Uri;
        }

        public Uri Identify(IEnumerable<string> ids, bool shortAudio)
        {
            UriBuilder builder = NewBuilder("/spid/v1.0/identify");
            builder.AddQuery("identificationProfileIds", string.Join(",", ids));
            if (shortAudio)
            {
                builder.AddQuery("shortAudio", "true");
            }

            return builder.Uri;
        }

        public Uri EnrollIdentificationProfile(string id, bool shortAudio)
        {
            UriBuilder builder = new UriBuilder(IdentificationProfiles(id));
            builder.AddSegment("enroll");
            if (shortAudio)
            {
                builder.AddQuery("shortAudio", "true");
            }
            
            return builder.Uri;
        }

        public Uri Operations()
        {
            return NewBuilder("/spid/v1.0/operations").Uri;
        }

        public Uri Operations(string id)
        {
            UriBuilder builder = new UriBuilder(Operations());
            builder.AddSegment(id);
            return builder.Uri;
        }

        public Uri VerificationPhrases(string locale)
        {
            UriBuilder builder = NewBuilder("/spid/v1.0/verificationPhrases");
            builder.AddQuery("locale", locale);
            return builder.Uri;
        }

        public Uri VerificationProfiles()
        {
            return NewBuilder("/spid/v1.0/verificationProfiles").Uri;
        }

        public Uri VerificationProfiles(string id)
        {
            UriBuilder builder = new UriBuilder(VerificationProfiles());
            builder.AddSegment(id);
            return builder.Uri;
        }

        public Uri EnrollVerificationProfile(string id)
        {
            UriBuilder builder = new UriBuilder(VerificationProfiles(id));
            builder.AddSegment("enroll");

            return builder.Uri;
        }

        public Uri ResetVerificationProfile(string id)
        {
            UriBuilder builder = new UriBuilder(VerificationProfiles(id));
            builder.AddSegment("reset");

            return builder.Uri;
        }

        public Uri Verify(string id)
        {
            UriBuilder builder = NewBuilder("/spid/v1.0/verify");
            builder.AddQuery("verificationProfileId", id);
            return builder.Uri;
        }
    }
}
