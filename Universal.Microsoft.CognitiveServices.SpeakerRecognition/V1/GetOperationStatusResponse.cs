﻿using Newtonsoft.Json;
using System;
using Universal.Common.Serialization;

namespace Universal.Microsoft.CognitiveServices.SpeakerRecognition.V1
{
    public class GetOperationStatusResponse : JsonSerializable<GetOperationStatusResponse>
    {
        /// <summary>
        /// The status of the operation.
        /// </summary>
        [JsonProperty("status")]
        public string Status { get; set; }
        /// <summary>
        /// Created date of the operation.
        /// </summary>
        [JsonProperty("createdDateTime")]
        public DateTime CreatedDateTime { get; set; }
        /// <summary>
        /// Last date of usage for this operation.
        /// </summary>
        [JsonProperty("lastActionDateTime")]
        public DateTime LastActionDateTime { get; set; }
        /// <summary>
        /// Detail message returned by this operation. Used in operations with failed status to show detail failure message.
        /// </summary>
        [JsonProperty("message")]
        public string Message { get; set; }
        /// <summary>
        /// An Json Object contains the processing result. This object exists only when the operation status is succeeded.
        /// </summary>
        [JsonProperty("processingResult")]
        public Result ProcessingResult { get; set; }
        
        public class Result : IIdentificationResult, IEnrollmentResult
        {
            /// <summary>
            /// The identified speaker identification profile id. If this value is 00000000-0000-0000-0000-000000000000, it means there's no speaker identification profile identified and the audio file to be identified belongs to none of the provided speaker identification profiles.
            /// </summary>
            [JsonProperty("identifiedProfileId")]
            public string IdentificationProfileId { get; set; }
            /// <summary>
            /// The confidence value of the identification. See <see cref="Confidences"/>.
            /// </summary>
            [JsonProperty("confidence")]
            public string Confidence { get; set; }

            /// <summary>
            /// Speaker identification profile enrollment status. See <see cref="EnrollmentStatuses"/>.
            /// </summary>
            [JsonProperty("enrollmentStatus")]
            public string EnrollmentStatus { get; set; }
            /// <summary>
            /// Remaining number of speech seconds to complete minimum enrollment.
            /// </summary>
            [JsonProperty("remainingEnrollmentSpeechTime")]
            public double RemainingEnrollmentSpeechTime { get; set; }
            /// <summary>
            /// Seconds of useful speech in enrollment audio.
            /// </summary>
            [JsonProperty("speechTime")]
            public double SpeechTime { get; set; }
            /// <summary>
            /// Speaker identification profile enrollment length in seconds of speech.
            /// </summary>
            [JsonProperty("enrollmentSpeechTime")]
            public double EnrollmentSpeechTime { get; set; }
            
            /// <summary>
            /// Returns the <see cref="Result"/> as an <see cref="IIdentificationResult"/>.
            /// </summary>
            /// <returns></returns>
            public IIdentificationResult AsIdentificationResult()
            {
                return this;
            }

            /// <summary>
            /// Returns the <see cref="Result"/> as an <see cref="IEnrollmentResult"/>.
            /// </summary>
            /// <returns></returns>
            public IEnrollmentResult AsEnrollmentResult()
            {
                return this;
            }
        }

        public interface IIdentificationResult
        {
            /// <summary>
            /// The identified speaker identification profile id. If this value is 00000000-0000-0000-0000-000000000000, it means there's no speaker identification profile identified and the audio file to be identified belongs to none of the provided speaker identification profiles.
            /// </summary>
            string IdentificationProfileId { get; set; }
            /// <summary>
            /// The confidence value of the identification.
            /// </summary>
            string Confidence { get; set; }
        }

        public interface IEnrollmentResult
        {
            /// <summary>
            /// Speaker identification profile enrollment status
            /// </summary>
            string EnrollmentStatus { get; set; }
            /// <summary>
            /// Remaining number of speech seconds to complete minimum enrollment.
            /// </summary>
            double RemainingEnrollmentSpeechTime { get; set; }
            /// <summary>
            /// Seconds of useful speech in enrollment audio.
            /// </summary>
            double SpeechTime { get; set; }
            /// <summary>
            /// Speaker identification profile enrollment length in seconds of speech.
            /// </summary>
            double EnrollmentSpeechTime { get; set; }
        }
    }
}
