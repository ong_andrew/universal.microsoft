﻿using System.Linq;
using Universal.Common;
using Universal.Common.Serialization;

namespace Universal.Microsoft.CognitiveServices.SpeakerRecognition.V1
{
    public class CreateIdentificationProfileEnrollmentResponse : JsonSerializable, IOperation
    {
        public string OperationLocation { get; set; }
        public string OperationId
        {
            get
            {
                return new UriBuilder(OperationLocation).Segments.Last();
            }
        }
    }
}