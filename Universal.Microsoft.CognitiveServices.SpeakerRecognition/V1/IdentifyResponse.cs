﻿using System.Linq;
using Universal.Common;

namespace Universal.Microsoft.CognitiveServices.SpeakerRecognition.V1
{
    public class IdentifyResponse : IOperation
    {
        public string OperationLocation { get; set; }
        public string OperationId
        {
            get
            {
                return new UriBuilder(OperationLocation).Segments.Last();
            }
        }
    }
}
