﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using Universal.Common.Serialization;

namespace Universal.Microsoft.CognitiveServices.SpeakerRecognition.V1
{
    public class GetAllVerificationProfilesResponse : JsonSerializable<GetAllVerificationProfilesResponse>, IList<GetAllVerificationProfilesResponse.VerificationProfile>
    {
        protected List<VerificationProfile> VerificationProfiles { get; set; }

        public int Count => ((IList<VerificationProfile>)VerificationProfiles).Count;

        public bool IsReadOnly => ((IList<VerificationProfile>)VerificationProfiles).IsReadOnly;

        public VerificationProfile this[int index] { get => ((IList<VerificationProfile>)VerificationProfiles)[index]; set => ((IList<VerificationProfile>)VerificationProfiles)[index] = value; }

        public GetAllVerificationProfilesResponse()
        {
            VerificationProfiles = new List<VerificationProfile>();
        }

        public class VerificationProfile
        {
            /// <summary>
            /// Id of the speaker verification profile.
            /// </summary>
            [JsonProperty("verificationProfileId")]
            public string VerificationProfileId { get; set; }
            /// <summary>
            /// Language locale of the speaker verification profile.
            /// </summary>
            [JsonProperty("locale")]
            public string Locale { get; set; }
            /// <summary>
            /// Speaker enrollments count.
            /// </summary>
            [JsonProperty("enrollmentsCount")]
            public int EnrollmentsCount { get; set; }
            /// <summary>
            /// Remaining number of required enrollments if enrollmentStatus == Enrolling.
            /// </summary>
            [JsonProperty("remainingEnrollmentsCount")]
            public int RemainingEnrollmentsCount { get; set; }
            /// <summary>
            /// Created date of the speaker verification profile.
            /// </summary>
            [JsonProperty("createdDateTime")]
            public DateTime CreatedDateTime { get; set; }
            /// <summary>
            /// Last date of usage for this profile.
            /// </summary>
            [JsonProperty("lastActionDateTime")]
            public DateTime LastActionDateTime { get; set; }
            /// <summary>
            /// Speaker verification profile enrollment status
            /// </summary>
            [JsonProperty("enrollmentStatus")]
            public string EnrollmentStatus { get; set; }
        }

        public int IndexOf(VerificationProfile item)
        {
            return ((IList<VerificationProfile>)VerificationProfiles).IndexOf(item);
        }

        public void Insert(int index, VerificationProfile item)
        {
            ((IList<VerificationProfile>)VerificationProfiles).Insert(index, item);
        }

        public void RemoveAt(int index)
        {
            ((IList<VerificationProfile>)VerificationProfiles).RemoveAt(index);
        }

        public void Add(VerificationProfile item)
        {
            ((IList<VerificationProfile>)VerificationProfiles).Add(item);
        }

        public void Clear()
        {
            ((IList<VerificationProfile>)VerificationProfiles).Clear();
        }

        public bool Contains(VerificationProfile item)
        {
            return ((IList<VerificationProfile>)VerificationProfiles).Contains(item);
        }

        public void CopyTo(VerificationProfile[] array, int arrayIndex)
        {
            ((IList<VerificationProfile>)VerificationProfiles).CopyTo(array, arrayIndex);
        }

        public bool Remove(VerificationProfile item)
        {
            return ((IList<VerificationProfile>)VerificationProfiles).Remove(item);
        }

        public IEnumerator<VerificationProfile> GetEnumerator()
        {
            return ((IList<VerificationProfile>)VerificationProfiles).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IList<VerificationProfile>)VerificationProfiles).GetEnumerator();
        }
    }
}
