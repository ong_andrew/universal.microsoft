﻿using Newtonsoft.Json;
using Universal.Common.Serialization;

namespace Universal.Microsoft.CognitiveServices.SpeakerRecognition.V1
{
    public class VerifyResponse : JsonSerializable<VerifyResponse>
    {
        /// <summary>
        /// The result of the verification operation. See <see cref="VerifyResults"/>.
        /// </summary>
        [JsonProperty("result")]
        public string Result { get; set; }
        /// <summary>
        /// The confidence level of the verification. See <see cref="Confidences"/>.
        /// </summary>
        [JsonProperty("confidence")]
        public string Confidence { get; set; }
        /// <summary>
        /// The recognized phrase of the verification audio file.
        /// </summary>
        [JsonProperty("phrase")]
        public string Phrase { get; set; }
    }
}
