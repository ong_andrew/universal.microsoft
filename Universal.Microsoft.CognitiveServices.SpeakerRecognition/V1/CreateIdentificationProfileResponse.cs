﻿using Newtonsoft.Json;
using Universal.Common.Serialization;

namespace Universal.Microsoft.CognitiveServices.SpeakerRecognition.V1
{
    public class CreateIdentificationProfileResponse : JsonSerializable<CreateIdentificationProfileResponse>
    {
        /// <summary>
        /// Id of the created speaker identification profile.
        /// </summary>
        [JsonProperty("identificationProfileId")]
        public string IdentificationProfileId { get; set; }
    }
}
