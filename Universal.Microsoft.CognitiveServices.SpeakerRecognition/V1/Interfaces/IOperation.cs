﻿namespace Universal.Microsoft.CognitiveServices.SpeakerRecognition.V1
{
    /// <summary>
    /// Represents a long running operation that can be polled through the Speaker Recognition APIs.
    /// </summary>
    public interface IOperation
    {
        string OperationId { get; }
    }
}
