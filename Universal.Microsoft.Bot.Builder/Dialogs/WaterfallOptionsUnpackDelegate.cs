﻿using Microsoft.Bot.Builder.Dialogs;
using System.Collections.Generic;

namespace Universal.Microsoft.Bot.Builder.Dialogs
{
    /// <summary>
    /// Delegate that represents the unpacking of <see cref="WaterfallStepContext.Options"/> into <see cref="WaterfallStepContext.Values"/>
    /// </summary>
    /// <param name="options"></param>
    /// <param name="values"></param>
    public delegate void WaterfallOptionsUnpackDelegate(object options, IDictionary<string, object> values);

    /// <summary>
    /// Delegate that represents the unpacking of <see cref="WaterfallStepContext.Options"/> into <see cref="WaterfallStepContext.Values"/>
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="options"></param>
    /// <param name="values"></param>
    public delegate void WaterfallOptionsUnpackDelegate<T>(T options, IDictionary<string, object> values);
}
