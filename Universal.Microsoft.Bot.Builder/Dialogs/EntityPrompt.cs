﻿using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Schema;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Universal.Microsoft.Bot.Builder.Dialogs
{
    /// <summary>
    /// Prompts the user for a complex entity.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class EntityPrompt<T> : ComponentDialog
        where T : class
    {
        /// <summary>
        /// Gets or sets the maximum number of attempts the user is allowed to enter free text without matches before the dialog is abandoned.
        /// Set to 0 to disable.
        /// </summary>
        public int MaxAttempts { get; set; } = 3;

        private static class Keys
        {
            public const string Attempts = "Attempts";
            public const string EntityId = "EntityId";
        }

        private const string NoneOfTheAbove = "None of the Above";

        private IReadOnlyDictionary<string, T> EntityMap { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="EntityPrompt{T}" /> class with the given dialog ID and entities.
        /// </summary>
        /// <param name="dialogId">The dialog ID assigned to this dialog.</param>
        /// <param name="entities">The entities the user is allowed to select from.</param>
        public EntityPrompt(string dialogId, IEnumerable<T> entities) : base(dialogId)
        {
            AddDialog(new TextPrompt(nameof(TextPrompt)));
            AddDialog(new WaterfallDialog(nameof(WaterfallDialog), new WaterfallStep[]
            {
                UnpackOptionsStepAsync,
                WaterfallSteps.Values.ExecuteIfNull(Keys.EntityId, PromptForEntityStepAsync),
                WaterfallSteps.Values.ExecuteIfNull(Keys.EntityId, ProcessTextInputStepAsync),
                WaterfallSteps.Values.ExecuteIfNull(Keys.EntityId, ProcessFollowupInputStepAsync),
                ReturnResultStepAsync
            }));

            InitialDialogId = nameof(WaterfallDialog);
            Dictionary<string, T> entityMap = new Dictionary<string, T>();

            foreach (T entity in entities)
            {
                entityMap.Add(GetKey(entity), entity);
            }

            EntityMap = entityMap;
        }

        #region WaterfallSteps
        private async Task<DialogTurnResult> UnpackOptionsStepAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            if (stepContext.Options is string)
            {
                string entityId = (string)stepContext.Options;

                if (EntityMap.ContainsKey(entityId))
                {
                    stepContext.Values.Add(Keys.EntityId, entityId);
                }
            }
            else if (stepContext.Options is T entity)
            {
                stepContext.Values.Add(Keys.EntityId, GetKey(entity));
            }

            stepContext.Values.Add(Keys.Attempts, 0);

            return await stepContext.NextAsync(null, cancellationToken);
        }

        private async Task<DialogTurnResult> PromptForEntityStepAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            return await stepContext.PromptAsync(nameof(TextPrompt), CreateInitialPromptOptions(EntityMap.Values), cancellationToken);
        }

        private async Task<DialogTurnResult> ProcessTextInputStepAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            string value = (string)stepContext.Result;

            stepContext.Values[Keys.Attempts] = ((int)stepContext.Values[Keys.Attempts]) + 1;

            if (EntityMap.ContainsKey(value))
            {
                stepContext.Values.Add(Keys.EntityId, value);
                return await stepContext.NextAsync(null, cancellationToken);
            }
            else
            {
                Dictionary<string, double> entityScores = new Dictionary<string, double>();

                foreach (KeyValuePair<string, T> entityEntry in EntityMap)
                {
                    double entityScore = CalculateEntityMatchScore(entityEntry.Value, value);
                    entityScores.Add(entityEntry.Key, entityScore);
                }

                List<string> shortlistedEntities = ShortlistEntities(entityScores).ToList();

                if (shortlistedEntities.Count > 1)
                {
                    return await stepContext.PromptAsync(nameof(TextPrompt), CreateFollowupPromptOptions(shortlistedEntities.Select(x => EntityMap[x]), value));
                }
                else if (shortlistedEntities.Count == 1)
                {
                    stepContext.Values.Add(Keys.EntityId, shortlistedEntities.Single());
                    return await stepContext.NextAsync(null, cancellationToken);
                }
                else
                {
                    if (MaxAttempts != 0 && (int)stepContext.Values[Keys.Attempts] >= MaxAttempts)
                    {
                        return await stepContext.EndDialogAsync(null, cancellationToken);
                    }
                    else
                    {
                        stepContext.ActiveDialog.State["stepIndex"] = (int)stepContext.ActiveDialog.State["stepIndex"] - 1;
                        return await stepContext.PromptAsync(nameof(TextPrompt), CreateNoResultsPromptOptions(EntityMap.Values, value));
                    }
                }
            }
        }

        private async Task<DialogTurnResult> ProcessFollowupInputStepAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            string value = (string)stepContext.Result;

            if (value == NoneOfTheAbove)
            {
                await OnAbandonFollowupAsync(stepContext, cancellationToken);
                return await stepContext.ReplaceDialogAsync(nameof(WaterfallDialog), null, cancellationToken);
            }
            else
            {
                if (EntityMap.ContainsKey(value))
                {
                    stepContext.Values.Add(Keys.EntityId, value);
                    return await stepContext.NextAsync(null, cancellationToken);
                }
                else
                {
                    await OnRestartingDuetoUnmatchedFollowupAsync(stepContext, cancellationToken, value);
                    return await stepContext.ReplaceDialogAsync(nameof(WaterfallDialog), null, cancellationToken);
                }
            }
        }

        private async Task<DialogTurnResult> ReturnResultStepAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            string entityId = stepContext.Values.ContainsKey(Keys.EntityId) ? stepContext.Values[Keys.EntityId] as string : null;

            return await stepContext.EndDialogAsync(entityId != null ? EntityMap[entityId] : (T)null, cancellationToken);
        }
        #endregion

        /// <summary>
        /// Called when a user chooses the "None of the Above" option after performing an initial search.
        /// </summary>
        /// <param name="stepContext"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        protected virtual async Task OnAbandonFollowupAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            await stepContext.Context.SendActivityAsync("Okay, let's start over.");
        }

        /// <summary>
        /// Called when the dialog is about to replace itself because the user entered non-matching free text after being presented a narrower set of options to choose from.
        /// </summary>
        /// <param name="stepContext"></param>
        /// <param name="cancellationToken"></param>
        /// <param name="input">The user's input string.</param>
        /// <returns></returns>
        protected virtual async Task OnRestartingDuetoUnmatchedFollowupAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken, string input)
        {
            await stepContext.Context.SendActivityAsync($"{input} was not one of the possible matches. Let's start again.");
        }

        /// <summary>
        /// Gets the unique string key for the entitiy.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns></returns>
        protected abstract string GetKey(T entity);
        /// <summary>
        /// Gets the string to display on chat elements representing the entity. Defaults to the entitiy's <see cref="object.ToString()" /> method.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns></returns>
        protected virtual string GetDisplayString(T entity)
        {
            return entity.ToString();
        }
        /// <summary>
        /// Returns a match score for a given entity against the provided input.
        /// </summary>
        /// <param name="entity">An entity.</param>
        /// <param name="input">The string the user input.</param>
        /// <returns></returns>
        protected abstract double CalculateEntityMatchScore(T entity, string input);

        /// <summary>
        /// Creates the <see cref="PromptOptions" /> used by the initial prompt for input.
        /// </summary>
        /// <param name="entities">The entities available for matching.</param>
        /// <returns></returns>
        protected virtual PromptOptions CreateInitialPromptOptions(IEnumerable<T> entities)
        {
            Activity activity = MessageFactory.Text($"Please enter or select a {typeof(T).Name.ToLowerInvariant()}.");
            activity.Attachments.Add(new HeroCard()
            {
                Buttons = entities.Take(10).Select(x => new CardAction()
                {
                    Title = GetDisplayString(x),
                    Value = GetKey(x),
                    Type = ActionTypes.ImBack
                }).ToList()
            }.ToAttachment());

            return new PromptOptions()
            {
                Prompt = activity
            };
        }

        /// <summary>
        /// Creates the <see cref="PromptOptions" /> used by the follow-up prompt.
        /// </summary>
        /// <param name="entities">The available matching entities for the followup match.</param>
        /// <param name="input">The string the user input.</param>
        /// <returns></returns>
        protected virtual PromptOptions CreateFollowupPromptOptions(IEnumerable<T> entities, string input)
        {
            Activity activity = MessageFactory.Text($"Found {entities.Count()} matches for {input}. Please select from the given options or select *None of the Above* if the entity you are looking for is not listed.");
            activity.Attachments.Add(new HeroCard()
            {
                Buttons = entities.Take(10).Select(x => new CardAction()
                {
                    Title = GetDisplayString(x),
                    Value = GetKey(x),
                    Type = ActionTypes.ImBack
                }).Concat(new CardAction[] { new CardAction()
                    {
                        Title = NoneOfTheAbove,
                        Value = NoneOfTheAbove,
                        Type = ActionTypes.ImBack
                    }
                }).ToList()
            }.ToAttachment());

            return new PromptOptions()
            {
                Prompt = activity
            };
        }

        /// <summary>
        /// Creates the <see cref="PromptOptions" /> used by the failure prompt.
        /// </summary>
        /// <param name="entities">The available entities to match against.</param>
        /// <param name="input">The string the user input.</param>
        /// <returns></returns>
        protected virtual PromptOptions CreateNoResultsPromptOptions(IEnumerable<T> entities, string input)
        {
            return new PromptOptions()
            {
                Prompt = MessageFactory.Text($"Couldn't match {input} to a {typeof(T).Name.ToLowerInvariant()}. Please try again.")
            };
        }

        /// <summary>
        /// Shortlists the entities based on the given scores.
        /// Returns an appropriate number of entity keys in the order they should be displayed.
        /// </summary>
        /// <param name="entityScores">A dictionary of the entity keys and their match scores.</param>
        /// <returns></returns>
        protected virtual IEnumerable<string> ShortlistEntities(IReadOnlyDictionary<string, double> entityScores)
        {
            return entityScores.OrderByDescending(x => x.Value).Select(x => x.Key).Take(5);
        }
    }
}
