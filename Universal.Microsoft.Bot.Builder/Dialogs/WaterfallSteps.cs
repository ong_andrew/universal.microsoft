﻿using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.Dialogs.Choices;
using System;
using System.Collections.Generic;
using System.Threading;
using Universal.Common.Reflection;

namespace Universal.Microsoft.Bot.Builder.Dialogs
{
    /// <summary>
    /// A catalog of reusable <see cref="WaterfallStep"/> components.
    /// </summary>
    public static class WaterfallSteps
    {
        /// <summary>
        /// Catalog of <see cref="WaterfallStep"/> simplifying interaction with <see cref="WaterfallStepContext.Values"/>.
        /// </summary>
        public static class Values
        {
            /// <summary>
            /// Executes the given <see cref="WaterfallStep"/> if <see cref="WaterfallStepContext.Values"/> satisfies the given predicate.
            /// </summary>
            /// <param name="predicate"></param>
            /// <param name="waterfallStep"></param>
            /// <returns></returns>
            public static WaterfallStep ExecuteIf(Func<IDictionary<string, object>, bool> predicate, WaterfallStep waterfallStep)
            {
                return async (WaterfallStepContext stepContext, CancellationToken cancellationToken) =>
                {
                    if (predicate(stepContext.Values))
                    {
                        return await waterfallStep(stepContext, cancellationToken);
                    }
                    else
                    {
                        return await stepContext.NextAsync(null, cancellationToken: cancellationToken);
                    }
                };
            }

            /// <summary>
            /// Executes the given <see cref="WaterfallStep"/> if <see cref="WaterfallStepContext.Values"/> does not contain the specified key or if the value stored at the key is null.
            /// If it does not execute, returns the value of the object stored in the dictionary as a result to the next step.
            /// </summary>
            /// <param name="waterfallStep"></param>
            /// <param name="key"></param>
            /// <returns></returns>
            public static WaterfallStep ExecuteIfNull(string key, WaterfallStep waterfallStep)
            {
                return async (WaterfallStepContext stepContext, CancellationToken cancellationToken) =>
                {
                    if (!stepContext.Values.ContainsKey(key) || stepContext.Values[key] == null)
                    {
                        return await waterfallStep(stepContext, cancellationToken);
                    }
                    else
                    {
                        return await stepContext.NextAsync(stepContext.Values[key], cancellationToken: cancellationToken);
                    }
                };
            }

            /// <summary>
            /// Creates a <see cref="WaterfallStep"/> that extracts the value from the <see cref="FoundChoice"/> stored in <see cref="WaterfallStepContext.Values"/> at the given key.
            /// </summary>
            /// <param name="key"></param>
            /// <returns></returns>
            public static WaterfallStep NormalizeFoundChoice(string key)
            {
                return async (WaterfallStepContext stepContext, CancellationToken cancellationToken) =>
                {
                    if (!stepContext.Values.ContainsKey(key))
                    {
                        throw new KeyNotFoundException($"The key {key} does not exist in the WaterfallStepContext.Values dictionary.");
                    }

                    if (stepContext.Values[key] is FoundChoice foundChoice)
                    {
                        stepContext.Values[key] = foundChoice.Value;
                    }
                    else
                    {
                        throw new KeyNotFoundException($"The object with key {key} is not an instance of {typeof(FoundChoice)}.");
                    }

                    return await stepContext.NextAsync(cancellationToken: cancellationToken);
                };
            }

            /// <summary>
            /// Executes the given prompt if the object stored with the given key is null.
            /// </summary>
            /// <param name="key"></param>
            /// <param name="promptDialogId"></param>
            /// <param name="promptOptions"></param>
            /// <returns></returns>
            public static WaterfallStep PromptFor(string key, string promptDialogId, PromptOptions promptOptions)
            {
                return ExecuteIfNull(key,
                    async (WaterfallStepContext stepContext, CancellationToken cancellationToken) =>
                    {
                        return await stepContext.PromptAsync(promptDialogId, promptOptions, cancellationToken);
                    });
            }

            /// <summary>
            /// Executes the given prompt if the object stored with the given key is null.
            /// </summary>
            /// <param name="key"></param>
            /// <param name="promptDialogId"></param>
            /// <param name="createPromptOptions"></param>
            /// <returns></returns>
            public static WaterfallStep PromptFor(string key, string promptDialogId, Func<IDictionary<string, object>, PromptOptions> createPromptOptions)
            {
                return ExecuteIfNull(key,
                    async (WaterfallStepContext stepContext, CancellationToken cancellationToken) =>
                    {
                        return await stepContext.PromptAsync(promptDialogId, createPromptOptions(stepContext.Values), cancellationToken);
                    });
            }

            /// <summary>
            /// Creates a <see cref="WaterfallStep"/> that stores the result of the previous step in the <see cref="WaterfallStepContext.Values"/> dictionary with the given key.
            /// </summary>
            /// <param name="key"></param>
            /// <returns></returns>
            public static WaterfallStep Store(string key)
            {
                return async (WaterfallStepContext stepContext, CancellationToken cancellationToken) =>
                {
                    object value = stepContext.Result;
                    if (!stepContext.Values.ContainsKey(key))
                    {
                        stepContext.Values.Add(key, value);
                    }
                    else
                    {
                        stepContext.Values[key] = value;
                    }

                    return await stepContext.NextAsync(cancellationToken: cancellationToken);
                };
            }

            /// <summary>
            /// Creates a <see cref="WaterfallStep"/> that allows the unpacking of <see cref="WaterfallStepContext.Options"/> into <see cref="WaterfallStepContext.Values"/>.
            /// </summary>
            /// <param name="unpackDelegate"></param>
            /// <returns></returns>
            public static WaterfallStep UnpackOptions(WaterfallOptionsUnpackDelegate unpackDelegate)
            {
                return async (WaterfallStepContext stepContext, CancellationToken cancellationToken) =>
                {
                    unpackDelegate?.Invoke(stepContext.Options, stepContext.Values);
                    return await stepContext.NextAsync(cancellationToken: cancellationToken);
                };
            }

            /// <summary>
            /// Creates a <see cref="WaterfallStep"/> that allows the unpacking of <see cref="WaterfallStepContext.Options"/> into <see cref="WaterfallStepContext.Values"/>.
            /// </summary>
            /// <typeparam name="T"></typeparam>
            /// <param name="unpackDelegate"></param>
            /// <returns></returns>
            public static WaterfallStep UnpackOptions<T>(WaterfallOptionsUnpackDelegate<T> unpackDelegate)
            {
                return async (WaterfallStepContext stepContext, CancellationToken cancellationToken) =>
                {
                    unpackDelegate?.Invoke((T)stepContext.Options, stepContext.Values);
                    return await stepContext.NextAsync(cancellationToken: cancellationToken);
                };
            }

            /// <summary>
            /// Creates a <see cref="WaterfallStep"/> that automatically unpacks the member values of <see cref="WaterfallStepContext.Options"/> into <see cref="WaterfallStepContext.Values"/>.
            /// </summary>
            /// <returns></returns>
            public static WaterfallStep UnpackOptionsByMemberValues()
            {
                return async (WaterfallStepContext stepContext, CancellationToken cancellationToken) =>
                {
                    if (stepContext.Options != null)
                    {
                        foreach (KeyValuePair<string, object> memberValue in stepContext.Options.GetMemberValues())
                        {
                            if (stepContext.Values.ContainsKey(memberValue.Key))
                            {
                                stepContext.Values[memberValue.Key] = memberValue.Value;
                            }
                            else
                            {
                                stepContext.Values.Add(memberValue);
                            }
                        }
                    }

                    return await stepContext.NextAsync(cancellationToken: cancellationToken);
                };
            }
        }
    }
}
