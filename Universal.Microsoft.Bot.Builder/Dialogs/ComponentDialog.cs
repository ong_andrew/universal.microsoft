﻿using Microsoft.Bot.Builder.Dialogs;
using System.Threading;
using System.Threading.Tasks;

namespace Universal.Microsoft.Bot.Builder.Dialogs
{
    /// <summary>
    /// A <see cref="ComponentDialog"/> which ensures that options are a type of <typeparamref name="TOptions"/> 
    /// when <see cref="BeginDialogAsync(DialogContext, object, CancellationToken)" /> is called, 
    /// creating a new instance if necessary.
    /// </summary>
    /// <typeparam name="TOptions">The type the options class should take.</typeparam>
    public class ComponentDialog<TOptions> : ComponentDialog
        where TOptions : class, new()
    {
        /*
        /// <summary>
        /// Initializes a new instance of the <see cref="ComponentDialog{TOptions}"/> class.
        /// </summary>
        public ComponentDialog() : base() { }
        */
        /// <summary>
        /// Initializes a new instance of the <see cref="ComponentDialog{TOptions}"/> class with the given dialog ID.
        /// </summary>
        /// <param name="dialogId"></param>
        public ComponentDialog(string dialogId) : base(dialogId) { }

        public override Task<DialogTurnResult> BeginDialogAsync(DialogContext outerDc, object options = null, CancellationToken cancellationToken = default)
        {
            if (options as TOptions is null)
            {
                return base.BeginDialogAsync(outerDc, new TOptions(), cancellationToken);
            }

            return base.BeginDialogAsync(outerDc, options, cancellationToken);
        }
    }
}